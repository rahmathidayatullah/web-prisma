"use client";
import { useState } from "react";

export function useSessionStorage<T>(
  key: string,
  initialValue: T
): [T, (value: T) => void] {
  const isSupported = typeof window !== "undefined" && window.sessionStorage;

  const [storedValue, setStoredValue] = useState<T>(() => {
    try {
      if (isSupported) {
        const item = sessionStorage.getItem(key);
        return item ? JSON.parse(item) : initialValue;
      }
      return initialValue;
    } catch (error) {
      console.error(error);
      return initialValue;
    }
  });

  const setValue = (value: T) => {
    try {
      if (isSupported) {
        setStoredValue(value);
        sessionStorage.setItem(key, JSON.stringify(value));
      }
    } catch (error) {
      console.error(error);
    }
  };

  return [storedValue, setValue];
}
