import { notification } from "antd";
import { NotificationPlacement } from "antd/es/notification/interface";
import { useCallback } from "react";

type MessageType = "error" | "info" | "success" | "warning";

const useMessage = () => {
  const [messageApi, contextHolder] = notification.useNotification();

  const openMessage = useCallback(
    (type: MessageType, message: string, placement?: NotificationPlacement) => {
      messageApi.open({
        type,
        message,
        placement,
      });
    },
    [messageApi]
  );

  const openNotification = useCallback(
    (placement: NotificationPlacement, title: string, description: string) => {
      messageApi.info({
        message: title,
        description,
        placement,
      });
    },
    [messageApi]
  );

  return { openMessage, openNotification, contextHolder };
};

export default useMessage;

// openMessage("warning", "Password yang di inputkan tidak sesuai", "top");
// openNotification("topLeft", "Berhasil", `Data berhasil di ${action}`);
