// // Import the functions you need from the SDKs you need
// import { initializeApp } from "firebase/app";
// import { getMessaging, getToken } from "firebase/messaging";
// // import { getAnalytics } from "firebase/analytics";
// // TODO: Add SDKs for Firebase products that you want to use
// // https://firebase.google.com/docs/web/setup#available-libraries

// // Your web app's Firebase configuration
// // For Firebase JS SDK v7.20.0 and later, measurementId is optional
// export const firebaseConfig = {
//   apikey: process.env.NEXT_PUBLIC_DB_FIREBASE_API_KEY,
//   authDomain: process.env.NEXT_PUBLIC_DB_FIREBASE_AUTH_DOMAIN,
//   projectId: process.env.NEXT_PUBLIC_DB_FIREBASE_PROJECT_ID,
//   storageBucket: process.env.NEXT_PUBLIC_DB_FIREBASE_STORAGE_BUCKET,
//   messagingSenderId: process.env.NEXT_PUBLIC_DB_FIREBASE_MESSANGING_SENDER_ID,
//   appId: process.env.NEXT_PUBLIC_DB_FIREBASE_APP_ID,
//   measurementId: process.env.NEXT_PUBLIC_DB_FIREBASE_MEASUREMENT_ID,
//   //   apikey: "AIzaSyDLwA8YNAFZlLoqcHf0ycNxkbtDbFzxgYI",
//   //   authDomain: "pushnotif-f274f.firebaseapp.com",
//   //   projectId: "pushnotif-f274f",
//   //   storageBucket: "pushnotif-f274f.appspot.com",
//   //   messagingSenderId: "76581023116",
//   //   appId: "1:76581023116:web:2d9e8e8bb1e94fe072faa9",
//   //   measurementId: "G-4DRMTF5W0Q",
// };

// // Initialize Firebase
// const app = initializeApp(firebaseConfig);
// // const analytics = getAnalytics(app);

// // Initialize Firebase Cloud Messaging and get a reference to the service
// export const messaging = getMessaging(app);

// export const generateToken = async () => {
//   const permission = await Notification.requestPermission();
//   if (permission === "granted") {
//     const token = await getToken(messaging, {
//       vapidKey: process.env.NEXT_PUBLIC_DB_FIREBASE_VAPID_KEY,
//     });
//   }
// };
