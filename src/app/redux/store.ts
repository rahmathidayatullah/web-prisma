import { configureStore } from "@reduxjs/toolkit";
import attendaceReducer from "./features/attendace/reducer";
import authReducer from "./features/auth/reducer";
import overtimeReducer from "./features/overtime/reducer";
import submissionReducer from "./features/submission/reducer";
import profileReducer from "./features/profile/reducer";
import userReducer from "./features/users/reducer";
import rolesReducer from "./features/roles/reducer";
import blocksReducer from "./features/blocks/reducer";
import phasesReducer from "./features/phases/reducer";
import masterDataAttendacesReducer from "./features/master-data-attendaces/reducer";
import shiftAttendacesReducer from "./features/master-data-attendaces/shifts/reducer";
import submissionCategoriesAttendacesReducer from "./features/master-data-attendaces/submission-categories/reducer";
import annoucementReducer from "./features/annoucement/reducer";
import divisiReducer from "./features/division/reducer";
import sitemapReducer from "./features/sitemaps/reducer";

export const store = configureStore({
  reducer: {
    attendace: attendaceReducer,
    auth: authReducer,
    overtime: overtimeReducer,
    submission: submissionReducer,
    profile: profileReducer,
    user: userReducer,
    roles: rolesReducer,
    phases: phasesReducer,
    blocks: blocksReducer,
    masterDataAttendaces: masterDataAttendacesReducer,
    submissionCategories: submissionCategoriesAttendacesReducer,
    shiftAttendaces: shiftAttendacesReducer,
    annoucement: annoucementReducer,
    divisi: divisiReducer,
    sitemap: sitemapReducer,
  },
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
