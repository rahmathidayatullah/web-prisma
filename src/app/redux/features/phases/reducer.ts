import {
  START_FETCH_PHASES,
  SUCCESS_FETCH_PHASES,
  ERROR_FETCH_PHASES,
  START_FETCH_PHASES_DETAIL,
  SUCCESS_FETCH_PHASES_DETAIL,
  ERROR_FETCH_PHASES_DETAIL,
  START_CREATE_PHASES,
  SUCCESS_CREATE_PHASES,
  ERROR_CREATE_PHASES,
  START_UPDATE_PHASES,
  SUCCESS_UPDATE_PHASES,
  ERROR_UPDATE_PHASES,
  START_DELETE_PHASES,
  SUCCESS_DELETE_PHASES,
  ERROR_DELETE_PHASES,
  CHANGE_PAGE,
  CHANGE_LIMIT,
  CHANGE_KEYWORD,
  RESET_CREATE_PHASES,
  RESET_UPDATE_PHASES,
  RESET_DELETE_PHASES,
  ERROR_FETCH_PHASES_ALL,
  SUCCESS_FETCH_PHASES_ALL,
  START_FETCH_PHASES_ALL,
  RESET_STATUS_DELETE_PHASES,
  RESET_STATUS_UPDATE_PHASES,
  RESET_STATUS_CREATE_PHASES,
} from "./constans";

const statusList = {
  idle: "idle",
  process: "process",
  success: "success",
  error: "error",
};

const initialState = {
  page: 1,
  take: 10,
  order: "DESC",
  keyword: "",
  dataPhases: [],
  dataPhasesDetail: null,
  dataPhasesCreate: null,
  amountOfData: 0,

  errorDelete: null,
  errorUpdate: null,
  errorCraete: null,

  dataDelete: null,
  dataUpdate: null,

  // role all
  statusListRoleAll: statusList.idle,
  dataListRoleAll: [],

  statusListPhases: statusList.idle,
  statusDetailPhases: statusList.idle,
  statusDeletePhases: statusList.idle,
  statusUpdatePhases: statusList.idle,
  statusCreatePhases: statusList.idle,
};

export default function phasesReducer(state = initialState, action: any) {
  switch (action.type) {
    case START_FETCH_PHASES_ALL:
      return {
        ...state,
        statusListPhaseAll: statusList.process,
      };
    case SUCCESS_FETCH_PHASES_ALL:
      return {
        ...state,
        statusListPhaseAll: statusList.success,
        dataListPhaseAll: action.data,
      };
    case ERROR_FETCH_PHASES_ALL:
      return {
        ...state,
        statusListPhaseAll: statusList.error,
      };

    case START_FETCH_PHASES:
      return {
        ...state,
        statusListPhases: statusList.process,
      };
    case SUCCESS_FETCH_PHASES:
      return {
        ...state,
        statusListPhases: statusList.success,
        dataPhases: action.data,
        amountOfData: action.amountOfData,
      };
    case ERROR_FETCH_PHASES:
      return {
        ...state,
        statusListPhases: statusList.error,
      };

    case START_FETCH_PHASES_DETAIL:
      return {
        ...state,
        statusDetailPhases: statusList.process,
      };
    case SUCCESS_FETCH_PHASES_DETAIL:
      return {
        ...state,
        statusDetailPhases: statusList.success,
        dataPhasesDetail: action.data,
      };
    case ERROR_FETCH_PHASES_DETAIL:
      return {
        ...state,
        statusDetailPhases: statusList.error,
      };

    case START_DELETE_PHASES:
      return {
        ...state,
        statusDeletePhases: statusList.process,
      };
    case SUCCESS_DELETE_PHASES:
      return {
        ...state,
        statusDeletePhases: statusList.success,
        dataDelete: action.data,
      };
    case ERROR_DELETE_PHASES:
      return {
        ...state,
        statusDeletePhases: statusList.error,
        errorDelete: action.data,
      };
    case RESET_DELETE_PHASES:
      return {
        ...state,
        statusDeletePhases: statusList.idle,
        dataDelete: null,
        errorDelete: null,
      };
    case RESET_STATUS_DELETE_PHASES:
      return {
        ...state,
        statusDeletePhases: statusList.idle,
      };

    case START_UPDATE_PHASES:
      return {
        ...state,
        statusUpdatePhases: statusList.process,
      };
    case SUCCESS_UPDATE_PHASES:
      return {
        ...state,
        statusUpdatePhases: statusList.success,
        dataUpdate: action.data,
      };
    case ERROR_UPDATE_PHASES:
      return {
        ...state,
        statusUpdatePhases: statusList.error,
        errorUpdate: action.data,
      };
    case RESET_UPDATE_PHASES:
      return {
        ...state,
        statusUpdatePhases: statusList.idle,
        dataUpdate: null,
        errorUpdate: null,
      };
    case RESET_STATUS_UPDATE_PHASES:
      return {
        ...state,
        statusUpdatePhases: statusList.idle,
      };

    case START_CREATE_PHASES:
      return {
        ...state,
        statusCreatePhases: statusList.process,
      };
    case SUCCESS_CREATE_PHASES:
      return {
        ...state,
        statusCreatePhases: statusList.success,
        dataPhasesCreate: action.data,
      };
    case ERROR_CREATE_PHASES:
      return {
        ...state,
        statusCreatePhases: statusList.error,
        errorCreate: action.data,
      };
    case RESET_CREATE_PHASES:
      return {
        ...state,
        statusCreatePhases: statusList.idle,
        dataPhasesCreate: null,
        errorCreate: null,
      };
    case RESET_STATUS_CREATE_PHASES:
      return {
        ...state,
        statusCreatePhases: statusList.idle,
      };

    case CHANGE_PAGE:
      return {
        ...state,
        page: action.value,
      };
    case CHANGE_LIMIT:
      return {
        ...state,
        take: action.value,
      };
    case CHANGE_KEYWORD:
      return {
        ...state,
        keyword: action.value,
        page: 1,
        take: 10,
      };
    default:
      return state;
  }
}
