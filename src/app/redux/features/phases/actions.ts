import debounce from "debounce-promise";

import {
  deletePhasesById,
  getPhases,
  getPhasesAll,
  getPhasesById,
  patchPhasesById,
  postPhases,
} from "@/app/api/phases";

import {
  START_FETCH_PHASES,
  SUCCESS_FETCH_PHASES,
  ERROR_FETCH_PHASES,
  START_FETCH_PHASES_DETAIL,
  SUCCESS_FETCH_PHASES_DETAIL,
  ERROR_FETCH_PHASES_DETAIL,
  START_CREATE_PHASES,
  SUCCESS_CREATE_PHASES,
  ERROR_CREATE_PHASES,
  START_UPDATE_PHASES,
  SUCCESS_UPDATE_PHASES,
  ERROR_UPDATE_PHASES,
  START_DELETE_PHASES,
  SUCCESS_DELETE_PHASES,
  ERROR_DELETE_PHASES,
  CHANGE_PAGE,
  CHANGE_LIMIT,
  CHANGE_KEYWORD,
  START_FETCH_PHASES_ALL,
  ERROR_FETCH_PHASES_ALL,
  SUCCESS_FETCH_PHASES_ALL,
} from "./constans";

const debounceGetPhases = debounce(getPhases, 1000);

export const fetchPhases = (id: string) => {
  return async (dispatch: any, getState: any) => {
    dispatch({
      type: START_FETCH_PHASES,
    });

    const page = getState().phases.page;
    const take = getState().phases.take;
    const order = getState().phases.order;
    const keyword = getState().phases.keyword;

    const params = {
      page,
      take,
      order,
      search: keyword,
    };

    try {
      const {
        data: { data, meta },
      } = await debounceGetPhases(params, id);

      dispatch({
        type: SUCCESS_FETCH_PHASES,
        data: data,
        amountOfData: meta.itemCount,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_PHASES,
      });
    }
  };
};
export const fetchPhasesAll = (id: string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_FETCH_PHASES_ALL,
    });

    try {
      const {
        data: { data },
      } = await getPhasesAll(id);

      let newData: any = [];

      if (data.length) {
        newData = data.map((item: any) => {
          return {
            ...item,
            label: item.name,
            value: item.id,
          };
        });
      }

      dispatch({
        type: SUCCESS_FETCH_PHASES_ALL,
        data: newData,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_PHASES_ALL,
      });
    }
  };
};

export const fetchPhasesDetail = (id: string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_FETCH_PHASES_DETAIL,
    });

    try {
      const {
        data: { data },
      } = await getPhasesById(id);

      dispatch({
        type: SUCCESS_FETCH_PHASES_DETAIL,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_PHASES_DETAIL,
      });
    }
  };
};

export const createPhases = (body: any, id: string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_CREATE_PHASES,
    });

    try {
      const {
        data: { data },
      } = await postPhases(body, id);

      dispatch({
        type: SUCCESS_CREATE_PHASES,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_CREATE_PHASES,
        data: error,
      });
    }
  };
};

export const removePhases = (id: string | number) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_DELETE_PHASES,
    });

    try {
      const {
        data: { data },
      } = await deletePhasesById(id);

      dispatch({
        type: SUCCESS_DELETE_PHASES,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_DELETE_PHASES,
        data: error,
      });
    }
  };
};

export const updatePhases = (id: string, body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_UPDATE_PHASES,
    });

    try {
      const {
        data: { data },
      } = await patchPhasesById(id, body);

      dispatch({
        type: SUCCESS_UPDATE_PHASES,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_UPDATE_PHASES,
        data: error,
      });
    }
  };
};

export const changeKeyword = (value: string) => {
  return {
    type: CHANGE_KEYWORD,
    value,
  };
};

export const changePage = (value: number) => {
  return {
    type: CHANGE_PAGE,
    value,
  };
};

export const changeLimit = (value: number) => {
  return {
    type: CHANGE_LIMIT,
    value,
  };
};
