export const START_FETCH_SUBMISSION = "START_FETCH_SUBMISSION";
export const SUCCESS_FETCH_SUBMISSION = "SUCCESS_FETCH_SUBMISSION";
export const ERROR_FETCH_SUBMISSION = "ERROR_FETCH_SUBMISSION";

export const START_EXPORT_SUBMISSION = "START_EXPORT_SUBMISSION";
export const SUCCESS_EXPORT_SUBMISSION = "SUCCESS_EXPORT_SUBMISSION";
export const ERROR_EXPORT_SUBMISSION = "ERROR_EXPORT_SUBMISSION";
export const RESET_EXPORT_SUBMISSION = "RESET_EXPORT_SUBMISSION";
export const RESET_STATUS_EXPORT_SUBMISSION = "RESET_STATUS_EXPORT_SUBMISSION";

export const START_POST_SUBMISSION = "START_POST_SUBMISSION";
export const SUCCESS_POST_SUBMISSION = "SUCCESS_POST_SUBMISSION";
export const ERROR_POST_SUBMISSION = "ERROR_POST_SUBMISSION";
export const RESET_POST_SUBMISSION = "RESET_POST_SUBMISSION";
export const RESET_STATUS_POST_SUBMISSION = "RESET_STATUS_POST_SUBMISSION";

export const START_UPDATE_SUBMISSION = "START_UPDATE_SUBMISSION";
export const SUCCESS_UPDATE_SUBMISSION = "SUCCESS_UPDATE_SUBMISSION";
export const ERROR_UPDATE_SUBMISSION = "ERROR_UPDATE_SUBMISSION";
export const RESET_UPDATE_SUBMISSION = "RESET_UPDATE_SUBMISSION";
export const RESET_STATUS_UPDATE_SUBMISSION = "RESET_STATUS_UPDATE_SUBMISSION";

export const START_FETCH_SUBMISSION_DETAIL = "START_FETCH_SUBMISSION_DETAIL";
export const SUCCESS_FETCH_SUBMISSION_DETAIL =
  "SUCCESS_FETCH_SUBMISSION_DETAIL";
export const ERROR_FETCH_SUBMISSION_DETAIL = "ERROR_FETCH_SUBMISSION_DETAIL";

export const START_APPROVE_SUBMISSION = "START_APPROVE_SUBMISSION";
export const SUCCESS_APPROVE_SUBMISSION = "SUCCESS_APPROVE_SUBMISSION";
export const ERROR_APPROVE_SUBMISSION = "ERROR_APPROVE_SUBMISSION";

export const START_REJECT_SUBMISSION = "START_REJECT_SUBMISSION";
export const SUCCESS_REJECT_SUBMISSION = "SUCCESS_REJECT_SUBMISSION";
export const ERROR_REJECT_SUBMISSION = "ERROR_REJECT_SUBMISSION";
export const RESET_STATUS_REJECT_APPROVE = "RESET_STATUS_REJECT_APPROVE";

export const CHANGE_PAGE = "CHANGE_PAGE";
export const CHANGE_LIMIT = "CHANGE_LIMIT";

export const CHANGE_KEYWORD = "CHANGE_KEYWORD";
export const CHANGE_DATE_RANGE = "CHANGE_DATE_RANGE";
