import {
  START_FETCH_SUBMISSION,
  SUCCESS_FETCH_SUBMISSION,
  ERROR_FETCH_SUBMISSION,
  START_EXPORT_SUBMISSION,
  SUCCESS_EXPORT_SUBMISSION,
  ERROR_EXPORT_SUBMISSION,
  START_UPDATE_SUBMISSION,
  SUCCESS_UPDATE_SUBMISSION,
  ERROR_UPDATE_SUBMISSION,
  RESET_UPDATE_SUBMISSION,
  START_POST_SUBMISSION,
  SUCCESS_POST_SUBMISSION,
  ERROR_POST_SUBMISSION,
  RESET_POST_SUBMISSION,
  START_APPROVE_SUBMISSION,
  SUCCESS_APPROVE_SUBMISSION,
  ERROR_APPROVE_SUBMISSION,
  START_REJECT_SUBMISSION,
  SUCCESS_REJECT_SUBMISSION,
  ERROR_REJECT_SUBMISSION,
  RESET_STATUS_REJECT_APPROVE,
  CHANGE_PAGE,
  CHANGE_LIMIT,
  CHANGE_KEYWORD,
  START_FETCH_SUBMISSION_DETAIL,
  SUCCESS_FETCH_SUBMISSION_DETAIL,
  ERROR_FETCH_SUBMISSION_DETAIL,
  CHANGE_DATE_RANGE,
} from "./constans";

const statusList = {
  idle: "idle",
  process: "process",
  success: "success",
  error: "error",
};

const initialState = {
  statusListSubmission: statusList.idle,
  page: 1,
  take: 10,
  order: "DESC",
  keyword: "",
  dataSubmission: [],
  statusApprove: statusList.idle,
  errorApprove: null,
  statusReject: statusList.idle,
  errorReject: null,
  amountOfData: 0,
  startDate: "",
  endDate: "",

  statusDetail: statusList.idle,
  dataDetail: null,
  dataErrorDetail: null,

  //
  statusUpdate: statusList.idle,
  dataUpdate: null,
  dataErrorUpdate: null,
  //
  statusCreate: statusList.idle,
  dataCreate: null,
  dataErrorCreate: null,
  //
  dataExport: null,
  errorExport: null,
  statusExport: statusList.idle,
};

export default function submissionReducer(state = initialState, action: any) {
  switch (action.type) {
    case START_EXPORT_SUBMISSION:
      return {
        ...state,
        statusExport: statusList.process,
      };
    case SUCCESS_EXPORT_SUBMISSION:
      return {
        ...state,
        statusExport: statusList.success,
        dataExport: action.data,
      };
    case ERROR_EXPORT_SUBMISSION:
      return {
        ...state,
        statusExport: statusList.error,
        errorExport: action.data,
      };

    case START_UPDATE_SUBMISSION:
      return {
        ...state,
        statusUpdate: statusList.process,
      };
    case SUCCESS_UPDATE_SUBMISSION:
      return {
        ...state,
        statusUpdate: statusList.success,
        dataUpdate: action.data,
      };
    case ERROR_UPDATE_SUBMISSION:
      return {
        ...state,
        statusUpdate: statusList.error,
        dataErrorUpdate: action.data,
      };
    case RESET_UPDATE_SUBMISSION:
      return {
        ...state,
        statusUpdate: statusList.idle,
        dataErrorUpdate: null,
        dataUpdate: null,
      };
    case START_POST_SUBMISSION:
      return {
        ...state,
        statusCreate: statusList.process,
      };
    case SUCCESS_POST_SUBMISSION:
      return {
        ...state,
        statusCreate: statusList.success,
        dataCreate: action.data,
      };
    case ERROR_POST_SUBMISSION:
      return {
        ...state,
        statusCreate: statusList.error,
        dataErrorCreate: action.data,
      };
    case RESET_POST_SUBMISSION:
      return {
        ...state,
        statusCreate: statusList.idle,
        dataErrorCreate: null,
        dataCreate: null,
      };
    case START_FETCH_SUBMISSION_DETAIL:
      return {
        ...state,
        statusDetail: statusList.process,
      };
    case SUCCESS_FETCH_SUBMISSION_DETAIL:
      return {
        ...state,
        statusDetail: statusList.success,
        dataDetail: action.data,
      };
    case ERROR_FETCH_SUBMISSION_DETAIL:
      return {
        ...state,
        statusDetail: statusList.error,
        dataErrorDetail: action.data,
      };
    case START_FETCH_SUBMISSION:
      return {
        ...state,
        statusListSubmission: statusList.process,
      };
    case SUCCESS_FETCH_SUBMISSION:
      return {
        ...state,
        statusListSubmission: statusList.success,
        dataSubmission: action.data,
        amountOfData: action.amountOfData,
      };
    case ERROR_FETCH_SUBMISSION:
      return {
        ...state,
        statusListSubmission: statusList.error,
      };
    case START_APPROVE_SUBMISSION:
      return {
        ...state,
        statusApprove: statusList.process,
      };
    case SUCCESS_APPROVE_SUBMISSION:
      return {
        ...state,
        statusApprove: statusList.success,
      };
    case ERROR_APPROVE_SUBMISSION:
      return {
        ...state,
        statusApprove: statusList.error,
        errorApprove: action.data,
      };
    case START_REJECT_SUBMISSION:
      return {
        ...state,
        statusReject: statusList.process,
      };
    case SUCCESS_REJECT_SUBMISSION:
      return {
        ...state,
        statusReject: statusList.success,
      };
    case ERROR_REJECT_SUBMISSION:
      return {
        ...state,
        statusReject: statusList.error,
        errorReject: action.data,
      };
    case RESET_STATUS_REJECT_APPROVE:
      return {
        ...state,
        statusReject: statusList.idle,
        statusApprove: statusList.idle,
        errorApprove: null,
        errorReject: null,
      };
    case CHANGE_PAGE:
      return {
        ...state,
        page: action.value,
      };
    case CHANGE_LIMIT:
      return {
        ...state,
        take: action.value,
      };
    case CHANGE_KEYWORD:
      return {
        ...state,
        keyword: action.value,
        page: 1,
        take: 10,
        startDate: "",
        endDate: "",
      };
    case CHANGE_DATE_RANGE:
      return {
        ...state,
        startDate: action.value[0],
        endDate: action.value[1],
        page: 1,
        take: 10,
      };
    default:
      return state;
  }
}
