import debounce from "debounce-promise";

import {
  approveSubmissions,
  getSubmissions,
  getSubmissionsById,
  getSubmissionsExport,
  patchSubmissions,
  postSubmissions,
  rejectSubmissions,
} from "@/app/api/submission";

import {
  START_FETCH_SUBMISSION,
  SUCCESS_FETCH_SUBMISSION,
  ERROR_FETCH_SUBMISSION,
  START_UPDATE_SUBMISSION,
  SUCCESS_UPDATE_SUBMISSION,
  ERROR_UPDATE_SUBMISSION,
  START_POST_SUBMISSION,
  SUCCESS_POST_SUBMISSION,
  ERROR_POST_SUBMISSION,
  START_APPROVE_SUBMISSION,
  SUCCESS_APPROVE_SUBMISSION,
  ERROR_APPROVE_SUBMISSION,
  START_REJECT_SUBMISSION,
  SUCCESS_REJECT_SUBMISSION,
  ERROR_REJECT_SUBMISSION,
  START_FETCH_SUBMISSION_DETAIL,
  SUCCESS_FETCH_SUBMISSION_DETAIL,
  ERROR_FETCH_SUBMISSION_DETAIL,
  START_EXPORT_SUBMISSION,
  SUCCESS_EXPORT_SUBMISSION,
  ERROR_EXPORT_SUBMISSION,
} from "./constans";
import {
  currentDateWithFormat,
  dateEndNow,
  dateStartNow,
  futureDateOneYear,
} from "@/app/utils/constants";

const debounceGetSubmissions = debounce(getSubmissions, 1000);

export const fetchSubmissions = () => {
  return async (dispatch: any, getState: any) => {
    dispatch({
      type: START_FETCH_SUBMISSION,
    });

    const page = getState().submission.page;
    const take = getState().submission.take;
    const order = getState().submission.order;
    const keyword = getState().submission.keyword;
    const startDate = getState().submission.startDate || currentDateWithFormat;
    const endDate = getState().submission.endDate || futureDateOneYear;

    const params = {
      page,
      take,
      order,
      search: keyword,
      endDate,
      startDate,
    };

    try {
      const {
        data: { data, meta },
      } = await debounceGetSubmissions(params);

      let newData = {
        data: [],
        meta: {
          itemCount: 0,
        },
      };

      if (data.length) {
        newData.data = data.map((item: any) => {
          return {
            ...item,
            approveBy: {
              name: "rahmat",
              status: item.status,
            },
          };
        });
        newData.meta = meta;
      }

      dispatch({
        type: SUCCESS_FETCH_SUBMISSION,
        data: newData.data,
        amountOfData: newData.meta.itemCount,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_SUBMISSION,
      });
    }
  };
};

export const fetchSubmissionsExports = () => {
  return async (dispatch: any, getState: any) => {
    dispatch({
      type: START_EXPORT_SUBMISSION,
    });

    const page = getState().submission.page;
    // const take = getState().attendace.take;
    // const order = getState().attendace.order;
    const keyword = getState().submission.keyword;
    // const startDate = getState().submission.startDate || currentDateWithFormat;
    // const endDate = getState().submission.endDate || futureDateOneYear;
    const startDate = getState().submission.startDate || dateStartNow; //current month 01;
    const endDate = getState().submission.endDate || dateEndNow; //current month 30/31;

    const params = {
      page,
      // take,
      // order,
      search: keyword,
      endDate,
      startDate,
    };

    try {
      const { data } = await getSubmissionsExport(params);
      dispatch({
        type: SUCCESS_EXPORT_SUBMISSION,
        data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_EXPORT_SUBMISSION,
        data: error,
      });
    }
  };
};

export const fetchSubmissionDetail = (id: string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_FETCH_SUBMISSION_DETAIL,
    });

    try {
      const {
        data: { data },
      } = await getSubmissionsById(id);
      dispatch({
        type: SUCCESS_FETCH_SUBMISSION_DETAIL,
        data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_SUBMISSION_DETAIL,
      });
    }
  };
};

export const createSubmissions = (body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_POST_SUBMISSION,
    });
    try {
      const {
        data: { data },
      } = await postSubmissions(body);
      dispatch({
        type: SUCCESS_POST_SUBMISSION,
        data,
      });
      dispatch(fetchSubmissions());
    } catch (error) {
      dispatch({
        type: ERROR_POST_SUBMISSION,
        data: error,
      });
    }
  };
};

export const updateSubmissions = (id: any, body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_UPDATE_SUBMISSION,
    });
    try {
      const {
        data: { data },
      } = await patchSubmissions(id, body);
      dispatch({
        type: SUCCESS_UPDATE_SUBMISSION,
        data,
      });
      dispatch(fetchSubmissions());
    } catch (error) {
      dispatch({
        type: ERROR_UPDATE_SUBMISSION,
        data: error,
      });
    }
  };
};

export const submissionsApprove = (id: any, body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_APPROVE_SUBMISSION,
    });
    try {
      await approveSubmissions(id, body);
      dispatch({
        type: SUCCESS_APPROVE_SUBMISSION,
      });
      dispatch(fetchSubmissions());
    } catch (error) {
      dispatch({
        type: ERROR_APPROVE_SUBMISSION,
        data: error,
      });
    }
  };
};

export const submissionsReject = (id: any, body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_REJECT_SUBMISSION,
    });
    try {
      await rejectSubmissions(id, body);
      dispatch({
        type: SUCCESS_REJECT_SUBMISSION,
      });
      dispatch(fetchSubmissions());
    } catch (error) {
      dispatch({
        type: ERROR_REJECT_SUBMISSION,
        data: error,
      });
    }
  };
};
