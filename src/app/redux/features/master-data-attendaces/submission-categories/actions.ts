import debounce from "debounce-promise";

import {
  START_FETCH_SUBMISSION_CATEGORIES,
  SUCCESS_FETCH_SUBMISSION_CATEGORIES,
  ERROR_FETCH_SUBMISSION_CATEGORIES,
  START_UPDATE_SUBMISSION_CATEGORIES,
  SUCCESS_UPDATE_SUBMISSION_CATEGORIES,
  ERROR_UPDATE_SUBMISSION_CATEGORIES,
  START_DELETE_SUBMISSION_CATEGORIES,
  SUCCESS_DELETE_SUBMISSION_CATEGORIES,
  ERROR_DELETE_SUBMISSION_CATEGORIES,
  START_CREATE_SUBMISSION_CATEGORIES,
  SUCCESS_CREATE_SUBMISSION_CATEGORIES,
  ERROR_CREATE_SUBMISSION_CATEGORIES,
  ERROR_FETCH_SUBMISSION_CATEGORIES_DETAIL,
  SUCCESS_FETCH_SUBMISSION_CATEGORIES_DETAIL,
  START_FETCH_SUBMISSION_CATEGORIES_DETAIL,
} from "./constants";
import {
  deleteSubmissionCategoriesById,
  getSubmissionCategories,
  getSubmissionCategoriesById,
  patchSubmissionCategoriesById,
  postSubmissionCategories,
} from "@/app/api/master-data";
import {
  currentDateWithFormat,
  futureDateOneYear,
} from "@/app/utils/constants";

const debounceGetSubmissionCategories = debounce(getSubmissionCategories, 1000);

export const fetchSubmissionCategories = () => {
  return async (dispatch: any, getState: any) => {
    dispatch({
      type: START_FETCH_SUBMISSION_CATEGORIES,
    });

    const page = getState().submissionCategories.page;
    const take = getState().submissionCategories.take;
    const order = getState().submissionCategories.order;
    const keyword = getState().submissionCategories.keyword;
    const startDate =
      getState().submissionCategories.startDate || currentDateWithFormat;
    const endDate =
      getState().submissionCategories.endDate || futureDateOneYear;

    const params = {
      page,
      take,
      order,
      search: keyword,
      endDate,
      startDate,
    };

    try {
      const {
        data: { data, meta },
      } = await debounceGetSubmissionCategories(params);

      let newData = {
        data: [],
        meta: {
          itemCount: 0,
        },
      };

      if (data.length) {
        newData.data = data.map((item: any) => {
          return {
            ...item,
            label: item.name,
            value: item.id,
          };
        });
        newData.meta = meta;
      }

      dispatch({
        type: SUCCESS_FETCH_SUBMISSION_CATEGORIES,
        data: newData.data,
        amountOfData: newData.meta.itemCount,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_SUBMISSION_CATEGORIES,
      });
    }
  };
};

export const fetchSubmissionCategoriesById = (id: string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_FETCH_SUBMISSION_CATEGORIES_DETAIL,
    });

    try {
      const {
        data: { data },
      } = await getSubmissionCategoriesById(id);

      dispatch({
        type: SUCCESS_FETCH_SUBMISSION_CATEGORIES_DETAIL,
        data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_SUBMISSION_CATEGORIES_DETAIL,
      });
    }
  };
};

export const createSubmissionCategories = (body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_CREATE_SUBMISSION_CATEGORIES,
    });

    try {
      const {
        data: { data },
      } = await postSubmissionCategories(body);

      dispatch({
        type: SUCCESS_CREATE_SUBMISSION_CATEGORIES,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_CREATE_SUBMISSION_CATEGORIES,
        data: error,
      });
    }
  };
};

export const removeSubmissionCategories = (id: number | string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_DELETE_SUBMISSION_CATEGORIES,
    });

    try {
      const {
        data: { data },
      } = await deleteSubmissionCategoriesById(id);

      dispatch({
        type: SUCCESS_DELETE_SUBMISSION_CATEGORIES,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_DELETE_SUBMISSION_CATEGORIES,
        data: error,
      });
    }
  };
};

export const updateSubmissionCategories = (id: string, body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_UPDATE_SUBMISSION_CATEGORIES,
    });

    try {
      const {
        data: { data },
      } = await patchSubmissionCategoriesById(id, body);

      dispatch({
        type: SUCCESS_UPDATE_SUBMISSION_CATEGORIES,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_UPDATE_SUBMISSION_CATEGORIES,
        data: error,
      });
    }
  };
};
