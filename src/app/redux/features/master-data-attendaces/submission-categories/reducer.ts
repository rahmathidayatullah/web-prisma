import {
  START_FETCH_SUBMISSION_CATEGORIES,
  SUCCESS_FETCH_SUBMISSION_CATEGORIES,
  ERROR_FETCH_SUBMISSION_CATEGORIES,
  CHANGE_PAGE_SUBMISSION_CATEGORIES,
  CHANGE_LIMIT_SUBMISSION_CATEGORIES,
  CHANGE_KEYWORD_SUBMISSION_CATEGORIES,
  CHANGE_DATE_RANGE_SUBMISSION_CATEGORIES,
  START_CREATE_SUBMISSION_CATEGORIES,
  SUCCESS_CREATE_SUBMISSION_CATEGORIES,
  ERROR_CREATE_SUBMISSION_CATEGORIES,
  RESET_CREATE_SUBMISSION_CATEGORIES,
  START_UPDATE_SUBMISSION_CATEGORIES,
  SUCCESS_UPDATE_SUBMISSION_CATEGORIES,
  RESET_UPDATE_SUBMISSION_CATEGORIES,
  ERROR_UPDATE_SUBMISSION_CATEGORIES,
  START_DELETE_SUBMISSION_CATEGORIES,
  SUCCESS_DELETE_SUBMISSION_CATEGORIES,
  ERROR_DELETE_SUBMISSION_CATEGORIES,
  RESET_DELETE_SUBMISSION_CATEGORIES,
  RESET_STATUS_CREATE_SUBMISSION_CATEGORIES,
  RESET_STATUS_DELETE_SUBMISSION_CATEGORIES,
  RESET_STATUS_UPDATE_SUBMISSION_CATEGORIES,
  START_FETCH_SUBMISSION_CATEGORIES_DETAIL,
  SUCCESS_FETCH_SUBMISSION_CATEGORIES_DETAIL,
  ERROR_FETCH_SUBMISSION_CATEGORIES_DETAIL,
} from "./constants";

const statusList = {
  idle: "idle",
  process: "process",
  success: "success",
  error: "error",
};

const initialState = {
  page: 1,
  take: 100,
  order: "DESC",
  keyword: "",
  startDate: "",
  endDate: "",
  data: [],
  amountOfData: 0,
  statusList: statusList.idle,

  dataDetail: null,
  dataDelete: null,
  dataCreate: null,
  dataUpdate: null,

  errorDelete: null,
  errorCreate: null,
  errorUpdate: null,

  statusDetail: statusList.idle,
  statusDelete: statusList.idle,
  statusUpdate: statusList.idle,
  statusCreate: statusList.idle,
};

export default function submissionCategoriesAttendacesReducer(
  state = initialState,
  action: any
) {
  switch (action.type) {
    case START_FETCH_SUBMISSION_CATEGORIES_DETAIL:
      return {
        ...state,
        statusDetail: statusList.process,
      };
    case SUCCESS_FETCH_SUBMISSION_CATEGORIES_DETAIL:
      return {
        ...state,
        statusDetail: statusList.success,
        dataDetail: action.data,
      };
    case ERROR_FETCH_SUBMISSION_CATEGORIES_DETAIL:
      return {
        ...state,
        statusDetail: statusList.error,
      };

    case START_FETCH_SUBMISSION_CATEGORIES:
      return {
        ...state,
        statusList: statusList.process,
      };
    case SUCCESS_FETCH_SUBMISSION_CATEGORIES:
      return {
        ...state,
        statusList: statusList.success,
        data: action.data,
        amountOfData: action.amountOfData,
      };
    case ERROR_FETCH_SUBMISSION_CATEGORIES:
      return {
        ...state,
        statusList: statusList.error,
      };

    case START_CREATE_SUBMISSION_CATEGORIES:
      return {
        ...state,
        statusCreate: statusList.process,
      };
    case SUCCESS_CREATE_SUBMISSION_CATEGORIES:
      return {
        ...state,
        statusCreate: statusList.success,
        dataCreate: action.data,
      };
    case ERROR_CREATE_SUBMISSION_CATEGORIES:
      return {
        ...state,
        statusCreate: statusList.error,
        errorCreate: action.data,
      };
    case RESET_CREATE_SUBMISSION_CATEGORIES:
      return {
        ...state,
        statusCreate: statusList.idle,
        dataCreate: null,
        errorCreate: null,
      };
    case RESET_STATUS_CREATE_SUBMISSION_CATEGORIES:
      return {
        ...state,
        statusCreate: statusList.idle,
      };

    case START_UPDATE_SUBMISSION_CATEGORIES:
      return {
        ...state,
        statusUpdate: statusList.process,
      };
    case SUCCESS_UPDATE_SUBMISSION_CATEGORIES:
      return {
        ...state,
        statusUpdate: statusList.success,
        dataUpdate: action.data,
      };
    case ERROR_UPDATE_SUBMISSION_CATEGORIES:
      return {
        ...state,
        statusUpdate: statusList.error,
        errorUpdate: action.data,
      };
    case RESET_UPDATE_SUBMISSION_CATEGORIES:
      return {
        ...state,
        statusUpdate: statusList.idle,
        dataUpdate: null,
        errorUpdate: null,
      };
    case RESET_STATUS_UPDATE_SUBMISSION_CATEGORIES:
      return {
        ...state,
        statusUpdate: statusList.idle,
      };

    case START_DELETE_SUBMISSION_CATEGORIES:
      return {
        ...state,
        statusDelete: statusList.process,
      };
    case SUCCESS_DELETE_SUBMISSION_CATEGORIES:
      return {
        ...state,
        statusDelete: statusList.success,
        dataDelete: action.data,
      };
    case ERROR_DELETE_SUBMISSION_CATEGORIES:
      return {
        ...state,
        statusDelete: statusList.error,
        errorDelete: action.data,
      };
    case RESET_DELETE_SUBMISSION_CATEGORIES:
      return {
        ...state,
        statusDelete: statusList.idle,
        dataDelete: null,
        errorDelete: null,
      };
    case RESET_STATUS_DELETE_SUBMISSION_CATEGORIES:
      return {
        ...state,
        statusDelete: statusList.idle,
      };

    case CHANGE_PAGE_SUBMISSION_CATEGORIES:
      return {
        ...state,
        page: action.value,
      };
    case CHANGE_LIMIT_SUBMISSION_CATEGORIES:
      return {
        ...state,
        take: action.value,
      };
    case CHANGE_KEYWORD_SUBMISSION_CATEGORIES:
      return {
        ...state,
        keyword: action.value,
        page: 1,
        take: 10,
        startDate: "",
        endDate: "",
      };

    case CHANGE_DATE_RANGE_SUBMISSION_CATEGORIES:
      return {
        ...state,
        startDate: action.value[0],
        endDate: action.value[1],
        page: 1,
        take: 10,
      };

    default:
      return state;
  }
}
