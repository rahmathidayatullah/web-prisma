import debounce from "debounce-promise";

import { getCompanies, getShifts } from "@/app/api/master-data";

import {
  START_FETCH_COMPANIES,
  SUCCESS_FETCH_COMPANIES,
  ERROR_FETCH_COMPANIES,
  //
  START_FETCH_SHIFTS,
  SUCCESS_FETCH_SHIFTS,
  ERROR_FETCH_SHIFTS,
  //
} from "./constans";
import {
  currentDateWithFormat,
  futureDateOneYear,
} from "@/app/utils/constants";

const debounceGetCompanies = debounce(getCompanies, 1000);
const debounceGetShifts = debounce(getShifts, 1000);

export const fetchCompanies = () => {
  return async (dispatch: any, getState: any) => {
    dispatch({
      type: START_FETCH_COMPANIES,
    });

    // const page = getState().masterData.pageCompanies;
    // const take = getState().masterData.takeCompanies;
    // const order = getState().masterData.orderCompanies;
    // const keyword = getState().masterData.keywordCompanies;
    // const startDate = getState().masterData.startDate || currentDateWithFormat;
    // const endDate = getState().masterData.endDate || futureDateOneYear;

    // const params = {
    //   page,
    //   take,
    //   order,
    //   search: keyword,
    //   startDate,
    //   endDate,
    // };

    try {
      const {
        // data: { data, meta },
        data: { data },
        // } = await getCompanies(params);
      } = await getCompanies();

      let newData: any = [];

      if (data.length) {
        newData = data.map((item: any) => {
          return {
            ...item,
            label: item.name,
            value: item.id,
          };
        });
      }

      dispatch({
        type: SUCCESS_FETCH_COMPANIES,
        data: newData,
        // amountOfData: meta.itemCount,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_COMPANIES,
      });
    }
  };
};

export const fetchShifts = () => {
  return async (dispatch: any, getState: any) => {
    dispatch({
      type: START_FETCH_SHIFTS,
    });

    // const page = getState().user.pageShifts;
    // const take = getState().user.takeShifts;
    // const order = getState().user.orderShifts;
    // const keyword = getState().user.keywordShifts;

    // const params = {
    //   page,
    //   take,
    //   order,
    //   keyword,
    // };

    try {
      // const {
      //   data: { data, meta },
      // } = await debounceGetShifts(params);
      const {
        data: { data },
      } = await getShifts();

      let newData: any = [];

      if (data.length) {
        newData = data.map((item: any) => {
          return {
            ...item,
            label: item.name,
            value: item.id,
          };
        });
      }

      dispatch({
        type: SUCCESS_FETCH_SHIFTS,
        data: newData,
        // amountOfData: meta.itemCount,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_SHIFTS,
      });
    }
  };
};
