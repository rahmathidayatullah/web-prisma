import {
  START_FETCH_COMPANIES,
  SUCCESS_FETCH_COMPANIES,
  ERROR_FETCH_COMPANIES,
  CHANGE_PAGE_COMPANIES,
  CHANGE_LIMIT_COMPANIES,
  CHANGE_KEYWORD_COMPANIES,
  //
  START_FETCH_SHIFTS,
  SUCCESS_FETCH_SHIFTS,
  ERROR_FETCH_SHIFTS,
  CHANGE_PAGE_SHIFTS,
  CHANGE_LIMIT_SHIFTS,
  CHANGE_KEYWORD_SHIFTS,
  CHANGE_DATE_RANGE_SHIFTS,
  CHANGE_DATE_RANGE_COMPANIES,
} from "./constans";

const statusList = {
  idle: "idle",
  process: "process",
  success: "success",
  error: "error",
};

const initialState = {
  // Companies
  pageCompanies: 1,
  takeCompanies: 100,
  orderCompanies: "DESC",
  keywordCompanies: "",
  startDateCompanies: "",
  endDateCompanies: "",
  dataCompanies: [],
  amountOfDataCompanies: 0,
  statusListCompanies: statusList.idle,
  statusDeleteCompanies: statusList.idle,
  statusUpdateCompanies: statusList.idle,
  statusCreateCompanies: statusList.idle,

  // Shifts
  pageShifts: 1,
  takeShifts: 100,
  orderShifts: "DESC",
  keywordShifts: "",
  startDateShifts: "",
  endDateShifts: "",
  dataShifts: [],
  amountOfDataShifts: 0,
  statusListShifts: statusList.idle,

  statusDeleteShifts: statusList.idle,
  statusUpdateShifts: statusList.idle,
  statusCreateShifts: statusList.idle,
};

export default function masterDataAttendacesReducer(
  state = initialState,
  action: any
) {
  switch (action.type) {
    // Companies
    case START_FETCH_COMPANIES:
      return {
        ...state,
        statusListCompanies: statusList.process,
      };
    case SUCCESS_FETCH_COMPANIES:
      return {
        ...state,
        statusListCompanies: statusList.success,
        dataCompanies: action.data,
        // amountOfDataCompanies: action.amountOfData,
      };
    case ERROR_FETCH_COMPANIES:
      return {
        ...state,
        statusListCompanies: statusList.error,
      };

    case CHANGE_PAGE_COMPANIES:
      return {
        ...state,
        pageCompanies: action.value,
      };
    case CHANGE_LIMIT_COMPANIES:
      return {
        ...state,
        takeCompanies: action.value,
      };
    case CHANGE_KEYWORD_COMPANIES:
      return {
        ...state,
        keyword: action.value,
        pageCompanies: 1,
        takeCompanies: 10,
        startDateCompanies: "",
        endDateCompanies: "",
      };

    case CHANGE_DATE_RANGE_COMPANIES:
      return {
        ...state,
        startDateCompanies: action.value[0],
        endDateCompanies: action.value[1],
        pageCompanies: 1,
        takeCompanies: 10,
      };

    // Shifts
    case START_FETCH_SHIFTS:
      return {
        ...state,
        statusListShifts: statusList.process,
      };
    case SUCCESS_FETCH_SHIFTS:
      return {
        ...state,
        statusListShifts: statusList.success,
        dataShifts: action.data,
        // amountOfData: action.amountOfData,
      };
    case ERROR_FETCH_SHIFTS:
      return {
        ...state,
        statusListShifts: statusList.error,
      };

    case CHANGE_PAGE_SHIFTS:
      return {
        ...state,
        pageShifts: action.value,
      };
    case CHANGE_LIMIT_SHIFTS:
      return {
        ...state,
        takeShifts: action.value,
      };
    case CHANGE_KEYWORD_SHIFTS:
      return {
        ...state,
        keyword: action.value,
        pageShifts: 1,
        takeShifts: 10,
        startDateShifts: "",
        endDateShifts: "",
      };

    case CHANGE_DATE_RANGE_SHIFTS:
      return {
        ...state,
        startDateShifts: action.value[0],
        endDateShifts: action.value[1],
        pageShifts: 1,
        takeShifts: 10,
      };

    default:
      return state;
  }
}
