// import debounce from "debounce-promise";

import { getShifts } from "@/app/api/master-data";

import {
  START_FETCH_SHIFTS,
  SUCCESS_FETCH_SHIFTS,
  ERROR_FETCH_SHIFTS,
  //
} from "./constans";
// import {
//   currentDateWithFormat,
//   futureDateOneYear,
// } from "@/app/utils/constants";

// const debounceGetCompanies = debounce(getCompanies, 1000);
// const debounceGetShifts = debounce(getShifts, 1000);

export const fetchShifts = () => {
  return async (dispatch: any, getState: any) => {
    dispatch({
      type: START_FETCH_SHIFTS,
    });

    // const page = getState().shiftAttendaces.pageShifts;
    // const take = getState().shiftAttendaces.takeShifts;
    // const order = getState().shiftAttendaces.orderShifts;
    // const keyword = getState().shiftAttendaces.keywordShifts;

    // const params = {
    //   page,
    //   take,
    //   order,
    //   keyword,
    // };

    try {
      // const {
      //   data: { data, meta },
      // } = await debounceGetShifts(params);
      const {
        data: { data },
      } = await getShifts();

      let newData: any = [];

      if (data.length) {
        newData = data.map((item: any) => {
          return {
            ...item,
            label: item.name,
            value: item.id,
          };
        });
      }

      dispatch({
        type: SUCCESS_FETCH_SHIFTS,
        data: newData,
        // amountOfData: meta.itemCount,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_SHIFTS,
      });
    }
  };
};
