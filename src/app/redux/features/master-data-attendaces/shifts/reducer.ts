import {
  START_FETCH_SHIFTS,
  SUCCESS_FETCH_SHIFTS,
  ERROR_FETCH_SHIFTS,
  CHANGE_PAGE_SHIFTS,
  CHANGE_LIMIT_SHIFTS,
  CHANGE_KEYWORD_SHIFTS,
  CHANGE_DATE_RANGE_SHIFTS,
} from "./constans";

const statusList = {
  idle: "idle",
  process: "process",
  success: "success",
  error: "error",
};

const initialState = {
  page: 1,
  take: 100,
  order: "DESC",
  keyword: "",
  startDate: "",
  endDate: "",
  data: [],
  amountOfData: 0,
  statusList: statusList.idle,

  statusDelete: statusList.idle,
  statusUpdate: statusList.idle,
  statusCreate: statusList.idle,
};

export default function shiftAttendacesReducer(
  state = initialState,
  action: any
) {
  switch (action.type) {
    case START_FETCH_SHIFTS:
      return {
        ...state,
        statusList: statusList.process,
      };
    case SUCCESS_FETCH_SHIFTS:
      return {
        ...state,
        statusList: statusList.success,
        data: action.data,
        // amountOfData: action.amountOfData,
      };
    case ERROR_FETCH_SHIFTS:
      return {
        ...state,
        statusList: statusList.error,
      };

    case CHANGE_PAGE_SHIFTS:
      return {
        ...state,
        page: action.value,
      };
    case CHANGE_LIMIT_SHIFTS:
      return {
        ...state,
        take: action.value,
      };
    case CHANGE_KEYWORD_SHIFTS:
      return {
        ...state,
        keyword: action.value,
        page: 1,
        take: 10,
        startDate: "",
        endDate: "",
      };

    case CHANGE_DATE_RANGE_SHIFTS:
      return {
        ...state,
        startDate: action.value[0],
        endDate: action.value[1],
        page: 1,
        take: 10,
      };

    default:
      return state;
  }
}
