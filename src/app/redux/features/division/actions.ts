import debounce from "debounce-promise";

import {
  START_FETCH_DIVISION,
  SUCCESS_FETCH_DIVISION,
  ERROR_FETCH_DIVISION,
  START_UPDATE_DIVISION,
  SUCCESS_UPDATE_DIVISION,
  ERROR_UPDATE_DIVISION,
  START_DELETE_DIVISION,
  SUCCESS_DELETE_DIVISION,
  ERROR_DELETE_DIVISION,
  START_CREATE_DIVISION,
  SUCCESS_CREATE_DIVISION,
  ERROR_CREATE_DIVISION,
  START_FETCH_DIVISION_ALL,
  SUCCESS_FETCH_DIVISION_ALL,
  ERROR_FETCH_DIVISION_ALL,
  START_FETCH_DIVISION_DETAIL,
  SUCCESS_FETCH_DIVISION_DETAIL,
  ERROR_FETCH_DIVISION_DETAIL,
} from "./constants";
import {
  deleteDivisionById,
  getDivision,
  getDivisionAll,
  getDivisionById,
  patchDivisionById,
  postDivision,
} from "@/app/api/master-data";
import {
  currentDateWithFormat,
  futureDateOneYear,
} from "@/app/utils/constants";

const debounceGetDivision = debounce(getDivision, 1000);
const debounceGetDivisionAll = debounce(getDivisionAll, 1000);

export const fetchDivision = () => {
  return async (dispatch: any, getState: any) => {
    dispatch({
      type: START_FETCH_DIVISION,
    });

    const page = getState().divisi.page;
    const take = getState().divisi.take;
    const order = getState().divisi.order;
    const keyword = getState().divisi.keyword;
    const startDate = getState().divisi.startDate || currentDateWithFormat;
    const endDate = getState().divisi.endDate || futureDateOneYear;

    const params = {
      page,
      take,
      order,
      search: keyword,
      endDate,
      startDate,
    };

    try {
      const {
        data: { data, meta },
      } = await debounceGetDivision(params);

      let newData = {
        data: [],
        meta: {
          itemCount: 0,
        },
      };

      if (data.length) {
        newData.data = data.map((item: any) => {
          return {
            ...item,
            label: item.name,
            value: item.id,
          };
        });
        newData.meta = meta;
      }

      dispatch({
        type: SUCCESS_FETCH_DIVISION,
        data: newData.data,
        amountOfData: newData.meta.itemCount,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_DIVISION,
      });
    }
  };
};

export const fetchDivisionById = (id: string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_FETCH_DIVISION_DETAIL,
    });

    try {
      const {
        data: { data },
      } = await getDivisionById(id);

      dispatch({
        type: SUCCESS_FETCH_DIVISION_DETAIL,
        data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_DIVISION_DETAIL,
      });
    }
  };
};

export const fetchDivisionAll = () => {
  return async (dispatch: any, getState: any) => {
    dispatch({
      type: START_FETCH_DIVISION_ALL,
    });
    try {
      const {
        data: { data },
      } = await debounceGetDivisionAll();

      let newData = {
        data: [],
      };

      if (data.length) {
        newData.data = data.map((item: any) => {
          return {
            ...item,
            label: item.name,
            value: item.id,
          };
        });
      }

      dispatch({
        type: SUCCESS_FETCH_DIVISION_ALL,
        data: newData.data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_DIVISION_ALL,
      });
    }
  };
};

export const createDivision = (body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_CREATE_DIVISION,
    });

    try {
      const {
        data: { data },
      } = await postDivision(body);

      dispatch({
        type: SUCCESS_CREATE_DIVISION,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_CREATE_DIVISION,
        data: error,
      });
    }
  };
};

export const removeDivision = (id: number | string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_DELETE_DIVISION,
    });

    try {
      const {
        data: { data },
      } = await deleteDivisionById(id);

      dispatch({
        type: SUCCESS_DELETE_DIVISION,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_DELETE_DIVISION,
        data: error,
      });
    }
  };
};

export const updateDivision = (id: string, body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_UPDATE_DIVISION,
    });

    try {
      const {
        data: { data },
      } = await patchDivisionById(id, body);

      dispatch({
        type: SUCCESS_UPDATE_DIVISION,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_UPDATE_DIVISION,
        data: error,
      });
    }
  };
};
