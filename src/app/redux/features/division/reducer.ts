import {
  START_FETCH_DIVISION,
  SUCCESS_FETCH_DIVISION,
  ERROR_FETCH_DIVISION,
  START_FETCH_DIVISION_ALL,
  SUCCESS_FETCH_DIVISION_ALL,
  ERROR_FETCH_DIVISION_ALL,
  START_UPDATE_DIVISION,
  SUCCESS_UPDATE_DIVISION,
  ERROR_UPDATE_DIVISION,
  RESET_UPDATE_DIVISION,
  START_DELETE_DIVISION,
  SUCCESS_DELETE_DIVISION,
  ERROR_DELETE_DIVISION,
  RESET_DELETE_DIVISION,
  START_CREATE_DIVISION,
  SUCCESS_CREATE_DIVISION,
  ERROR_CREATE_DIVISION,
  RESET_CREATE_DIVISION,
  CHANGE_DATE_RANGE_DIVISION,
  CHANGE_KEYWORD_DIVISION,
  CHANGE_LIMIT_DIVISION,
  CHANGE_PAGE_DIVISION,
  RESET_STATUS_CREATE_DIVISION,
  RESET_STATUS_UPDATE_DIVISION,
  RESET_STATUS_DELETE_DIVISION,
  START_FETCH_DIVISION_DETAIL,
  SUCCESS_FETCH_DIVISION_DETAIL,
  ERROR_FETCH_DIVISION_DETAIL,
  SELECT_DIVISI,
} from "./constants";

const statusList = {
  idle: "idle",
  process: "process",
  success: "success",
  error: "error",
};

const initialState = {
  page: 1,
  take: 10,
  order: "DESC",
  keyword: "",
  startDate: "",
  endDate: "",
  data: [],
  amountOfData: 0,
  statusList: statusList.idle,
  statusListAll: statusList.idle,

  dataAll: [],
  selectDivisi: null,
  dataDetail: null,
  dataDelete: null,
  dataCreate: null,
  dataUpdate: null,

  errorDelete: null,
  errorCreate: null,
  errorUpdate: null,

  statusDetail: statusList.idle,
  statusDelete: statusList.idle,
  statusUpdate: statusList.idle,
  statusCreate: statusList.idle,
};

export default function divisiReducer(state = initialState, action: any) {
  switch (action.type) {
    case START_FETCH_DIVISION_ALL:
      return {
        ...state,
        statusListAll: statusList.process,
      };
    case SUCCESS_FETCH_DIVISION_ALL:
      return {
        ...state,
        statusListAll: statusList.success,
        dataAll: action.data,
      };
    case ERROR_FETCH_DIVISION_ALL:
      return {
        ...state,
        statusListAll: statusList.error,
      };

    case SELECT_DIVISI:
      return {
        ...state,
        selectDivisi: action.value,
      };

    case START_FETCH_DIVISION_DETAIL:
      return {
        ...state,
        statusDetail: statusList.process,
      };
    case SUCCESS_FETCH_DIVISION_DETAIL:
      return {
        ...state,
        statusDetail: statusList.success,
        dataDetail: action.data,
      };
    case ERROR_FETCH_DIVISION_DETAIL:
      return {
        ...state,
        statusDetail: statusList.error,
      };

    case START_FETCH_DIVISION:
      return {
        ...state,
        statusList: statusList.process,
      };
    case SUCCESS_FETCH_DIVISION:
      return {
        ...state,
        statusList: statusList.success,
        data: action.data,
        amountOfData: action.amountOfData,
      };
    case ERROR_FETCH_DIVISION:
      return {
        ...state,
        statusList: statusList.error,
      };

    case START_CREATE_DIVISION:
      return {
        ...state,
        statusCreate: statusList.process,
      };
    case SUCCESS_CREATE_DIVISION:
      return {
        ...state,
        statusCreate: statusList.success,
        dataCreate: action.data,
      };
    case ERROR_CREATE_DIVISION:
      return {
        ...state,
        statusCreate: statusList.error,
        errorCreate: action.data,
      };
    case RESET_CREATE_DIVISION:
      return {
        ...state,
        statusCreate: statusList.idle,
        dataCreate: null,
        errorCreate: null,
      };
    case RESET_STATUS_CREATE_DIVISION:
      return {
        ...state,
        statusCreate: statusList.idle,
      };

    case START_UPDATE_DIVISION:
      return {
        ...state,
        statusUpdate: statusList.process,
      };
    case SUCCESS_UPDATE_DIVISION:
      return {
        ...state,
        statusUpdate: statusList.success,
        dataUpdate: action.data,
      };
    case ERROR_UPDATE_DIVISION:
      return {
        ...state,
        statusUpdate: statusList.error,
        errorUpdate: action.data,
      };
    case RESET_UPDATE_DIVISION:
      return {
        ...state,
        statusUpdate: statusList.idle,
        dataUpdate: null,
        errorUpdate: null,
      };
    case RESET_STATUS_UPDATE_DIVISION:
      return {
        ...state,
        statusUpdate: statusList.idle,
      };

    case START_DELETE_DIVISION:
      return {
        ...state,
        statusDelete: statusList.process,
      };
    case SUCCESS_DELETE_DIVISION:
      return {
        ...state,
        statusDelete: statusList.success,
        dataDelete: action.data,
      };
    case ERROR_DELETE_DIVISION:
      return {
        ...state,
        statusDelete: statusList.error,
        errorDelete: action.data,
      };
    case RESET_DELETE_DIVISION:
      return {
        ...state,
        statusDelete: statusList.idle,
        dataDelete: null,
        errorDelete: null,
      };
    case RESET_STATUS_DELETE_DIVISION:
      return {
        ...state,
        statusDelete: statusList.idle,
      };

    case CHANGE_PAGE_DIVISION:
      return {
        ...state,
        page: action.value,
      };
    case CHANGE_LIMIT_DIVISION:
      return {
        ...state,
        take: action.value,
      };
    case CHANGE_KEYWORD_DIVISION:
      return {
        ...state,
        keyword: action.value,
        page: 1,
        take: 10,
        startDate: "",
        endDate: "",
      };
    case CHANGE_DATE_RANGE_DIVISION:
      return {
        ...state,
        startDate: action.value[0],
        endDate: action.value[1],
        page: 1,
        take: 10,
      };

    default:
      return state;
  }
}
