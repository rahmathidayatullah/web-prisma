import { getUsersProfile, patchUsersProfile } from "@/app/api/profile";
import {
  START_FETCH_PROFILE,
  SUCCESS_FETCH_PROFILE,
  ERROR_FETCH_PROFILE,
  START_UPDATE_PROFILE,
  SUCCESS_UPDATE_PROFILE,
  ERROR_UPDATE_PROFILE,
} from "./constans";
import { axiosInstance } from "@/app/utils/axios";

export const fetchUserProfile = () => {
  return async (dispatch: any) => {
    dispatch({
      type: START_FETCH_PROFILE,
    });
    try {
      const {
        data: { data },
      } = await getUsersProfile();
      dispatch({
        type: SUCCESS_FETCH_PROFILE,
        data,
      });
    } catch (error: any) {
      if (error?.response?.status === 401) {
        localStorage.clear();
        alert("error 401, Anda tidak memiliki akses silahkan login kembali");
        window.location.href = "/login";
      }
      dispatch({
        type: ERROR_FETCH_PROFILE,
      });
    }
  };
};

export const fetchUserProfileFromMobile = (token: string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_FETCH_PROFILE,
    });
    try {
      const {
        data: { data },
      } = await axiosInstance.get(`/users/profile`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      const userData = {
        access_token: token,
        user: data.user,
      };

      localStorage.setItem("userData", JSON.stringify(userData));

      alert("berhasil fetch user profile");
      dispatch({
        type: SUCCESS_FETCH_PROFILE,
        data,
      });
    } catch (error: any) {
      alert(
        `tidak bisa access halaman gagal mendapatkan token ${error?.message}`
      );
      if (error?.response?.status === 401) {
        localStorage.clear();
        alert("error 401, Anda tidak memiliki akses silahkan login kembali");
        window.location.href = "/login";
      }
      dispatch({
        type: ERROR_FETCH_PROFILE,
      });
    }
  };
};

export const updateProfile = (body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_UPDATE_PROFILE,
    });
    try {
      const {
        data: { data },
      } = await patchUsersProfile(body);
      dispatch({
        type: SUCCESS_UPDATE_PROFILE,
        data,
      });
    } catch (error: any) {
      if (error?.response?.status === 401) {
        localStorage.clear();
        alert("error 401, Anda tidak memiliki akses silahkan login kembali");
        window.location.href = "/login";
      }
      dispatch({
        type: ERROR_UPDATE_PROFILE,
        data: error,
      });
    }
  };
};
