import {
  START_FETCH_PROFILE,
  SUCCESS_FETCH_PROFILE,
  ERROR_FETCH_PROFILE,
  START_UPDATE_PROFILE,
  SUCCESS_UPDATE_PROFILE,
  ERROR_UPDATE_PROFILE,
  RESET_UPDATE_PROFILE,
} from "./constans";

const statusList = {
  idle: "idle",
  process: "process",
  success: "success",
  error: "error",
};

const initialState = {
  statusListProfile: statusList.idle,
  dataProfile: null,

  dataUpdateProfile: null,
  errorUpdateProfile: null,
  statusUpdateProfile: statusList.idle,
};

export default function profileReducer(state = initialState, action: any) {
  switch (action.type) {
    case START_FETCH_PROFILE:
      return {
        ...state,
        statusListProfile: statusList.process,
      };
    case SUCCESS_FETCH_PROFILE:
      return {
        ...state,
        statusListProfile: statusList.success,
        dataProfile: action.data,
      };
    case ERROR_FETCH_PROFILE:
      return {
        ...state,
        statusListProfile: statusList.error,
      };

    case START_UPDATE_PROFILE:
      return {
        ...state,
        statusUpdateProfile: statusList.process,
      };
    case SUCCESS_UPDATE_PROFILE:
      return {
        ...state,
        statusUpdateProfile: statusList.success,
        dataUpdateProfile: action.data,
      };
    case ERROR_UPDATE_PROFILE:
      return {
        ...state,
        statusUpdateProfile: statusList.error,
        errorUpdateProfile: action.data,
      };
    case RESET_UPDATE_PROFILE:
      return {
        ...state,
        statusUpdateProfile: statusList.idle,
        dataUpdateProfile: null,
        errorUpdateProfile: null,
      };

    default:
      return state;
  }
}
