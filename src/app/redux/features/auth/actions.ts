import {
  login,
  postForgotPassword,
  postUpdateForgotPassword,
} from "@/app/api/auth";
import {
  START_LOGIN,
  SUCCESS_LOGIN,
  ERROR_LOGIN,
  START_FORGOT_PASSWORD,
  SUCCESS_FORGOT_PASSWORD,
  ERROR_FORGOT_PASSWORD,
  SUCCESS_LOGOUT,
  START_FORGOT_PASSWORD_RESET,
  SUCCESS_FORGOT_PASSWORD_RESET,
  ERROR_FORGOT_PASSWORD_RESET,
} from "./constans";

export const postLogin = (payload: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_LOGIN,
    });
    try {
      const {
        data: { data },
      } = await login(payload);
      dispatch({
        type: SUCCESS_LOGIN,
        userData: data,
      });
    } catch (error: any) {
      dispatch({
        type: ERROR_LOGIN,
        dataError: error?.response?.data ?? null,
      });
    }
  };
};

export const forgotPassword = (payload: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_FORGOT_PASSWORD,
    });
    try {
      const {
        data: { data },
      } = await postForgotPassword(payload);
      dispatch({
        type: SUCCESS_FORGOT_PASSWORD,
        data,
      });
    } catch (error: any) {
      dispatch({
        type: ERROR_FORGOT_PASSWORD,
        error: error?.response?.data ?? null,
      });
    }
  };
};

export const forgotPasswordReset = (
  payload: any,
  paramsToken: string | any
) => {
  const body = {
    password: payload.password,
    token: paramsToken,
  };
  return async (dispatch: any) => {
    dispatch({
      type: START_FORGOT_PASSWORD_RESET,
    });
    try {
      const {
        data: { data },
      } = await postUpdateForgotPassword(body);
      dispatch({
        type: SUCCESS_FORGOT_PASSWORD_RESET,
        data,
      });
    } catch (error: any) {
      dispatch({
        type: ERROR_FORGOT_PASSWORD_RESET,
        error: error?.response?.data ?? null,
      });
    }
  };
};

export const logout = () => {
  return async (dispatch: any) => {
    dispatch({
      type: SUCCESS_LOGOUT,
    });
  };
};
