import {
  START_LOGIN,
  SUCCESS_LOGIN,
  ERROR_LOGIN,
  START_FORGOT_PASSWORD,
  SUCCESS_FORGOT_PASSWORD,
  ERROR_FORGOT_PASSWORD,
  RESET_FORGOT_PASSWORD,
  RESET_STATUS_FORGOT_PASSWORD,
  START_FORGOT_PASSWORD_RESET,
  SUCCESS_FORGOT_PASSWORD_RESET,
  ERROR_FORGOT_PASSWORD_RESET,
  RESET_FORGOT_PASSWORD_RESET,
  RESET_STATUS_FORGOT_PASSWORD_RESET,
  START_LOGOUT,
  SUCCESS_LOGOUT,
  ERROR_LOGOUT,
  CLEAR_STATE,
} from "./constans";

const statusList = {
  idle: "idle",
  process: "process",
  success: "success",
  error: "error",
};

const initialState = {
  userData: {
    access_token: "",
    user: {
      name: "",
      role: {
        name: "",
      },
      shift: {
        start_time: "",
        end_time: "",
      },
    },
  },
  statusLogin: statusList.idle,
  statusLogout: statusList.idle,
  dataError: null,

  errorForgot: null,
  statusForgot: statusList.idle,
  dataForgot: null,

  errorForgotReset: null,
  statusForgotReset: statusList.idle,
  dataForgotReset: null,
};

export default function authReducer(state = initialState, action: any) {
  switch (action.type) {
    case START_FORGOT_PASSWORD_RESET:
      return {
        ...state,
        statusForgotReset: statusList.process,
      };
    case SUCCESS_FORGOT_PASSWORD_RESET:
      return {
        ...state,
        statusForgotReset: statusList.success,
        dataForgotReset: action.data,
      };
    case ERROR_FORGOT_PASSWORD_RESET:
      return {
        ...state,
        statusForgotReset: statusList.error,
        errorForgotReset: action.error,
      };
    case RESET_FORGOT_PASSWORD_RESET:
      return {
        ...state,
        statusForgotReset: statusList.idle,
        dataForgotReset: null,
        errorForgotReset: null,
      };
    case RESET_STATUS_FORGOT_PASSWORD_RESET:
      return {
        ...state,
        statusForgotReset: statusList.idle,
      };
    case START_FORGOT_PASSWORD:
      return {
        ...state,
        statusForgot: statusList.process,
      };
    case SUCCESS_FORGOT_PASSWORD:
      return {
        ...state,
        statusForgot: statusList.success,
        dataForgot: action.data,
      };
    case ERROR_FORGOT_PASSWORD:
      return {
        ...state,
        statusForgot: statusList.error,
        errorForgot: action.error,
      };
    case RESET_FORGOT_PASSWORD:
      return {
        ...state,
        statusForgot: statusList.idle,
        dataForgot: null,
        errorForgot: null,
      };
    case RESET_STATUS_FORGOT_PASSWORD:
      return {
        ...state,
        statusForgot: statusList.idle,
      };
    case START_LOGIN:
      return {
        ...state,
        statusLogin: statusList.process,
      };
    case SUCCESS_LOGIN:
      return {
        ...state,
        statusLogin: statusList.success,
        userData: action.userData,
      };

    case CLEAR_STATE:
      return {
        ...state,
        statusLogin: statusList.idle,
        userData: {
          access_token: "",
          user: {
            name: "",
            role: {
              name: "",
            },
            shift: {
              start_time: "",
              end_time: "",
            },
          },
        },
      };
    case ERROR_LOGIN:
      return {
        ...state,
        statusLogin: statusList.error,
        dataError: action.dataError,
      };

    case START_LOGOUT:
      return {
        ...state,
        statusLogout: statusList.process,
      };
    case SUCCESS_LOGOUT:
      return {
        ...state,
        statusLogout: statusList.success,
        statusLogin: statusList.idle,
        userData: {
          access_token: "",
          user: {
            name: "",
            role: {
              name: "",
            },
            shift: {
              start_time: "",
              end_time: "",
            },
          },
        },
      };
    case ERROR_LOGOUT:
      return {
        ...state,
        statusLogout: statusList.error,
      };

    default:
      return state;
  }
}
