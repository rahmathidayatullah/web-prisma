import {
  START_FETCH_ATTENDACE,
  SUCCESS_FETCH_ATTENDACE,
  ERROR_FETCH_ATTENDACE,
  START_EXPORT_ATTENDACE,
  SUCCESS_EXPORT_ATTENDACE,
  ERROR_EXPORT_ATTENDACE,
  START_APPROVE_ATTENDACE,
  SUCCESS_APPROVE_ATTENDACE,
  ERROR_APPROVE_ATTENDACE,
  START_REJECT_ATTENDACE,
  SUCCESS_REJECT_ATTENDACE,
  ERROR_REJECT_ATTENDACE,
  RESET_STATUS_REJECT_APPROVE,
  CHANGE_PAGE,
  CHANGE_LIMIT,
  CHANGE_KEYWORD,
  START_UPDATE_ATTENDACE,
  SUCCESS_UPDATE_ATTENDACE,
  ERROR_UPDATE_ATTENDACE,
  RESET_UPDATE_ATTENDACE,
  START_FETCH_ATTENDACE_DETAIL,
  SUCCESS_FETCH_ATTENDACE_DETAIL,
  ERROR_FETCH_ATTENDACE_DETAIL,
  CHANGE_DATE_RANGE,
  RESET_EXPORT_ATTENDACE,
  RESET_STATUS_EXPORT_ATTENDACE,
  RESET_STATUS_UPDATE_ATTENDACE,
} from "./constans";

const statusList = {
  idle: "idle",
  process: "process",
  success: "success",
  error: "error",
};

const initialState = {
  statusListAttendace: statusList.idle,
  page: 1,
  take: 10,
  order: "DESC",
  keyword: "",
  startDate: "",
  endDate: "",
  dataAttendace: [],
  statusApprove: statusList.idle,
  statusReject: statusList.idle,
  amountOfData: 0,
  //
  statusUpdate: statusList.idle,
  dataUpdate: null,
  //
  statusDetail: statusList.idle,
  dataDetail: null,
  //
  dataExport: null,
  errorExport: null,
  statusExport: statusList.idle,
};

export default function attendaceReducer(state = initialState, action: any) {
  switch (action.type) {
    case START_EXPORT_ATTENDACE:
      return {
        ...state,
        statusExport: statusList.process,
      };
    case SUCCESS_EXPORT_ATTENDACE:
      return {
        ...state,
        statusExport: statusList.success,
        dataExport: action.data,
      };
    case ERROR_EXPORT_ATTENDACE:
      return {
        ...state,
        statusExport: statusList.error,
        errorExport: action.data,
      };
    case RESET_EXPORT_ATTENDACE:
      return {
        ...state,
        statusExport: statusList.idle,
        dataExport: null,
        errorExport: null,
      };
    case RESET_STATUS_EXPORT_ATTENDACE:
      return {
        ...state,
        statusExport: statusList.idle,
      };
    case START_FETCH_ATTENDACE_DETAIL:
      return {
        ...state,
        statusDetail: statusList.process,
      };
    case SUCCESS_FETCH_ATTENDACE_DETAIL:
      return {
        ...state,
        statusDetail: statusList.success,
        dataDetail: action.data,
      };
    case ERROR_FETCH_ATTENDACE_DETAIL:
      return {
        ...state,
        statusDetail: statusList.error,
      };
    case START_UPDATE_ATTENDACE:
      return {
        ...state,
        statusUpdate: statusList.process,
      };
    case SUCCESS_UPDATE_ATTENDACE:
      return {
        ...state,
        statusUpdate: statusList.success,
        dataUpdate: action.data,
      };
    case ERROR_UPDATE_ATTENDACE:
      return {
        ...state,
        statusUpdate: statusList.error,
        errorUpdate: action.data,
      };
    case RESET_UPDATE_ATTENDACE:
      return {
        ...state,
        statusUpdate: statusList.idle,
        dataUpdate: null,
        errorUpdate: null,
      };
    case RESET_STATUS_UPDATE_ATTENDACE:
      return {
        ...state,
        statusUpdate: statusList.idle,
      };
    case START_FETCH_ATTENDACE:
      return {
        ...state,
        statusListAttendace: statusList.process,
      };
    case SUCCESS_FETCH_ATTENDACE:
      return {
        ...state,
        statusListAttendace: statusList.success,
        dataAttendace: action.data,
        amountOfData: action.amountOfData,
      };
    case ERROR_FETCH_ATTENDACE:
      return {
        ...state,
        statusListAttendace: statusList.error,
      };
    case START_APPROVE_ATTENDACE:
      return {
        ...state,
        statusApprove: statusList.process,
      };
    case SUCCESS_APPROVE_ATTENDACE:
      return {
        ...state,
        statusApprove: statusList.success,
      };
    case ERROR_APPROVE_ATTENDACE:
      return {
        ...state,
        statusApprove: statusList.error,
      };
    case START_REJECT_ATTENDACE:
      return {
        ...state,
        statusReject: statusList.process,
      };
    case SUCCESS_REJECT_ATTENDACE:
      return {
        ...state,
        statusReject: statusList.success,
      };
    case ERROR_REJECT_ATTENDACE:
      return {
        ...state,
        statusReject: statusList.error,
      };
    case RESET_STATUS_REJECT_APPROVE:
      return {
        ...state,
        statusReject: statusList.idle,
        statusApprove: statusList.idle,
      };
    case CHANGE_PAGE:
      return {
        ...state,
        page: action.value,
      };
    case CHANGE_LIMIT:
      return {
        ...state,
        take: action.value,
      };
    case CHANGE_KEYWORD:
      return {
        ...state,
        keyword: action.value,
        page: 1,
        take: 10,
      };

    case CHANGE_DATE_RANGE:
      return {
        ...state,
        startDate: action.value[0],
        endDate: action.value[1],
        page: 1,
        take: 10,
      };
    default:
      return state;
  }
}
