import debounce from "debounce-promise";

import {
  approveAttendaces,
  getAttendances,
  getAttendancesById,
  getAttendancesExport,
  patchAttendances,
  rejectAttendaces,
} from "@/app/api/attendace";

import {
  START_FETCH_ATTENDACE,
  SUCCESS_FETCH_ATTENDACE,
  ERROR_FETCH_ATTENDACE,
  START_FETCH_ATTENDACE_DETAIL,
  SUCCESS_FETCH_ATTENDACE_DETAIL,
  ERROR_FETCH_ATTENDACE_DETAIL,
  START_APPROVE_ATTENDACE,
  SUCCESS_APPROVE_ATTENDACE,
  ERROR_APPROVE_ATTENDACE,
  START_REJECT_ATTENDACE,
  SUCCESS_REJECT_ATTENDACE,
  ERROR_REJECT_ATTENDACE,
  START_UPDATE_ATTENDACE,
  SUCCESS_UPDATE_ATTENDACE,
  ERROR_UPDATE_ATTENDACE,
  START_EXPORT_ATTENDACE,
  SUCCESS_EXPORT_ATTENDACE,
  ERROR_EXPORT_ATTENDACE,
} from "./constans";
import {
  currentDateWithFormat,
  dateEndNow,
  dateStartNow,
  futureDateOneMonth,
  futureDateOneYear,
  removeEmptyAttributes,
} from "@/app/utils/constants";

const debounceGetAttendances = debounce(getAttendances, 1000);

export const fetchAttendaces = () => {
  return async (dispatch: any, getState: any) => {
    dispatch({
      type: START_FETCH_ATTENDACE,
    });

    const page = getState().attendace.page;
    const take = getState().attendace.take;
    const order = getState().attendace.order;
    const keyword = getState().attendace.keyword;
    const startDate = getState().attendace.startDate || currentDateWithFormat;
    const endDate = getState().attendace.endDate || futureDateOneYear;
    const division = getState().divisi.selectDivisi || "";

    const params = {
      page,
      take,
      order,
      search: keyword,
      endDate,
      startDate,
      division,
    };

    const newParams = removeEmptyAttributes(params);

    try {
      const {
        data: { data, meta },
      } = await debounceGetAttendances(newParams);

      let newData: any = {
        data: [],
        meta: {
          itemCount: 0,
        },
      };

      if (data.length) {
        newData.data = data.map((item: any) => {
          return {
            ...item,
            pointlocationClockIn:
              item.clockInLatitude && item.clockInLongitude
                ? `${item.clockInLatitude},${item.clockInLongitude}`
                : null,
            pointlocationClockOut:
              item.clockOutLatitude && item.clockOutLongitude
                ? `${item.clockOutLatitude},${item.clockOutLongitude}`
                : null,
            byAction: {
              name: item.actionBy ? item.actionBy.name : null,
              status: item.status,
              editBy: item.editedBy ? item.editedBy.name : null,
            },
            shift: {
              in: item.shiftIn,
              out: item.shiftOut,
            },
          };
        });
        newData.meta = meta;
      }

      dispatch({
        type: SUCCESS_FETCH_ATTENDACE,
        data: newData.data,
        amountOfData: newData.meta.itemCount,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_ATTENDACE,
      });
    }
  };
};

export const fetchAttendacesExports = () => {
  return async (dispatch: any, getState: any) => {
    dispatch({
      type: START_EXPORT_ATTENDACE,
    });

    const page = getState().attendace.page;
    // const take = getState().attendace.take;
    // const order = getState().attendace.order;
    const keyword = getState().attendace.keyword;
    const startDate = getState().attendace.startDate || dateStartNow; //current month 01;
    const endDate = getState().attendace.endDate || dateEndNow; //current month 30/31;

    const params = {
      page,
      // take,
      // order,
      search: keyword,
      endDate,
      startDate,
    };

    try {
      const { data } = await getAttendancesExport(params);
      dispatch({
        type: SUCCESS_EXPORT_ATTENDACE,
        data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_EXPORT_ATTENDACE,
        data: error,
      });
    }
  };
};

export const fetchAttendacesDetail = (id: string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_FETCH_ATTENDACE_DETAIL,
    });

    try {
      const {
        data: { data },
      } = await getAttendancesById(id);
      dispatch({
        type: SUCCESS_FETCH_ATTENDACE_DETAIL,
        data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_ATTENDACE_DETAIL,
      });
    }
  };
};

export const attendacesApprove = (id: any, body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_APPROVE_ATTENDACE,
    });
    await approveAttendaces(id, body);
    dispatch({
      type: SUCCESS_APPROVE_ATTENDACE,
    });
    dispatch(fetchAttendaces());
    try {
    } catch (error) {
      dispatch({
        type: ERROR_APPROVE_ATTENDACE,
      });
    }
  };
};
export const updateAttendaces = (id: any, body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_UPDATE_ATTENDACE,
    });
    const {
      data: { data },
    } = await patchAttendances(id, body);
    dispatch({
      type: SUCCESS_UPDATE_ATTENDACE,
      data,
    });
    dispatch(fetchAttendaces());
    try {
    } catch (error) {
      dispatch({
        type: ERROR_UPDATE_ATTENDACE,
      });
    }
  };
};

export const attendacesReject = (id: any, body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_REJECT_ATTENDACE,
    });
    await rejectAttendaces(id, body);
    dispatch({
      type: SUCCESS_REJECT_ATTENDACE,
    });
    dispatch(fetchAttendaces());
    try {
    } catch (error) {
      dispatch({
        type: ERROR_REJECT_ATTENDACE,
      });
    }
  };
};
