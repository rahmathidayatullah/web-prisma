import {
  START_FETCH_BLOCKS,
  SUCCESS_FETCH_BLOCKS,
  ERROR_FETCH_BLOCKS,
  START_FETCH_BLOCKS_DETAIL,
  SUCCESS_FETCH_BLOCKS_DETAIL,
  ERROR_FETCH_BLOCKS_DETAIL,
  START_CREATE_BLOCKS,
  SUCCESS_CREATE_BLOCKS,
  ERROR_CREATE_BLOCKS,
  START_UPDATE_BLOCKS,
  SUCCESS_UPDATE_BLOCKS,
  ERROR_UPDATE_BLOCKS,
  START_DELETE_BLOCKS,
  SUCCESS_DELETE_BLOCKS,
  ERROR_DELETE_BLOCKS,
  CHANGE_PAGE,
  CHANGE_LIMIT,
  CHANGE_KEYWORD,
  RESET_CREATE_BLOCKS,
  RESET_UPDATE_BLOCKS,
  RESET_DELETE_BLOCKS,
  ERROR_FETCH_BLOCKS_ALL,
  SUCCESS_FETCH_BLOCKS_ALL,
  START_FETCH_BLOCKS_ALL,
  SELECT_BLOCK,
  RESET_STATUS_DELETE_BLOCKS,
  RESET_STATUS_UPDATE_BLOCKS,
  RESET_STATUS_CREATE_BLOCKS,
} from "./constans";

const statusList = {
  idle: "idle",
  process: "process",
  success: "success",
  error: "error",
};

const initialState = {
  page: 1,
  take: 10,
  order: "DESC",
  keyword: "",
  dataBlocks: [],
  dataBlocksDetail: null,
  dataBlocksCreate: null,
  amountOfData: 0,

  errorDelete: null,
  errorUpdate: null,
  errorCraete: null,

  dataDelete: null,
  dataUpdate: null,

  // role all
  statusListBlockAll: statusList.idle,
  dataListBlockAll: [],
  selectBlock: null,

  statusListBlocks: statusList.idle,
  statusDetailBlocks: statusList.idle,
  statusDeleteBlocks: statusList.idle,
  statusUpdateBlocks: statusList.idle,
  statusCreateBlocks: statusList.idle,
};

export default function blocksReducer(state = initialState, action: any) {
  switch (action.type) {
    case START_FETCH_BLOCKS_ALL:
      return {
        ...state,
        statusListBlockAll: statusList.process,
      };
    case SUCCESS_FETCH_BLOCKS_ALL:
      return {
        ...state,
        statusListBlockAll: statusList.success,
        dataListBlockAll: action.data,
      };
    case ERROR_FETCH_BLOCKS_ALL:
      return {
        ...state,
        statusListBlockAll: statusList.error,
      };
    case SELECT_BLOCK:
      return {
        ...state,
        selectBlock: action.value,
      };

    case START_FETCH_BLOCKS:
      return {
        ...state,
        statusListBlocks: statusList.process,
      };
    case SUCCESS_FETCH_BLOCKS:
      return {
        ...state,
        statusListBlocks: statusList.success,
        dataBlocks: action.data,
        amountOfData: action.amountOfData,
      };
    case ERROR_FETCH_BLOCKS:
      return {
        ...state,
        statusListBlocks: statusList.error,
      };

    case START_FETCH_BLOCKS_DETAIL:
      return {
        ...state,
        statusDetailBlocks: statusList.process,
      };
    case SUCCESS_FETCH_BLOCKS_DETAIL:
      return {
        ...state,
        statusDetailBlocks: statusList.success,
        dataBlocksDetail: action.data,
      };
    case ERROR_FETCH_BLOCKS_DETAIL:
      return {
        ...state,
        statusDetailBlocks: statusList.error,
      };

    case START_DELETE_BLOCKS:
      return {
        ...state,
        statusDeleteBlocks: statusList.process,
      };
    case SUCCESS_DELETE_BLOCKS:
      return {
        ...state,
        statusDeleteBlocks: statusList.success,
        dataDelete: action.data,
      };
    case ERROR_DELETE_BLOCKS:
      return {
        ...state,
        statusDeleteBlocks: statusList.error,
        errorDelete: action.data,
      };
    case RESET_DELETE_BLOCKS:
      return {
        ...state,
        statusDeleteBlocks: statusList.idle,
        dataDelete: null,
        errorDelete: null,
      };
    case RESET_STATUS_DELETE_BLOCKS:
      return {
        ...state,
        statusDeleteBlocks: statusList.idle,
      };

    case START_UPDATE_BLOCKS:
      return {
        ...state,
        statusUpdateBlocks: statusList.process,
      };
    case SUCCESS_UPDATE_BLOCKS:
      return {
        ...state,
        statusUpdateBlocks: statusList.success,
        dataUpdate: action.data,
      };
    case ERROR_UPDATE_BLOCKS:
      return {
        ...state,
        statusUpdateBlocks: statusList.error,
        errorUpdate: action.data,
      };
    case RESET_UPDATE_BLOCKS:
      return {
        ...state,
        statusUpdateBlocks: statusList.idle,
        dataUpdate: null,
        errorUpdate: null,
      };
    case RESET_STATUS_UPDATE_BLOCKS:
      return {
        ...state,
        statusUpdateBlocks: statusList.idle,
      };

    case START_CREATE_BLOCKS:
      return {
        ...state,
        statusCreateBlocks: statusList.process,
      };
    case SUCCESS_CREATE_BLOCKS:
      return {
        ...state,
        statusCreateBlocks: statusList.success,
        dataBlocksCreate: action.data,
      };
    case ERROR_CREATE_BLOCKS:
      return {
        ...state,
        statusCreateBlocks: statusList.error,
        errorCreate: action.data,
      };
    case RESET_CREATE_BLOCKS:
      return {
        ...state,
        statusCreateBlocks: statusList.idle,
        dataBlocksCreate: null,
        errorCreate: null,
      };
    case RESET_STATUS_CREATE_BLOCKS:
      return {
        ...state,
        statusCreateBlocks: statusList.idle,
      };

    case CHANGE_PAGE:
      return {
        ...state,
        page: action.value,
      };
    case CHANGE_LIMIT:
      return {
        ...state,
        take: action.value,
      };
    case CHANGE_KEYWORD:
      return {
        ...state,
        keyword: action.value,
        page: 1,
        take: 10,
      };
    default:
      return state;
  }
}
