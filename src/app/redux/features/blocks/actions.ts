import debounce from "debounce-promise";

import {
  deleteBlocksById,
  getBlocks,
  getBlocksAll,
  getBlocksById,
  patchBlocksById,
  postBlocks,
} from "@/app/api/blocks";

import {
  START_FETCH_BLOCKS,
  SUCCESS_FETCH_BLOCKS,
  ERROR_FETCH_BLOCKS,
  START_FETCH_BLOCKS_DETAIL,
  SUCCESS_FETCH_BLOCKS_DETAIL,
  ERROR_FETCH_BLOCKS_DETAIL,
  START_CREATE_BLOCKS,
  SUCCESS_CREATE_BLOCKS,
  ERROR_CREATE_BLOCKS,
  START_UPDATE_BLOCKS,
  SUCCESS_UPDATE_BLOCKS,
  ERROR_UPDATE_BLOCKS,
  START_DELETE_BLOCKS,
  SUCCESS_DELETE_BLOCKS,
  ERROR_DELETE_BLOCKS,
  CHANGE_PAGE,
  CHANGE_LIMIT,
  CHANGE_KEYWORD,
  START_FETCH_BLOCKS_ALL,
  ERROR_FETCH_BLOCKS_ALL,
  SUCCESS_FETCH_BLOCKS_ALL,
} from "./constans";

const debounceGetBlocks = debounce(getBlocks, 1000);

export const fetchBlocks = (id: string) => {
  return async (dispatch: any, getState: any) => {
    dispatch({
      type: START_FETCH_BLOCKS,
    });

    const page = getState().blocks.page;
    const take = getState().blocks.take;
    const order = getState().blocks.order;
    const keyword = getState().blocks.keyword;

    const params = {
      page,
      take,
      order,
      search: keyword,
    };

    try {
      const {
        data: { data, meta },
      } = await debounceGetBlocks(params, id);

      dispatch({
        type: SUCCESS_FETCH_BLOCKS,
        data: data,
        amountOfData: meta.itemCount,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_BLOCKS,
      });
    }
  };
};
export const fetchBlocksAll = (id: string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_FETCH_BLOCKS_ALL,
    });

    try {
      const {
        data: { data },
      } = await getBlocksAll(id);

      let newData: any = [];

      if (data.length) {
        newData = data.map((item: any) => {
          return {
            ...item,
            label: item.name,
            value: item.id,
          };
        });
      }

      dispatch({
        type: SUCCESS_FETCH_BLOCKS_ALL,
        data: newData,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_BLOCKS_ALL,
      });
    }
  };
};

export const fetchBlocksDetail = (id: string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_FETCH_BLOCKS_DETAIL,
    });

    try {
      const {
        data: { data },
      } = await getBlocksById(id);

      dispatch({
        type: SUCCESS_FETCH_BLOCKS_DETAIL,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_BLOCKS_DETAIL,
      });
    }
  };
};

export const createBlocks = (body: any, id: string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_CREATE_BLOCKS,
    });

    try {
      const {
        data: { data },
      } = await postBlocks(body, id);

      dispatch({
        type: SUCCESS_CREATE_BLOCKS,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_CREATE_BLOCKS,
        data: error,
      });
    }
  };
};

export const removeBlocks = (id: string | number) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_DELETE_BLOCKS,
    });

    try {
      const {
        data: { data },
      } = await deleteBlocksById(id);

      dispatch({
        type: SUCCESS_DELETE_BLOCKS,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_DELETE_BLOCKS,
        data: error,
      });
    }
  };
};

export const updateBlocks = (id: string, body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_UPDATE_BLOCKS,
    });

    try {
      const {
        data: { data },
      } = await patchBlocksById(id, body);

      dispatch({
        type: SUCCESS_UPDATE_BLOCKS,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_UPDATE_BLOCKS,
        data: error,
      });
    }
  };
};

export const changeKeyword = (value: string) => {
  return {
    type: CHANGE_KEYWORD,
    value,
  };
};

export const changePage = (value: number) => {
  return {
    type: CHANGE_PAGE,
    value,
  };
};

export const changeLimit = (value: number) => {
  return {
    type: CHANGE_LIMIT,
    value,
  };
};
