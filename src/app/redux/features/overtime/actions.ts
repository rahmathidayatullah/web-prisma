import debounce from "debounce-promise";

import {
  approveOvertime,
  getOvertimes,
  getOvertimesById,
  getOvertimesExport,
  patchOvertimes,
  postOvertimes,
  rejectOvertime,
} from "@/app/api/overtime";

import {
  START_FETCH_OVERTIME,
  SUCCESS_FETCH_OVERTIME,
  ERROR_FETCH_OVERTIME,
  START_APPROVE_OVERTIME,
  SUCCESS_APPROVE_OVERTIME,
  ERROR_APPROVE_OVERTIME,
  START_REJECT_OVERTIME,
  SUCCESS_REJECT_OVERTIME,
  ERROR_REJECT_OVERTIME,
  START_FETCH_OVERTIME_DETAIL,
  SUCCESS_FETCH_OVERTIME_DETAIL,
  ERROR_FETCH_OVERTIME_DETAIL,
  START_POST_OVERTIME,
  ERROR_POST_OVERTIME,
  SUCCESS_POST_OVERTIME,
  ERROR_UPDATE_OVERTIME,
  SUCCESS_UPDATE_OVERTIME,
  START_UPDATE_OVERTIME,
  START_EXPORT_OVERTIME,
  ERROR_EXPORT_OVERTIME,
  SUCCESS_EXPORT_OVERTIME,
} from "./constans";
import {
  currentDateWithFormat,
  dateEndNow,
  dateStartNow,
  futureDateOneYear,
} from "@/app/utils/constants";

const debounceGetOvertimes = debounce(getOvertimes, 1000);

export const fetchOvertimes = () => {
  return async (dispatch: any, getState: any) => {
    dispatch({
      type: START_FETCH_OVERTIME,
    });

    const page = getState().overtime.page;
    const take = getState().overtime.take;
    const order = getState().overtime.order;
    const keyword = getState().overtime.keyword;
    const startDate = getState().overtime.startDate || currentDateWithFormat;
    const endDate = getState().overtime.endDate || futureDateOneYear;

    const params = {
      page,
      take,
      order,
      search: keyword,
      endDate,
      startDate,
    };

    try {
      const {
        data: { data, meta },
      } = await debounceGetOvertimes(params);

      let newData = {
        data: [],
        meta: {
          itemCount: 0,
        },
      };

      if (data.length) {
        newData.data = data.map((item: any) => {
          return {
            ...item,
            pointlocation:
              item.clockInLatitude && item.clockOutLongitude
                ? `${item.clockInLatitude},${item.clockOutLongitude}`
                : null,
            byAction: {
              name: item.actionBy ? item.actionBy.name : null,
              status: item.status,
              editBy: item.editedBy ? item.editedBy.name : null,
            },
          };
        });
        newData.meta = meta;
      }

      dispatch({
        type: SUCCESS_FETCH_OVERTIME,
        data: newData.data,
        amountOfData: meta.itemCount,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_OVERTIME,
      });
    }
  };
};

export const fetchOvertimesExports = () => {
  return async (dispatch: any, getState: any) => {
    dispatch({
      type: START_EXPORT_OVERTIME,
    });

    const page = getState().overtime.page;
    // const take = getState().overtime.take;
    // const order = getState().overtime.order;
    const keyword = getState().overtime.keyword;
    // const startDate = getState().overtime.startDate || currentDateWithFormat;
    // const endDate = getState().overtime.endDate || futureDateOneYear;
    const startDate = getState().overtime.startDate || dateStartNow; //current month 01;
    const endDate = getState().overtime.endDate || dateEndNow; //current month 30/31;

    const params = {
      page,
      // take,
      // order,
      search: keyword,
      endDate,
      startDate,
    };

    try {
      const { data } = await getOvertimesExport(params);
      dispatch({
        type: SUCCESS_EXPORT_OVERTIME,
        data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_EXPORT_OVERTIME,
        data: error,
      });
    }
  };
};

export const fetchOvertimesById = (id: string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_FETCH_OVERTIME_DETAIL,
    });
    try {
      const {
        data: { data },
      } = await getOvertimesById(id);

      dispatch({
        type: SUCCESS_FETCH_OVERTIME_DETAIL,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_OVERTIME_DETAIL,
        data: error,
      });
    }
  };
};

export const createOvertimes = (body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_POST_OVERTIME,
    });
    try {
      const {
        data: { data },
      } = await postOvertimes(body);

      dispatch({
        type: SUCCESS_POST_OVERTIME,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_POST_OVERTIME,
        data: error,
      });
    }
  };
};

export const updateOvertimes = (id: string, body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_UPDATE_OVERTIME,
    });
    try {
      const {
        data: { data },
      } = await patchOvertimes(id, body);

      dispatch({
        type: SUCCESS_UPDATE_OVERTIME,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_UPDATE_OVERTIME,
        data: error,
      });
    }
  };
};

export const overtimesApprove = (id: any, body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_APPROVE_OVERTIME,
    });
    try {
      await approveOvertime(id, body);
      dispatch({
        type: SUCCESS_APPROVE_OVERTIME,
      });
      dispatch(fetchOvertimes());
    } catch (error) {
      dispatch({
        type: ERROR_APPROVE_OVERTIME,
        data: error,
      });
    }
  };
};

export const overtimesReject = (id: any, body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_REJECT_OVERTIME,
    });
    try {
      await rejectOvertime(id, body);
      dispatch({
        type: SUCCESS_REJECT_OVERTIME,
      });
      dispatch(fetchOvertimes());
    } catch (error) {
      dispatch({
        type: ERROR_REJECT_OVERTIME,
        data: error,
      });
    }
  };
};
