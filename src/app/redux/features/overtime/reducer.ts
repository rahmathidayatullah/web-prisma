import {
  START_FETCH_OVERTIME,
  SUCCESS_FETCH_OVERTIME,
  ERROR_FETCH_OVERTIME,
  START_POST_OVERTIME,
  SUCCESS_POST_OVERTIME,
  ERROR_POST_OVERTIME,
  RESET_POST_OVERTIME,
  START_UPDATE_OVERTIME,
  SUCCESS_UPDATE_OVERTIME,
  ERROR_UPDATE_OVERTIME,
  RESET_UPDATE_OVERTIME,
  START_FETCH_OVERTIME_DETAIL,
  SUCCESS_FETCH_OVERTIME_DETAIL,
  ERROR_FETCH_OVERTIME_DETAIL,
  START_APPROVE_OVERTIME,
  SUCCESS_APPROVE_OVERTIME,
  ERROR_APPROVE_OVERTIME,
  START_REJECT_OVERTIME,
  SUCCESS_REJECT_OVERTIME,
  ERROR_REJECT_OVERTIME,
  RESET_STATUS_REJECT_APPROVE,
  CHANGE_PAGE,
  CHANGE_LIMIT,
  CHANGE_KEYWORD,
  CHANGE_DATE_RANGE,
  START_EXPORT_OVERTIME,
  SUCCESS_EXPORT_OVERTIME,
  ERROR_EXPORT_OVERTIME,
} from "./constans";

const statusList = {
  idle: "idle",
  process: "process",
  success: "success",
  error: "error",
};

const initialState = {
  statusListOvertime: statusList.idle,
  page: 1,
  take: 10,
  order: "DESC",
  keyword: "",
  startDate: "",
  endDate: "",
  dataOvertime: [],
  statusApprove: statusList.idle,
  errorApprove: null,
  statusReject: statusList.idle,
  errorReject: null,
  amountOfData: 0,
  //
  dataUpdate: null,
  errorUpdate: null,
  statusUpdate: statusList.idle,
  //
  dataCreate: null,
  errorCreate: null,
  statusCreate: statusList.idle,
  //
  dataDetail: null,
  errorDetail: null,
  statusDetail: statusList.idle,
  //
  dataExport: null,
  errorExport: null,
  statusExport: statusList.idle,
};

export default function overtimeReducer(state = initialState, action: any) {
  switch (action.type) {
    case START_EXPORT_OVERTIME:
      return {
        ...state,
        statusExport: statusList.process,
      };
    case SUCCESS_EXPORT_OVERTIME:
      return {
        ...state,
        statusExport: statusList.success,
        dataExport: action.data,
      };
    case ERROR_EXPORT_OVERTIME:
      return {
        ...state,
        statusExport: statusList.error,
        errorExport: action.data,
      };

    case START_UPDATE_OVERTIME:
      return {
        ...state,
        statusUpdate: statusList.process,
      };
    case SUCCESS_UPDATE_OVERTIME:
      return {
        ...state,
        statusUpdate: statusList.success,
        dataUpdate: action.data,
      };
    case ERROR_UPDATE_OVERTIME:
      return {
        ...state,
        statusUpdate: statusList.error,
        errorUpdate: action.data,
      };
    case RESET_UPDATE_OVERTIME:
      return {
        ...state,
        statusUpdate: statusList.idle,
        dataUpdate: null,
        errorUpdate: null,
      };
    case START_POST_OVERTIME:
      return {
        ...state,
        statusCreate: statusList.process,
      };
    case SUCCESS_POST_OVERTIME:
      return {
        ...state,
        statusCreate: statusList.success,
        dataCreate: action.data,
      };
    case ERROR_POST_OVERTIME:
      return {
        ...state,
        statusCreate: statusList.error,
        errorCreate: action.data,
      };
    case RESET_POST_OVERTIME:
      return {
        ...state,
        statusCreate: statusList.idle,
        dataCreate: null,
        errorCreate: null,
      };
    case START_FETCH_OVERTIME_DETAIL:
      return {
        ...state,
        statusDetail: statusList.process,
      };
    case SUCCESS_FETCH_OVERTIME_DETAIL:
      return {
        ...state,
        statusDetail: statusList.success,
        dataDetail: action.data,
      };
    case ERROR_FETCH_OVERTIME_DETAIL:
      return {
        ...state,
        statusDetail: statusList.error,
        errorDetail: action.data,
      };
    case START_FETCH_OVERTIME:
      return {
        ...state,
        statusListOvertime: statusList.process,
      };
    case SUCCESS_FETCH_OVERTIME:
      return {
        ...state,
        statusListOvertime: statusList.success,
        dataOvertime: action.data,
        amountOfData: action.amountOfData,
      };
    case ERROR_FETCH_OVERTIME:
      return {
        ...state,
        statusListOvertime: statusList.error,
      };
    case START_APPROVE_OVERTIME:
      return {
        ...state,
        statusApprove: statusList.process,
      };
    case SUCCESS_APPROVE_OVERTIME:
      return {
        ...state,
        statusApprove: statusList.success,
      };
    case ERROR_APPROVE_OVERTIME:
      return {
        ...state,
        statusApprove: statusList.error,
        errorApprove: action.data,
      };
    case START_REJECT_OVERTIME:
      return {
        ...state,
        statusReject: statusList.process,
      };
    case SUCCESS_REJECT_OVERTIME:
      return {
        ...state,
        statusReject: statusList.success,
      };
    case ERROR_REJECT_OVERTIME:
      return {
        ...state,
        statusReject: statusList.error,
        errorReject: action.data,
      };
    case RESET_STATUS_REJECT_APPROVE:
      return {
        ...state,
        statusReject: statusList.idle,
        statusApprove: statusList.idle,
        errorApprove: null,
        errorReject: null,
      };
    case CHANGE_PAGE:
      return {
        ...state,
        page: action.value,
      };
    case CHANGE_LIMIT:
      return {
        ...state,
        take: action.value,
      };
    case CHANGE_KEYWORD:
      return {
        ...state,
        keyword: action.value,
        page: 1,
        take: 10,
        startDate: "",
        endDate: "",
      };
    case CHANGE_DATE_RANGE:
      return {
        ...state,
        startDate: action.value[0],
        endDate: action.value[1],
        page: 1,
        take: 10,
      };
    default:
      return state;
  }
}
