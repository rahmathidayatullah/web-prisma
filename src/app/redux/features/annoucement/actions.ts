import debounce from "debounce-promise";

import {
  deleteAnnoucementById,
  getAnnoucement,
  getAnnoucementById,
  patchAnnoucementById,
  postAnnoucement,
} from "@/app/api/annoucement";

import {
  START_FETCH_ANNOUCEMENT,
  SUCCESS_FETCH_ANNOUCEMENT,
  ERROR_FETCH_ANNOUCEMENT,
  START_CREATE_ANNOUCEMENT,
  SUCCESS_CREATE_ANNOUCEMENT,
  ERROR_CREATE_ANNOUCEMENT,
  START_UPDATE_ANNOUCEMENT,
  SUCCESS_UPDATE_ANNOUCEMENT,
  ERROR_UPDATE_ANNOUCEMENT,
  START_DELETE_ANNOUCEMENT,
  SUCCESS_DELETE_ANNOUCEMENT,
  ERROR_DELETE_ANNOUCEMENT,
  START_FETCH_ANNOUCEMENT_DETAIL,
  SUCCESS_FETCH_ANNOUCEMENT_DETAIL,
  ERROR_FETCH_ANNOUCEMENT_DETAIL,
} from "./constans";
import {
  currentDateWithFormat,
  futureDateOneYear,
} from "@/app/utils/constants";

const debounceGetAnnoucement = debounce(getAnnoucement, 1000);

export const fetchAnnoucements = () => {
  return async (dispatch: any, getState: any) => {
    dispatch({
      type: START_FETCH_ANNOUCEMENT,
    });

    const page = getState().annoucement.page;
    const take = getState().annoucement.take;
    const order = getState().annoucement.order;
    const keyword = getState().annoucement.keyword;
    const startDate = getState().annoucement.startDate || currentDateWithFormat;
    const endDate = getState().annoucement.endDate || futureDateOneYear;

    const params = {
      page,
      take,
      order,
      search: keyword,
      endDate,
      startDate,
    };

    try {
      const {
        data: { data, meta },
      } = await debounceGetAnnoucement(params);
      dispatch({
        type: SUCCESS_FETCH_ANNOUCEMENT,
        // data: newData,
        data,
        amountOfData: meta.itemCount,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_ANNOUCEMENT,
      });
    }
  };
};

export const fetchAnnoucementDetail = (id: string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_FETCH_ANNOUCEMENT_DETAIL,
    });

    try {
      const {
        data: { data },
      } = await getAnnoucementById(id);
      dispatch({
        type: SUCCESS_FETCH_ANNOUCEMENT_DETAIL,
        data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_ANNOUCEMENT_DETAIL,
      });
    }
  };
};

export const createAnnoucement = (body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_CREATE_ANNOUCEMENT,
    });

    try {
      const {
        data: { data },
      } = await postAnnoucement(body);

      dispatch({
        type: SUCCESS_CREATE_ANNOUCEMENT,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_CREATE_ANNOUCEMENT,
        data: error,
      });
    }
  };
};

export const removeAnnoucement = (id: number | string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_DELETE_ANNOUCEMENT,
    });

    try {
      const {
        data: { data },
      } = await deleteAnnoucementById(id);

      dispatch({
        type: SUCCESS_DELETE_ANNOUCEMENT,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_DELETE_ANNOUCEMENT,
        data: error,
      });
    }
  };
};

export const updateAnnoucement = (id: string, body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_UPDATE_ANNOUCEMENT,
    });

    try {
      const {
        data: { data },
      } = await patchAnnoucementById(id, body);

      dispatch({
        type: SUCCESS_UPDATE_ANNOUCEMENT,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_UPDATE_ANNOUCEMENT,
        data: error,
      });
    }
  };
};
