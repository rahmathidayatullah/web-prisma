import {
  START_FETCH_ANNOUCEMENT,
  SUCCESS_FETCH_ANNOUCEMENT,
  ERROR_FETCH_ANNOUCEMENT,
  START_CREATE_ANNOUCEMENT,
  SUCCESS_CREATE_ANNOUCEMENT,
  ERROR_CREATE_ANNOUCEMENT,
  RESET_CREATE_ANNOUCEMENT,
  START_UPDATE_ANNOUCEMENT,
  SUCCESS_UPDATE_ANNOUCEMENT,
  ERROR_UPDATE_ANNOUCEMENT,
  RESET_UPDATE_ANNOUCEMENT,
  START_DELETE_ANNOUCEMENT,
  SUCCESS_DELETE_ANNOUCEMENT,
  ERROR_DELETE_ANNOUCEMENT,
  RESET_DELETE_ANNOUCEMENT,
  START_FETCH_ANNOUCEMENT_DETAIL,
  SUCCESS_FETCH_ANNOUCEMENT_DETAIL,
  ERROR_FETCH_ANNOUCEMENT_DETAIL,
  CHANGE_PAGE,
  CHANGE_LIMIT,
  CHANGE_KEYWORD,
  CHANGE_DATE_RANGE,
  RESET_STATUS_CREATE_ANNOUCEMENT,
  RESET_STATUS_UPDATE_ANNOUCEMENT,
  RESET_STATUS_DELETE_ANNOUCEMENT,
} from "./constans";

const statusList = {
  idle: "idle",
  process: "process",
  success: "success",
  error: "error",
};

const initialState = {
  page: 1,
  take: 10,
  order: "DESC",
  keyword: "",
  startDate: "",
  endDate: "",
  amountOfData: 0,
  statusList: statusList.idle,
  dataList: [],

  statusUpdate: statusList.idle,
  dataUpdate: null,
  errorUpdate: null,

  statusCreate: statusList.idle,
  dataCreate: null,
  errorCreate: null,

  statusDetail: statusList.idle,
  dataDetail: null,

  statusDelete: statusList.idle,
  dataDelete: null,
  errorDelete: null,
};

export default function annoucementReducer(state = initialState, action: any) {
  switch (action.type) {
    case START_FETCH_ANNOUCEMENT_DETAIL:
      return {
        ...state,
        statusDetail: statusList.process,
      };
    case SUCCESS_FETCH_ANNOUCEMENT_DETAIL:
      return {
        ...state,
        statusDetail: statusList.success,
        dataDetail: action.data,
      };
    case ERROR_FETCH_ANNOUCEMENT_DETAIL:
      return {
        ...state,
        statusDetail: statusList.error,
      };

    case START_UPDATE_ANNOUCEMENT:
      return {
        ...state,
        statusUpdate: statusList.process,
      };
    case SUCCESS_UPDATE_ANNOUCEMENT:
      return {
        ...state,
        statusUpdate: statusList.success,
        dataUpdate: action.data,
      };
    case ERROR_UPDATE_ANNOUCEMENT:
      return {
        ...state,
        statusUpdate: statusList.error,
        errorUpdate: action.data,
      };
    case RESET_UPDATE_ANNOUCEMENT:
      return {
        ...state,
        statusUpdate: statusList.idle,
        dataUpdate: null,
        errorUpdate: null,
      };
    case RESET_STATUS_UPDATE_ANNOUCEMENT:
      return {
        ...state,
        statusUpdate: statusList.idle,
      };

    case START_DELETE_ANNOUCEMENT:
      return {
        ...state,
        statusDelete: statusList.process,
      };
    case SUCCESS_DELETE_ANNOUCEMENT:
      return {
        ...state,
        statusDelete: statusList.success,
        dataDelete: action.data,
      };
    case ERROR_DELETE_ANNOUCEMENT:
      return {
        ...state,
        statusDelete: statusList.error,
        errorDelete: action.data,
      };
    case RESET_DELETE_ANNOUCEMENT:
      return {
        ...state,
        statusDelete: statusList.idle,
        dataDelete: null,
        errorDelete: null,
      };
    case RESET_STATUS_DELETE_ANNOUCEMENT:
      return {
        ...state,
        statusDelete: statusList.idle,
      };

    case START_CREATE_ANNOUCEMENT:
      return {
        ...state,
        statusCreate: statusList.process,
      };
    case SUCCESS_CREATE_ANNOUCEMENT:
      return {
        ...state,
        statusCreate: statusList.success,
        dataCreate: action.data,
      };
    case ERROR_CREATE_ANNOUCEMENT:
      return {
        ...state,
        statusCreate: statusList.error,
        errorCreate: action.data,
      };
    case RESET_CREATE_ANNOUCEMENT:
      return {
        ...state,
        statusCreate: statusList.idle,
        dataCreate: null,
        errorCreate: null,
      };
    case RESET_STATUS_CREATE_ANNOUCEMENT:
      return {
        ...state,
        statusCreate: statusList.idle,
      };

    case START_FETCH_ANNOUCEMENT:
      return {
        ...state,
        statusList: statusList.process,
      };
    case SUCCESS_FETCH_ANNOUCEMENT:
      return {
        ...state,
        statusList: statusList.success,
        dataList: action.data,
        amountOfData: action.amountOfData,
      };
    case ERROR_FETCH_ANNOUCEMENT:
      return {
        ...state,
        statusList: statusList.error,
      };

    case CHANGE_PAGE:
      return {
        ...state,
        page: action.value,
      };
    case CHANGE_LIMIT:
      return {
        ...state,
        take: action.value,
      };
    case CHANGE_KEYWORD:
      return {
        ...state,
        keyword: action.value,
        page: 1,
        take: 10,
      };
    case CHANGE_DATE_RANGE:
      return {
        ...state,
        startDate: action.value[0],
        endDate: action.value[1],
        page: 1,
        take: 10,
      };

    default:
      return state;
  }
}
