import {
  START_FETCH_USER,
  SUCCESS_FETCH_USER,
  ERROR_FETCH_USER,
  START_FETCH_USER_DETAIL,
  SUCCESS_FETCH_USER_DETAIL,
  ERROR_FETCH_USER_DETAIL,
  START_CREATE_USER,
  SUCCESS_CREATE_USER,
  ERROR_CREATE_USER,
  START_UPDATE_USER,
  SUCCESS_UPDATE_USER,
  ERROR_UPDATE_USER,
  START_DELETE_USER,
  SUCCESS_DELETE_USER,
  ERROR_DELETE_USER,
  CHANGE_PAGE,
  CHANGE_LIMIT,
  CHANGE_KEYWORD,
  RESET_CREATE_USER,
  RESET_UPDATE_USER,
  RESET_DELETE_USER,
  CHANGE_DATE_RANGE,
  RESET_STATUS_UPDATE_USER,
  RESET_STATUS_DELETE_USER,
  RESET_STATUS_CREATE_USER,
} from "./constans";

const statusList = {
  idle: "idle",
  process: "process",
  success: "success",
  error: "error",
};

const initialState = {
  page: 1,
  take: 10,
  order: "DESC",
  keyword: "",
  startDate: "",
  endDate: "",

  dataUser: [],
  // dataError create
  dataError: null,
  dataUserDetail: null,
  dataUserCreate: null,
  amountOfData: 0,

  dataDelete: null,
  errorDelete: null,

  dataUpdate: null,
  errorUpdate: null,

  statusListUser: statusList.idle,
  statusDetailUser: statusList.idle,
  statusDeleteUser: statusList.idle,
  statusUpdateUser: statusList.idle,
  statusCreateUser: statusList.idle,
};

export default function userReducer(state = initialState, action: any) {
  switch (action.type) {
    case START_FETCH_USER:
      return {
        ...state,
        statusListUser: statusList.process,
      };
    case SUCCESS_FETCH_USER:
      return {
        ...state,
        statusListUser: statusList.success,
        dataUser: action.data,
        amountOfData: action.amountOfData,
      };
    case ERROR_FETCH_USER:
      return {
        ...state,
        statusListUser: statusList.error,
      };

    case START_FETCH_USER_DETAIL:
      return {
        ...state,
        statusDetailUser: statusList.process,
      };
    case SUCCESS_FETCH_USER_DETAIL:
      return {
        ...state,
        statusDetailUser: statusList.success,
        dataUserDetail: action.data,
      };
    case ERROR_FETCH_USER_DETAIL:
      return {
        ...state,
        statusDetailUser: statusList.error,
      };

    case START_DELETE_USER:
      return {
        ...state,
        statusDeleteUser: statusList.process,
      };
    case SUCCESS_DELETE_USER:
      return {
        ...state,
        statusDeleteUser: statusList.success,
        dataDelete: action.data,
      };
    case ERROR_DELETE_USER:
      return {
        ...state,
        statusDeleteUser: statusList.error,
        errorDelete: action.data,
      };
    case RESET_DELETE_USER:
      return {
        ...state,
        statusDeleteUser: statusList.idle,
        dataDelete: null,
        errorDelete: null,
      };
    case RESET_STATUS_DELETE_USER:
      return {
        ...state,
        statusDeleteUser: statusList.idle,
      };

    case START_UPDATE_USER:
      return {
        ...state,
        statusUpdateUser: statusList.process,
      };
    case SUCCESS_UPDATE_USER:
      return {
        ...state,
        statusUpdateUser: statusList.success,
        dataUpdate: action.data,
      };
    case ERROR_UPDATE_USER:
      return {
        ...state,
        statusUpdateUser: statusList.error,
        errorUpdate: action.data,
      };
    case RESET_UPDATE_USER:
      return {
        ...state,
        statusUpdateUser: statusList.idle,
        dataUpdate: null,
        errorUpdate: null,
      };
    case RESET_STATUS_UPDATE_USER:
      return {
        ...state,
        statusUpdateUser: statusList.idle,
      };

    case START_CREATE_USER:
      return {
        ...state,
        statusCreateUser: statusList.process,
      };
    case SUCCESS_CREATE_USER:
      return {
        ...state,
        statusCreateUser: statusList.success,
        dataUserCreate: action.data,
      };
    case ERROR_CREATE_USER:
      return {
        ...state,
        statusCreateUser: statusList.error,
        dataError: action.data,
      };
    case RESET_CREATE_USER:
      return {
        ...state,
        statusCreateUser: statusList.idle,
        dataUserCreate: null,
        dataError: null,
      };

    case RESET_STATUS_CREATE_USER:
      return {
        ...state,
        statusCreateUser: statusList.idle,
      };

    case CHANGE_PAGE:
      return {
        ...state,
        page: action.value,
      };
    case CHANGE_LIMIT:
      return {
        ...state,
        take: action.value,
      };
    case CHANGE_KEYWORD:
      return {
        ...state,
        keyword: action.value,
        page: 1,
        take: 10,
        startDate: "",
        endDate: "",
      };
    case CHANGE_DATE_RANGE:
      return {
        ...state,
        startDate: action.value[0],
        endDate: action.value[1],
        page: 1,
        take: 10,
      };
    default:
      return state;
  }
}
