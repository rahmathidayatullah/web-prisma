import debounce from "debounce-promise";

import {
  deleteUsersById,
  getUsers,
  getUsersById,
  patchUsersById,
  postUsers,
} from "@/app/api/users";

import {
  START_FETCH_USER,
  SUCCESS_FETCH_USER,
  ERROR_FETCH_USER,
  START_FETCH_USER_DETAIL,
  SUCCESS_FETCH_USER_DETAIL,
  ERROR_FETCH_USER_DETAIL,
  START_CREATE_USER,
  SUCCESS_CREATE_USER,
  ERROR_CREATE_USER,
  START_UPDATE_USER,
  SUCCESS_UPDATE_USER,
  ERROR_UPDATE_USER,
  START_DELETE_USER,
  SUCCESS_DELETE_USER,
  ERROR_DELETE_USER,
  CHANGE_PAGE,
  CHANGE_LIMIT,
  CHANGE_KEYWORD,
} from "./constans";
import {
  currentDateWithFormat,
  futureDateOneYear,
  removeEmptyAttributes,
} from "@/app/utils/constants";

const debounceGetUsers = debounce(getUsers, 1000);

export const fetchUsers = () => {
  return async (dispatch: any, getState: any) => {
    dispatch({
      type: START_FETCH_USER,
    });

    const page = getState().user.page;
    const take = getState().user.take;
    const order = getState().user.order;
    const keyword = getState().user.keyword;
    const startDate = getState().user.startDate || currentDateWithFormat;
    const endDate = getState().user.endDate || futureDateOneYear;
    const division = getState().divisi.selectDivisi || "";

    const params = {
      page,
      take,
      order,
      search: keyword,
      endDate,
      startDate,
      division,
    };

    const newParams = removeEmptyAttributes(params);

    try {
      const {
        data: { data, meta },
      } = await debounceGetUsers(newParams);

      let newData = {
        data: [],
        meta: {
          itemCount: 0,
        },
      };

      if (data.length) {
        newData.data = data.map((item: any) => {
          return {
            ...item,
            label: item.name,
            value: item.id,
          };
        });
        newData.meta = meta;
      }

      dispatch({
        type: SUCCESS_FETCH_USER,
        data: newData.data,
        amountOfData: meta.itemCount,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_USER,
      });
    }
  };
};

export const fetchUsersDetail = (id: string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_FETCH_USER_DETAIL,
    });

    try {
      const {
        data: { data },
      } = await getUsersById(id);

      dispatch({
        type: SUCCESS_FETCH_USER_DETAIL,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_USER_DETAIL,
      });
    }
  };
};

export const createUser = (body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_CREATE_USER,
    });

    try {
      const {
        data: { data },
      } = await postUsers(body);

      dispatch({
        type: SUCCESS_CREATE_USER,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_CREATE_USER,
        data: error,
      });
    }
  };
};

export const removeUser = (id: number | string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_DELETE_USER,
    });

    try {
      const {
        data: { data },
      } = await deleteUsersById(id);

      dispatch({
        type: SUCCESS_DELETE_USER,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_DELETE_USER,
        data: error,
      });
    }
  };
};

export const updateUser = (id: string, body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_UPDATE_USER,
    });

    try {
      const {
        data: { data },
      } = await patchUsersById(id, body);

      dispatch({
        type: SUCCESS_UPDATE_USER,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_UPDATE_USER,
        data: error,
      });
    }
  };
};

export const changeKeyword = (value: string) => {
  return {
    type: CHANGE_KEYWORD,
    value,
  };
};

export const changePage = (value: number) => {
  return {
    type: CHANGE_PAGE,
    value,
  };
};

export const changeLimit = (value: number) => {
  return {
    type: CHANGE_LIMIT,
    value,
  };
};
