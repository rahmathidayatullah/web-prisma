import debounce from "debounce-promise";

import {
  deleteRolesById,
  getRoles,
  getRolesAll,
  getRolesById,
  patchRolesById,
  postRoles,
} from "@/app/api/roles";

import {
  START_FETCH_ROLES,
  SUCCESS_FETCH_ROLES,
  ERROR_FETCH_ROLES,
  START_FETCH_ROLES_DETAIL,
  SUCCESS_FETCH_ROLES_DETAIL,
  ERROR_FETCH_ROLES_DETAIL,
  START_CREATE_ROLES,
  SUCCESS_CREATE_ROLES,
  ERROR_CREATE_ROLES,
  START_UPDATE_ROLES,
  SUCCESS_UPDATE_ROLES,
  ERROR_UPDATE_ROLES,
  START_DELETE_ROLES,
  SUCCESS_DELETE_ROLES,
  ERROR_DELETE_ROLES,
  CHANGE_PAGE,
  CHANGE_LIMIT,
  CHANGE_KEYWORD,
  START_FETCH_ROLES_ALL,
  ERROR_FETCH_ROLES_ALL,
  SUCCESS_FETCH_ROLES_ALL,
} from "./constans";

const debounceGetRoles = debounce(getRoles, 1000);

export const fetchRoles = () => {
  return async (dispatch: any, getState: any) => {
    dispatch({
      type: START_FETCH_ROLES,
    });

    const page = getState().roles.page;
    const take = getState().roles.take;
    const order = getState().roles.order;
    const keyword = getState().roles.keyword;

    const params = {
      page,
      take,
      order,
      search: keyword,
    };

    try {
      const {
        data: { data, meta },
      } = await debounceGetRoles(params);

      dispatch({
        type: SUCCESS_FETCH_ROLES,
        data: data,
        amountOfData: meta.itemCount,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_ROLES,
      });
    }
  };
};
export const fetchRolesAll = () => {
  return async (dispatch: any) => {
    dispatch({
      type: START_FETCH_ROLES_ALL,
    });

    try {
      const {
        data: { data },
      } = await getRolesAll();

      let newData: any = [];

      if (data.length) {
        newData = data.map((item: any) => {
          return {
            ...item,
            label: item.name,
            value: item.id,
          };
        });
      }

      dispatch({
        type: SUCCESS_FETCH_ROLES_ALL,
        data: newData,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_ROLES_ALL,
      });
    }
  };
};

export const fetchRolesDetail = (id: string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_FETCH_ROLES_DETAIL,
    });

    try {
      const {
        data: { data },
      } = await getRolesById(id);

      dispatch({
        type: SUCCESS_FETCH_ROLES_DETAIL,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_ROLES_DETAIL,
      });
    }
  };
};

export const createRoles = (body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_CREATE_ROLES,
    });

    try {
      const {
        data: { data },
      } = await postRoles(body);

      dispatch({
        type: SUCCESS_CREATE_ROLES,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_CREATE_ROLES,
        data: error,
      });
    }
  };
};

export const removeRoles = (id: string | number) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_DELETE_ROLES,
    });

    try {
      const {
        data: { data },
      } = await deleteRolesById(id);

      dispatch({
        type: SUCCESS_DELETE_ROLES,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_DELETE_ROLES,
        data: error,
      });
    }
  };
};

export const updateRoles = (id: string, body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_UPDATE_ROLES,
    });

    try {
      const {
        data: { data },
      } = await patchRolesById(id, body);

      dispatch({
        type: SUCCESS_UPDATE_ROLES,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_UPDATE_ROLES,
        data: error,
      });
    }
  };
};

export const changeKeyword = (value: string) => {
  return {
    type: CHANGE_KEYWORD,
    value,
  };
};

export const changePage = (value: number) => {
  return {
    type: CHANGE_PAGE,
    value,
  };
};

export const changeLimit = (value: number) => {
  return {
    type: CHANGE_LIMIT,
    value,
  };
};
