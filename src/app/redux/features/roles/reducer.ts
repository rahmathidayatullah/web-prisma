import {
  START_FETCH_ROLES,
  SUCCESS_FETCH_ROLES,
  ERROR_FETCH_ROLES,
  START_FETCH_ROLES_DETAIL,
  SUCCESS_FETCH_ROLES_DETAIL,
  ERROR_FETCH_ROLES_DETAIL,
  START_CREATE_ROLES,
  SUCCESS_CREATE_ROLES,
  ERROR_CREATE_ROLES,
  START_UPDATE_ROLES,
  SUCCESS_UPDATE_ROLES,
  ERROR_UPDATE_ROLES,
  START_DELETE_ROLES,
  SUCCESS_DELETE_ROLES,
  ERROR_DELETE_ROLES,
  CHANGE_PAGE,
  CHANGE_LIMIT,
  CHANGE_KEYWORD,
  RESET_CREATE_ROLES,
  RESET_UPDATE_ROLES,
  RESET_DELETE_ROLES,
  ERROR_FETCH_ROLES_ALL,
  SUCCESS_FETCH_ROLES_ALL,
  START_FETCH_ROLES_ALL,
  RESET_STATUS_DELETE_ROLES,
  RESET_STATUS_UPDATE_ROLES,
  RESET_STATUS_CREATE_ROLES,
} from "./constans";

const statusList = {
  idle: "idle",
  process: "process",
  success: "success",
  error: "error",
};

const initialState = {
  page: 1,
  take: 10,
  order: "DESC",
  keyword: "",
  dataRoles: [],
  dataRolesDetail: null,
  dataRolesCreate: null,
  amountOfData: 0,

  errorDelete: null,
  errorUpdate: null,
  errorCraete: null,

  dataDelete: null,
  dataUpdate: null,

  // role all
  statusListRoleAll: statusList.idle,
  dataListRoleAll: [],

  statusListRoles: statusList.idle,
  statusDetailRoles: statusList.idle,
  statusDeleteRoles: statusList.idle,
  statusUpdateRoles: statusList.idle,
  statusCreateRoles: statusList.idle,
};

export default function rolesReducer(state = initialState, action: any) {
  switch (action.type) {
    case START_FETCH_ROLES_ALL:
      return {
        ...state,
        statusListRoleAll: statusList.process,
      };
    case SUCCESS_FETCH_ROLES_ALL:
      return {
        ...state,
        statusListRoleAll: statusList.success,
        dataListRoleAll: action.data,
      };
    case ERROR_FETCH_ROLES_ALL:
      return {
        ...state,
        statusListRoleAll: statusList.error,
      };

    case START_FETCH_ROLES:
      return {
        ...state,
        statusListRoles: statusList.process,
      };
    case SUCCESS_FETCH_ROLES:
      return {
        ...state,
        statusListRoles: statusList.success,
        dataRoles: action.data,
        amountOfData: action.amountOfData,
      };
    case ERROR_FETCH_ROLES:
      return {
        ...state,
        statusListRoles: statusList.error,
      };

    case START_FETCH_ROLES_DETAIL:
      return {
        ...state,
        statusDetailRoles: statusList.process,
      };
    case SUCCESS_FETCH_ROLES_DETAIL:
      return {
        ...state,
        statusDetailRoles: statusList.success,
        dataRolesDetail: action.data,
      };
    case ERROR_FETCH_ROLES_DETAIL:
      return {
        ...state,
        statusDetailRoles: statusList.error,
      };

    case START_DELETE_ROLES:
      return {
        ...state,
        statusDeleteRoles: statusList.process,
      };
    case SUCCESS_DELETE_ROLES:
      return {
        ...state,
        statusDeleteRoles: statusList.success,
        dataDelete: action.data,
      };
    case ERROR_DELETE_ROLES:
      return {
        ...state,
        statusDeleteRoles: statusList.error,
        errorDelete: action.data,
      };
    case RESET_DELETE_ROLES:
      return {
        ...state,
        statusDeleteRoles: statusList.idle,
        dataDelete: null,
        errorDelete: null,
      };
    case RESET_STATUS_DELETE_ROLES:
      return {
        ...state,
        statusDeleteRoles: statusList.idle,
      };

    case START_UPDATE_ROLES:
      return {
        ...state,
        statusUpdateRoles: statusList.process,
      };
    case SUCCESS_UPDATE_ROLES:
      return {
        ...state,
        statusUpdateRoles: statusList.success,
        dataUpdate: action.data,
      };
    case ERROR_UPDATE_ROLES:
      return {
        ...state,
        statusUpdateRoles: statusList.error,
        errorUpdate: action.data,
      };
    case RESET_UPDATE_ROLES:
      return {
        ...state,
        statusUpdateRoles: statusList.idle,
        dataUpdate: null,
        errorUpdate: null,
      };
    case RESET_STATUS_UPDATE_ROLES:
      return {
        ...state,
        statusUpdateRoles: statusList.idle,
      };

    case START_CREATE_ROLES:
      return {
        ...state,
        statusCreateRoles: statusList.process,
      };
    case SUCCESS_CREATE_ROLES:
      return {
        ...state,
        statusCreateRoles: statusList.success,
        dataRolesCreate: action.data,
      };
    case ERROR_CREATE_ROLES:
      return {
        ...state,
        statusCreateRoles: statusList.error,
        errorCreate: action.data,
      };
    case RESET_CREATE_ROLES:
      return {
        ...state,
        statusCreateRoles: statusList.idle,
        dataRolesCreate: null,
        errorCreate: null,
      };
    case RESET_STATUS_CREATE_ROLES:
      return {
        ...state,
        statusCreateRoles: statusList.idle,
      };

    case CHANGE_PAGE:
      return {
        ...state,
        page: action.value,
      };
    case CHANGE_LIMIT:
      return {
        ...state,
        take: action.value,
      };
    case CHANGE_KEYWORD:
      return {
        ...state,
        keyword: action.value,
        page: 1,
        take: 10,
      };
    default:
      return state;
  }
}
