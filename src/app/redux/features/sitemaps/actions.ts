import debounce from "debounce-promise";

import {
  // project
  START_FETCH_PROJECT,
  SUCCESS_FETCH_PROJECT,
  ERROR_FETCH_PROJECT,
  START_FETCH_PROJECT_DETAIL,
  SUCCESS_FETCH_PROJECT_DETAIL,
  ERROR_FETCH_PROJECT_DETAIL,
  START_CREATE_PROJECT,
  SUCCESS_CREATE_PROJECT,
  ERROR_CREATE_PROJECT,
  RESET_CREATE_PROJECT,
  RESET_STATUS_CREATE_PROJECT,
  START_UPDATE_PROJECT,
  SUCCESS_UPDATE_PROJECT,
  ERROR_UPDATE_PROJECT,
  RESET_UPDATE_PROJECT,
  RESET_STATUS_UPDATE_PROJECT,
  START_DELETE_PROJECT,
  SUCCESS_DELETE_PROJECT,
  ERROR_DELETE_PROJECT,
  RESET_DELETE_PROJECT,
  RESET_STATUS_DELETE_PROJECT,

  // unit
  START_FETCH_UNIT,
  SUCCESS_FETCH_UNIT,
  ERROR_FETCH_UNIT,
  START_FETCH_UNIT_DETAIL,
  SUCCESS_FETCH_UNIT_DETAIL,
  ERROR_FETCH_UNIT_DETAIL,
  START_CREATE_UNIT,
  SUCCESS_CREATE_UNIT,
  ERROR_CREATE_UNIT,
  RESET_CREATE_UNIT,
  RESET_STATUS_CREATE_UNIT,
  START_UPDATE_UNIT,
  SUCCESS_UPDATE_UNIT,
  ERROR_UPDATE_UNIT,
  RESET_UPDATE_UNIT,
  RESET_STATUS_UPDATE_UNIT,
  START_DELETE_UNIT,
  SUCCESS_DELETE_UNIT,
  ERROR_DELETE_UNIT,
  RESET_DELETE_UNIT,
  RESET_STATUS_DELETE_UNIT,
  SET_DIALOG_EDIT_UNIT,
} from "./constants";
import {
  getProject,
  deleteProjectById,
  deleteUnitById,
  getProjectById,
  getUnit,
  getUnitById,
  patchProjectById,
  postProject,
  postUnit,
} from "@/app/api/sitemapsss";
import { removeEmptyAttributes } from "@/app/utils/constants";

const debounceGetProjects = debounce(getProject, 1000);
const debounceGetUnits = debounce(getUnit, 1000);

export const fetchProjects = () => {
  return async (dispatch: any, getState: any) => {
    dispatch({
      type: START_FETCH_PROJECT,
    });

    const page = getState().sitemap.page;
    const take = getState().sitemap.take;
    const order = getState().sitemap.order;
    const keyword = getState().sitemap.keyword;

    const params = {
      page,
      take,
      order,
      search: keyword,
    };

    const newParams = removeEmptyAttributes(params);

    try {
      const {
        data: { data, meta },
      } = await debounceGetProjects(newParams);

      let newData = {
        data: [],
        meta: {
          itemCount: 0,
        },
      };

      if (data.length) {
        newData.data = data;
        newData.meta = meta;
      }

      dispatch({
        type: SUCCESS_FETCH_PROJECT,
        data: newData.data,
        amountOfData: newData.meta.itemCount,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_PROJECT,
        data: error,
      });
    }
  };
};

export const fetchProjectDetail = (id: string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_FETCH_PROJECT_DETAIL,
    });

    try {
      const {
        data: { data },
      } = await getProjectById(id);

      dispatch({
        type: SUCCESS_FETCH_PROJECT_DETAIL,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_PROJECT_DETAIL,
        data: error,
      });
    }
  };
};

export const createProject = (body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_CREATE_PROJECT,
    });

    try {
      const formData: any = new FormData();
      formData.append("project_name", body.project_name);
      formData.append("project_image", body.project_image);
      formData.append("company", body.company);
      formData.append("address", body.address);
      // formData.append("strategic_price", body.strategic_price);
      // formData.append("base_price", body.base_price);
      formData.append("land_area", body.land_area);
      formData.append("building_area", body.building_area);
      formData.append("siteplan_image", body.siteplan_image);
      if (body?.detail_image?.length) {
        body.detail_image.forEach((element: any) => {
          formData.append("detail_image", element);
        });
      } else {
        formData.append("detail_image", null);
      }

      // for (var pair of formData.entries()) {
      //   console.log("create pair", pair);
      //   // console.log(pair[0] + ", " + pair[1]);
      // }
      const {
        data: { data },
      } = await postProject(formData);

      dispatch({
        type: SUCCESS_CREATE_PROJECT,
        data: data,
      });
    } catch (error) {
      console.log("error", error);
      dispatch({
        type: ERROR_CREATE_PROJECT,
        data: error,
      });
    }
  };
};

export const removeProject = (id: number | string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_DELETE_PROJECT,
    });

    try {
      const {
        data: { data },
      } = await deleteProjectById(id);

      dispatch({
        type: SUCCESS_DELETE_PROJECT,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_DELETE_PROJECT,
        data: error,
      });
    }
  };
};

export const updateProject = (id: string, body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_UPDATE_PROJECT,
    });

    try {
      const formData: any = new FormData();
      formData.append("project_name", body.project_name);
      formData.append("project_image", body.project_image);
      formData.append("company", body.company);
      formData.append("address", body.address);
      // formData.append("strategic_price", body.strategic_price);
      // formData.append("base_price", body.base_price);
      formData.append("land_area", body.land_area);
      formData.append("building_area", body.building_area);

      formData.append("siteplan_image", body.siteplan_image);

      if (body?.detail_image?.length) {
        body.detail_image.forEach((element: any) => {
          formData.append("detail_image", element);
        });
      } else {
        formData.append("detail_image", null);
      }

      // Create a new File object
      // const file = new File([""], "filename.txt", { type: "text/plain" });

      // if (body.siteplan_image === "no_update") {
      //   formData.append("siteplan_image", file, "no_update.txt");
      // } else {
      //   formData.append("siteplan_image", body.siteplan_image);
      // }
      // if (body.detail_image != "no_update") {
      //   body.detail_image.forEach((element: any) => {
      //     formData.append("detail_image", element);
      //   });
      // } else {
      //   formData.append("detail_image", file, "no_update.txt");
      // }
      // for (var pair of formData.entries()) {
      //   console.log("update pair", pair);
      //   // console.log(pair[0] + ", " + pair[1]);
      // }
      const {
        data: { data },
      } = await patchProjectById(id, formData);

      dispatch({
        type: SUCCESS_UPDATE_PROJECT,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_UPDATE_PROJECT,
        data: error,
      });
    }
  };
};

export const fetchUnitsByProject = (id: string) => {
  return async (dispatch: any, getState: any) => {
    dispatch({
      type: START_FETCH_UNIT,
    });

    // const page = getState().sitemap.pageUnit;
    // const take = getState().sitemap.takeUnit;
    // const order = getState().sitemap.orderUnit;
    // const keyword = getState().sitemap.keywordUnit;
    // const order = getState().sitemap.order;
    const phase = getState().sitemap.tahap;
    const block = getState().sitemap.blok;
    const status = getState().sitemap.unit;

    const params = {
      phase,
      block,
      status,
    };

    const newParams = removeEmptyAttributes(params);

    try {
      const {
        // data: { data, meta },
        data: { data },
      } = await debounceGetUnits(newParams, id);
      await dispatch(fetchProjectDetail(id));

      let newData = {
        data: [],
        meta: {
          itemCount: 0,
        },
      };

      if (data.length) {
        newData.data = data;
        // newData.meta = meta;
      }

      dispatch({
        type: SUCCESS_FETCH_UNIT,
        data: newData.data,
        // amountOfData: newData.meta.itemCount,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_UNIT,
        data: error,
      });
    }
  };
};

export const fetchUnitDetail = (id: string) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_FETCH_UNIT_DETAIL,
    });

    try {
      const {
        data: { data },
      } = await getUnitById(id);

      dispatch({
        type: SUCCESS_FETCH_UNIT_DETAIL,
        data: data,
      });
      dispatch({
        type: SET_DIALOG_EDIT_UNIT,
        value: true,
      });
    } catch (error) {
      dispatch({
        type: ERROR_FETCH_UNIT_DETAIL,
        data: error,
      });
    }
  };
};

export const createUnit = (body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_CREATE_UNIT,
    });

    try {
      const {
        data: { data },
      } = await postUnit(body);

      dispatch({
        type: SUCCESS_CREATE_UNIT,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_CREATE_UNIT,
        data: error,
      });
    }
  };
};

export const removeUnit = (
  id: number | string
  // body: {
  //   password: string;
  // }
) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_DELETE_UNIT,
    });

    try {
      const {
        data: { data },
        // } = await deleteUnitById(id, body);
      } = await deleteUnitById(id);

      dispatch({
        type: SUCCESS_DELETE_UNIT,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_DELETE_UNIT,
        data: error,
      });
    }
  };
};

export const updateUnit = (body: any) => {
  return async (dispatch: any) => {
    dispatch({
      type: START_UPDATE_UNIT,
    });

    try {
      const {
        data: { data },
      } = await postUnit(body);

      dispatch({
        type: SUCCESS_UPDATE_UNIT,
        data: data,
      });
    } catch (error) {
      dispatch({
        type: ERROR_UPDATE_UNIT,
        data: error,
      });
    }
  };
};
