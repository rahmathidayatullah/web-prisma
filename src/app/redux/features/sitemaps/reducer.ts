import {
  // project
  START_FETCH_PROJECT,
  SUCCESS_FETCH_PROJECT,
  ERROR_FETCH_PROJECT,
  START_FETCH_PROJECT_DETAIL,
  SUCCESS_FETCH_PROJECT_DETAIL,
  ERROR_FETCH_PROJECT_DETAIL,
  START_CREATE_PROJECT,
  SUCCESS_CREATE_PROJECT,
  ERROR_CREATE_PROJECT,
  RESET_CREATE_PROJECT,
  RESET_STATUS_CREATE_PROJECT,
  START_UPDATE_PROJECT,
  SUCCESS_UPDATE_PROJECT,
  ERROR_UPDATE_PROJECT,
  RESET_UPDATE_PROJECT,
  RESET_STATUS_UPDATE_PROJECT,
  START_DELETE_PROJECT,
  SUCCESS_DELETE_PROJECT,
  ERROR_DELETE_PROJECT,
  RESET_DELETE_PROJECT,
  RESET_STATUS_DELETE_PROJECT,

  // unit
  START_FETCH_UNIT,
  SUCCESS_FETCH_UNIT,
  ERROR_FETCH_UNIT,
  START_FETCH_UNIT_DETAIL,
  SUCCESS_FETCH_UNIT_DETAIL,
  ERROR_FETCH_UNIT_DETAIL,
  START_CREATE_UNIT,
  SUCCESS_CREATE_UNIT,
  ERROR_CREATE_UNIT,
  RESET_CREATE_UNIT,
  RESET_STATUS_CREATE_UNIT,
  START_UPDATE_UNIT,
  SUCCESS_UPDATE_UNIT,
  ERROR_UPDATE_UNIT,
  RESET_UPDATE_UNIT,
  RESET_STATUS_UPDATE_UNIT,
  START_DELETE_UNIT,
  SUCCESS_DELETE_UNIT,
  ERROR_DELETE_UNIT,
  RESET_DELETE_UNIT,
  RESET_STATUS_DELETE_UNIT,
  SET_DIALOG_EDIT_UNIT,
  SET_DIALOG_CREATE_UNIT,
  CHANGE_LIMIT,
  CHANGE_KEYWORD,
  CHANGE_PAGE,
  CHANGE_PHASE,
  CHANGE_BLOCK,
  CHANGE_UNIT,
  RESET_STATUS_FETCH_PROJECT_DETAIL,
  CHANGE_ORDER,
} from "./constants";

const statusList = {
  idle: "idle",
  process: "process",
  success: "success",
  error: "error",
};

const initialState = {
  // project
  // filter project
  page: 1,
  take: 10,
  // order: "DESC",
  order: "DESC",
  keyword: "",

  amountOfData: 0,
  dataList: [],
  dataErrorList: null,
  statusList: statusList.idle,

  statusDetail: statusList.idle,
  dataDetail: null,
  dataErrorDetail: null,

  statusUpdate: statusList.idle,
  dataUpdate: null,
  dataErrorUpdate: null,

  statusCreate: statusList.idle,
  dataCreate: null,
  dataErrorCreate: null,

  statusDelete: statusList.idle,
  dataDelete: null,
  dataErrorDelete: null,

  // unit
  // filter unit
  pageUnit: 1,
  takeUnit: 10,
  orderUnit: "DESC",
  tahap: null,
  blok: null,
  unit: null,
  selectPath: "",

  dialogEditUnit: false,
  dialogCreateUnit: false,

  amountOfDataUnit: 0,
  dataListUnit: [],
  dataErrorListUnit: null,
  statusListUnit: statusList.idle,

  statusDetailUnit: statusList.idle,
  dataDetailUnit: null,
  dataErrorDetailUnit: null,

  statusUpdateUnit: statusList.idle,
  dataUpdateUnit: null,
  dataErrorUpdateUnit: null,

  statusCreateUnit: statusList.idle,
  dataCreateUnit: null,
  dataErrorCreateUnit: null,

  statusDeleteUnit: statusList.idle,
  dataDeleteUnit: null,
  dataErrorDeleteUnit: null,
};

export default function sitemapReducer(state = initialState, action: any) {
  switch (action.type) {
    // project
    case START_FETCH_PROJECT:
      return {
        ...state,
        statusList: statusList.process,
      };
    case SUCCESS_FETCH_PROJECT:
      return {
        ...state,
        statusList: statusList.success,
        dataList: action.data,
        amountOfData: action.amountOfData,
      };
    case ERROR_FETCH_PROJECT:
      return {
        ...state,
        statusList: statusList.error,
        dataErrorList: action.data,
      };

    case START_FETCH_PROJECT_DETAIL:
      return {
        ...state,
        statusDetail: statusList.process,
      };
    case SUCCESS_FETCH_PROJECT_DETAIL:
      return {
        ...state,
        statusDetail: statusList.success,
        dataDetail: action.data,
      };
    case ERROR_FETCH_PROJECT_DETAIL:
      return {
        ...state,
        statusDetail: statusList.error,
        dataErrorDetail: action.data,
      };

    case RESET_STATUS_FETCH_PROJECT_DETAIL:
      return {
        ...state,
        statusDetail: statusList.idle,
      };

    case START_CREATE_PROJECT:
      return {
        ...state,
        statusCreate: statusList.process,
      };
    case SUCCESS_CREATE_PROJECT:
      return {
        ...state,
        statusCreate: statusList.success,
        dataCreate: action.data,
      };
    case ERROR_CREATE_PROJECT:
      return {
        ...state,
        statusCreate: statusList.error,
        dataErrorCreate: action.data,
      };
    case RESET_CREATE_PROJECT:
      return {
        ...state,
        statusCreate: statusList.idle,
        dataCreate: null,
        dataErrorCreate: null,
      };

    case RESET_STATUS_CREATE_PROJECT:
      return {
        ...state,
        statusCreate: statusList.idle,
      };

    case START_UPDATE_PROJECT:
      return {
        ...state,
        statusUpdate: statusList.process,
      };
    case SUCCESS_UPDATE_PROJECT:
      return {
        ...state,
        statusUpdate: statusList.success,
        dataUpdate: action.data,
      };
    case ERROR_UPDATE_PROJECT:
      return {
        ...state,
        statusUpdate: statusList.error,
        dataErrorUpdate: action.data,
      };
    case RESET_STATUS_UPDATE_PROJECT:
      return {
        ...state,
        statusUpdate: statusList.idle,
      };

    case RESET_UPDATE_PROJECT:
      return {
        ...state,
        statusUpdate: statusList.idle,
        dataUpdate: null,
        dataErrorUpdate: null,
      };

    case START_DELETE_PROJECT:
      return {
        ...state,
        statusDelete: statusList.process,
      };
    case SUCCESS_DELETE_PROJECT:
      return {
        ...state,
        statusDelete: statusList.success,
        dataDelete: action.data,
      };
    case ERROR_DELETE_PROJECT:
      return {
        ...state,
        statusDelete: statusList.error,
        dataErrorDelete: action.data,
      };
    case RESET_STATUS_DELETE_PROJECT:
      return {
        ...state,
        statusDelete: statusList.idle,
      };

    case RESET_DELETE_PROJECT:
      return {
        ...state,
        statusDelete: statusList.idle,
        dataDelete: null,
        dataErrorDelete: null,
      };

    // unit
    case START_FETCH_UNIT:
      return {
        ...state,
        statusListUnit: statusList.process,
      };
    case SUCCESS_FETCH_UNIT:
      return {
        ...state,
        statusListUnit: statusList.success,
        dataListUnit: action.data,
        // amountOfDataUnit: action.amountOfData,
      };
    case ERROR_FETCH_UNIT:
      return {
        ...state,
        statusListUnit: statusList.error,
        dataErrorListUnit: action.data,
      };

    case START_FETCH_UNIT_DETAIL:
      return {
        ...state,
        statusDetailUnit: statusList.process,
      };
    case SUCCESS_FETCH_UNIT_DETAIL:
      return {
        ...state,
        statusDetailUnit: statusList.success,
        dataDetailUnit: action.data,
      };
    case ERROR_FETCH_UNIT_DETAIL:
      return {
        ...state,
        statusDetailUnit: statusList.error,
        dataErrorDetailUnit: action.data,
      };

    case START_CREATE_UNIT:
      return {
        ...state,
        statusCreateUnit: statusList.process,
      };
    case SUCCESS_CREATE_UNIT:
      return {
        ...state,
        statusCreateUnit: statusList.success,
        dataCreateUnit: action.data,
      };
    case ERROR_CREATE_UNIT:
      return {
        ...state,
        statusCreateUnit: statusList.error,
        dataErrorCreateUnit: action.data,
      };
    case RESET_CREATE_UNIT:
      return {
        ...state,
        statusCreateUnit: statusList.idle,
        dataCreateUnit: null,
        dataErrorCreateUnit: null,
      };

    case RESET_STATUS_CREATE_UNIT:
      return {
        ...state,
        statusCreateUnit: statusList.idle,
      };

    case START_UPDATE_UNIT:
      return {
        ...state,
        statusUpdateUnit: statusList.process,
      };
    case SUCCESS_UPDATE_UNIT:
      return {
        ...state,
        statusUpdateUnit: statusList.success,
        dataUpdateUnit: action.data,
      };
    case ERROR_UPDATE_UNIT:
      return {
        ...state,
        statusUpdateUnit: statusList.error,
        dataErrorUpdateUnit: action.data,
      };
    case RESET_UPDATE_UNIT:
      return {
        ...state,
        statusUpdateUnit: statusList.idle,
        dataUpdateUnit: null,
        dataErrorUpdateUnit: null,
      };

    case RESET_STATUS_UPDATE_UNIT:
      return {
        ...state,
        statusUpdateUnit: statusList.idle,
      };

    case START_DELETE_UNIT:
      return {
        ...state,
        statusDeleteUnit: statusList.process,
      };
    case SUCCESS_DELETE_UNIT:
      return {
        ...state,
        statusDeleteUnit: statusList.success,
        dataDeleteUnit: action.data,
      };
    case ERROR_DELETE_UNIT:
      return {
        ...state,
        statusDeleteUnit: statusList.error,
        dataErrorDeleteUnit: action.data,
      };
    case RESET_DELETE_UNIT:
      return {
        ...state,
        statusDeleteUnit: statusList.idle,
        dataDeleteUnit: null,
        dataErrorDeleteUnit: null,
      };

    case RESET_STATUS_DELETE_UNIT:
      return {
        ...state,
        statusDeleteUnit: statusList.idle,
      };
    case SET_DIALOG_EDIT_UNIT:
      return {
        ...state,
        dialogEditUnit: action.value,
      };
    case SET_DIALOG_CREATE_UNIT:
      return {
        ...state,
        dialogCreateUnit: action.value,
      };

    case CHANGE_PHASE:
      return {
        ...state,
        tahap: action.value,
      };
    case CHANGE_BLOCK:
      return {
        ...state,
        blok: action.value,
      };
    case CHANGE_UNIT:
      return {
        ...state,
        unit: action.value,
      };
    case CHANGE_ORDER:
      return {
        ...state,
        order: action.value,
      };

    case CHANGE_PAGE:
      return {
        ...state,
        page: action.value,
      };
    case CHANGE_LIMIT:
      return {
        ...state,
        take: action.value,
      };
    case CHANGE_KEYWORD:
      return {
        ...state,
        keyword: action.value,
      };

    default:
      return state;
  }
}
