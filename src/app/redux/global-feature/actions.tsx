import {
  CHANGE_VALUE,
  ERROR_FETCH_DATA,
  START_FETCH_DATA,
  SUCCESS_FETCH_DATA,
} from "./contants";

export const startFetchData = () => {
  return {
    type: START_FETCH_DATA,
  };
};
export const errorFetchData = (error: any) => {
  return {
    type: ERROR_FETCH_DATA,
    error,
  };
};
export const successFetchData = (data: any) => {
  return {
    type: SUCCESS_FETCH_DATA,
    data,
  };
};

export const changeValue = (value: any) => {
  return {
    type: CHANGE_VALUE,
    value,
  };
};
