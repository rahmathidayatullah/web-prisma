"use client";

import React, { useEffect, useState } from "react";

import moment from "moment";
import dayjs from "dayjs";
import { useRouter } from "next/navigation";
import { formItemLayout } from "./interface";
import Link from "next/link";

import { useDispatch, useSelector } from "react-redux";

import { TimePicker, Input, Form } from "antd";

import CUploadSingle from "@/app/components/molecules/c-upload-single";
import CButtonAction from "@/app/components/molecules/c-button-action";
import CSelect from "@/app/components/molecules/c-select";
import CDateSingle from "@/app/components/molecules/c-date-single";
import { SaveOutlined, ReloadOutlined } from "@ant-design/icons";

import { routerMenu } from "@/app/constants/routes";
import { fetchUsers } from "@/app/redux/features/users/actions";
import {
  fetchOvertimesById,
  updateOvertimes,
} from "@/app/redux/features/overtime/actions";
import { RESET_UPDATE_OVERTIME } from "@/app/redux/features/overtime/constans";
import { useGlobalContext } from "@/app/context/store";

const FormUpdate = ({ params }: { params: { id: string } }) => {
  const { openMessage } = useGlobalContext();
  const router = useRouter();
  const dispatch: any = useDispatch();
  const [form] = Form.useForm();

  const overtime = useSelector((state: any) => state.overtime);
  const { dataUpdate, errorUpdate, statusUpdate, statusDetail, dataDetail } =
    overtime;

  const user = useSelector((state: any) => state.user);
  const { dataUser, statusListUser } = user;

  const [clockInPhoto, setClockInPhoto] = useState("");
  const [dataClockInPhoto, setDataClockInPhoto] = useState<any>([]);
  const handleUploadClockInPhoto = (base64Data: any) => {
    setClockInPhoto(base64Data);
  };

  const [clockOutPhoto, setClockOutPhoto] = useState("");
  const [dataClockOutPhoto, setDataClockOutPhoto] = useState<any>([]);
  const handleUploadClockOutPhoto = (base64Data: any) => {
    setClockOutPhoto(base64Data);
  };

  const [optionUser, setOptionUser] = useState([]);

  const [clockInDate, setClockInDate] = useState("");
  const [clockOutDate, setClockOutDate] = useState("");

  const [clockInTime, setClockInTime] = useState("");
  const [clockOutTime, setClockOutTime] = useState("");

  const onReset = () => {
    form.resetFields();
    setClockInPhoto("");
    setDataClockInPhoto("");
    setClockOutPhoto("");
    setDataClockOutPhoto("");
    setClockInDate("");
    setClockOutDate("");
    setClockInTime("");
    setClockOutTime("");
    dispatch({ type: RESET_UPDATE_OVERTIME });
  };

  const changeTimeClockIn = (date: any, dateString: any) => {
    setClockInTime(dateString);
  };
  const changeTimeClockOut = (date: any, dateString: any) => {
    setClockOutTime(dateString);
  };
  const changeDateClockIn = (date: any, dateString: any) => {
    setClockInDate(dateString);
  };
  const changeDateClockOut = (date: any, dateString: any) => {
    setClockOutDate(dateString);
  };

  const onFinish = (values: any) => {
    const body = {
      clockIn: `${clockInDate} ${clockInTime}`,
      clockOut: `${clockOutDate} ${clockOutTime}`,
      clockInLatitude: values.clockInLatitude,
      clockInLongitude: values.clockInLongitude,
      clockOutLatitude: values.clockOutLatitude,
      clockOutLongitude: values.clockOutLongitude,
      descriptionIn: values.descriptionIn,
      descriptionOut: values.descriptionOut,
      userId: values.userId,

      clockInPhoto: clockInPhoto,
      clockOutPhoto: clockOutPhoto,
    };
    dispatch(updateOvertimes(params.id, body));
  };

  const onChangeUser = (item: any) => {};

  const onSetValueDetail = () => {
    // const dummyData: any = {
    //   clockIn: "2024-01-23 19:32:00",
    //   clockInPhoto:
    //     "https://media.istockphoto.com/id/1311107708/id/foto/siswa-wanita-afrika-amerika-yang-penuh-gaya-yang-terfokus-dengan-gimbal-afro-belajar-dari.jpg?s=1024x1024&w=is&k=20&c=sXlULqd5onkQuEtdram8LoULXozG_J7MfQhQCItBehw=",
    //   clockInLongitude: "-6.397095119672464",
    //   clockInLatitude: "106.80629659206716",
    //   clockOut: "2024-01-23 19:00:00",
    //   clockOutPhoto:
    //     "https://media.istockphoto.com/id/1311107708/id/foto/siswa-wanita-afrika-amerika-yang-penuh-gaya-yang-terfokus-dengan-gimbal-afro-belajar-dari.jpg?s=1024x1024&w=is&k=20&c=sXlULqd5onkQuEtdram8LoULXozG_J7MfQhQCItBehw=",

    //   clockOutLongitude: "-6.397095119672464",
    //   clockOutLatitude: "106.80629659206716",
    //   descriptionIn: "descrtiption in tes",
    //   descriptionOut: "descrtiption out tes",
    //   userId: 1,
    // };

    Object.keys(dataDetail).forEach((key) => {
      const value = dataDetail[key];
      if (key === "user") {
        form.setFieldValue("userId", value.id);
      } else if (key === "clockIn") {
        if (value) {
          const date = moment(value, "DD-MM-YYYY HH:mm:ss").format(
            "YYYY-MM-DD"
          );
          const time = moment(value, "DD-MM-YYYY HH:mm:ss").format("HH:mm:ss");

          setClockInDate(date);
          setClockInTime(time);

          form.setFieldValue("clockInTime", dayjs(time, "HH:mm:ss"));
          form.setFieldValue("clockInDate", dayjs(date, "YYYY-MM-DD"));
        }
      } else if (key === "clockOut") {
        if (value) {
          const date = moment(value, "DD-MM-YYYY HH:mm:ss").format(
            "YYYY-MM-DD"
          );
          const time = moment(value, "DD-MM-YYYY HH:mm:ss").format("HH:mm:ss");

          setClockOutDate(date);
          setClockOutTime(time);

          form.setFieldValue("clockOutTime", dayjs(time, "HH:mm:ss"));
          form.setFieldValue("clockOutDate", dayjs(date, "YYYY-MM-DD"));
        }
      } else if (key === "clockInPhoto") {
        if (value) {
          setDataClockInPhoto([
            {
              uid: "1",
              name: "photoClockIn.png",
              status: "done",
              url: value,
            },
          ]);
          form.setFieldValue(key, value);
        }
      } else if (key === "clockOutPhoto") {
        if (value) {
          setDataClockOutPhoto([
            {
              uid: "1",
              name: "photoClockOut.png",
              status: "done",
              url: value,
            },
          ]);
          form.setFieldValue(key, value);
        }
      } else {
        form.setFieldValue(key, value);
      }
    });
  };

  useEffect(() => {
    if (statusDetail === "success") {
      onSetValueDetail();
    }
    if (statusUpdate === "success") {
      openMessage("success", "Berhasil merubah data lembur karyawan .", "top");
      onReset();
      router.push(routerMenu.OVERTIME);
    }

    if (statusUpdate === "error") {
      openMessage(
        "error",
        errorUpdate?.response?.data?.messsage ??
          "Gagal merubah data lembur karyawan .",
        "top"
      );
    }

    if (statusListUser === "success") {
      setOptionUser(dataUser);
    }
  }, [statusUpdate, statusDetail, statusListUser]);

  useEffect(() => {
    dispatch(fetchUsers());
    dispatch(fetchOvertimesById(params.id));
  }, [dispatch, params.id]);

  return (
    <Form
      {...formItemLayout}
      onFinish={onFinish}
      layout="vertical"
      style={{ width: "100%" }}
      form={form}
      name="control-hooks"
    >
      <div className="grid grid-cols-12 gap-5">
        <div className="col-span-6">
          <div className="grid grid-cols-12 gap-3">
            <div className="col-span-6">
              <Form.Item
                label="Jam Absen Masuk"
                name="clockInTime"
                rules={[{ required: true, message: "Please input!" }]}
              >
                <TimePicker
                  className="w-full"
                  onChange={changeTimeClockIn}
                  defaultOpenValue={dayjs("00:00:00", "HH:mm:ss")}
                />
              </Form.Item>
            </div>

            <div className="col-span-6">
              <Form.Item
                label="Tanggal Absen Masuk"
                name="clockInDate"
                rules={[{ required: true, message: "Please input!" }]}
              >
                <CDateSingle
                  onChange={changeDateClockIn}
                  value={dayjs(form.getFieldValue("clockInDate"), "YYYY-MM-DD")}
                />
              </Form.Item>
            </div>
          </div>
          <div className="grid grid-cols-12 gap-3">
            <div className="col-span-6">
              <Form.Item
                label="Jam Absen Keluar"
                name="clockOutTime"
                rules={[{ required: true, message: "Please input!" }]}
              >
                <TimePicker
                  className="w-full"
                  onChange={changeTimeClockOut}
                  defaultOpenValue={dayjs("00:00:00", "HH:mm:ss")}
                />
              </Form.Item>
            </div>

            <div className="col-span-6">
              <Form.Item
                label="Tanggal Absen Keluar"
                name="clockOutDate"
                rules={[{ required: true, message: "Please input!" }]}
              >
                <CDateSingle
                  onChange={changeDateClockOut}
                  value={dayjs(
                    form.getFieldValue("clockOutDate"),
                    "YYYY-MM-DD"
                  )}
                />
              </Form.Item>
            </div>
          </div>

          <Form.Item label="Deskripsi absen masuk" name="descriptionIn">
            <Input />
          </Form.Item>
          <Form.Item label="Deskripsi absen keluar" name="descriptionOut">
            <Input />
          </Form.Item>
          <Form.Item
            label="Foto Absen Masuk"
            name="clockInPhoto"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <div className="border rounded-md">
              <CUploadSingle
                onChange={handleUploadClockInPhoto}
                dataFileList={dataClockInPhoto}
              />
            </div>
          </Form.Item>
          <Form.Item
            label="Foto Absen Keluar"
            name="clockOutPhoto"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <div className="border rounded-md">
              <CUploadSingle
                onChange={handleUploadClockOutPhoto}
                dataFileList={dataClockOutPhoto}
              />
            </div>
          </Form.Item>

          <Form.Item label="Latitude absen masuk" name="clockInLatitude">
            <Input />
          </Form.Item>
          <Form.Item label="Longitude absen masuk" name="clockInLongitude">
            <Input />
          </Form.Item>
          <Form.Item label="Latitude absen keluar" name="clockOutLatitude">
            <Input />
          </Form.Item>
          <Form.Item label="Longitude absen keluar" name="clockOutLongitude">
            <Input />
          </Form.Item>
          <Form.Item label="Karyawan" name="userId">
            <CSelect
              disabled
              dataOption={optionUser}
              onChange={onChangeUser}
              value={form.getFieldValue("userId")}
            />
          </Form.Item>
        </div>
      </div>
      <div className="grid grid-cols-12 pt-10 mt-10 border-t">
        <div className="col-span-12">
          <Form.Item>
            <div className="flex gap-5 items-center justify-start">
              <Link href={routerMenu.OVERTIME}>
                <CButtonAction
                  icon={<ReloadOutlined />}
                  htmlType="button"
                  disabled={statusUpdate === "process"}
                  loading={statusUpdate === "process"}
                >
                  Kembali
                </CButtonAction>
              </Link>
              <CButtonAction
                icon={<SaveOutlined />}
                disabled={statusUpdate === "process"}
                loading={statusUpdate === "process"}
                htmlType="submit"
                variant="primary"
              >
                Ubah data
              </CButtonAction>
            </div>
          </Form.Item>
        </div>
      </div>
    </Form>
  );
};

export default FormUpdate;
