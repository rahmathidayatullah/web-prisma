import React from "react";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data lembur"
      description="Data informasi lembur, dari seluruh kegiatan karyawan"
    />
  );
};

export default Heading;
