"use client";

import React, { useEffect, useState } from "react";

import { useRouter } from "next/navigation";
import { formItemLayout } from "./interface";

import dayjs from "dayjs";

import { useDispatch, useSelector } from "react-redux";

import { TimePicker, Input, Form } from "antd";
import CUploadSingle from "@/app/components/molecules/c-upload-single";
import Link from "next/link";
import CButtonAction from "@/app/components/molecules/c-button-action";
import { SaveOutlined, ReloadOutlined } from "@ant-design/icons";
import { routerMenu } from "@/app/constants/routes";
import { fetchUsers } from "@/app/redux/features/users/actions";
import CSelect from "@/app/components/molecules/c-select";
import { createOvertimes } from "@/app/redux/features/overtime/actions";
import { RESET_POST_OVERTIME } from "@/app/redux/features/overtime/constans";
import { useGlobalContext } from "@/app/context/store";
import CDateSingle from "@/app/components/molecules/c-date-single";

const FormUpdate = () => {
  const { openMessage } = useGlobalContext();
  const router = useRouter();
  const dispatch: any = useDispatch();
  const [form] = Form.useForm();

  const overtime = useSelector((state: any) => state.overtime);
  const { statusCreate, errorCreate } = overtime;

  const user = useSelector((state: any) => state.user);
  const { dataUser, statusListUser } = user;

  const [clockInPhoto, setClockInPhoto] = useState("");
  const handleUploadClockInPhoto = (base64Data: any) => {
    setClockInPhoto(base64Data);
  };

  const [clockOutPhoto, setClockOutPhoto] = useState("");
  const handleUploadClockOutPhoto = (base64Data: any) => {
    setClockOutPhoto(base64Data);
  };

  const [optionUser, setOptionUser] = useState([]);

  const [clockInDate, setClockInDate] = useState("");
  const [clockOutDate, setClockOutDate] = useState("");

  const [clockInTime, setClockInTime] = useState("");
  const [clockOutTime, setClockOutTime] = useState("");

  const onReset = () => {
    form.resetFields();
    setClockInPhoto("");
    setClockOutPhoto("");
    setClockInDate("");
    setClockOutDate("");
    setClockInTime("");
    setClockOutTime("");
    dispatch({ type: RESET_POST_OVERTIME });
  };

  const changeTimeClockIn = (date: any, dateString: any) => {
    setClockInTime(dateString);
  };
  const changeTimeClockOut = (date: any, dateString: any) => {
    setClockOutTime(dateString);
  };
  const changeDateClockIn = (date: any, dateString: any) => {
    setClockInDate(dateString);
  };
  const changeDateClockOut = (date: any, dateString: any) => {
    setClockOutDate(dateString);
  };

  const onFinish = (values: any) => {
    const body = {
      clockIn: `${clockInDate} ${clockInTime}`,
      clockOut: `${clockOutDate} ${clockOutTime}`,
      clockInLatitude: values.clockInLatitude,
      clockInLongitude: values.clockInLongitude,
      clockOutLatitude: values.clockOutLatitude,
      clockOutLongitude: values.clockOutLongitude,
      descriptionIn: values.descriptionIn,
      descriptionOut: values.descriptionOut,
      userId: values.userId,

      clockInPhoto: clockInPhoto,
      clockOutPhoto: clockOutPhoto,
    };
    dispatch(createOvertimes(body));
  };

  const onChangeUser = (item: any) => {};

  useEffect(() => {
    if (statusCreate === "success") {
      openMessage("success", "Berhasil menambahkan lembur karyawan .", "top");
      onReset();
      router.push(routerMenu.OVERTIME);
    }

    if (statusCreate === "error") {
      openMessage(
        "error",
        errorCreate?.response?.data?.message ??
          "Gagal menambahkan lembur karyawan .",
        "top"
      );
    }
    if (statusListUser === "success") {
      setOptionUser(dataUser);
    }
  }, [statusCreate, statusListUser]);

  useEffect(() => {
    dispatch(fetchUsers());
  }, [dispatch]);

  return (
    <Form
      {...formItemLayout}
      onFinish={onFinish}
      layout="vertical"
      style={{ width: "100%" }}
      form={form}
      name="control-hooks"
    >
      <div className="grid grid-cols-12 gap-5">
        <div className="col-span-6">
          <div className="grid grid-cols-12 gap-3">
            <div className="col-span-6">
              <Form.Item
                label="Jam Absen Masuk"
                name="clockInTime"
                rules={[{ required: true, message: "Please input!" }]}
              >
                <TimePicker
                  className="w-full"
                  onChange={changeTimeClockIn}
                  defaultOpenValue={dayjs("00:00:00", "HH:mm:ss")}
                />
              </Form.Item>
            </div>

            <div className="col-span-6">
              <Form.Item
                label="Tanggal Absen Masuk"
                name="clockInDate"
                rules={[{ required: true, message: "Please input!" }]}
              >
                <CDateSingle
                  onChange={changeDateClockIn}
                  value={dayjs(form.getFieldValue("clockInDate"), "YYYY-MM-DD")}
                />
              </Form.Item>
            </div>
          </div>
          <div className="grid grid-cols-12 gap-3">
            <div className="col-span-6">
              <Form.Item
                label="Jam Absen Keluar"
                name="clockOutTime"
                rules={[{ required: true, message: "Please input!" }]}
              >
                <TimePicker
                  className="w-full"
                  onChange={changeTimeClockOut}
                  defaultOpenValue={dayjs("00:00:00", "HH:mm:ss")}
                />
              </Form.Item>
            </div>

            <div className="col-span-6">
              <Form.Item
                label="Tanggal Absen Keluar"
                name="clockOutDate"
                rules={[{ required: true, message: "Please input!" }]}
              >
                <CDateSingle
                  onChange={changeDateClockOut}
                  value={dayjs(
                    form.getFieldValue("clockOutDate"),
                    "YYYY-MM-DD"
                  )}
                />
              </Form.Item>
            </div>
          </div>

          <Form.Item label="Deskripsi absen masuk" name="descriptionIn">
            <Input />
          </Form.Item>
          <Form.Item label="Deskripsi absen keluar" name="descriptionOut">
            <Input />
          </Form.Item>
          <Form.Item
            label="Foto Absen Masuk"
            name="clockInPhoto"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <div className="border rounded-md">
              <CUploadSingle onChange={handleUploadClockInPhoto} />
            </div>
          </Form.Item>
          <Form.Item
            label="Foto Absen Keluar"
            name="clockOutPhoto"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <div className="border rounded-md">
              <CUploadSingle onChange={handleUploadClockOutPhoto} />
            </div>
          </Form.Item>

          <Form.Item label="Latitude absen masuk" name="clockInLatitude">
            <Input />
          </Form.Item>
          <Form.Item label="Longitude absen masuk" name="clockInLongitude">
            <Input />
          </Form.Item>
          <Form.Item label="Latitude absen keluar" name="clockOutLatitude">
            <Input />
          </Form.Item>
          <Form.Item label="Longitude absen keluar" name="clockOutLongitude">
            <Input />
          </Form.Item>
          <Form.Item
            label="Karyawan"
            name="userId"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <CSelect
              dataOption={optionUser}
              onChange={onChangeUser}
              value={form.getFieldValue("userId")}
            />
          </Form.Item>
        </div>
      </div>
      <div className="grid grid-cols-12 pt-10 mt-10 border-t">
        <div className="col-span-12">
          <Form.Item>
            <div className="flex gap-5 items-center justify-start">
              <Link href={routerMenu.OVERTIME}>
                <CButtonAction
                  icon={<ReloadOutlined />}
                  htmlType="button"
                  disabled={statusCreate === "process"}
                  loading={statusCreate === "process"}
                >
                  Kembali
                </CButtonAction>
              </Link>
              <CButtonAction
                icon={<SaveOutlined />}
                disabled={statusCreate === "process"}
                loading={statusCreate === "process"}
                htmlType="submit"
                variant="primary"
              >
                Tambah data
              </CButtonAction>
            </div>
          </Form.Item>
        </div>
      </div>
    </Form>
  );
};

export default FormUpdate;
