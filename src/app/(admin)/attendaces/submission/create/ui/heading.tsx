"use client";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data pengajuan"
      description="Menambahkan data pengajuan baru berdasarkan karyawan"
    />
  );
};

export default Heading;
