"use client";

import React, { useEffect, useState } from "react";

import { useRouter } from "next/navigation";
import { formItemLayout } from "./interface";

import { useDispatch, useSelector } from "react-redux";

import { Input, Form } from "antd";
import CUploadSingle from "@/app/components/molecules/c-upload-single";
import Link from "next/link";
import CButtonAction from "@/app/components/molecules/c-button-action";
import { SaveOutlined, ReloadOutlined } from "@ant-design/icons";
import { routerMenu } from "@/app/constants/routes";
import CDateSingle from "@/app/components/molecules/c-date-single";
import CSelect from "@/app/components/molecules/c-select";
import { fetchUsers } from "@/app/redux/features/users/actions";
import { RESET_POST_SUBMISSION } from "@/app/redux/features/submission/constans";
import { createSubmissions } from "@/app/redux/features/submission/actions";
import { fetchSubmissionCategories } from "@/app/redux/features/master-data-attendaces/submission-categories/actions";
import { useGlobalContext } from "@/app/context/store";

const FormCreate = () => {
  const { openMessage } = useGlobalContext();
  const router = useRouter();
  const dispatch: any = useDispatch();
  const [form] = Form.useForm();

  const user = useSelector((state: any) => state.user);
  const { dataUser, statusListUser } = user;

  const submissionCategories = useSelector(
    (state: any) => state.submissionCategories
  );
  const { data, statusList } = submissionCategories;

  const submission = useSelector((state: any) => state.submission);
  const { statusCreate, dataErrorCreate } = submission;

  const [submissionFile, setSubmissionFile] = useState("");
  const handleUploadSubmissionFile = (base64Data: any) => {
    setSubmissionFile(base64Data);
  };

  const [otherSubmissionFile, setOtherSubmissionFile] = useState("");
  const handleUploadOtherSubmissionFile = (base64Data: any) => {
    setOtherSubmissionFile(base64Data);
  };

  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");

  const onReset = () => {
    form.resetFields();
    setStartDate("");
    setEndDate("");
    setSubmissionFile("");
    setOtherSubmissionFile("");
    dispatch({ type: RESET_POST_SUBMISSION });
  };

  const changeStartDate = (date: any, dateString: any) => {
    setStartDate(dateString);
  };
  const changeEndDate = (date: any, dateString: any) => {
    setEndDate(dateString);
  };

  const onFinish = (values: any) => {
    const body = {
      ...values,
      startDate: startDate,
      endDate: endDate,
      submissionFile: submissionFile,
      otherSubmissionFile: otherSubmissionFile,
    };
    dispatch(createSubmissions(body));
  };

  const [optionSubmissionCategory, setOptionSubmissionCategory] = useState([]);
  const [optionUser, setOptionUser] = useState([]);

  const onChangeUser = (item: any) => {};
  const onChangeSubmissionCategory = (item: any) => {};

  useEffect(() => {
    if (statusCreate === "success") {
      openMessage(
        "success",
        "Berhasil menambahkan pengajuan karyawan .",
        "top"
      );
      onReset();
      router.push(routerMenu.SUBMISSION);
    }
    if (statusCreate === "error") {
      openMessage(
        "error",
        dataErrorCreate?.response?.data?.message ??
          "Gagal menambahkan pengajuan baru .",
        "top"
      );
    }
    if (statusListUser === "success") {
      setOptionUser(dataUser);
    }
    if (statusList === "success") {
      setOptionSubmissionCategory(data);
    }
    return () => {};
  }, [statusCreate, statusList, statusListUser]);

  useEffect(() => {
    dispatch(fetchUsers());
    dispatch(fetchSubmissionCategories());
  }, [dispatch]);

  return (
    <>
      <Form
        {...formItemLayout}
        onFinish={onFinish}
        layout="vertical"
        style={{ width: "100%" }}
        form={form}
        name="control-hooks"
      >
        <div className="grid grid-cols-12 gap-5">
          <div className="col-span-6">
            <Form.Item
              label="Mulai pengajuan"
              name="startDate"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CDateSingle onChange={changeStartDate} />
            </Form.Item>
            <Form.Item
              label="Berakhir pengajuan"
              name="endDate"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CDateSingle onChange={changeEndDate} />
            </Form.Item>
            <Form.Item label="Deskripsi submission" name="description">
              <Input />
            </Form.Item>
            <Form.Item label="Foto Lampiran 1" name="submissionFile">
              <div className="border rounded-md">
                <CUploadSingle onChange={handleUploadSubmissionFile} />
              </div>
            </Form.Item>
            <Form.Item label="Foto Lampiran 2" name="otherSubmissionFile">
              <div className="border rounded-md">
                <CUploadSingle onChange={handleUploadOtherSubmissionFile} />
              </div>
            </Form.Item>
            <Form.Item
              label="Karyawan"
              name="userId"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CSelect
                dataOption={optionUser}
                onChange={onChangeUser}
                value={form.getFieldValue("userId")}
              />
            </Form.Item>
            <Form.Item
              label="Kategori pengajuan"
              name="submissionCategoryId"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CSelect
                dataOption={optionSubmissionCategory}
                onChange={onChangeSubmissionCategory}
                value={form.getFieldValue("submissionCategoryId")}
              />
            </Form.Item>
          </div>
        </div>
        <div className="grid grid-cols-12 pt-10 mt-10 border-t">
          <div className="col-span-12">
            <Form.Item>
              <div className="flex gap-5 items-center justify-start">
                <Link href={routerMenu.SUBMISSION}>
                  <CButtonAction
                    icon={<ReloadOutlined />}
                    htmlType="button"
                    disabled={statusCreate === "process"}
                    loading={statusCreate === "process"}
                  >
                    Kembali
                  </CButtonAction>
                </Link>
                <CButtonAction
                  icon={<SaveOutlined />}
                  disabled={statusCreate === "process"}
                  loading={statusCreate === "process"}
                  htmlType="submit"
                  variant="primary"
                >
                  Tambah data
                </CButtonAction>
              </div>
            </Form.Item>
          </div>
        </div>
      </Form>
    </>
  );
};

export default FormCreate;
