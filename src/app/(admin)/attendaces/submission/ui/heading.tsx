import React from "react";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data pengajuan"
      description="Data informasi pengajuan, dari seluruh kegiatan karyawan"
    />
  );
};

export default Heading;
