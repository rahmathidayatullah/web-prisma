"use client";

import React, { useEffect, useState } from "react";
import {
  Checkbox,
  Space,
  Table,
  Button,
  CheckboxOptionType,
  DropdownProps,
  MenuProps,
  Tag,
  Form,
  Input,
} from "antd";
import type { TableColumnsType } from "antd";
import { Image as ImgAntd } from "antd";
import type { SearchProps } from "antd/es/input/Search";
import { DownloadOutlined, PlusOutlined } from "@ant-design/icons";

import { DataType } from "./interface";
import moment from "moment";
import "moment/locale/id";

// redux
import { useDispatch, useSelector } from "react-redux";
import {
  fetchSubmissions,
  fetchSubmissionsExports,
  submissionsApprove,
} from "@/app/redux/features/submission/actions";
import {
  CHANGE_DATE_RANGE,
  CHANGE_KEYWORD,
  RESET_EXPORT_SUBMISSION,
  RESET_STATUS_EXPORT_SUBMISSION,
  RESET_STATUS_REJECT_APPROVE,
} from "@/app/redux/features/submission/constans";

// componets
import CSearch from "@/app/components/molecules/c-search";
import CDateRange from "@/app/components/molecules/c-date-range";
import CDropdown from "@/app/components/molecules/c-dropdown";
import CButton from "@/app/components/molecules/c-button";
import CDialog from "@/app/components/molecules/c-dialog";
import Link from "next/link";
import { routerMenu } from "@/app/constants/routes";
import { useGlobalContext } from "@/app/context/store";
import { openNewTab } from "@/app/utils/constants";
import { useSessionStorage } from "@/app/hooks/useSessionStorage";

const CTable: React.FC = () => {
  const { openNotification, openMessage } = useGlobalContext();
  const [form] = Form.useForm();
  const dispatch: any = useDispatch();
  const submissions = useSelector((state: any) => state.submission);

  const {
    statusApprove,
    statusReject,
    statusListSubmission,
    page,
    take,
    keyword,
    endDate,
    startDate,
    errorApprove,

    statusExport,
    dataExport,
    errorExport,
  } = submissions;
  const [openSourceType, setOpenSourceType] = useState(false);
  const [open, setOpen] = useState(false);
  const [showDialogConfirm, setShowDialogConfirm] = useState(false);
  const handleOpenChangeSourceType: DropdownProps["onOpenChange"] = (
    nextOpen,
    info
  ) => {
    if (info.source === "trigger" || nextOpen) {
      setOpenSourceType(nextOpen);
    }
  };
  const coloumTable: TableColumnsType<DataType> = [
    {
      title: "Nama",
      dataIndex: "user",
      key: "1",
      render: (value) => {
        if (value) {
          return value.name;
        }
        return "-";
      },
    },
    {
      title: "Jabatan",
      dataIndex: "user",
      key: "2",
      render: (value) => {
        if (value) {
          return value.role.name;
        }
        return "-";
      },
    },
    {
      title: "Status Pengajuan",
      dataIndex: "submissionCategory",
      key: "3",
      render: (value) => {
        if (value) {
          return value.name;
        }
        return "-";
      },
    },
    {
      title: "Waktu Mulai",
      dataIndex: "startDate",
      key: "4",
      render: (value) => {
        if (value) {
          return moment(value, "YYYY-MM-DD").format("DD MMMM YYYY");
        }
        return "-";
      },
    },
    {
      title: "Waktu Selesai",
      dataIndex: "endDate",
      key: "5",
      render: (value) => {
        if (value) {
          return moment(value, "YYYY-MM-DD").format("DD MMMM YYYY");
        }
        return "-";
      },
    },
    {
      title: "Lampiran 1",
      dataIndex: "submissionFile",
      key: "6",
      render: (value) => {
        if (value) {
          return (
            <ImgAntd width={100} height={100} src="error" fallback={value} />
          );
        }
        return "-";
      },
    },
    {
      title: "Lampiran 2",
      dataIndex: "otherSubmissionFile",
      key: "7",
      render: (value) => {
        if (value) {
          return (
            <ImgAntd width={100} height={100} src="error" fallback={value} />
          );
        }
        return "-";
      },
    },
    {
      title: "Deskripsi",
      dataIndex: "description",
      key: "8",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Status",
      dataIndex: "approveBy",
      key: "9",
      render: (value) => {
        if (value) {
          return (
            <div className="flex items-center gap-2">
              <Tag
                color={
                  value.status === "Approve"
                    ? "green"
                    : value === "Reject"
                    ? "red"
                    : "orange"
                }
              >
                {value.status}
              </Tag>
              {value.status === "Approve" || value.status === "Reject" ? (
                <Tag color="red">{value.name}</Tag>
              ) : (
                ""
              )}
            </div>
          );
        }
        return "-";
      },
    },
    {
      title: "Aksi",
      key: "10",
      render: (_, record: any) => {
        return (
          <div className="flex items-center gap-4">
            <Link href={`${routerMenu.SUBMISSION}/${record.id}`}>
              <Button
                style={{ backgroundColor: "#FBB03B" }}
                size="small"
                type="primary"
              >
                Edit
              </Button>
            </Link>
            <Button
              style={{ backgroundColor: "#219C90" }}
              size="small"
              type="primary"
              onClick={() => {
                setId(record.id), setOpen(true);
                setAction("Approve");
              }}
            >
              Approve
            </Button>
            <Button
              style={{ backgroundColor: "#FF5151", color: "white" }}
              size="small"
              type="default"
              danger
              onClick={() => {
                setId(record.id), setOpen(true);
                setAction("Reject");
              }}
            >
              Reject
            </Button>
          </div>
        );
      },
    },
  ];

  const [id, setId] = useState("");
  const [action, setAction] = useState("");

  const defaultCheckedList = coloumTable.map((item: any) => item.key as string);

  const [checkedList, setCheckedList] = useSessionStorage<string[]>(
    "data-submission",
    defaultCheckedList
  );

  const newColumns = coloumTable.map((item: any) => ({
    ...item,
    hidden: !checkedList.includes(item.key as string),
  }));

  const options = coloumTable.map(({ key, title }: any) => ({
    label: title,
    value: key,
  }));

  const items: MenuProps["items"] = [
    {
      label: (
        <Checkbox.Group
          className="flex flex-col gap-3"
          value={checkedList}
          options={options as CheckboxOptionType[]}
          onChange={(value) => {
            setCheckedList(value as string[]);
          }}
        />
      ),
      key: "0",
    },
  ];

  const onSearch: SearchProps["onSearch"] = (value, _e, info) => {};

  const approveSubmission = () => {
    if (!form.getFieldValue("password")) {
      openMessage("warning", "Kolom password harus diisi", "top");
    } else {
      if (action === "Approve") {
        dispatch(
          submissionsApprove(id, {
            status: "Approve",
            password: form.getFieldValue("password"),
          })
        );
      } else {
        dispatch(
          submissionsApprove(id, {
            status: "Reject",
            password: form.getFieldValue("password"),
          })
        );
      }
    }
  };

  useEffect(() => {
    dispatch(fetchSubmissions());
    return () => {
      dispatch({
        type: RESET_STATUS_REJECT_APPROVE,
      });
      form.resetFields();
    };
  }, [dispatch, page, take, keyword, endDate, startDate]);

  useEffect(() => {
    if (statusApprove === "success") {
      setOpen(false);
      setShowDialogConfirm(false);
      openNotification("topLeft", "Berhasil", `Data berhasil di ${action}`);
      form.resetFields();
      dispatch({
        type: RESET_STATUS_REJECT_APPROVE,
      });
    }
    if (statusApprove === "error") {
      if (
        errorApprove?.response?.data?.message === "Wrong password"
          ? "Password yang di inputkan tidak sesuai"
          : "Terjadi kesalah "
      ) {
        openMessage("warning", "Password yang di inputkan tidak sesuai", "top");
      } else {
        openMessage("warning", "Terjadi kesalahan", "top");
      }
      dispatch({
        type: RESET_STATUS_REJECT_APPROVE,
      });
    }

    if (statusExport === "success") {
      openNewTab(dataExport.fileUrl);
      dispatch({ type: RESET_EXPORT_SUBMISSION });
    }
    if (statusExport === "error") {
      openMessage(
        "error",
        errorExport?.response?.data?.message ?? "Terjadi kesalahan",
        "top"
      );
      dispatch({ type: RESET_STATUS_EXPORT_SUBMISSION });
    }
  }, [dispatch, statusReject, statusApprove, statusExport]);
  return (
    <>
      <div className="flex flex-wrap gap-2 sm:gap-5 items-center justify-between my-5">
        <Space direction="vertical">
          <CSearch
            onSearch={onSearch}
            value={keyword}
            onChange={(event: any) =>
              dispatch({ type: CHANGE_KEYWORD, value: event.target.value })
            }
          />
        </Space>
        <div className="flex flex-wrap items-center gap-2 sm:gap-5">
          <CDateRange
            onChange={(_: any, dateString: any) =>
              dispatch({ type: CHANGE_DATE_RANGE, value: dateString })
            }
          />
          <CDropdown
            variant="sortir-coloum"
            menu={{ items }}
            onOpenChange={handleOpenChangeSourceType}
            open={openSourceType}
          />
          <CButton
            icon={<DownloadOutlined />}
            variant="primary"
            onClick={() => dispatch(fetchSubmissionsExports())}
            loading={statusExport === "process"}
            disabled={statusExport === "process"}
          >
            Export Data
          </CButton>
          <Link href={routerMenu.CREATE_SUBMISSION}>
            <CButton icon={<PlusOutlined />} variant="primary">
              Tambah Pengajuan
            </CButton>
          </Link>
        </div>
      </div>
      <Table
        pagination={false}
        columns={newColumns}
        dataSource={submissions.dataSubmission}
        style={{ marginTop: 24 }}
        loading={statusListSubmission === "process"}
        rowKey="id"
        scroll={{ x: 1500 }}
      />

      {/* modal */}
      <CDialog
        title={`${action} data pengajuan ?`}
        open={open}
        titleActionOk="Ya"
        titleActionCancel="Tidak"
        actionCancel={() => {
          setOpen(false);
        }}
        actionOk={() => {
          setShowDialogConfirm(true);
          setOpen(false);
          form.resetFields();
        }}
      ></CDialog>

      <CDialog
        title={`Konfirmasi password untuk melakukan ${action} ?`}
        open={showDialogConfirm}
        titleActionOk="Ya"
        titleActionCancel="Tidak"
        actionCancel={() => setShowDialogConfirm(false)}
        actionOk={approveSubmission}
        loadingBtn={statusApprove === "process" || statusReject === "process"}
      >
        <Form
          onFinish={approveSubmission}
          layout="vertical"
          style={{ width: "100%" }}
          form={form}
          name="control-hooks"
        >
          <Form.Item
            label="Password"
            name="password"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <Input.Password />
          </Form.Item>
        </Form>
      </CDialog>
    </>
  );
};

export default CTable;
