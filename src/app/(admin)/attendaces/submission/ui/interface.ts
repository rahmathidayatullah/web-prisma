export interface DataType {
  key: string;

  user: {
    name: string | null;
    role: {
      name: string | null;
    };
  };
  status: string | null;
  approveBy: {
    name: string;
  };

  startDate: string | null;
  endDate: string | null;
  submissionCategory: string | null;
  submissionFile: string | null;
  otherSubmissionFile: string | null;
  description: string | null;
  action: any;
}

export const dataTable: DataType[] = [
  // {
  //   key: "1",
  //   name: "Rahmat",
  //   role: 32,
  //   status: "Pending",
  //   clockIn: "08:00",
  //   clockOut: "17:00",
  //   date: "01 01 2024",
  //   foto_selfie: "Image",
  //   action: "Image",
  // },
  // {
  //   key: "2",
  //   name: "Rahmat",
  //   role: 32,
  //   status: "Pending",
  //   clockIn: "08:00",
  //   clockOut: "17:00",
  //   date: "01 01 2024",
  //   foto_selfie: "Image",
  //   action: "Image",
  // },
  // {
  //   key: "3",
  //   name: "Rahmat",
  //   role: 32,
  //   status: "Pending",
  //   clockIn: "08:00",
  //   clockOut: "17:00",
  //   date: "01 01 2024",
  //   foto_selfie: "Image",
  //   action: "Image",
  // },
];
