"use client";

import React, { useEffect, useState } from "react";

import { useRouter } from "next/navigation";
import { formItemLayout } from "./interface";

import dayjs from "dayjs";

import { useDispatch, useSelector } from "react-redux";

import { TimePicker, Input, Form } from "antd";
import CUploadSingle from "@/app/components/molecules/c-upload-single";
import Link from "next/link";
import CButtonAction from "@/app/components/molecules/c-button-action";
import { SaveOutlined, ReloadOutlined } from "@ant-design/icons";
import { routerMenu } from "@/app/constants/routes";
import {
  fetchSubmissionDetail,
  updateSubmissions,
} from "@/app/redux/features/submission/actions";
import { RESET_UPDATE_SUBMISSION } from "@/app/redux/features/submission/constans";
import CDateSingle from "@/app/components/molecules/c-date-single";
import CSelect from "@/app/components/molecules/c-select";
import { fetchUsers } from "@/app/redux/features/users/actions";
import { fetchSubmissionCategories } from "@/app/redux/features/master-data-attendaces/submission-categories/actions";
import { useGlobalContext } from "@/app/context/store";
const dateFormat = "YYYY-MM-DD";

const FormUpdate = ({ params }: { params: { id: string } }) => {
  const { openMessage } = useGlobalContext();
  const router = useRouter();
  const dispatch: any = useDispatch();
  const [form] = Form.useForm();

  const user = useSelector((state: any) => state.user);
  const { dataUser, statusListUser } = user;

  const submissionCategories = useSelector(
    (state: any) => state.submissionCategories
  );
  const { data, statusList } = submissionCategories;

  const submission = useSelector((state: any) => state.submission);
  const { statusUpdate, statusDetail, dataDetail } = submission;

  const [submissionFile, setSubmissionFile] = useState("");
  const [dataSubmissionFile, setDataSubmissionFile] = useState<any>([]);
  const handleUploadSubmissionFile = (base64Data: any) => {
    setSubmissionFile(base64Data);
  };

  const [otherSubmissionFile, setOtherSubmissionFile] = useState("");
  const [dataOtherSubmissionFile, setDataOtherSubmissionFile] = useState<any>(
    []
  );
  const handleUploadOtherSubmissionFile = (base64Data: any) => {
    setOtherSubmissionFile(base64Data);
  };

  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");

  const onReset = () => {
    form.resetFields();
    setStartDate("");
    setEndDate("");
    setSubmissionFile("");
    setOtherSubmissionFile("");
    dispatch({ type: RESET_UPDATE_SUBMISSION });
  };

  const changeStartDate = (date: any, dateString: any) => {
    setStartDate(dateString);
  };
  const changeEndDate = (date: any, dateString: any) => {
    setEndDate(dateString);
  };

  const onFinish = (values: any) => {
    const body = {
      ...values,
      startDate: startDate,
      endDate: endDate,
      submissionFile: submissionFile,
      otherSubmissionFile: otherSubmissionFile,
    };
    dispatch(updateSubmissions(params.id, body));
  };

  const [optionSubmissionCategory, setOptionSubmissionCategory] = useState([]);
  const [optionUser, setOptionUser] = useState([]);

  const onChangeUser = (item: any) => {};
  const onChangeSubmissionCategory = (item: any) => {};

  const onSetValueDetail = () => {
    // const dummyData: any = {
    //   startDate: "2024-04-01",
    //   endDate: "2024-04-01",
    //   submissionFile:
    //     "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAOCAMAAAD+MweGAAADAFBMVEUAAAAAAFUAAKoAAP8AJAAAJFUAJKoAJP8ASQAASVUASaoASf8AbQAAbVUAbaoAbf8AkgAAklUAkqoAkv8AtgAAtlUAtqoAtv8A2wAA21UA26oA2/8A/wAA/1UA/6oA//8kAAAkAFUkAKokAP8kJAAkJFUkJKokJP8kSQAkSVUkSaokSf8kbQAkbVUkbaokbf8kkgAkklUkkqokkv8ktgAktlUktqoktv8k2wAk21Uk26ok2/8k/wAk/1Uk/6ok//9JAABJAFVJAKpJAP9JJABJJFVJJKpJJP9JSQBJSVVJSapJSf9JbQBJbVVJbapJbf9JkgBJklVJkqpJkv9JtgBJtlVJtqpJtv9J2wBJ21VJ26pJ2/9J/wBJ/1VJ/6pJ//9tAABtAFVtAKptAP9tJABtJFVtJKptJP9tSQBtSVVtSaptSf9tbQBtbVVtbaptbf9tkgBtklVtkqptkv9ttgBttlVttqpttv9t2wBt21Vt26pt2/9t/wBt/1Vt/6pt//+SAACSAFWSAKqSAP+SJACSJFWSJKqSJP+SSQCSSVWSSaqSSf+SbQCSbVWSbaqSbf+SkgCSklWSkqqSkv+StgCStlWStqqStv+S2wCS21WS26qS2/+S/wCS/1WS/6qS//+2AAC2AFW2AKq2AP+2JAC2JFW2JKq2JP+2SQC2SVW2Saq2Sf+2bQC2bVW2baq2bf+2kgC2klW2kqq2kv+2tgC2tlW2tqq2tv+22wC221W226q22/+2/wC2/1W2/6q2///bAADbAFXbAKrbAP/bJADbJFXbJKrbJP/bSQDbSVXbSarbSf/bbQDbbVXbbarbbf/bkgDbklXbkqrbkv/btgDbtlXbtqrbtv/b2wDb21Xb26rb2//b/wDb/1Xb/6rb////AAD/AFX/AKr/AP//JAD/JFX/JKr/JP//SQD/SVX/Sar/Sf//bQD/bVX/bar/bf//kgD/klX/kqr/kv//tgD/tlX/tqr/tv//2wD/21X/26r/2////wD//1X//6r////qm24uAAAA1ElEQVR42h1PMW4CQQwc73mlFJGCQChFIp0Rh0RBGV5AFUXKC/KPfCFdqryEgoJ8IX0KEF64q0PPnow3jT2WxzNj+gAgAGfvvDdCQIHoSnGYcGDE2nH92DoRqTYJ2bTcsKgqhIi47VdgAWNmwFSFA1UAAT2sSFcnq8a3x/zkkJrhaHT3N+hD3aH7ZuabGHX7bsSMhxwTJLr3evf1e0nBVcwmqcTZuatKoJaB7dSHjTZdM0G1HBTWefly//q2EB7/BEvk5vmzeQaJ7/xKPImpzv8/s4grhAxHl0DsqGUAAAAASUVORK5CYII=",
    //   otherSubmissionFile:
    //     "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABEAAAAOCAMAAAD+MweGAAADAFBMVEUAAAAAAFUAAKoAAP8AJAAAJFUAJKoAJP8ASQAASVUASaoASf8AbQAAbVUAbaoAbf8AkgAAklUAkqoAkv8AtgAAtlUAtqoAtv8A2wAA21UA26oA2/8A/wAA/1UA/6oA//8kAAAkAFUkAKokAP8kJAAkJFUkJKokJP8kSQAkSVUkSaokSf8kbQAkbVUkbaokbf8kkgAkklUkkqokkv8ktgAktlUktqoktv8k2wAk21Uk26ok2/8k/wAk/1Uk/6ok//9JAABJAFVJAKpJAP9JJABJJFVJJKpJJP9JSQBJSVVJSapJSf9JbQBJbVVJbapJbf9JkgBJklVJkqpJkv9JtgBJtlVJtqpJtv9J2wBJ21VJ26pJ2/9J/wBJ/1VJ/6pJ//9tAABtAFVtAKptAP9tJABtJFVtJKptJP9tSQBtSVVtSaptSf9tbQBtbVVtbaptbf9tkgBtklVtkqptkv9ttgBttlVttqpttv9t2wBt21Vt26pt2/9t/wBt/1Vt/6pt//+SAACSAFWSAKqSAP+SJACSJFWSJKqSJP+SSQCSSVWSSaqSSf+SbQCSbVWSbaqSbf+SkgCSklWSkqqSkv+StgCStlWStqqStv+S2wCS21WS26qS2/+S/wCS/1WS/6qS//+2AAC2AFW2AKq2AP+2JAC2JFW2JKq2JP+2SQC2SVW2Saq2Sf+2bQC2bVW2baq2bf+2kgC2klW2kqq2kv+2tgC2tlW2tqq2tv+22wC221W226q22/+2/wC2/1W2/6q2///bAADbAFXbAKrbAP/bJADbJFXbJKrbJP/bSQDbSVXbSarbSf/bbQDbbVXbbarbbf/bkgDbklXbkqrbkv/btgDbtlXbtqrbtv/b2wDb21Xb26rb2//b/wDb/1Xb/6rb////AAD/AFX/AKr/AP//JAD/JFX/JKr/JP//SQD/SVX/Sar/Sf//bQD/bVX/bar/bf//kgD/klX/kqr/kv//tgD/tlX/tqr/tv//2wD/21X/26r/2////wD//1X//6r////qm24uAAAA1ElEQVR42h1PMW4CQQwc73mlFJGCQChFIp0Rh0RBGV5AFUXKC/KPfCFdqryEgoJ8IX0KEF64q0PPnow3jT2WxzNj+gAgAGfvvDdCQIHoSnGYcGDE2nH92DoRqTYJ2bTcsKgqhIi47VdgAWNmwFSFA1UAAT2sSFcnq8a3x/zkkJrhaHT3N+hD3aH7ZuabGHX7bsSMhxwTJLr3evf1e0nBVcwmqcTZuatKoJaB7dSHjTZdM0G1HBTWefly//q2EB7/BEvk5vmzeQaJ7/xKPImpzv8/s4grhAxHl0DsqGUAAAAASUVORK5CYII=",
    //   description: "description test",
    //   submissionCategoryId: 1,
    //   userId: 1,
    // };
    Object.keys(dataDetail).forEach((key) => {
      const value = dataDetail[key];
      if (key === "submissionCategory") {
        form.setFieldValue("submissionCategoryId", value.id);
      } else if (key === "user") {
        form.setFieldValue("userId", value.id);
      } else if (key === "startDate") {
        if (value) {
          setStartDate(value);
          form.setFieldValue(key, dayjs(value, dateFormat));
        }
      } else if (key === "endDate") {
        if (value) {
          setEndDate(value);
          form.setFieldValue(key, dayjs(value, dateFormat));
        }
      } else if (key === "submissionFile") {
        if (value) {
          setDataSubmissionFile([
            {
              uid: "1",
              name: "fotoLampiran1.png",
              status: "done",
              url: value,
            },
          ]);
          form.setFieldValue(key, value);
        }
      } else if (key === "otherSubmissionFile") {
        if (value) {
          setDataOtherSubmissionFile([
            {
              uid: "1",
              name: "fotoLampiran2.png",
              status: "done",
              url: value,
            },
          ]);
          form.setFieldValue(key, value);
        }
      } else {
        form.setFieldValue(key, value);
      }
    });
  };

  useEffect(() => {
    if (statusDetail === "success") {
      onSetValueDetail();
    }
    if (statusUpdate === "success") {
      openMessage(
        "success",
        "Berhasil merubah data pengajuan karyawan .",
        "top"
      );
      onReset();
      router.push(routerMenu.SUBMISSION);
    }

    if (statusUpdate === "error") {
      openMessage("error", "Gagal merubah data submission karyawan .", "top");
    }

    if (statusListUser === "success") {
      setOptionUser(dataUser);
    }
    if (statusList === "success") {
      setOptionSubmissionCategory(data);
    }
  }, [statusUpdate, statusDetail, statusList, statusListUser]);

  useEffect(() => {
    dispatch(fetchUsers());
    dispatch(fetchSubmissionCategories());
    dispatch(fetchSubmissionDetail(params.id));
  }, [dispatch, params.id]);

  return (
    <Form
      {...formItemLayout}
      onFinish={onFinish}
      layout="vertical"
      style={{ width: "100%" }}
      form={form}
      name="control-hooks"
    >
      <div className="grid grid-cols-12 gap-5">
        <div className="col-span-6">
          <Form.Item
            label="Mulai pengajuan"
            name="startDate"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <CDateSingle
              onChange={changeStartDate}
              value={dayjs(form.getFieldValue("startDate"), dateFormat)}
            />
          </Form.Item>
          <Form.Item
            label="Berakhir pengajuan"
            name="endDate"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <CDateSingle
              onChange={changeEndDate}
              value={dayjs(form.getFieldValue("endDate"), dateFormat)}
            />
          </Form.Item>
          <Form.Item label="Deskripsi pengajuan" name="description">
            <Input />
          </Form.Item>
          <Form.Item label="Foto Lampiran 1" name="submissionFile">
            <div className="border rounded-md">
              <CUploadSingle
                onChange={handleUploadSubmissionFile}
                dataFileList={dataSubmissionFile}
              />
            </div>
          </Form.Item>
          <Form.Item label="Foto Lampiran 2" name="otherSubmissionFile">
            <div className="border rounded-md">
              <CUploadSingle
                onChange={handleUploadOtherSubmissionFile}
                dataFileList={dataOtherSubmissionFile}
              />
            </div>
          </Form.Item>
          <Form.Item
            label="Karyawan"
            name="userId"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <CSelect
              dataOption={optionUser}
              onChange={onChangeUser}
              value={form.getFieldValue("userId")}
            />
          </Form.Item>
          <Form.Item
            label="Kategori pengajuan"
            name="submissionCategoryId"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <CSelect
              dataOption={optionSubmissionCategory}
              onChange={onChangeSubmissionCategory}
              value={form.getFieldValue("submissionCategoryId")}
            />
          </Form.Item>
        </div>
      </div>
      <div className="grid grid-cols-12 pt-10 mt-10 border-t">
        <div className="col-span-12">
          <Form.Item>
            <div className="flex gap-5 items-center justify-start">
              <Link href={routerMenu.SUBMISSION}>
                <CButtonAction
                  icon={<ReloadOutlined />}
                  htmlType="button"
                  disabled={statusDetail === "process"}
                  loading={statusDetail === "process"}
                >
                  Kembali
                </CButtonAction>
              </Link>
              <CButtonAction
                icon={<SaveOutlined />}
                disabled={statusDetail === "process"}
                loading={statusDetail === "process"}
                htmlType="submit"
                variant="primary"
              >
                Ubah data
              </CButtonAction>
            </div>
          </Form.Item>
        </div>
      </div>
    </Form>
  );
};

export default FormUpdate;
