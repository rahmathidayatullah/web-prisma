"use client";
import CHeading from "@/app/components/molecules/c-heading";
import { useSelector } from "react-redux";

const Heading = () => {
  const submission = useSelector((state: any) => state.submission);
  const { dataDetail } = submission;

  return (
    <CHeading
      title={`Informasi data submission karyawan ${
        dataDetail?.user?.name ?? "-"
      }`}
      description={`Data detail submission karyawan, ubah data submission karyawan ${
        dataDetail?.user?.name ?? "-"
      }`}
    />
  );
};

export default Heading;
