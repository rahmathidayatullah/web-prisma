"use client";

import React, { useEffect } from "react";

import { useRouter } from "next/navigation";
import { formItemLayout } from "./interface";

import { useDispatch, useSelector } from "react-redux";

import { Input, Form } from "antd";
import Link from "next/link";
import CButtonAction from "@/app/components/molecules/c-button-action";
import { SaveOutlined, ReloadOutlined } from "@ant-design/icons";
import { routerMenu } from "@/app/constants/routes";
import { useGlobalContext } from "@/app/context/store";
import { createSubmissionCategories } from "@/app/redux/features/master-data-attendaces/submission-categories/actions";
import {
  RESET_CREATE_SUBMISSION_CATEGORIES,
  RESET_STATUS_CREATE_SUBMISSION_CATEGORIES,
} from "@/app/redux/features/master-data-attendaces/submission-categories/constants";

const FormCreate = () => {
  const { openMessage } = useGlobalContext();
  const router = useRouter();
  const dispatch: any = useDispatch();
  const [form] = Form.useForm();

  const submissionCategories = useSelector(
    (state: any) => state.submissionCategories
  );
  const { errorCreate, statusCreate } = submissionCategories;

  const onReset = () => {
    form.resetFields();
    dispatch({ type: RESET_CREATE_SUBMISSION_CATEGORIES });
  };

  const onFinish = (values: any) => {
    const body = {
      ...values,
    };
    dispatch(createSubmissionCategories(body));
  };

  useEffect(() => {
    if (statusCreate === "success") {
      openMessage(
        "success",
        "Berhasil menambahkan kategori pengajuan baru .",
        "top"
      );
      onReset();
      router.push(routerMenu.CATEGORY_SUBMISSION);
    }
    if (statusCreate === "error") {
      openMessage(
        "error",
        errorCreate?.response?.data?.message ??
          "Gagal menambahkan kategori pengajuan baru .",
        "top"
      );
      dispatch({ type: RESET_STATUS_CREATE_SUBMISSION_CATEGORIES });
    }
  }, [dispatch, statusCreate]);

  return (
    <>
      <Form
        {...formItemLayout}
        onFinish={onFinish}
        layout="vertical"
        style={{ width: "100%" }}
        form={form}
        name="control-hooks"
      >
        <div className="grid grid-cols-12 gap-5">
          <div className="col-span-6">
            <Form.Item
              label="Nama Pengajuan"
              name="name"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <Input />
            </Form.Item>
          </div>
        </div>
        <div className="grid grid-cols-12 pt-10 mt-10 border-t">
          <div className="col-span-12">
            <Form.Item>
              <div className="flex gap-5 items-center justify-start">
                <Link href={routerMenu.CATEGORY_SUBMISSION}>
                  <CButtonAction
                    icon={<ReloadOutlined />}
                    htmlType="button"
                    disabled={statusCreate === "process"}
                    loading={statusCreate === "process"}
                  >
                    Kembali
                  </CButtonAction>
                </Link>
                <CButtonAction
                  icon={<SaveOutlined />}
                  disabled={statusCreate === "process"}
                  loading={statusCreate === "process"}
                  htmlType="submit"
                  variant="primary"
                >
                  Tambah data
                </CButtonAction>
              </div>
            </Form.Item>
          </div>
        </div>
      </Form>
    </>
  );
};

export default FormCreate;
