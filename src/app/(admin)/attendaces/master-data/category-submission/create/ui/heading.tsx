"use client";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data kategori pengajaun"
      description="Menambahkan data kategori pengajaun baru untuk karyawan"
    />
  );
};

export default Heading;
