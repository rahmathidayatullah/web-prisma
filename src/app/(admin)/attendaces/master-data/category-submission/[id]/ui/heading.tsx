"use client";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data kategori pengajuan"
      description="Merubah data kategori pengajuan untuk karyawan"
    />
  );
};

export default Heading;
