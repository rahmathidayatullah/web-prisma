import React from "react";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data kategori pengajuan"
      description="Data informasi pengajuan, seluruh data kategori pengajuan"
    />
  );
};

export default Heading;
