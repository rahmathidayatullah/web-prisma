export interface DataType {
  key: string;
  id: number;
  title1: string | null;
  title2: string | null;
  description1: string | null;
  description2: string | null;
  img1: string | null;
  img2: string | null;
}

export const columns: DataType[] = [
  {
    key: "1",
    id: 1,
    title1: "title1",
    title2: "title2",
    description1: "description1",
    description2: "description2",
    img1: "img1",
    img2: "img2",
  },
  {
    key: "2",
    id: 2,
    title1: "title1",
    title2: "title2",
    description1: "description1",
    description2: "description2",
    img1: "img1",
    img2: "img2",
  },
  {
    key: "3",
    id: 3,
    title1: "title1",
    title2: "title2",
    description1: "description1",
    description2: "description2",
    img1: "img1",
    img2: "img2",
  },
];
