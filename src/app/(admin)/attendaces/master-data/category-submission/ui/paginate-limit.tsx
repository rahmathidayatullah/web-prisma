"use client";
import React from "react";
import { Pagination } from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  CHANGE_LIMIT_SUBMISSION_CATEGORIES,
  CHANGE_PAGE_SUBMISSION_CATEGORIES,
} from "@/app/redux/features/master-data-attendaces/submission-categories/constants";

const PaginateLimit: React.FC = () => {
  const dispatch: any = useDispatch();
  const submissionCategories = useSelector(
    (state: any) => state.submissionCategories
  );
  const { amountOfData, page, take } = submissionCategories;

  return (
    <Pagination
      current={page}
      defaultCurrent={page}
      pageSize={take}
      defaultPageSize={take}
      total={amountOfData}
      onChange={(page: number) =>
        dispatch({ type: CHANGE_PAGE_SUBMISSION_CATEGORIES, value: page })
      }
      onShowSizeChange={(_, size: number) =>
        dispatch({ type: CHANGE_LIMIT_SUBMISSION_CATEGORIES, value: size })
      }
    />
  );
};

export default PaginateLimit;
