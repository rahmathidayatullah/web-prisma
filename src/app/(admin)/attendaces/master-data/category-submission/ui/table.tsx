"use client";
import React, { useEffect, useState } from "react";

// antd
import { Button, Form, Space, Input } from "antd";
import type { DropdownProps, MenuProps, TableProps } from "antd";
import { SearchProps } from "antd/es/input";

// redux
import { useDispatch, useSelector } from "react-redux";

// comopnents
import CTable from "@/app/components/molecules/c-table";
import CSearch from "@/app/components/molecules/c-search";
import CDateRange from "@/app/components/molecules/c-date-range";
import CDropdown from "@/app/components/molecules/c-dropdown";
import CButton from "@/app/components/molecules/c-button";
import CCheckboxGroup from "@/app/components/molecules/c-checkbox-group";
import Link from "next/link";
import { routerMenu } from "@/app/constants/routes";
import "moment/locale/id";
import CDialog from "@/app/components/molecules/c-dialog";
import { PlusOutlined } from "@ant-design/icons";
import { DataType } from "./interface";
import {
  CHANGE_DATE_RANGE_SUBMISSION_CATEGORIES,
  CHANGE_KEYWORD_SUBMISSION_CATEGORIES,
  RESET_DELETE_SUBMISSION_CATEGORIES,
  RESET_STATUS_DELETE_SUBMISSION_CATEGORIES,
} from "@/app/redux/features/master-data-attendaces/submission-categories/constants";
import {
  fetchSubmissionCategories,
  removeSubmissionCategories,
} from "@/app/redux/features/master-data-attendaces/submission-categories/actions";
import { useGlobalContext } from "@/app/context/store";

const UITable: React.FC = () => {
  const { openNotification } = useGlobalContext();
  const dispatch: any = useDispatch();
  const submissionCategories = useSelector(
    (state: any) => state.submissionCategories
  );
  const {
    page,
    take,
    keyword,
    startDate,
    endDate,
    data,
    statusList,

    errorDelete,
    statusDelete,
  } = submissionCategories;

  const [form] = Form.useForm();
  const [idUser, setIdUser] = useState<number | string>("");
  const [openSourceType, setOpenSourceType] = useState(false);
  const [showDialogDelete, setShowDialogDelete] = useState(false);
  const [showDialogDeleteConfirm, setShowDialogDeleteConfirm] = useState(false);

  const columns: TableProps<DataType>["columns"] = [
    {
      title: "Kategori",
      dataIndex: "name",
      key: "name",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Action",
      key: "action",
      render: (_, record) => (
        <div className="flex items-center gap-4">
          <Link href={`${routerMenu.CATEGORY_SUBMISSION}/${record.id}`}>
            <Button
              style={{ backgroundColor: "#219C90" }}
              size="small"
              type="primary"
            >
              Detail
            </Button>
          </Link>
          <Button
            style={{ backgroundColor: "#FF5151", color: "white" }}
            size="small"
            type="default"
            danger
            onClick={() => {
              setShowDialogDelete(true), setIdUser(record.id);
            }}
          >
            Hapus
          </Button>
        </div>
      ),
    },
  ];
  const defaultCheckedList = columns.map((item: any) => item.key as string);
  const [checkedList, setCheckedList] = useState(defaultCheckedList);

  const newColumns = columns.map((item: any) => ({
    ...item,
    hidden: !checkedList.includes(item.key as string),
  }));

  const options = columns.map(({ key, title }: any) => ({
    label: title,
    value: key,
  }));

  const items: MenuProps["items"] = [
    {
      label: (
        <CCheckboxGroup
          value={checkedList}
          options={options}
          onChange={(value: any) => {
            setCheckedList(value as string[]);
          }}
        />
      ),
      key: "0",
    },
  ];

  const onSearch: SearchProps["onSearch"] = (value, _e, info) => {};

  const handleOpenChangeSourceType: DropdownProps["onOpenChange"] = (
    nextOpen,
    info
  ) => {
    if (info.source === "trigger" || nextOpen) {
      setOpenSourceType(nextOpen);
    }
  };

  const onFinishDelete = () => {
    dispatch(removeSubmissionCategories(idUser));
  };

  useEffect(() => {
    dispatch(fetchSubmissionCategories());
    if (statusDelete === "success") {
      setShowDialogDeleteConfirm(false);
      openNotification("topLeft", "Berhasil menghapus data kategori pengajuan");
      dispatch({ type: RESET_DELETE_SUBMISSION_CATEGORIES });
    }
    if (statusDelete === "error") {
      openNotification(
        "topLeft",
        errorDelete?.response?.data?.message ??
          "Terjadi kesalahan, gagal menghapus data kategori pengajuan"
      );
      dispatch({ type: RESET_STATUS_DELETE_SUBMISSION_CATEGORIES });
    }
  }, [dispatch, page, take, keyword, endDate, startDate, statusDelete]);

  return (
    <>
      <div className="flex flex-wrap gap-2 sm:gap-5 items-center justify-between my-5">
        <Space direction="vertical">
          {/* <CSearch
            onSearch={onSearch}
            value={keyword}
            onChange={(event: any) =>
              dispatch({
                type: CHANGE_KEYWORD_SUBMISSION_CATEGORIES,
                value: event.target.value,
              })
            }
          /> */}
        </Space>
        <div className="flex flex-wrap items-center gap-2 sm:gap-5">
          {/* <CDateRange
            onChange={(_: any, dateString: any) =>
              dispatch({
                type: CHANGE_DATE_RANGE_SUBMISSION_CATEGORIES,
                value: dateString,
              })
            }
          /> */}
          <CDropdown
            variant="sortir-coloum"
            menu={{ items }}
            onOpenChange={handleOpenChangeSourceType}
            open={openSourceType}
          />
          <Link href={routerMenu.CREATE_CATEGORY_SUBMISSION}>
            <CButton icon={<PlusOutlined />} variant="primary">
              Tambah Kategori Pengajuan
            </CButton>
          </Link>
        </div>
      </div>
      <CTable
        pagination={false}
        columns={newColumns}
        dataSource={data}
        loading={statusList === "process"}
        scroll={{ x: 1500 }}
        rowKey="id"
      />
      <CDialog
        title="Anda yakin menghapus data katergori pengajuan ?"
        open={showDialogDelete}
        titleActionOk="Ya"
        titleActionCancel="Tidak"
        actionCancel={() => setShowDialogDelete(false)}
        actionOk={() => {
          setShowDialogDeleteConfirm(true);
          setShowDialogDelete(false);
        }}
      ></CDialog>
      <CDialog
        title="Konfirmasi password untuk menghapus data katergori pengajuan ?"
        open={showDialogDeleteConfirm}
        titleActionOk="Setuju"
        titleActionCancel="Tidak"
        actionCancel={() => setShowDialogDeleteConfirm(false)}
        actionOk={onFinishDelete}
        loadingBtn={statusDelete === "process"}
      >
        <Form
          onFinish={onFinishDelete}
          layout="vertical"
          style={{ width: "100%" }}
          form={form}
          name="control-hooks"
        >
          <Form.Item
            label="Password"
            name="password"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <Input.Password />
          </Form.Item>
        </Form>
      </CDialog>
    </>
  );
};

export default UITable;
