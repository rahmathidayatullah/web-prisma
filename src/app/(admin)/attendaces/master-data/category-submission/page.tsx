import React from "react";
import PaginateLimit from "./ui/paginate-limit";
import UITable from "./ui/table";
import Heading from "./ui/heading";

const Page = () => {
  return (
    <>
      <div className="flex flex-col justify-center items-start h-full">
        <div className="w-full">
          <Heading />
        </div>
        <div className="h-full w-full overflow-scroll">
          <UITable />
          <div className="mt-10 flex justify-end">
            <PaginateLimit />
          </div>
        </div>
      </div>
    </>
  );
};

export default Page;
