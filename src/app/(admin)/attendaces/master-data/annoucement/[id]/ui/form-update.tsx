"use client";
import React, { useEffect, useState } from "react";
import { Form, Input, Radio } from "antd";
import { useDispatch, useSelector } from "react-redux";
import CButtonAction from "@/app/components/molecules/c-button-action";
import CDateSingle from "@/app/components/molecules/c-date-single";
import Link from "next/link";
import { routerMenu } from "@/app/constants/routes";
import CUploadSingle from "@/app/components/molecules/c-upload-single";
import { PlusOutlined, ReloadOutlined } from "@ant-design/icons";
import {
  fetchAnnoucementDetail,
  updateAnnoucement,
} from "@/app/redux/features/annoucement/actions";
import dayjs from "dayjs";
import moment from "moment";
import {
  RESET_STATUS_UPDATE_ANNOUCEMENT,
  RESET_UPDATE_ANNOUCEMENT,
} from "@/app/redux/features/annoucement/constans";
import { useRouter } from "next/navigation";
import { useGlobalContext } from "@/app/context/store";
const dateFormat = "YYYY-MM-DD";

const FormCreate = ({ params }: { params: { id: string } }) => {
  const { openMessage } = useGlobalContext();
  const router = useRouter();
  const dispatch: any = useDispatch();
  const [form] = Form.useForm();

  const annoucement = useSelector((state: any) => state.annoucement);
  const { statusUpdate, statusDetail, dataDetail, errorUpdate } = annoucement;

  const [image1, setImage1] = useState("");
  const [dataListImage1, setDataListImage1] = useState<any>([]);
  const handleUploadCompleteImage1 = (base64Data: any) => {
    setImage1(base64Data);
  };
  const [image2, setImage2] = useState("");
  const [dataListImage2, setDataListImage2] = useState<any>([]);
  const handleUploadCompleteImage2 = (base64Data: any) => {
    setImage2(base64Data);
  };

  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };

  const onFinish = (values: any) => {
    const body = {
      ...values,
      image1,
      image2,
      // startDate: moment(startDate).format("YYYY-MM-DD"),
      // endDate: moment(endDate).format("YYYY-MM-DD"),
      startDate: null,
      endDate: null,
      publish: values.publish === "true" ? true : false,
    };
    dispatch(updateAnnoucement(params.id, body));
  };

  const onReset = () => {
    form.resetFields();
    setImage1("");
    setImage2("");
    setStartDate("");
    setEndDate("");
    dispatch({
      type: RESET_UPDATE_ANNOUCEMENT,
    });
  };

  const changePickerStartDate = (date: any, dateString: any) => {
    setStartDate(dateString);
  };
  const changePickerEndDate = (date: any, dateString: any) => {
    setEndDate(dateString);
  };

  const onSetValueDetail = () => {
    Object.keys(dataDetail).forEach((key) => {
      const value = dataDetail[key];
      if (key === "publish") {
        form.setFieldValue(key, `${value}`);
      } else if (key === "startDate") {
        if (value) {
          setStartDate(value);
          form.setFieldValue(
            key,
            dayjs(moment(value).format("YYYY-MM-DD"), dateFormat)
          );
        }
      } else if (key === "endDate") {
        if (value) {
          setEndDate(value);
          form.setFieldValue(
            key,
            dayjs(
              dayjs(moment(value).format("YYYY-MM-DD"), dateFormat),
              dateFormat
            )
          );
        }
      } else if (key === "image1") {
        if (value) {
          setDataListImage1([
            {
              uid: "1",
              name: "foto1.png",
              status: "done",
              url: value,
            },
          ]);
        }
        form.setFieldValue(key, value);
      } else if (key === "image2") {
        if (value) {
          setDataListImage2([
            {
              uid: "1",
              name: "foto2.png",
              status: "done",
              url: value,
            },
          ]);
        }
        form.setFieldValue(key, value);
      } else {
        form.setFieldValue(key, value);
      }
    });
  };

  useEffect(() => {
    if (statusDetail === "success") {
      onSetValueDetail();
    }
    if (statusUpdate === "success") {
      openMessage("success", "Berhasil mengubah data pengumuman .", "top");
      onReset();
      router.push(routerMenu.ANNOUCEMENT);
    }
    if (statusUpdate === "error") {
      openMessage(
        "error",
        errorUpdate?.response?.data?.message ??
          "Gagal mengubah data pengumuman .",
        "top"
      );
      dispatch({
        type: RESET_STATUS_UPDATE_ANNOUCEMENT,
      });
    }
  }, [dispatch, statusDetail, statusUpdate]);

  useEffect(() => {
    dispatch(fetchAnnoucementDetail(params.id));
  }, [dispatch, params.id]);

  return (
    <Form
      {...formItemLayout}
      onFinish={onFinish}
      layout="vertical"
      style={{ width: "100%" }}
      form={form}
      name="control-hooks"
    >
      <div className="grid grid-cols-12 gap-5">
        <div className="col-span-12">
          <Form.Item
            label="Judul"
            name="title1"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <Input.TextArea />
          </Form.Item>

          {/* <Form.Item
            label="Judul 2"
            name="title2"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <Input.TextArea />
          </Form.Item> */}
        </div>
        <div className="col-span-12">
          <Form.Item
            label="Deskripsi"
            name="description1"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <Input.TextArea rows={5} />
          </Form.Item>

          {/* <Form.Item
            label="Deskripsi 2"
            name="description2"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <Input.TextArea rows={5} />
          </Form.Item> */}
        </div>
        {/* <div className="col-span-12">
          <Form.Item
            label="Mulai Ditampilkan"
            name="startDate"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <CDateSingle onChange={changePickerStartDate} />
          </Form.Item>
          <Form.Item
            label="Berakhir Ditampilkan"
            name="endDate"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <CDateSingle onChange={changePickerEndDate} />
          </Form.Item>
        </div> */}

        <div className="col-span-12">
          <div className="grid grid-cols-12 gap-5">
            <div className="col-span-12">
              <Form.Item
                label="Foto"
                name="image1"
                rules={[{ required: true, message: "Please input!" }]}
              >
                <div className="border rounded-md">
                  <CUploadSingle
                    onChange={handleUploadCompleteImage1}
                    dataFileList={dataListImage1}
                  />
                </div>
              </Form.Item>
            </div>
            {/* <div className="col-span-12">
              <Form.Item
                label="Foto 2"
                name="image2"
                rules={[{ required: true, message: "Please input!" }]}
              >
                <div className="border rounded-md">
                  <CUploadSingle
                    onChange={handleUploadCompleteImage2}
                    dataFileList={dataListImage2}
                  />
                </div>
              </Form.Item>
            </div> */}
            <div className="col-span-12">
              <Form.Item
                label="Tampilkan/tidak"
                name="publish"
                rules={[{ required: true, message: "Please input!" }]}
              >
                <Radio.Group>
                  <Radio value="true">Tampilkan</Radio>
                  <Radio value="false">Sembuyikan</Radio>
                </Radio.Group>
              </Form.Item>
            </div>
          </div>
        </div>
      </div>
      <div className="grid grid-cols-12 pt-10 mt-10 border-t">
        <div className="col-span-12">
          <Form.Item>
            <div className="flex gap-5 items-center justify-end">
              <Link href={routerMenu.ANNOUCEMENT}>
                <CButtonAction
                  icon={<ReloadOutlined />}
                  htmlType="button"
                  disabled={statusUpdate === "process"}
                  loading={statusUpdate === "process"}
                >
                  Kembali
                </CButtonAction>
              </Link>

              <CButtonAction
                icon={<PlusOutlined />}
                disabled={statusUpdate === "process"}
                loading={statusUpdate === "process"}
                htmlType="submit"
                variant="primary"
              >
                Ubah data
              </CButtonAction>
            </div>
          </Form.Item>
        </div>
      </div>
    </Form>
  );
};

export default FormCreate;
