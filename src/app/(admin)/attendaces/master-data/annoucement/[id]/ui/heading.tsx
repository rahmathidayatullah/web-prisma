import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data pengumuman"
      description="Mengubah data pengumuman, judul, deskripsi dan foto"
    />
  );
};

export default Heading;
