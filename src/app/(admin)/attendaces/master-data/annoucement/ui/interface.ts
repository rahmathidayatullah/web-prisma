export interface DataType {
  key: string;
  id: number;
  title1: string | null;
  title2: string | null;
  description1: string | null;
  description2: string | null;
  startDate: string | null;
  endDate: string | null;
  image1: string | null;
  image2: string | null;
  publish: boolean | null;
}

export const columns: DataType[] = [
  // {
  //   key: "1",
  //   id: 1,
  //   title1: "title1",
  //   title2: "title2",
  //   startDate: "description1",
  //   description2: "description2",
  //   image1: "image1",
  //   image2: "image2",
  // },
  // {
  //   key: "2",
  //   id: 2,
  //   title1: "title1",
  //   title2: "title2",
  //   description1: "description1",
  //   description2: "description2",
  //   image1: "image1",
  //   image2: "image2",
  // },
  // {
  //   key: "3",
  //   id: 3,
  //   title1: "title1",
  //   title2: "title2",
  //   description1: "description1",
  //   description2: "description2",
  //   image1: "image1",
  //   image2: "image2",
  // },
];
