import React from "react";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data pengumuman"
      description="Data informasi pengumuman, seluruh data pengumuman"
    />
  );
};

export default Heading;
