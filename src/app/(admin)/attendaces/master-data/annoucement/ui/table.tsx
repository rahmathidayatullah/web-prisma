"use client";
import React, { useEffect, useState } from "react";

// antd
import { Button, Form, Space, Input, notification, Tag } from "antd";
import type { DropdownProps, MenuProps, TableProps } from "antd";
import { Image as ImgAntd } from "antd";
import { SearchProps } from "antd/es/input";

// import { DataType } from "./interface/table";

// redux
import { useDispatch, useSelector } from "react-redux";

// comopnents
import CTable from "@/app/components/molecules/c-table";
import CSearch from "@/app/components/molecules/c-search";
import CDateRange from "@/app/components/molecules/c-date-range";
import CDropdown from "@/app/components/molecules/c-dropdown";
import CButton from "@/app/components/molecules/c-button";
import CCheckboxGroup from "@/app/components/molecules/c-checkbox-group";
import Link from "next/link";
import { routerMenu } from "@/app/constants/routes";
import moment from "moment";
import "moment/locale/id";
import CDialog from "@/app/components/molecules/c-dialog";
import { DownloadOutlined, PlusOutlined } from "@ant-design/icons";
import { DataType, columns } from "./interface";
import {
  CHANGE_DATE_RANGE,
  CHANGE_KEYWORD,
  RESET_DELETE_ANNOUCEMENT,
  RESET_STATUS_DELETE_ANNOUCEMENT,
} from "@/app/redux/features/annoucement/constans";
import {
  fetchAnnoucements,
  removeAnnoucement,
} from "@/app/redux/features/annoucement/actions";
import { useGlobalContext } from "@/app/context/store";

const UITable: React.FC = () => {
  const { openNotification } = useGlobalContext();
  const dispatch: any = useDispatch();
  const annoucement = useSelector((state: any) => state.annoucement);
  const {
    page,
    take,
    keyword,
    startDate,
    endDate,
    statusList,
    dataList,
    //
    errorDelete,
    statusDelete,
  } = annoucement;

  const [form] = Form.useForm();
  const [idUser, setIdUser] = useState<number | string>("");
  const [openSourceType, setOpenSourceType] = useState(false);
  const [showDialogDelete, setShowDialogDelete] = useState(false);
  const [showDialogDeleteConfirm, setShowDialogDeleteConfirm] = useState(false);

  const columns: TableProps<DataType>["columns"] = [
    {
      title: "Foto",
      dataIndex: "image1",
      key: "image1",
      render: (value) => {
        if (value) {
          return (
            <ImgAntd width={100} height={100} src="error" fallback={value} />
          );
        }
        return "-";
      },
    },
    {
      title: "Judul",
      dataIndex: "title1",
      key: "title1",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    // {
    //   title: "Judul 2",
    //   dataIndex: "title2",
    //   key: "title2",
    //   render: (value) => {
    //     if (value) {
    //       return value;
    //     }
    //     return "-";
    //   },
    // },
    {
      title: "Deskripsi",
      dataIndex: "description1",
      key: "description1",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    // {
    //   title: "Deskripsi 2",
    //   dataIndex: "description2",
    //   key: "description1",
    //   render: (value) => {
    //     if (value) {
    //       return value;
    //     }
    //     return "-";
    //   },
    // },

    // {
    //   title: "Foto 2",
    //   dataIndex: "image2",
    //   key: "image2",
    //   render: (value) => {
    //     if (value) {
    //       return (
    //         <ImgAntd width={100} height={100} src="error" fallback={value} />
    //       );
    //     }
    //     return "-";
    //   },
    // },
    // {
    //   title: "Tanggal mulai",
    //   dataIndex: "startDate",
    //   key: "startDate",
    //   render: (value) => {
    //     if (value) {
    //       return moment(value, "YYYY-MM-DD").format("DD MMMM YYYY");
    //     }
    //     return "-";
    //   },
    // },
    // {
    //   title: "Tanggal berakhir",
    //   dataIndex: "endDate",
    //   key: "endDate",
    //   render: (value) => {
    //     if (value) {
    //       return moment(value, "YYYY-MM-DD").format("DD MMMM YYYY");
    //     }
    //     return "-";
    //   },
    // },
    {
      title: "Terbitkan",
      dataIndex: "publish",
      key: "publish",
      render: (value) => {
        if (value === true || value === false) {
          return (
            <Tag color={value ? "green" : "red"}>{value ? "Ya" : "Tidak"}</Tag>
          );
        }
        return "-";
      },
    },
    {
      title: "Action",
      key: "action",
      render: (_, record) => (
        <div className="flex items-center gap-4">
          <Link href={`${routerMenu.ANNOUCEMENT}/${record.id}`}>
            <Button
              style={{ backgroundColor: "#219C90" }}
              size="small"
              type="primary"
            >
              Edit
            </Button>
          </Link>
          <Button
            style={{ backgroundColor: "#FF5151", color: "white" }}
            size="small"
            type="default"
            danger
            onClick={() => {
              setShowDialogDelete(true), setIdUser(record.id);
              form.resetFields();
            }}
          >
            Hapus
          </Button>
        </div>
      ),
    },
  ];
  const defaultCheckedList = columns.map((item: any) => item.key as string);
  const [checkedList, setCheckedList] = useState(defaultCheckedList);

  const newColumns = columns.map((item: any) => ({
    ...item,
    hidden: !checkedList.includes(item.key as string),
  }));

  const options = columns.map(({ key, title }: any) => ({
    label: title,
    value: key,
  }));

  const items: MenuProps["items"] = [
    {
      label: (
        <CCheckboxGroup
          value={checkedList}
          options={options}
          onChange={(value: any) => {
            setCheckedList(value as string[]);
          }}
        />
      ),
      key: "0",
    },
  ];

  const onSearch: SearchProps["onSearch"] = (value, _e, info) => {};

  const handleOpenChangeSourceType: DropdownProps["onOpenChange"] = (
    nextOpen,
    info
  ) => {
    if (info.source === "trigger" || nextOpen) {
      setOpenSourceType(nextOpen);
    }
  };

  const onFinishDelete = () => {
    dispatch(removeAnnoucement(idUser));
  };

  useEffect(() => {
    dispatch(fetchAnnoucements());

    if (statusDelete === "success") {
      setShowDialogDeleteConfirm(false);
      openNotification(
        "topLeft",
        "Berhasil",
        "Data pengummuman berhasil dihapus"
      );
      dispatch({ type: RESET_DELETE_ANNOUCEMENT });
    }
    if (statusDelete === "error") {
      openNotification(
        "topLeft",
        "Terjadi kesalahan",
        errorDelete?.response?.data?.message ??
          "Gagal menghapus data pengumuman"
      );
      dispatch({ type: RESET_STATUS_DELETE_ANNOUCEMENT });
    }
  }, [dispatch, page, take, keyword, endDate, startDate, statusDelete]);
  return (
    <>
      <div className="flex flex-wrap gap-2 sm:gap-5 items-center justify-between my-5">
        <Space direction="vertical">
          {/* <CSearch
            onSearch={onSearch}
            value={keyword}
            onChange={(event: any) =>
              dispatch({ type: CHANGE_KEYWORD, value: event.target.value })
            }
          /> */}
        </Space>
        <div className="flex flex-wrap items-center gap-2 sm:gap-5">
          {/* <CDateRange
            onChange={(_: any, dateString: any) =>
              dispatch({ type: CHANGE_DATE_RANGE, value: dateString })
            }
          /> */}
          <CDropdown
            variant="sortir-coloum"
            menu={{ items }}
            onOpenChange={handleOpenChangeSourceType}
            open={openSourceType}
          />
          <Link href={routerMenu.CREATE_ANNOUCEMENT}>
            <CButton icon={<PlusOutlined />} variant="primary">
              Tambah Pengumuman
            </CButton>
          </Link>
        </div>
      </div>
      <CTable
        pagination={false}
        columns={newColumns}
        dataSource={dataList}
        loading={statusList === "process"}
        scroll={{ x: 2300 }}
        rowKey="id"
      />
      <CDialog
        title="Anda yakin menghapus data karyawan ?"
        open={showDialogDelete}
        titleActionOk="Ya"
        titleActionCancel="Tidak"
        actionCancel={() => setShowDialogDelete(false)}
        actionOk={() => {
          setShowDialogDeleteConfirm(true);
          setShowDialogDelete(false);
        }}
      ></CDialog>
      <CDialog
        title="Konfirmasi password untuk menghapus data karyawan ?"
        open={showDialogDeleteConfirm}
        titleActionOk="Setuju"
        titleActionCancel="Tidak"
        actionCancel={() => setShowDialogDeleteConfirm(false)}
        actionOk={onFinishDelete}
        loadingBtn={statusDelete === "process"}
      >
        <Form
          onFinish={onFinishDelete}
          layout="vertical"
          style={{ width: "100%" }}
          form={form}
          name="control-hooks"
        >
          <Form.Item
            label="Password"
            name="password"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <Input.Password />
          </Form.Item>
        </Form>
      </CDialog>
    </>
  );
};

export default UITable;
