import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data pengumuman"
      description="Menambahkan data pengumuman baru, judul, deskripsi dan foto"
    />
  );
};

export default Heading;
