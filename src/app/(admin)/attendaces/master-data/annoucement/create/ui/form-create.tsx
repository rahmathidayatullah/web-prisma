"use client";
import React, { useEffect, useState } from "react";
import { Form, Input, Radio } from "antd";
import { useDispatch, useSelector } from "react-redux";
import CButtonAction from "@/app/components/molecules/c-button-action";
import CDateSingle from "@/app/components/molecules/c-date-single";
import Link from "next/link";
import { routerMenu } from "@/app/constants/routes";
import CUploadSingle from "@/app/components/molecules/c-upload-single";
import { PlusOutlined, ReloadOutlined } from "@ant-design/icons";
import { useRouter } from "next/navigation";
import {
  RESET_CREATE_ANNOUCEMENT,
  RESET_STATUS_CREATE_ANNOUCEMENT,
} from "@/app/redux/features/annoucement/constans";
import { createAnnoucement } from "@/app/redux/features/annoucement/actions";
import moment from "moment";
import { useGlobalContext } from "@/app/context/store";

const FormCreate = () => {
  const { openMessage } = useGlobalContext();
  const router = useRouter();
  const dispatch: any = useDispatch();
  const [form] = Form.useForm();

  const annoucement = useSelector((state: any) => state.annoucement);
  const { errorCreate, statusCreate } = annoucement;

  const [image1, setImage1] = useState("");
  const handleUploadCompleteImage1 = (base64Data: any) => {
    setImage1(base64Data);
  };
  const [image2, setImage2] = useState("");
  const handleUploadCompleteImage2 = (base64Data: any) => {
    setImage2(base64Data);
  };

  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
    wrapperCol: {
      xs: { span: 24 },
      sm: { span: 12 },
    },
  };

  const onFinish = (values: any) => {
    const body = {
      ...values,
      image1,
      image2,
      // startDate: moment(startDate).format("YYYY-MM-DD"),
      // endDate: moment(endDate).format("YYYY-MM-DD"),
      startDate: null,
      endDate: null,
      publish: values.publish === "true" ? true : false,
    };
    dispatch(createAnnoucement(body));
  };

  const onReset = () => {
    form.resetFields();
    setImage1("");
    setImage2("");
    setStartDate("");
    setEndDate("");
    dispatch({
      type: RESET_CREATE_ANNOUCEMENT,
    });
  };

  const changePickerStartDate = (date: any, dateString: any) => {
    setStartDate(dateString);
  };
  const changePickerEndDate = (date: any, dateString: any) => {
    setEndDate(dateString);
  };

  useEffect(() => {
    if (statusCreate === "success") {
      openMessage("success", "Berhasil menambahkan pengumuman baru .", "top");
      onReset();
      router.push(routerMenu.ANNOUCEMENT);
    }
    if (statusCreate === "error") {
      openMessage(
        "error",
        errorCreate?.response?.data?.message ??
          "Gagal menambahkan pengumuman baru .",
        "top"
      );

      dispatch({
        type: RESET_STATUS_CREATE_ANNOUCEMENT,
      });
    }
  }, [dispatch, statusCreate]);

  return (
    <Form
      {...formItemLayout}
      onFinish={onFinish}
      layout="vertical"
      style={{ width: "100%" }}
      form={form}
      name="control-hooks"
    >
      <div className="grid grid-cols-12 gap-5">
        <div className="col-span-12">
          <Form.Item
            label="Judul"
            name="title1"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <Input.TextArea />
          </Form.Item>

          {/* <Form.Item
            label="Judul 2"
            name="title2"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <Input.TextArea />
          </Form.Item> */}
        </div>
        <div className="col-span-12">
          <Form.Item
            label="Deskripsi"
            name="description1"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <Input.TextArea rows={5} />
          </Form.Item>

          {/* <Form.Item
            label="Deskripsi 2"
            name="description2"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <Input.TextArea rows={5} />
          </Form.Item> */}
        </div>
        {/* <div className="col-span-12">
          <Form.Item
            label="Mulai Ditampilkan"
            name="startDate"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <CDateSingle onChange={changePickerStartDate} />
          </Form.Item>
          <Form.Item
            label="Berakhir Ditampilkan"
            name="endDate"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <CDateSingle onChange={changePickerEndDate} />
          </Form.Item>
        </div> */}

        <div className="col-span-12">
          <div className="grid grid-cols-12 gap-5">
            <div className="col-span-12">
              <Form.Item
                label="Foto"
                name="image1"
                rules={[{ required: true, message: "Please input!" }]}
              >
                <div className="border rounded-md">
                  <CUploadSingle onChange={handleUploadCompleteImage1} />
                </div>
              </Form.Item>
            </div>
            {/* <div className="col-span-12">
              <Form.Item
                label="Foto 2"
                name="image2"
                rules={[{ required: true, message: "Please input!" }]}
              >
                <div className="border rounded-md">
                  <CUploadSingle onChange={handleUploadCompleteImage2} />
                </div>
              </Form.Item>
            </div> */}
            <div className="col-span-12">
              <Form.Item
                label="Tampilkan/tidak"
                name="publish"
                rules={[{ required: true, message: "Please input!" }]}
              >
                <div className="border rounded-md px-2">
                  <Radio.Group>
                    <Radio value="true">Tampilkan</Radio>
                    <Radio value="false">Sembuyikan</Radio>
                  </Radio.Group>
                </div>
              </Form.Item>
            </div>
          </div>
        </div>
      </div>
      <div className="grid grid-cols-12 pt-10 mt-10 border-t">
        <div className="col-span-12">
          <Form.Item>
            <div className="flex gap-5 items-center justify-end">
              <Link href={routerMenu.ANNOUCEMENT}>
                <CButtonAction
                  icon={<ReloadOutlined />}
                  htmlType="button"
                  disabled={statusCreate === "process"}
                  loading={statusCreate === "process"}
                >
                  Kembali
                </CButtonAction>
              </Link>

              <CButtonAction
                icon={<PlusOutlined />}
                disabled={statusCreate === "process"}
                loading={statusCreate === "process"}
                htmlType="submit"
                variant="primary"
              >
                Tambah data
              </CButtonAction>
            </div>
          </Form.Item>
        </div>
      </div>
    </Form>
  );
};

export default FormCreate;
