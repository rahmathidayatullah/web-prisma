export interface DataType {
  key: string;
  user: {
    name: string | null;
    role: {
      name: string | null;
    };
  };
  byAction: {
    status: string | null;
    name: string | null;
    editBy: string | null;
  };
  clockIn: string | null;
  clockOut: string | null;
  date: string | null;
  clockInPhoto: string | null;
  clockOutPhoto: string | null;
  descriptionIn: string | null;
  descriptionOut: string | null;
  totalLate: string | null;
  workStatus: string | null;
  pointlocationClockIn: string | null;
  pointlocationClockOut: string | null;
  action: any;
}

export const dataTable: DataType[] = [
  // {
  //   key: "1",
  //   name: "Rahmat",
  //   role: 32,
  //   status: "Pending",
  //   clockIn: "08:00",
  //   clockOut: "17:00",
  //   date: "01 01 2024",
  //   foto_selfie: "Image",
  //   action: "Image",
  // },
  // {
  //   key: "2",
  //   name: "Rahmat",
  //   role: 32,
  //   status: "Pending",
  //   clockIn: "08:00",
  //   clockOut: "17:00",
  //   date: "01 01 2024",
  //   foto_selfie: "Image",
  //   action: "Image",
  // },
  // {
  //   key: "3",
  //   name: "Rahmat",
  //   role: 32,
  //   status: "Pending",
  //   clockIn: "08:00",
  //   clockOut: "17:00",
  //   date: "01 01 2024",
  //   foto_selfie: "Image",
  //   action: "Image",
  // },
];
