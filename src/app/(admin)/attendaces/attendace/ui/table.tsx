"use client";

import React, { useEffect, useState } from "react";
import {
  Checkbox,
  Space,
  Table,
  Button,
  CheckboxOptionType,
  DropdownProps,
  MenuProps,
  Tag,
} from "antd";
import type { TableColumnsType } from "antd";
import { Image as ImgAntd } from "antd";
import type { SearchProps } from "antd/es/input/Search";
import { DownloadOutlined } from "@ant-design/icons";

import { DataType } from "./interface";
import moment from "moment";
import "moment/locale/id";
import Link from "next/link";

// redux
import { useDispatch, useSelector } from "react-redux";
import {
  attendacesApprove,
  fetchAttendaces,
  fetchAttendacesExports,
} from "@/app/redux/features/attendace/actions";
import {
  CHANGE_DATE_RANGE,
  CHANGE_KEYWORD,
  RESET_EXPORT_ATTENDACE,
  RESET_STATUS_EXPORT_ATTENDACE,
  RESET_STATUS_REJECT_APPROVE,
} from "@/app/redux/features/attendace/constans";

// componets
import CSearch from "@/app/components/molecules/c-search";
import CDateRange from "@/app/components/molecules/c-date-range";
import CDropdown from "@/app/components/molecules/c-dropdown";
import CButton from "@/app/components/molecules/c-button";
import CDialog from "@/app/components/molecules/c-dialog";
import { routerMenu } from "@/app/constants/routes";
import { useGlobalContext } from "@/app/context/store";
import { openNewTab } from "@/app/utils/constants";
import CSelect from "@/app/components/molecules/c-select";
import { SELECT_DIVISI } from "@/app/redux/features/division/constants";
import { fetchDivisionAll } from "@/app/redux/features/division/actions";
import { useSessionStorage } from "@/app/hooks/useSessionStorage";
// import { generateToken, messaging } from "@/app/notifications/firebase";
// import { getMessaging, getToken, onMessage } from "firebase/messaging";
// import { initializeApp } from "firebase/app";
// import {
//   firebaseConfig,
//   generateToken,
//   messaging,
// } from "@/app/notifications/firebase";

const UITable: React.FC = () => {
  const { openMessage } = useGlobalContext();
  const dispatch: any = useDispatch();
  const attendaces = useSelector((state: any) => state.attendace);

  const {
    statusApprove,
    statusReject,
    statusListAttendace,
    page,
    take,
    keyword,
    endDate,
    startDate,
    //
    dataExport,
    errorExport,
    statusExport,
  } = attendaces;

  const divisi = useSelector((state: any) => state.divisi);
  const { dataAll: dataDivisiAll, selectDivisi } = divisi;

  const [openSourceType, setOpenSourceType] = useState(false);
  const [open, setOpen] = useState(false);

  const handleOpenChangeSourceType: DropdownProps["onOpenChange"] = (
    nextOpen,
    info
  ) => {
    if (info.source === "trigger" || nextOpen) {
      setOpenSourceType(nextOpen);
    }
  };
  const coloumTable: TableColumnsType<DataType> = [
    {
      title: "Nama",
      dataIndex: "user",
      key: "1",
      render: (value) => {
        if (value) {
          return value.name;
        }
        return "-";
      },
    },
    {
      title: "Jabatan",
      dataIndex: "user",
      key: "2",
      render: (value) => {
        if (value) {
          return value.role.name;
        }
        return "-";
      },
    },
    {
      title: "Absen Masuk",
      dataIndex: "clockIn",
      key: "3",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Absen Keluar",
      dataIndex: "clockOut",
      key: "4",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Shift",
      dataIndex: "shift",
      key: "shift",
      render: (value) => {
        if (value) {
          return (
            <span>
              {value.in} - {value.out}
            </span>
          );
        }
        return "-";
      },
    },
    {
      title: "Terlambat",
      dataIndex: "totalLate",
      key: "5",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Date",
      dataIndex: "date",
      key: "6",
      render: (value) => {
        if (value) {
          return moment(value).format("DD MMMM YYYY");
        }
        return "-";
      },
    },
    {
      title: "Foto Absen Masuk",
      dataIndex: "clockInPhoto",
      key: "7",
      render: (value) => {
        if (value) {
          return (
            <ImgAntd width={100} height={100} src="error" fallback={value} />
          );
        }
        return "-";
      },
    },
    {
      title: "Foto Absen Keluar",
      dataIndex: "clockOutPhoto",
      key: "8",
      render: (value) => {
        if (value) {
          return (
            <ImgAntd width={100} height={100} src="error" fallback={value} />
          );
        }
        return "-";
      },
    },
    {
      title: "Deskripsi Masuk",
      dataIndex: "descriptionIn",
      key: "9",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Deskripsi Keluar",
      dataIndex: "descriptionOut",
      key: "10",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Status Kerja",
      dataIndex: "workStatus",
      key: "11",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Lokasi Absen Masuk",
      dataIndex: "pointlocationClockIn",
      key: "pointlocationClockIn",
      render: (value) => {
        if (Boolean(value)) {
          return (
            <Link
              href={`https://www.google.com/maps/search/?api=1&query=${value}`}
              rel="noopener noreferrer"
              target="_blank"
            >
              Lihat lokasi
            </Link>
          );
        }
        return "Tidak ada lokasi";
      },
    },
    {
      title: "Lokasi Absen Keluar",
      dataIndex: "pointlocationClockOut",
      key: "pointlocationClockOut",
      render: (value) => {
        if (Boolean(value)) {
          return (
            <Link
              href={`https://www.google.com/maps/search/?api=1&query=${value}`}
              rel="noopener noreferrer"
              target="_blank"
            >
              Lihat lokasi
            </Link>
          );
        }
        return "Tidak ada lokasi";
      },
    },
    {
      title: "Status",
      dataIndex: "byAction",
      key: "13",
      render: (value) => {
        if (value) {
          return (
            <div className="flex flex-col items-start justify-center gap-2">
              <div className="flex items-center gap-2">
                <Tag
                  color={
                    value.status === "Approve"
                      ? "green"
                      : value.status === "Reject"
                      ? "red"
                      : "orange"
                  }
                >
                  {value.status}
                </Tag>
                {value.status === "Approve" || value.status === "Reject" ? (
                  <Tag color="red">{value.name ?? "-"}</Tag>
                ) : (
                  ""
                )}
              </div>
              {value.editBy ? (
                <div className="flex items-center gap-2">
                  <Tag color="blue">Edited</Tag>
                  <Tag color="blue">{value.editBy ?? "-"}</Tag>
                </div>
              ) : (
                ""
              )}
            </div>
          );
        }
        return "-";
      },
    },
    {
      title: "Aksi",
      key: "14",
      render: (_, record: any) => {
        return (
          <div className="flex items-center gap-4">
            <Link href={`${routerMenu.ATTENDACE}/${record.id}`}>
              <Button
                style={{ backgroundColor: "#FBB03B" }}
                size="small"
                type="primary"
              >
                Edit
              </Button>
            </Link>
            <Button
              style={{ backgroundColor: "#219C90" }}
              size="small"
              type="primary"
              loading={
                statusApprove === "process" || statusReject === "process"
              }
              disabled={
                statusApprove === "process" || statusReject === "process"
              }
              onClick={() => {
                setId(record.id),
                  // setOpen(true);
                  setAction("approve");
                approveSubmission(record.id, "approve");
              }}
            >
              Approve
            </Button>
            <Button
              style={{ backgroundColor: "#FF5151", color: "white" }}
              size="small"
              type="default"
              danger
              onClick={() => {
                setId(record.id),
                  // setOpen(true);
                  setAction("reject");
                approveSubmission(record.id, "reject");
              }}
            >
              Reject
            </Button>
          </div>
        );
      },
    },
  ];

  const [id, setId] = useState("");
  const [action, setAction] = useState("");

  const defaultCheckedList = coloumTable.map((item: any) => item.key as string);

  const [checkedList, setCheckedList] = useSessionStorage<string[]>(
    "data-attendace",
    defaultCheckedList
  );

  const newColumns = coloumTable.map((item: any) => ({
    ...item,
    hidden: !checkedList.includes(item.key as string),
  }));

  const options = coloumTable.map(({ key, title }: any) => ({
    label: title,
    value: key,
  }));

  const items: MenuProps["items"] = [
    {
      label: (
        <Checkbox.Group
          className="flex flex-col gap-3"
          value={checkedList}
          options={options as CheckboxOptionType[]}
          onChange={(value) => {
            setCheckedList(value as string[]);
          }}
        />
      ),
      key: "0",
    },
  ];

  const onSearch: SearchProps["onSearch"] = (value, _e, info) =>
    console.log(info?.source, value);

  const hideModal = () => {
    setOpen(false);
  };

  const approveSubmission = (paramId: any, paramAction: any) => {
    // if (action === "approve") {
    if (paramAction === "approve") {
      dispatch(
        attendacesApprove(paramId, {
          status: "Approve",
        })
      );
    } else {
      dispatch(
        attendacesApprove(paramId, {
          status: "Reject",
        })
      );
    }
  };

  const onChangeDivisi = (e: any) => {
    dispatch({
      type: SELECT_DIVISI,
      value: e,
    });
  };

  useEffect(() => {
    dispatch(fetchAttendaces());
    dispatch(fetchDivisionAll());
    return () => {
      dispatch({
        type: RESET_STATUS_REJECT_APPROVE,
      });
    };
  }, [dispatch, page, take, keyword, endDate, startDate, selectDivisi]);

  useEffect(() => {
    if (statusApprove === "success") {
      setOpen(false);
      openMessage("success", `Data berhasil di ${action}`, "top");
      dispatch({
        type: RESET_STATUS_REJECT_APPROVE,
      });
    }
    if (statusReject === "success") {
      setOpen(false);
      openMessage("success", `Data berhasil di ${action}`, "top");
      dispatch({
        type: RESET_STATUS_REJECT_APPROVE,
      });
    }
    if (statusExport === "success") {
      openNewTab(dataExport.fileUrl);
      dispatch({ type: RESET_EXPORT_ATTENDACE });
    }
    if (statusExport === "error") {
      openMessage(
        "error",
        errorExport?.response?.data?.message ?? "Terjadi kesalahan",
        "top"
      );
      dispatch({ type: RESET_STATUS_EXPORT_ATTENDACE });
    }
  }, [dispatch, statusReject, statusApprove, statusExport]);

  // useEffect(() => {
  //   // console.log("12312321");
  //   // // Initialize Firebase
  //   // const app = initializeApp(firebaseConfig);
  //   // // const analytics = getAnalytics(app);
  //   // // Initialize Firebase Cloud Messaging and get a reference to the service
  //   // const messaging = getMessaging(app);
  //   // async () => {
  //   //   const permission = await Notification.requestPermission();
  //   //   console.log("perrmisssion", permission);
  //   //   if (permission === "granted") {
  //   //     const token = await getToken(messaging, {
  //   //       vapidKey: process.env.NEXT_PUBLIC_DB_FIREBASE_VAPID_KEY,
  //   //     });
  //   //     console.log("token", token);
  //   //   }
  //   // };
  //   generateToken();
  //   onMessage(messaging, (payload) => {
  //     console.log("payload", payload);
  //     success(payload.notification?.title);
  //     dispatch(fetchAttendaces());
  //   });
  // }, []);

  return (
    <>
      <div className="flex flex-wrap gap-2 sm:gap-5 items-center justify-between my-5">
        <Space direction="horizontal">
          <CSearch
            onSearch={onSearch}
            value={keyword}
            onChange={(event: any) =>
              dispatch({ type: CHANGE_KEYWORD, value: event.target.value })
            }
          />

          <CSelect
            dataOption={dataDivisiAll}
            value={selectDivisi}
            disabled={!dataDivisiAll.length}
            placeholder="Filter by divisi"
            onChange={onChangeDivisi}
          />
        </Space>
        <div className="flex flex-wrap items-center gap-2 sm:gap-5">
          <CDateRange
            onChange={(_: any, dateString: any) =>
              dispatch({ type: CHANGE_DATE_RANGE, value: dateString })
            }
          />
          <CDropdown
            variant="sortir-coloum"
            menu={{ items }}
            onOpenChange={handleOpenChangeSourceType}
            open={openSourceType}
          />
          <CButton
            icon={<DownloadOutlined />}
            variant="primary"
            onClick={() => dispatch(fetchAttendacesExports())}
            loading={statusExport === "process"}
            disabled={statusExport === "process"}
          >
            Export Data
          </CButton>
        </div>
      </div>

      <Table
        pagination={false}
        columns={newColumns}
        dataSource={attendaces.dataAttendace}
        style={{ marginTop: 24 }}
        loading={statusListAttendace === "process"}
        scroll={{ x: 2500 }}
        rowKey="id"
      />
      {/* <CDialog
        title="Anda yakin ingin melakukan aksi ini ?"
        open={open}
        titleActionOk="Ya"
        titleActionCancel="Tidak"
        actionCancel={hideModal}
        actionOk={approveSubmission}
        loadingBtn={statusApprove === "process" || statusReject === "process"}
      ></CDialog> */}
    </>
  );
};

export default UITable;
