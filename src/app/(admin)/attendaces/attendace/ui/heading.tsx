import React from "react";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data absensi"
      description="Data informasi absensi, dari seluruh kegiatan karyawan"
    />
  );
};

export default Heading;
