"use client";
import CHeading from "@/app/components/molecules/c-heading";
import { useSelector } from "react-redux";

const Heading = () => {
  const attendace = useSelector((state: any) => state.attendace);
  const { dataDetail } = attendace;

  return (
    <CHeading
      title={`Informasi data absensi karyawan ${dataDetail?.user?.name ?? "-"}`}
      description={`Data detail absensi karyawan, ubah data absensi karyawan ${
        dataDetail?.user?.name ?? "-"
      }`}
    />
  );
};

export default Heading;
