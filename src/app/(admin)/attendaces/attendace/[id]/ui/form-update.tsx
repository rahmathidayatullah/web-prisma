"use client";

import React, { useEffect, useState } from "react";

import { useRouter } from "next/navigation";
import { formItemLayout } from "./interface";

import dayjs from "dayjs";

import { useDispatch, useSelector } from "react-redux";
import {
  fetchAttendacesDetail,
  updateAttendaces,
} from "@/app/redux/features/attendace/actions";

import { TimePicker, Input, Form } from "antd";
import CUploadSingle from "@/app/components/molecules/c-upload-single";
import Link from "next/link";
import CButtonAction from "@/app/components/molecules/c-button-action";
import { SaveOutlined, ReloadOutlined } from "@ant-design/icons";
import { routerMenu } from "@/app/constants/routes";
import {
  RESET_STATUS_UPDATE_ATTENDACE,
  RESET_UPDATE_ATTENDACE,
} from "@/app/redux/features/attendace/constans";
import { useGlobalContext } from "@/app/context/store";
import CDialog from "@/app/components/molecules/c-dialog";
const dateFormat = "HH:mm:ss";

const FormUpdate = ({ params }: { params: { id: string } }) => {
  const { openMessage } = useGlobalContext();
  const router = useRouter();
  const dispatch: any = useDispatch();
  const [form] = Form.useForm();
  const [formConfirm] = Form.useForm();

  const attendace = useSelector((state: any) => state.attendace);

  const { statusUpdate, statusDetail, dataDetail } = attendace;

  const [clockInPhoto, setClockInPhoto] = useState("");
  const [dataClockInPhoto, setDataClockInPhoto] = useState<any>([]);
  const handleUploadClockInPhoto = (base64Data: any) => {
    setClockInPhoto(base64Data);
  };

  const [clockOutPhoto, setClockOutPhoto] = useState("");
  const [dataClockOutPhoto, setDataClockOutPhoto] = useState<any>([]);
  const handleUploadClockOutPhoto = (base64Data: any) => {
    setClockOutPhoto(base64Data);
  };

  const [clockIn, setClockIn] = useState("");
  const [clockOut, setClockOut] = useState("");

  const onReset = () => {
    form.resetFields();
    setClockInPhoto("");
    setDataClockInPhoto("");
    setClockOutPhoto("");
    setDataClockOutPhoto("");
    setClockIn("");
    setClockOut("");
    dispatch({ type: RESET_UPDATE_ATTENDACE });
  };

  const changeTimeClockIn = (date: any, dateString: any) => {
    setClockIn(dateString);
  };
  const changeTimeClockOut = (date: any, dateString: any) => {
    setClockOut(dateString);
  };

  const [open, setOpen] = useState(false);
  const [showDialogConfirm, setShowDialogConfirm] = useState(false);
  const [valueForm, setValueForm] = useState<any>(null);

  const onFinish = (values: any) => {
    setValueForm(values);
    setOpen(true);
  };

  const onSureUpdate = () => {
    const body = {
      ...valueForm,
      clockIn: clockIn,
      clockOut: clockOut,
      clockInPhoto: clockInPhoto,
      clockOutPhoto: clockOutPhoto,
    };
    dispatch(updateAttendaces(params.id, body));
  };

  const onSetValueDetail = () => {
    Object.keys(dataDetail).forEach((key) => {
      const value = dataDetail[key];
      if (key === "clockIn") {
        if (value) {
          setClockIn(value);
          form.setFieldValue(key, dayjs(value, dateFormat));
        }
      } else if (key === "clockOut") {
        if (value) {
          setClockOut(value);
          form.setFieldValue(key, dayjs(value, dateFormat));
        }
      } else if (key === "clockInPhoto") {
        if (value) {
          setDataClockInPhoto([
            {
              uid: "1",
              name: "photoClockIn.png",
              status: "done",
              url: value,
            },
          ]);
          form.setFieldValue(key, value);
        }
      } else if (key === "clockOutPhoto") {
        if (value) {
          setDataClockOutPhoto([
            {
              uid: "1",
              name: "photoClockOut.png",
              status: "done",
              url: value,
            },
          ]);
          form.setFieldValue(key, value);
        }
      } else {
        form.setFieldValue(key, value);
      }
    });
  };

  useEffect(() => {
    if (statusDetail === "success") {
      onSetValueDetail();
    }
    if (statusUpdate === "success") {
      setShowDialogConfirm(false);
      formConfirm.resetFields();
      openMessage("success", "Berhasil merubah data absensi karyawan .", "top");
      onReset();
      router.push(routerMenu.ATTENDACE);
    }

    if (statusUpdate === "error") {
      setShowDialogConfirm(false);
      formConfirm.resetFields();
      openMessage("error", "Gagal merubah data absensi karyawan .", "top");
      dispatch({ type: RESET_STATUS_UPDATE_ATTENDACE });
    }
  }, [statusUpdate, statusDetail]);

  useEffect(() => {
    dispatch(fetchAttendacesDetail(params.id));
  }, [dispatch, params.id]);

  return (
    <>
      <Form
        {...formItemLayout}
        onFinish={onFinish}
        layout="vertical"
        style={{ width: "100%" }}
        form={form}
        name="control-hooks"
      >
        <div className="grid grid-cols-12 gap-5">
          <div className="col-span-6">
            <Form.Item
              label="Absen Masuk"
              name="clockIn"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <TimePicker
                onChange={changeTimeClockIn}
                defaultOpenValue={dayjs("00:00:00", "HH:mm:ss")}
              />
            </Form.Item>
            <Form.Item
              label="Absen Keluar"
              name="clockOut"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <TimePicker
                onChange={changeTimeClockOut}
                defaultOpenValue={dayjs("00:00:00", "HH:mm:ss")}
              />
            </Form.Item>
            <Form.Item label="Deskripsi absen masuk" name="descriptionIn">
              <Input />
            </Form.Item>
            <Form.Item label="Deskripsi absen keluar" name="descriptionOut">
              <Input />
            </Form.Item>
            <Form.Item
              label="Foto Absen Masuk"
              name="clockInPhoto"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <div className="border rounded-md">
                <CUploadSingle
                  onChange={handleUploadClockInPhoto}
                  dataFileList={dataClockInPhoto}
                />
              </div>
            </Form.Item>
            <Form.Item
              label="Foto Absen Keluar"
              name="clockOutPhoto"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <div className="border rounded-md">
                <CUploadSingle
                  onChange={handleUploadClockOutPhoto}
                  dataFileList={dataClockOutPhoto}
                />
              </div>
            </Form.Item>

            <Form.Item label="Latitude absen masuk" name="clockInLatitude">
              <Input />
            </Form.Item>
            <Form.Item label="Longitude absen masuk" name="clockInLongitude">
              <Input />
            </Form.Item>
            <Form.Item label="Latitude absen keluar" name="clockOutLatitude">
              <Input />
            </Form.Item>
            <Form.Item label="Longitude absen keluar" name="clockOutLongitude">
              <Input />
            </Form.Item>
          </div>
        </div>
        <div className="grid grid-cols-12 pt-10 mt-10 border-t">
          <div className="col-span-12">
            <Form.Item>
              <div className="flex gap-5 items-center justify-start">
                <Link href={routerMenu.ATTENDACE}>
                  <CButtonAction
                    icon={<ReloadOutlined />}
                    htmlType="button"
                    disabled={statusDetail === "process"}
                    loading={statusDetail === "process"}
                  >
                    Kembali
                  </CButtonAction>
                </Link>
                <CButtonAction
                  icon={<SaveOutlined />}
                  disabled={statusDetail === "process"}
                  loading={statusDetail === "process"}
                  htmlType="submit"
                  variant="primary"
                >
                  Ubah data
                </CButtonAction>
              </div>
            </Form.Item>
          </div>
        </div>
      </Form>
      <CDialog
        title="Anda yakin merubah data absen ?"
        open={open}
        titleActionOk="Ya"
        titleActionCancel="Tidak"
        actionCancel={() => setOpen(false)}
        actionOk={() => {
          setShowDialogConfirm(true);
          setOpen(false);
        }}
      ></CDialog>
      <CDialog
        title="Konfirmasi password untuk merubah data absen ?"
        open={showDialogConfirm}
        titleActionOk="Setuju"
        titleActionCancel="Tidak"
        actionCancel={() => {
          setShowDialogConfirm(false);
          formConfirm.resetFields();
        }}
        actionOk={onSureUpdate}
        loadingBtn={statusUpdate === "process"}
      >
        <Form
          onFinish={onSureUpdate}
          layout="vertical"
          style={{ width: "100%" }}
          form={formConfirm}
          name="control-hooks"
        >
          <Form.Item
            label="Password"
            name="password"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <Input.Password />
          </Form.Item>
        </Form>
      </CDialog>
    </>
  );
};

export default FormUpdate;
