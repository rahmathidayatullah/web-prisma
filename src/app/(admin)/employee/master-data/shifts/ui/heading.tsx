import React from "react";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data shifts"
      description="Data informasi shifts, seluruh data shifts"
    />
  );
};

export default Heading;
