"use client";

import React, { useEffect } from "react";

import { useRouter } from "next/navigation";
import { formItemLayout } from "./interface";

import { useDispatch, useSelector } from "react-redux";

import { Input, Form } from "antd";
import Link from "next/link";
import CButtonAction from "@/app/components/molecules/c-button-action";
import { SaveOutlined, ReloadOutlined } from "@ant-design/icons";
import { routerMenu } from "@/app/constants/routes";
import { useGlobalContext } from "@/app/context/store";
import {
  RESET_CREATE_DIVISION,
  RESET_STATUS_CREATE_DIVISION,
} from "@/app/redux/features/division/constants";
import { createDivision } from "@/app/redux/features/division/actions";

const FormCreate = () => {
  const { openMessage } = useGlobalContext();
  const router = useRouter();
  const dispatch: any = useDispatch();
  const [form] = Form.useForm();

  const divisi = useSelector((state: any) => state.divisi);
  const { errorCreate, statusCreate } = divisi;

  const onReset = () => {
    form.resetFields();
    dispatch({ type: RESET_CREATE_DIVISION });
  };

  const onFinish = (values: any) => {
    const body = {
      ...values,
    };
    dispatch(createDivision(body));
  };

  useEffect(() => {
    if (statusCreate === "success") {
      openMessage("success", "Berhasil menambahkan divisi baru .", "top");
      onReset();
      router.push(routerMenu.MASTER_DATA_DIVISION);
    }
    if (statusCreate === "error") {
      openMessage(
        "error",
        errorCreate?.response?.data?.message ??
          "Gagal menambahkan divisi baru .",
        "top"
      );
      dispatch({ type: RESET_STATUS_CREATE_DIVISION });
    }
  }, [dispatch, statusCreate]);

  return (
    <>
      <Form
        {...formItemLayout}
        onFinish={onFinish}
        layout="vertical"
        style={{ width: "100%" }}
        form={form}
        name="control-hooks"
      >
        <div className="grid grid-cols-12 gap-5">
          <div className="col-span-6">
            <Form.Item
              label="Nama Divisi"
              name="name"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <Input />
            </Form.Item>
          </div>
        </div>
        <div className="grid grid-cols-12 pt-10 mt-10 border-t">
          <div className="col-span-12">
            <Form.Item>
              <div className="flex gap-5 items-center justify-start">
                <Link href={routerMenu.MASTER_DATA_DIVISION}>
                  <CButtonAction
                    icon={<ReloadOutlined />}
                    htmlType="button"
                    disabled={statusCreate === "process"}
                    loading={statusCreate === "process"}
                  >
                    Kembali
                  </CButtonAction>
                </Link>
                <CButtonAction
                  icon={<SaveOutlined />}
                  disabled={statusCreate === "process"}
                  loading={statusCreate === "process"}
                  htmlType="submit"
                  variant="primary"
                >
                  Tambah data
                </CButtonAction>
              </div>
            </Form.Item>
          </div>
        </div>
      </Form>
    </>
  );
};

export default FormCreate;
