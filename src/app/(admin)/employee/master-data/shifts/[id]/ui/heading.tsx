"use client";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading title="Informasi data divisi" description="Merubah data divisi" />
  );
};

export default Heading;
