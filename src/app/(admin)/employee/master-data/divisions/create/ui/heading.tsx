"use client";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data divisi"
      description="Menambahkan data divisi baru"
    />
  );
};

export default Heading;
