"use client";

import React, { useEffect } from "react";

import { useRouter } from "next/navigation";
import { formItemLayout } from "./interface";

import { useDispatch, useSelector } from "react-redux";

import { Input, Form } from "antd";
import Link from "next/link";
import CButtonAction from "@/app/components/molecules/c-button-action";
import { SaveOutlined, ReloadOutlined } from "@ant-design/icons";
import { routerMenu } from "@/app/constants/routes";
import { useGlobalContext } from "@/app/context/store";
import {
  RESET_STATUS_UPDATE_DIVISION,
  RESET_UPDATE_DIVISION,
} from "@/app/redux/features/division/constants";
import {
  fetchDivisionById,
  updateDivision,
} from "@/app/redux/features/division/actions";

const FormUpdate = ({ params }: { params: { id: string } }) => {
  const { openMessage } = useGlobalContext();
  const router = useRouter();
  const dispatch: any = useDispatch();
  const [form] = Form.useForm();

  const divisi = useSelector((state: any) => state.divisi);
  const { dataDetail, statusDetail, errorUpdate, statusUpdate } = divisi;

  const onReset = () => {
    form.resetFields();
    dispatch({ type: RESET_UPDATE_DIVISION });
  };

  const onFinish = (values: any) => {
    const body = {
      ...values,
    };
    dispatch(updateDivision(params.id, body));
  };

  const onSetValueDetail = () => {
    Object.keys(dataDetail).forEach((key) => {
      const value = dataDetail[key];
      form.setFieldValue(key, value);
    });
  };

  useEffect(() => {
    if (statusDetail === "success") {
      onSetValueDetail();
    }

    if (statusUpdate === "success") {
      openMessage("success", "Berhasil merubah data divisi .", "top");
      onReset();
      router.push(routerMenu.MASTER_DATA_DIVISION);
    }
    if (statusUpdate === "error") {
      openMessage(
        "error",
        errorUpdate?.response?.data?.message ?? "Gagal merubah data divisi .",
        "top"
      );
      dispatch({ type: RESET_STATUS_UPDATE_DIVISION });
    }
  }, [dispatch, statusUpdate, statusDetail]);

  useEffect(() => {
    dispatch(fetchDivisionById(params.id));
  }, [dispatch, params.id]);

  return (
    <>
      <Form
        {...formItemLayout}
        onFinish={onFinish}
        layout="vertical"
        style={{ width: "100%" }}
        form={form}
        name="control-hooks"
      >
        <div className="grid grid-cols-12 gap-5">
          <div className="col-span-6">
            <Form.Item
              label="Nama Divisi"
              name="name"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <Input />
            </Form.Item>
          </div>
        </div>
        <div className="grid grid-cols-12 pt-10 mt-10 border-t">
          <div className="col-span-12">
            <Form.Item>
              <div className="flex gap-5 items-center justify-start">
                <Link href={routerMenu.MASTER_DATA_DIVISION}>
                  <CButtonAction
                    icon={<ReloadOutlined />}
                    htmlType="button"
                    disabled={statusUpdate === "process"}
                    loading={statusUpdate === "process"}
                  >
                    Kembali
                  </CButtonAction>
                </Link>
                <CButtonAction
                  icon={<SaveOutlined />}
                  disabled={statusUpdate === "process"}
                  loading={statusUpdate === "process"}
                  htmlType="submit"
                  variant="primary"
                >
                  Ubah data
                </CButtonAction>
              </div>
            </Form.Item>
          </div>
        </div>
      </Form>
    </>
  );
};

export default FormUpdate;
