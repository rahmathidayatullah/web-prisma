"use client";
import React from "react";
import { Pagination } from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  CHANGE_LIMIT_DIVISION,
  CHANGE_PAGE_DIVISION,
} from "@/app/redux/features/division/constants";

const PaginateLimit: React.FC = () => {
  const dispatch: any = useDispatch();
  const divisi = useSelector((state: any) => state.divisi);
  const { amountOfData, page, take } = divisi;

  return (
    <Pagination
      current={page}
      defaultCurrent={page}
      pageSize={take}
      defaultPageSize={take}
      total={amountOfData}
      onChange={(page: number) =>
        dispatch({ type: CHANGE_PAGE_DIVISION, value: page })
      }
      onShowSizeChange={(_, size: number) =>
        dispatch({ type: CHANGE_LIMIT_DIVISION, value: size })
      }
    />
  );
};

export default PaginateLimit;
