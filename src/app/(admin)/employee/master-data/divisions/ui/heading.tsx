import React from "react";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data divisi"
      description="Data informasi divisi, seluruh data divisi"
    />
  );
};

export default Heading;
