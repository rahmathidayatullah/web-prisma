import React from "react";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data jabatan"
      description="Data informasi jabatan, seluruh data jabatan"
    />
  );
};

export default Heading;
