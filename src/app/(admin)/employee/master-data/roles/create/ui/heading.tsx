"use client";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data jabatan"
      description="Menambahkan data jabatan baru"
    />
  );
};

export default Heading;
