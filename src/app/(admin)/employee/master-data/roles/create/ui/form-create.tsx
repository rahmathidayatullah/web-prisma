"use client";

import React, { useEffect } from "react";

import { useRouter } from "next/navigation";
import { formItemLayout } from "./interface";

import { useDispatch, useSelector } from "react-redux";

import { Input, Form } from "antd";
import Link from "next/link";
import CButtonAction from "@/app/components/molecules/c-button-action";
import { SaveOutlined, ReloadOutlined } from "@ant-design/icons";
import { routerMenu } from "@/app/constants/routes";
import { useGlobalContext } from "@/app/context/store";
import {
  RESET_CREATE_ROLES,
  RESET_STATUS_CREATE_ROLES,
} from "@/app/redux/features/roles/constans";
import { createRoles } from "@/app/redux/features/roles/actions";
import { fetchShifts } from "@/app/redux/features/master-data-attendaces/actions";
import { fetchDivisionAll } from "@/app/redux/features/division/actions";
import CSelect from "@/app/components/molecules/c-select";

const FormCreate = () => {
  const { openMessage } = useGlobalContext();
  const router = useRouter();
  const dispatch: any = useDispatch();
  const [form] = Form.useForm();
  const shiftAttendaces = useSelector((state: any) => state.shiftAttendaces);
  const { data: dataAllShift } = shiftAttendaces;
  const divisi = useSelector((state: any) => state.divisi);
  const { dataAll: dataAllDivisi } = divisi;

  const roles = useSelector((state: any) => state.roles);
  const { errorCreate, statusCreateRoles } = roles;

  const onReset = () => {
    form.resetFields();
    dispatch({ type: RESET_CREATE_ROLES });
  };

  const onFinish = (values: any) => {
    const body = {
      ...values,
    };
    dispatch(createRoles(body));
  };

  useEffect(() => {
    if (statusCreateRoles === "success") {
      openMessage("success", "Berhasil menambahkan jabatan baru .", "top");
      onReset();
      router.push(routerMenu.MASTER_DATA_ROLES);
    }
    if (statusCreateRoles === "error") {
      openMessage(
        "error",
        errorCreate?.response?.data?.message ??
          "Gagal menambahkan jabatan baru .",
        "top"
      );
      dispatch({ type: RESET_STATUS_CREATE_ROLES });
    }
  }, [dispatch, statusCreateRoles]);

  useEffect(() => {
    dispatch(fetchShifts());
    dispatch(fetchDivisionAll());
  }, []);

  return (
    <>
      <Form
        {...formItemLayout}
        onFinish={onFinish}
        layout="vertical"
        style={{ width: "100%" }}
        form={form}
        name="control-hooks"
      >
        <div className="grid grid-cols-12 gap-5">
          <div className="col-span-6">
            <Form.Item
              label="Nama Jabatan"
              name="name"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Shift"
              name="shiftId"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CSelect dataOption={dataAllShift} />
            </Form.Item>
            <Form.Item
              label="Divisi"
              name="divisionId"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CSelect dataOption={dataAllDivisi} />
            </Form.Item>
          </div>
        </div>
        <div className="grid grid-cols-12 pt-10 mt-10 border-t">
          <div className="col-span-12">
            <Form.Item>
              <div className="flex gap-5 items-center justify-start">
                <Link href={routerMenu.MASTER_DATA_ROLES}>
                  <CButtonAction
                    icon={<ReloadOutlined />}
                    htmlType="button"
                    disabled={statusCreateRoles === "process"}
                    loading={statusCreateRoles === "process"}
                  >
                    Kembali
                  </CButtonAction>
                </Link>
                <CButtonAction
                  icon={<SaveOutlined />}
                  disabled={statusCreateRoles === "process"}
                  loading={statusCreateRoles === "process"}
                  htmlType="submit"
                  variant="primary"
                >
                  Tambah data
                </CButtonAction>
              </div>
            </Form.Item>
          </div>
        </div>
      </Form>
    </>
  );
};

export default FormCreate;
