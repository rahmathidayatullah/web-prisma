"use client";

import React, { useEffect } from "react";

import { useRouter } from "next/navigation";
import { formItemLayout } from "./interface";

import { useDispatch, useSelector } from "react-redux";

import { Input, Form } from "antd";
import Link from "next/link";
import CButtonAction from "@/app/components/molecules/c-button-action";
import { SaveOutlined, ReloadOutlined } from "@ant-design/icons";
import { routerMenu } from "@/app/constants/routes";
import { useGlobalContext } from "@/app/context/store";
import {
  RESET_STATUS_UPDATE_ROLES,
  RESET_UPDATE_ROLES,
} from "@/app/redux/features/roles/constans";
import {
  fetchRolesDetail,
  updateRoles,
} from "@/app/redux/features/roles/actions";

const FormUpdate = ({ params }: { params: { id: string } }) => {
  const { openMessage } = useGlobalContext();
  const router = useRouter();
  const dispatch: any = useDispatch();
  const [form] = Form.useForm();

  const roles = useSelector((state: any) => state.roles);
  const { dataRolesDetail, statusDetailRoles, errorUpdate, statusUpdateRoles } =
    roles;

  const onReset = () => {
    form.resetFields();
    dispatch({ type: RESET_UPDATE_ROLES });
  };

  const onFinish = (values: any) => {
    const body = {
      ...values,
    };
    dispatch(updateRoles(params.id, body));
  };

  const onSetValueDetail = () => {
    Object.keys(dataRolesDetail).forEach((key) => {
      const value = dataRolesDetail[key];
      form.setFieldValue(key, value);
    });
  };

  useEffect(() => {
    if (statusDetailRoles === "success") {
      onSetValueDetail();
    }

    if (statusUpdateRoles === "success") {
      openMessage("success", "Berhasil merubah data jabatan .", "top");
      onReset();
      router.push(routerMenu.MASTER_DATA_ROLES);
    }
    if (statusUpdateRoles === "error") {
      openMessage(
        "error",
        errorUpdate?.response?.data?.message ?? "Gagal merubah data jabatan .",
        "top"
      );
      dispatch({ type: RESET_STATUS_UPDATE_ROLES });
    }
  }, [dispatch, statusUpdateRoles, statusDetailRoles]);

  useEffect(() => {
    dispatch(fetchRolesDetail(params.id));
  }, [dispatch, params.id]);

  return (
    <>
      <Form
        {...formItemLayout}
        onFinish={onFinish}
        layout="vertical"
        style={{ width: "100%" }}
        form={form}
        name="control-hooks"
      >
        <div className="grid grid-cols-12 gap-5">
          <div className="col-span-6">
            <Form.Item
              label="Nama Jabatan"
              name="name"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <Input />
            </Form.Item>
          </div>
        </div>
        <div className="grid grid-cols-12 pt-10 mt-10 border-t">
          <div className="col-span-12">
            <Form.Item>
              <div className="flex gap-5 items-center justify-start">
                <Link href={routerMenu.MASTER_DATA_ROLES}>
                  <CButtonAction
                    icon={<ReloadOutlined />}
                    htmlType="button"
                    disabled={statusUpdateRoles === "process"}
                    loading={statusUpdateRoles === "process"}
                  >
                    Kembali
                  </CButtonAction>
                </Link>
                <CButtonAction
                  icon={<SaveOutlined />}
                  disabled={statusUpdateRoles === "process"}
                  loading={statusUpdateRoles === "process"}
                  htmlType="submit"
                  variant="primary"
                >
                  Ubah data
                </CButtonAction>
              </div>
            </Form.Item>
          </div>
        </div>
      </Form>
    </>
  );
};

export default FormUpdate;
