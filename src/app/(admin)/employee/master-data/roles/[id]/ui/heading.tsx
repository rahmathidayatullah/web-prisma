"use client";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data jabatan"
      description="Merubah data jabatan"
    />
  );
};

export default Heading;
