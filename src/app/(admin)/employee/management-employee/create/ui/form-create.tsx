"use client";
import React, { useEffect, useState } from "react";
import { Form, Input } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { createUser } from "@/app/redux/features/users/actions";
import CButtonAction from "@/app/components/molecules/c-button-action";
import CDateSingle from "@/app/components/molecules/c-date-single";
import CSelect from "@/app/components/molecules/c-select";
import { fetchRolesAll } from "@/app/redux/features/roles/actions";
import { fetchCompanies } from "@/app/redux/features/master-data-attendaces/actions";
import Link from "next/link";
import { routerMenu } from "@/app/constants/routes";
import CUploadSingle from "@/app/components/molecules/c-upload-single";
import { PlusOutlined, ReloadOutlined } from "@ant-design/icons";
import { useRouter } from "next/navigation";
import { useGlobalContext } from "@/app/context/store";
import CSelectMultiple from "@/app/components/molecules/c-select-multiple";
import {
  RESET_CREATE_USER,
  RESET_STATUS_CREATE_USER,
} from "@/app/redux/features/users/constans";
import CInputNumber from "@/app/components/molecules/c-input-number";
import CDialog from "@/app/components/molecules/c-dialog";

const FormCreate = () => {
  const { openMessage } = useGlobalContext();
  const router = useRouter();
  const dispatch: any = useDispatch();
  const [form] = Form.useForm();
  const [formConfirm] = Form.useForm();
  const [valueForm, setValueForm] = useState<any>(null);

  const [open, setOpen] = useState(false);
  const [showDialogConfirm, setShowDialogConfirm] = useState(false);

  const user = useSelector((state: any) => state.user);
  const { statusCreateUser, dataUserCreate, dataError } = user;

  const roles = useSelector((state: any) => state.roles);
  const { statusListRoleAll, dataListRoleAll } = roles;

  const masterDataAttendaces = useSelector(
    (state: any) => state.masterDataAttendaces
  );
  const { statusListCompanies, dataCompanies } = masterDataAttendaces;

  const [basePhoto, setBasePhoto] = useState("");
  const handleUploadCompleteBasePhoto = (base64Data: any) => {
    setBasePhoto(base64Data);
  };
  const [basePhotoKK, setBasePhotoKK] = useState("");
  const handleUploadCompletePhotoKK = (base64Data: any) => {
    setBasePhotoKK(base64Data);
  };
  const [basePhotoNPWP, setBasePhotoNPWP] = useState("");
  const handleUploadCompletePhotoNPWP = (base64Data: any) => {
    setBasePhotoNPWP(base64Data);
  };
  const [basePhotoKTP, setBasePhotoKTP] = useState("");
  const handleUploadCompletePhotoKTP = (base64Data: any) => {
    setBasePhotoKTP(base64Data);
  };
  const [basePhotoIjazah, setBasePhotoIjazah] = useState("");
  const handleUploadCompletePhotoIjazah = (base64Data: any) => {
    setBasePhotoIjazah(base64Data);
  };

  const [start_work_date, setStart_work_date] = useState("");
  const [birthDate, setbirthDate] = useState("");

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      // sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      // sm: { span: 14 },
    },
  };

  const onFinish = (values: any) => {
    setValueForm(values);
    setOpen(true);
  };

  const onSureCreate = () => {
    const body = {
      ...valueForm,
      start_work_date,
      birthDate: birthDate === "" ? null : birthDate,
      photo: basePhoto,
      photoKK: basePhotoKK,
      photoNPWP: basePhotoNPWP,
      photoKTP: basePhotoKTP,
      photoIjazah: basePhotoIjazah,
    };
    dispatch(createUser(body));
  };

  const onReset = () => {
    form.resetFields();
    setBasePhoto("");
    setBasePhotoKK("");
    setBasePhotoNPWP("");
    setBasePhotoKTP("");
    setBasePhotoIjazah("");
    setBasePhoto("");
    setStart_work_date("");
    setbirthDate("");
    dispatch({ type: RESET_CREATE_USER });
  };

  const changePickerStartWorkDate = (date: any, dateString: any) => {
    setStart_work_date(dateString);
  };
  const changePickerBirthDate = (date: any, dateString: any) => {
    setbirthDate(dateString);
  };

  useEffect(() => {
    dispatch(fetchRolesAll());
    dispatch(fetchCompanies());
  }, [dispatch]);
  useEffect(() => {
    if (statusCreateUser === "success") {
      setShowDialogConfirm(false);
      formConfirm.resetFields();
      openMessage("success", "Berhasil menambahkan karyawan baru .", "top");
      onReset();
      router.push(routerMenu.EMPLOYEE);
    }
    if (statusCreateUser === "error") {
      setShowDialogConfirm(false);
      formConfirm.resetFields();
      openMessage(
        "error",
        dataError?.response?.data?.message ??
          "Terjadi kesalahan, gagal menambahkan karyawan baru .",
        "top"
      );
      dispatch({ type: RESET_STATUS_CREATE_USER });
    }
    if (statusListRoleAll === "success") {
      setOptionRole(dataListRoleAll);
    }
    if (statusListCompanies === "success") {
      setOptionCompanies(dataCompanies);
    }
  }, [statusListCompanies, statusCreateUser, statusListRoleAll]);

  const optionGender = [
    { label: "Laki - laki", value: "Laki-laki" },
    { label: "Perempuan", value: "Perempuan" },
  ];

  const [optionRole, setOptionRole] = useState([]);
  const [optionCompanies, setOptionCompanies] = useState([]);

  const onChangeGender = (item: any) => {};
  const onChangeRole = (item: any) => {};
  const onChangeShift = (item: any) => {};

  return (
    <>
      <Form
        {...formItemLayout}
        onFinish={onFinish}
        layout="vertical"
        style={{ width: "100%" }}
        form={form}
        name="control-hooks"
      >
        <div className="grid grid-cols-12 gap-5">
          <div className="col-span-12 md:col-span-4 lg:col-span-3">
            <Form.Item
              label="Nama"
              name="name"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Email"
              name="email"
              rules={[
                {
                  type: "email",
                  message: "The input is not valid E-mail!",
                },
                { required: true, message: "Please input!" },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="password"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <Input.Password />
            </Form.Item>

            <Form.Item
              label="NRP"
              name="no_nrp"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CInputNumber style={{ width: "100%" }} />
            </Form.Item>

            <Form.Item
              label="Mulai Kerja"
              name="start_work_date"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CDateSingle onChange={changePickerStartWorkDate} />
            </Form.Item>

            <Form.Item label="Sallary" name="sallary">
              <CInputNumber rupiah style={{ width: "100%" }} />
            </Form.Item>
            <Form.Item label="JKM" name="jkm">
              <CInputNumber style={{ width: "100%" }} />
            </Form.Item>
          </div>

          <div className="col-span-12 md:col-span-4 lg:col-span-3">
            <Form.Item label="JHT" name="jht">
              <CInputNumber style={{ width: "100%" }} />
            </Form.Item>
            <Form.Item label="JKK" name="jkk">
              <CInputNumber style={{ width: "100%" }} />
            </Form.Item>
            <Form.Item
              label="NIK"
              name="nik"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CInputNumber style={{ width: "100%" }} />
            </Form.Item>
            <Form.Item label="Tanggal Lahir" name="birthDate">
              <CDateSingle onChange={changePickerBirthDate} />
            </Form.Item>

            <Form.Item label="NPWP" name="npwp">
              <CInputNumber style={{ width: "100%" }} />
            </Form.Item>

            <Form.Item
              label="Alamat"
              name="address"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="No Hp"
              name="phoneNumber"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CInputNumber style={{ width: "100%" }} />
            </Form.Item>
          </div>
          <div className="col-span-12 md:col-span-4 lg:col-span-3">
            <Form.Item
              label="No Darurat"
              name="emergencyContact"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CInputNumber style={{ width: "100%" }} />
            </Form.Item>

            <Form.Item
              label="Jenis Kelamin"
              name="gender"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CSelect
                dataOption={optionGender}
                onChange={onChangeGender}
                value={form.getFieldValue("gender")}
              />
            </Form.Item>

            <Form.Item label="Nama Bank" name="bank_name">
              <Input />
            </Form.Item>
            <Form.Item label="No Rek" name="account_number">
              <CInputNumber style={{ width: "100%" }} />
            </Form.Item>
            <Form.Item label="Nama Rek" name="account_name">
              <Input />
            </Form.Item>

            <Form.Item
              label="Jabatan"
              name="roleId"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CSelect
                dataOption={optionRole}
                onChange={onChangeRole}
                value={form.getFieldValue("roleId")}
              />
            </Form.Item>
            <Form.Item
              label="Nama Perusahaan"
              name="companyIds"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CSelectMultiple
                options={optionCompanies}
                onChange={onChangeShift}
                value={form.getFieldValue("companyIds")}
              />
            </Form.Item>
          </div>
          <div className="col-span-12 md:col-span-4 lg:col-span-3">
            <Form.Item label="Foto Profil" name="photo">
              <div className="border rounded-md">
                <CUploadSingle onChange={handleUploadCompleteBasePhoto} />
              </div>
            </Form.Item>
            <Form.Item label="Foto KK" name="photoKK">
              <div className="border rounded-md">
                <CUploadSingle onChange={handleUploadCompletePhotoKK} />
              </div>
            </Form.Item>
            <Form.Item label="Foto NPWP" name="photoNPWP">
              <div className="border rounded-md">
                <CUploadSingle onChange={handleUploadCompletePhotoNPWP} />
              </div>
            </Form.Item>
            <Form.Item
              label="Foto KTP"
              name="photoKTP"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <div className="border rounded-md">
                <CUploadSingle onChange={handleUploadCompletePhotoKTP} />
              </div>
            </Form.Item>
            <Form.Item
              label="Foto Ijazah"
              style={{ width: "100%" }}
              name="photoIjazah"
            >
              <div className="border rounded-md">
                <CUploadSingle onChange={handleUploadCompletePhotoIjazah} />
              </div>
            </Form.Item>
          </div>
        </div>
        <div className="grid grid-cols-12 pt-10 mt-10 border-t">
          <div className="col-span-12">
            <Form.Item>
              <div className="flex gap-5 items-center justify-end">
                <Link href={routerMenu.EMPLOYEE}>
                  <CButtonAction
                    icon={<ReloadOutlined />}
                    htmlType="button"
                    disabled={statusCreateUser === "process"}
                    loading={statusCreateUser === "process"}
                  >
                    Kembali
                  </CButtonAction>
                </Link>

                <CButtonAction
                  icon={<PlusOutlined />}
                  disabled={statusCreateUser === "process"}
                  loading={statusCreateUser === "process"}
                  htmlType="submit"
                  variant="primary"
                >
                  Tambah data
                </CButtonAction>
              </div>
            </Form.Item>
          </div>
        </div>
      </Form>
      <CDialog
        title="Anda yakin menambah data karyawan ?"
        open={open}
        titleActionOk="Ya"
        titleActionCancel="Tidak"
        actionCancel={() => setOpen(false)}
        actionOk={() => {
          setShowDialogConfirm(true);
          setOpen(false);
        }}
      ></CDialog>
      <CDialog
        title="Konfirmasi password untuk menambahkan karyawan baru ?"
        open={showDialogConfirm}
        titleActionOk="Setuju"
        titleActionCancel="Tidak"
        actionCancel={() => {
          setShowDialogConfirm(false);
          formConfirm.resetFields();
        }}
        actionOk={onSureCreate}
        loadingBtn={statusCreateUser === "process"}
      >
        <Form
          onFinish={onSureCreate}
          layout="vertical"
          style={{ width: "100%" }}
          form={formConfirm}
          name="control-hooks"
        >
          <Form.Item
            label="Password"
            name="password"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <Input.Password />
          </Form.Item>
        </Form>
      </CDialog>
    </>
  );
};

export default FormCreate;
