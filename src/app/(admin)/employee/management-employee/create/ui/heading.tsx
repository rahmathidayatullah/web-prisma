import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data karyawan"
      description="Menambahkan data karyawan baru, lengkap dengan identitas"
    />
  );
};

export default Heading;
