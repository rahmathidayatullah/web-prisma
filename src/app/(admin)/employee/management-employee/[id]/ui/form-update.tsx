"use client";
import React, { useEffect, useState } from "react";
import { Form, Input } from "antd";

import { useDispatch, useSelector } from "react-redux";
import {
  fetchUsersDetail,
  updateUser,
} from "@/app/redux/features/users/actions";
import CButtonAction from "@/app/components/molecules/c-button-action";
import CDateSingle from "@/app/components/molecules/c-date-single";
import CSelect from "@/app/components/molecules/c-select";
import { fetchRolesAll } from "@/app/redux/features/roles/actions";
import { fetchCompanies } from "@/app/redux/features/master-data-attendaces/actions";
import Link from "next/link";
import { routerMenu } from "@/app/constants/routes";
import dayjs from "dayjs";
import moment from "moment";
import CUploadSingle from "@/app/components/molecules/c-upload-single";
import { SaveOutlined, ReloadOutlined } from "@ant-design/icons";
import {
  RESET_STATUS_UPDATE_USER,
  RESET_UPDATE_USER,
} from "@/app/redux/features/users/constans";
import { useRouter } from "next/navigation";
import { useGlobalContext } from "@/app/context/store";
import CSelectMultiple from "@/app/components/molecules/c-select-multiple";
import CInputNumber from "@/app/components/molecules/c-input-number";
import CDialog from "@/app/components/molecules/c-dialog";
const dateFormat = "YYYY-MM-DD";

const FormUpdate = ({ params }: { params: { id: string } }) => {
  const { openMessage } = useGlobalContext();
  const router = useRouter();
  const dispatch: any = useDispatch();
  const [form] = Form.useForm();
  const [formConfirm] = Form.useForm();
  const [valueForm, setValueForm] = useState<any>(null);

  const [open, setOpen] = useState(false);
  const [showDialogConfirm, setShowDialogConfirm] = useState(false);

  const user = useSelector((state: any) => state.user);
  const { statusDetailUser, statusUpdateUser, dataUserDetail, errorUpdate } =
    user;

  const roles = useSelector((state: any) => state.roles);
  const { statusListRoleAll, dataListRoleAll } = roles;

  const masterDataAttendaces = useSelector(
    (state: any) => state.masterDataAttendaces
  );
  const { statusListCompanies, dataCompanies } = masterDataAttendaces;

  const [basePhoto, setBasePhoto] = useState("");
  const [dataListBasePhoto, setDataListBasePhoto] = useState<any>([]);
  const handleUploadCompleteBasePhoto = (base64Data: any) => {
    setBasePhoto(base64Data);
  };

  const [basePhotoKK, setBasePhotoKK] = useState("");
  const [dataListBasePhotoKK, setDataListBasePhotoKK] = useState<any>([]);
  const handleUploadCompletePhotoKK = (base64Data: any) => {
    setBasePhotoKK(base64Data);
  };
  const [basePhotoNPWP, setBasePhotoNPWP] = useState("");
  const [dataLisBasePhotoNPWP, setDataListBasePhotoNPWP] = useState<any>([]);
  const handleUploadCompletePhotoNPWP = (base64Data: any) => {
    setBasePhotoNPWP(base64Data);
  };
  const [basePhotoKTP, setBasePhotoKTP] = useState("");
  const [dataListbasePhotoKTP, setDataListBasePhotoKTP] = useState<any>([]);
  const handleUploadCompletePhotoKTP = (base64Data: any) => {
    setBasePhotoKTP(base64Data);
  };
  const [basePhotoIjazah, setBasePhotoIjazah] = useState("");
  const [dataListBasePhotoIjazah, setDataListBasePhotoIjazah] = useState<any>(
    []
  );
  const handleUploadCompletePhotoIjazah = (base64Data: any) => {
    setBasePhotoIjazah(base64Data);
  };

  const [start_work_date, setStart_work_date] = useState("");
  const [birthDate, setbirthDate] = useState("2024-03-24");

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      // sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      // sm: { span: 14 },
    },
  };

  const onFinish = (values: any) => {
    setValueForm(values);
    setOpen(true);
  };

  const onSureUpdate = () => {
    const body = {
      ...valueForm,
      // password: values.password ?? "-",
      start_work_date,
      birthDate: birthDate === "" ? null : birthDate,
      photo: basePhoto,
      photoKK: basePhotoKK,
      photoNPWP: basePhotoNPWP,
      photoKTP: basePhotoKTP,
      photoIjazah: basePhotoIjazah,
    };

    dispatch(updateUser(params.id, body));
  };

  const onReset = () => {
    form.resetFields();
    setBasePhoto("");
    setBasePhotoKK("");
    setBasePhotoNPWP("");
    setBasePhotoKTP("");
    setBasePhotoIjazah("");
    setBasePhoto("");
    setStart_work_date("");
    setbirthDate("");
    dispatch({ type: RESET_UPDATE_USER });
  };

  const changePickerStartWorkDate = (date: any, dateString: any) => {
    setStart_work_date(dateString);
  };
  const changePickerBirthDate = (date: any, dateString: any) => {
    setbirthDate(dateString);
  };

  const onSetValueDetail = () => {
    Object.keys(dataUserDetail).forEach((key) => {
      const value = dataUserDetail[key];
      if (key === "gender") {
        const genderOption = optionGender.find(
          (option) => option.value === value
        );
        if (genderOption) {
          form.setFieldValue(key, genderOption.value);
        }
      } else if (key === "birthDate") {
        if (value) {
          setbirthDate(value);
          form.setFieldValue(key, dayjs(value, dateFormat));
        }
      } else if (key === "photo") {
        if (value) {
          setDataListBasePhoto([
            {
              uid: "1",
              name: "photo.png",
              status: "done",
              url: value,
            },
          ]);
        }
        form.setFieldValue(key, value);
      } else if (key === "photoKK") {
        if (value) {
          setDataListBasePhotoKK([
            {
              uid: "1",
              name: "photoKK.png",
              status: "done",
              url: value,
            },
          ]);
        }
        form.setFieldValue(key, value);
      } else if (key === "photoNPWP") {
        if (value) {
          setDataListBasePhotoNPWP([
            {
              uid: "1",
              name: "photoNPWP.png",
              status: "done",
              url: value,
            },
          ]);
        }
        form.setFieldValue(key, value);
      } else if (key === "photoKTP") {
        if (value) {
          setDataListBasePhotoKTP([
            {
              uid: "1",
              name: "photoKTP.png",
              status: "done",
              url: value,
            },
          ]);
        }
        form.setFieldValue(key, value);
      } else if (key === "photoIjazah") {
        if (value) {
          setDataListBasePhotoIjazah([
            {
              uid: "1",
              name: "photoIjazah.png",
              status: "done",
              url: value,
            },
          ]);
        }
        form.setFieldValue(key, value);
      } else if (key === "role") {
        if (value) {
          form.setFieldValue("roleId", value.id);
        }
      } else if (key === "companies") {
        if (value.length) {
          const valueCompanies = value.map((item: any) => item.id);
          form.setFieldValue("companyIds", valueCompanies);
        } else {
          form.setFieldValue("companyIds", []);
        }
      } else if (key === "start_work_date") {
        if (value) {
          setStart_work_date(value);
          form.setFieldValue(
            key,
            dayjs(moment(value).format("YYYY-MM-DD"), dateFormat)
          );
        }
      } else {
        form.setFieldValue(key, value);
      }
    });
  };

  useEffect(() => {
    dispatch(fetchRolesAll());
    dispatch(fetchCompanies());
    dispatch(fetchUsersDetail(params.id));
  }, [dispatch, params.id]);

  useEffect(() => {
    if (statusDetailUser === "success") {
      onSetValueDetail();
    }
    if (statusUpdateUser === "success") {
      setShowDialogConfirm(false);
      formConfirm.resetFields();
      openMessage("success", "Berhasil merubah karyawan baru .", "top");
      onReset();
      router.push(routerMenu.EMPLOYEE);
    }
    if (statusUpdateUser === "error") {
      setShowDialogConfirm(false);
      formConfirm.resetFields();
      openMessage(
        "error",
        errorUpdate?.response?.data?.message ??
          "Terjadi kesalahan, gagal merubah karyawan baru .",
        "top"
      );
      dispatch({ type: RESET_STATUS_UPDATE_USER });
    }
    if (statusListRoleAll === "success") {
      setOptionRole(dataListRoleAll);
    }
    if (statusListCompanies === "success") {
      setOptionCompanies(dataCompanies);
    }
  }, [
    statusListCompanies,
    statusUpdateUser,
    statusListRoleAll,
    statusDetailUser,
  ]);

  const optionGender = [
    { label: "Laki - laki", value: "Laki-laki" },
    { label: "Perempuan", value: "Perempuan" },
  ];

  const [optionRole, setOptionRole] = useState([]);
  const [optionCompanies, setOptionCompanies] = useState([]);

  const onChangeGender = (item: any) => {};
  const onChangeRole = (item: any) => {};
  const onChangeShift = (item: any) => {};
  return (
    <>
      <Form
        {...formItemLayout}
        onFinish={onFinish}
        layout="vertical"
        style={{ width: "100%" }}
        form={form}
        name="control-hooks"
      >
        <div className="grid grid-cols-12 gap-5">
          <div className="col-span-12 md:col-span-4 lg:col-span-3">
            <Form.Item
              label="Nama"
              name="name"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Email"
              name="email"
              rules={[
                {
                  type: "email",
                  message: "The input is not valid E-mail!",
                },
                { required: true, message: "Please input!" },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item label="Isi Jika Ingin Ubah Password" name="password">
              <Input.Password />
            </Form.Item>

            <Form.Item
              label="NRP"
              name="no_nrp"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CInputNumber style={{ width: "100%" }} />
            </Form.Item>

            <Form.Item
              label="Mulai Kerja"
              name="start_work_date"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CDateSingle
                onChange={changePickerStartWorkDate}
                value={dayjs(form.getFieldValue("start_work_date"), dateFormat)}
              />
            </Form.Item>

            <Form.Item label="Sallary" name="sallary">
              <CInputNumber rupiah style={{ width: "100%" }} />
            </Form.Item>
            <Form.Item label="JKM" name="jkm">
              <CInputNumber style={{ width: "100%" }} />
            </Form.Item>
          </div>

          <div className="col-span-12 md:col-span-4 lg:col-span-3">
            <Form.Item label="JHT" name="jht">
              <CInputNumber style={{ width: "100%" }} />
            </Form.Item>
            <Form.Item label="JKK" name="jkk">
              <CInputNumber style={{ width: "100%" }} />
            </Form.Item>
            <Form.Item
              label="NIK"
              name="nik"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CInputNumber style={{ width: "100%" }} />
            </Form.Item>
            <Form.Item label="Tanggal Lahir" name="birthDate">
              <CDateSingle
                value={dayjs(form.getFieldValue("birthDate"), dateFormat)}
                onChange={changePickerBirthDate}
              />
            </Form.Item>

            <Form.Item label="NPWP" name="npwp">
              <CInputNumber style={{ width: "100%" }} />
            </Form.Item>

            <Form.Item label="Alamat" name="address">
              <Input />
            </Form.Item>
            <Form.Item
              label="No Hp"
              name="phoneNumber"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CInputNumber style={{ width: "100%" }} />
            </Form.Item>
          </div>
          <div className="col-span-12 md:col-span-4 lg:col-span-3">
            <Form.Item
              label="No Darurat"
              name="emergencyContact"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CInputNumber style={{ width: "100%" }} />
            </Form.Item>

            <Form.Item
              label="Jenis Kelamin"
              name="gender"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CSelect
                dataOption={optionGender}
                onChange={onChangeGender}
                value={form.getFieldValue("gender")}
              />
            </Form.Item>

            <Form.Item label="Nama Bank" name="bank_name">
              <Input />
            </Form.Item>
            <Form.Item label="No Rek" name="account_number">
              <CInputNumber style={{ width: "100%" }} />
            </Form.Item>
            <Form.Item label="Nama Rek" name="account_name">
              <Input />
            </Form.Item>

            <Form.Item
              label="Jabatan"
              name="roleId"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CSelect
                dataOption={optionRole}
                onChange={onChangeRole}
                value={form.getFieldValue("roleId")}
              />
            </Form.Item>
            <Form.Item
              label="Nama Perusahaan"
              name="companyIds"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CSelectMultiple
                options={optionCompanies}
                onChange={onChangeShift}
                value={form.getFieldValue("companyIds")}
              />
            </Form.Item>
          </div>
          <div className="col-span-12 md:col-span-4 lg:col-span-3">
            <Form.Item label="Foto Profil" name="photo">
              <div className="border rounded-md">
                <CUploadSingle
                  onChange={handleUploadCompleteBasePhoto}
                  dataFileList={dataListBasePhoto}
                />
              </div>
            </Form.Item>
            <Form.Item label="Foto KK" name="photoKK">
              <div className="border rounded-md">
                <CUploadSingle
                  onChange={handleUploadCompletePhotoKK}
                  dataFileList={dataListBasePhotoKK}
                />
              </div>
            </Form.Item>
            <Form.Item label="Foto NPWP" name="photoNPWP">
              <div className="border rounded-md">
                <CUploadSingle
                  onChange={handleUploadCompletePhotoNPWP}
                  dataFileList={dataLisBasePhotoNPWP}
                />
              </div>
            </Form.Item>
            <Form.Item
              label="Foto KTP"
              name="photoKTP"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <div className="border rounded-md">
                <CUploadSingle
                  onChange={handleUploadCompletePhotoKTP}
                  dataFileList={dataListbasePhotoKTP}
                />
              </div>
            </Form.Item>
            <Form.Item
              label="Foto Ijazah"
              style={{ width: "100%" }}
              name="photoIjazah"
            >
              <div className="border rounded-md">
                <CUploadSingle
                  onChange={handleUploadCompletePhotoIjazah}
                  dataFileList={dataListBasePhotoIjazah}
                />
              </div>
            </Form.Item>
          </div>
        </div>
        <div className="grid grid-cols-12 pt-10 mt-10 border-t">
          <div className="col-span-12">
            <Form.Item>
              <div className="flex gap-5 items-center justify-end">
                <Link href={routerMenu.EMPLOYEE}>
                  <CButtonAction
                    icon={<ReloadOutlined />}
                    htmlType="button"
                    disabled={statusUpdateUser === "process"}
                    loading={statusUpdateUser === "process"}
                  >
                    Kembali
                  </CButtonAction>
                </Link>
                <CButtonAction
                  icon={<SaveOutlined />}
                  disabled={statusUpdateUser === "process"}
                  loading={statusUpdateUser === "process"}
                  htmlType="submit"
                  variant="primary"
                >
                  Ubah data
                </CButtonAction>
              </div>
            </Form.Item>
          </div>
        </div>
      </Form>
      <CDialog
        title="Anda yakin merubah data karyawan ?"
        open={open}
        titleActionOk="Ya"
        titleActionCancel="Tidak"
        actionCancel={() => setOpen(false)}
        actionOk={() => {
          setShowDialogConfirm(true);
          setOpen(false);
        }}
      ></CDialog>
      <CDialog
        title="Konfirmasi password untuk merubah karyawan baru ?"
        open={showDialogConfirm}
        titleActionOk="Setuju"
        titleActionCancel="Tidak"
        actionCancel={() => {
          setShowDialogConfirm(false);
          formConfirm.resetFields();
        }}
        actionOk={onSureUpdate}
        loadingBtn={statusUpdateUser === "process"}
      >
        <Form
          onFinish={onSureUpdate}
          layout="vertical"
          style={{ width: "100%" }}
          form={formConfirm}
          name="control-hooks"
        >
          <Form.Item
            label="Password"
            name="password"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <Input.Password />
          </Form.Item>
        </Form>
      </CDialog>
    </>
  );
};

export default FormUpdate;
