"use client";
import CHeading from "@/app/components/molecules/c-heading";
import { useSelector } from "react-redux";

const Heading = () => {
  const user = useSelector((state: any) => state.user);
  const { dataUserDetail } = user;
  return (
    <CHeading
      title={`Informasi data Karyawan ${dataUserDetail?.name ?? "-"}`}
      description="Data detail karyawan, ubah data karyawan"
    />
  );
};

export default Heading;
