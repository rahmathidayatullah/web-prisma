import React from "react";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data Karyawan"
      description="Data informasi karyawan, mendaftarkan data karyawan baru dan melihat
    seluruh data karyawan"
    />
  );
};

export default Heading;
