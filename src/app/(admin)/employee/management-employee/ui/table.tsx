"use client";
import React, { useEffect, useState } from "react";

// antd
import { Button, Form, Space, Input, notification } from "antd";
import type { DropdownProps, MenuProps, TableProps } from "antd";
import { Image as ImgAntd } from "antd";
import { SearchProps } from "antd/es/input";

import { DataType } from "./interface/table";

// redux
import { useDispatch, useSelector } from "react-redux";
import { fetchUsers, removeUser } from "@/app/redux/features/users/actions";
import {
  CHANGE_DATE_RANGE,
  CHANGE_KEYWORD,
  RESET_DELETE_USER,
} from "@/app/redux/features/users/constans";

// comopnents
import CTable from "@/app/components/molecules/c-table";
import CSearch from "@/app/components/molecules/c-search";
import CDateRange from "@/app/components/molecules/c-date-range";
import CDropdown from "@/app/components/molecules/c-dropdown";
import CButton from "@/app/components/molecules/c-button";
import CCheckboxGroup from "@/app/components/molecules/c-checkbox-group";
import Link from "next/link";
import { routerMenu } from "@/app/constants/routes";
import moment from "moment";
import "moment/locale/id";
import { formatRupiah } from "@/app/constants/helper";
import CDialog from "@/app/components/molecules/c-dialog";
import { DownloadOutlined, PlusOutlined } from "@ant-design/icons";
import { useGlobalContext } from "@/app/context/store";
import CSelect from "@/app/components/molecules/c-select";
import { fetchDivisionAll } from "@/app/redux/features/division/actions";
import { SELECT_DIVISI } from "@/app/redux/features/division/constants";
import { useSessionStorage } from "@/app/hooks/useSessionStorage";

const UITable: React.FC = () => {
  const { openNotification } = useGlobalContext();
  const dispatch: any = useDispatch();
  const user = useSelector((state: any) => state.user);
  const divisi = useSelector((state: any) => state.divisi);
  const { dataAll: dataDivisiAll, selectDivisi } = divisi;
  const {
    statusDeleteUser,
    statusListUser,
    page,
    take,
    keyword,
    endDate,
    startDate,
    dataUser,
  } = user;

  const [form] = Form.useForm();
  const [idUser, setIdUser] = useState<number | string>("");
  const [openSourceType, setOpenSourceType] = useState(false);
  const [showDialogDelete, setShowDialogDelete] = useState(false);
  const [showDialogDeleteConfirm, setShowDialogDeleteConfirm] = useState(false);

  const columns: TableProps<DataType>["columns"] = [
    {
      title: "Name",
      dataIndex: "name",
      key: "name",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "NRP",
      dataIndex: "no_nrp",
      key: "no_nrp",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Waktu mulai kerja",
      dataIndex: "start_work_date",
      key: "start_work_date",
      render: (value) => {
        if (value) {
          return moment(value).format("DD MMMM YYYY");
        }
        return "-";
      },
    },
    {
      title: "Sallary",
      dataIndex: "sallary",
      key: "sallary",
      render: (value) => {
        if (value) {
          return formatRupiah(value);
        }
        return "-";
      },
    },
    {
      title: "JKM",
      dataIndex: "jkm",
      key: "jkm",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "JHT",
      dataIndex: "jht",
      key: "jht",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "JKK",
      dataIndex: "jkk",
      key: "jkk",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "NIK",
      dataIndex: "nik",
      key: "nik",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Tanngal Lahir",
      dataIndex: "birthDate",
      key: "birthDate",
      render: (value) => {
        if (value) {
          return moment(value).format("DD MMMM YYYY");
        }
        return "-";
      },
    },
    {
      title: "NPWP",
      dataIndex: "npwp",
      key: "npwp",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Alamat",
      dataIndex: "address",
      key: "address",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "No HP",
      dataIndex: "phoneNumber",
      key: "phoneNumber",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "No Darurat",
      dataIndex: "emergencyContact",
      key: "emergencyContact",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Foto",
      dataIndex: "photo",
      key: "photo",
      render: (value) => {
        if (value) {
          return (
            <ImgAntd width={100} height={100} src="error" fallback={value} />
          );
        }
        return "-";
      },
    },
    {
      title: "Foto KK",
      dataIndex: "photoKK",
      key: "photoKK",
      render: (value) => {
        if (value) {
          return (
            <ImgAntd width={100} height={100} src="error" fallback={value} />
          );
        }
        return "-";
      },
    },
    {
      title: "Foto KTP",
      dataIndex: "photoKTP",
      key: "photoKTP",
      render: (value) => {
        if (value) {
          return (
            <ImgAntd width={100} height={100} src="error" fallback={value} />
          );
        }
        return "-";
      },
    },
    {
      title: "Foto NPWP",
      dataIndex: "photoNPWP",
      key: "photoNPWP",
      render: (value) => {
        if (value) {
          return (
            <ImgAntd width={100} height={100} src="error" fallback={value} />
          );
        }
        return "-";
      },
    },
    {
      title: "Foto Ijazah",
      dataIndex: "photoIjazah",
      key: "photoIjazah",
      render: (value) => {
        if (value) {
          return (
            <ImgAntd width={100} height={100} src="error" fallback={value} />
          );
        }
        return "-";
      },
    },
    {
      title: "Aksi",
      key: "action",
      render: (_, record) => (
        <div className="flex items-center gap-4">
          <Link href={`${routerMenu.EMPLOYEE}/${record.id}`}>
            <Button
              style={{ backgroundColor: "#219C90" }}
              size="small"
              type="primary"
            >
              Detail
            </Button>
          </Link>
          <Button
            style={{ backgroundColor: "#FF5151", color: "white" }}
            size="small"
            type="default"
            danger
            onClick={() => {
              setShowDialogDelete(true), setIdUser(record.id);
            }}
          >
            Hapus
          </Button>
        </div>
      ),
    },
  ];
  const defaultCheckedList = columns.map((item: any) => item.key as string);

  const [checkedList, setCheckedList] = useSessionStorage<string[]>(
    "data-karyawan",
    defaultCheckedList
  );

  const newColumns = columns.map((item: any) => ({
    ...item,
    hidden: !checkedList.includes(item.key as string),
  }));

  const options = columns.map(({ key, title }: any) => ({
    label: title,
    value: key,
  }));

  const items: MenuProps["items"] = [
    {
      label: (
        <CCheckboxGroup
          value={checkedList}
          options={options}
          onChange={(value: any) => {
            setCheckedList(value as string[]);
          }}
        />
      ),
      key: "0",
    },
  ];

  const onSearch: SearchProps["onSearch"] = (value, _e, info) => {};

  const handleOpenChangeSourceType: DropdownProps["onOpenChange"] = (
    nextOpen,
    info
  ) => {
    if (info.source === "trigger" || nextOpen) {
      setOpenSourceType(nextOpen);
    }
  };

  const onFinishDelete = () => {
    dispatch(removeUser(idUser));
  };

  const onChangeDivisi = (e: any) => {
    dispatch({
      type: SELECT_DIVISI,
      value: e,
    });
  };

  useEffect(() => {
    dispatch(fetchUsers());
    dispatch(fetchDivisionAll());
    if (statusDeleteUser === "success") {
      setShowDialogDeleteConfirm(false);
      openNotification("topLeft", "Data berhasil dihapus");
      dispatch({ type: RESET_DELETE_USER });
    }
  }, [
    dispatch,
    page,
    take,
    keyword,
    endDate,
    startDate,
    statusDeleteUser,
    selectDivisi,
  ]);
  return (
    <>
      <div className="flex flex-wrap gap-2 sm:gap-5 items-center justify-between my-5">
        <Space direction="horizontal">
          <CSearch
            onSearch={onSearch}
            value={keyword}
            onChange={(event: any) =>
              dispatch({ type: CHANGE_KEYWORD, value: event.target.value })
            }
          />
          <CSelect
            dataOption={dataDivisiAll}
            value={selectDivisi}
            disabled={!dataDivisiAll.length}
            placeholder="Filter by divisi"
            onChange={onChangeDivisi}
          />
        </Space>
        <div className="flex flex-wrap items-center gap-2 sm:gap-5">
          {/* <CDateRange
            onChange={(_: any, dateString: any) =>
              dispatch({ type: CHANGE_DATE_RANGE, value: dateString })
            }
          /> */}
          <CDropdown
            variant="sortir-coloum"
            menu={{ items }}
            onOpenChange={handleOpenChangeSourceType}
            open={openSourceType}
          />
          {/* <CButton
            icon={<DownloadOutlined />}
            variant="primary"
            onClick={() => alert("onprogress")}
          >
            Export Data
          </CButton> */}
          <Link href={routerMenu.CREATE_EMPLOYEE}>
            <CButton icon={<PlusOutlined />} variant="primary">
              Tambah Karyawan
            </CButton>
          </Link>
        </div>
      </div>
      <CTable
        pagination={false}
        columns={newColumns}
        dataSource={dataUser}
        loading={statusListUser === "process"}
        scroll={{ x: 3500 }}
        rowKey="id"
      />
      <CDialog
        title="Anda yakin menghapus data karyawan ?"
        open={showDialogDelete}
        titleActionOk="Ya"
        titleActionCancel="Tidak"
        actionCancel={() => setShowDialogDelete(false)}
        actionOk={() => {
          setShowDialogDeleteConfirm(true);
          setShowDialogDelete(false);
        }}
      ></CDialog>
      <CDialog
        title="Konfirmasi password untuk menghapus data karyawan ?"
        open={showDialogDeleteConfirm}
        titleActionOk="Setuju"
        titleActionCancel="Tidak"
        actionCancel={() => setShowDialogDeleteConfirm(false)}
        actionOk={onFinishDelete}
        loadingBtn={statusDeleteUser === "process"}
      >
        <Form
          onFinish={onFinishDelete}
          layout="vertical"
          style={{ width: "100%" }}
          form={form}
          name="control-hooks"
        >
          <Form.Item
            label="Password"
            name="password"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <Input.Password />
          </Form.Item>
        </Form>
      </CDialog>
    </>
  );
};

export default UITable;
