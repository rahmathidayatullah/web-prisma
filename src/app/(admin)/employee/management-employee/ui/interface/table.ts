export interface DataType {
  id: number;
  name: string | null;
  email: string | null;
  no_nrp: string | null;
  start_work_date: string | null;
  sallary: number | null;
  jkm: number | null;
  jht: number | null;
  jkk: number | null;
  nik: string | null;
  birthDate: string | null;
  npwp: string | null;
  address: string | null;
  phoneNumber: string | null;
  emergencyContact: string | null;
  photo: string | null;
  photoKK: string | null;
  photoKTP: string | null;
  photoNPWP: string | null;
  photoIjazah: string | null;
  createdAt: string | null;
  updatedAt: string | null;
  deletedAt: string | null;
  //
  key?: string;
}
export const dataTable: DataType[] = [
  {
    id: 1,
    name: "Lina edit 3",
    email: "hrd@mail.com",
    no_nrp: "5678",
    start_work_date: null,
    sallary: null,
    jkm: null,
    jht: null,
    jkk: null,
    nik: "9101112",
    birthDate: null,
    npwp: "1234",
    address: "Lampung",
    phoneNumber: "089630912247",
    emergencyContact: "089630912248",
    photo: "profiles/1-hrd@mail.com/photo-2024-03-21-19-25-19.jpeg",
    photoKK: null,
    photoKTP: null,
    photoNPWP: null,
    photoIjazah: null,
    createdAt: "2024-03-07T06:12:43.407Z",
    updatedAt: "2024-03-21T05:25:19.000Z",
    deletedAt: null,
  },
  {
    id: 2,
    name: "john Doe",
    email: "ohndoe@gmail.com",
    no_nrp: "99829",
    start_work_date: "2020-12-31T17:00:00.000Z",
    sallary: 1000000,
    jkm: 0,
    jht: 0,
    jkk: 0,
    nik: null,
    birthDate: null,
    npwp: null,
    address: null,
    phoneNumber: null,
    emergencyContact: null,
    photo: "profiles/1-johndoe@gmail.com/kk-2024-03-20-16-50-22.png",
    photoKK: null,
    photoKTP: null,
    photoNPWP: null,
    photoIjazah: null,
    createdAt: "2024-03-20T02:50:24.650Z",
    updatedAt: "2024-03-20T03:38:18.993Z",
    deletedAt: null,
  },
  {
    id: 8,
    name: "john Doe",
    email: "johndoe@gmail.com",
    no_nrp: "99829",
    start_work_date: "2020-12-31T17:00:00.000Z",
    sallary: 1000000,
    jkm: 0,
    jht: 0,
    jkk: 0,
    nik: null,
    birthDate: null,
    npwp: null,
    address: null,
    phoneNumber: null,
    emergencyContact: null,
    photo: "profiles/1-johndoe@gmail.com/kk-2024-03-20-17-38-24.png",
    photoKK: null,
    photoKTP: null,
    photoNPWP: null,
    photoIjazah: null,
    createdAt: "2024-03-20T03:38:25.760Z",
    updatedAt: "2024-03-20T03:38:25.760Z",
    deletedAt: null,
  },
];
