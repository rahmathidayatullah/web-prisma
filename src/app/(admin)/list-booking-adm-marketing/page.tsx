import React from "react";
import Heading from "./ui/heading";
import UITable from "./ui/table";
import PaginateLimit from "./ui/paginate-limit";

const Page = () => {
  return (
    <>
      <div className="flex flex-col justify-center items-start h-full">
        <div className="w-full">
          <Heading />
        </div>
        <div className="h-full w-full overflow-scroll">
          <UITable />
          <div className="mt-10 flex justify-end">
            <PaginateLimit />
          </div>
        </div>
      </div>
    </>
  );
};

export default Page;
