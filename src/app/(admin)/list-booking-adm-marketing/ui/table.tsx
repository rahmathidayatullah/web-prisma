"use client";

import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { DataType, dataTable } from "./interface";
import { Button, Space, Table, type TableColumnsType } from "antd";
import Link from "next/link";
import { routerMenu } from "@/app/constants/routes";
import CSearch from "@/app/components/molecules/c-search";

const UITable = () => {
  //   const dispatch: any = useDispatch();
  const coloumTable: TableColumnsType<DataType> = [
    {
      title: "Nama",
      dataIndex: "name",
      key: "name",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Status Booking",
      dataIndex: "status_booking",
      key: "status_booking",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Project",
      dataIndex: "project",
      key: "project",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Tahap",
      dataIndex: "tahap",
      key: "tahap",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Blok",
      dataIndex: "blok",
      key: "blok",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Nomor Unit",
      dataIndex: "no_unit",
      key: "no_unit",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Deadline Berkas",
      dataIndex: "deadline_berkas",
      key: "deadline_berkas",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Aksi",
      key: "14",
      render: (_, record: any) => {
        return (
          <div className="flex items-center gap-4">
            <Link
              href={`${routerMenu.ADM_MARKETING_LIST_BOOKING}/${record.id}`}
            >
              <Button
                style={{ backgroundColor: "#FBB03B" }}
                size="small"
                type="primary"
              >
                Detail
              </Button>
            </Link>
          </div>
        );
      },
    },
  ];
  return (
    <div>
      <div className="flex flex-wrap gap-2 sm:gap-5 items-center justify-between my-5">
        <Space direction="horizontal">
          <CSearch
            // onSearch={onSearch}
            onSearch={() => {}}
            // value={keyword}
            // onChange={(event: any) =>
            //   dispatch({ type: CHANGE_KEYWORD, value: event.target.value })
            // }
          />

          {/* <CSelect
            dataOption={dataDivisiAll}
            value={selectDivisi}
            disabled={!dataDivisiAll.length}
            placeholder="Filter by divisi"
            onChange={onChangeDivisi}
          /> */}
        </Space>
        {/* <div className="flex flex-wrap items-center gap-2 sm:gap-5">
          <CDateRange
            onChange={(_: any, dateString: any) =>
              dispatch({ type: CHANGE_DATE_RANGE, value: dateString })
            }
          />
          <CDropdown
            variant="sortir-coloum"
            menu={{ items }}
            onOpenChange={handleOpenChangeSourceType}
            open={openSourceType}
          />
          <CButton
            icon={<DownloadOutlined />}
            variant="primary"
            onClick={() => dispatch(fetchAttendacesExports())}
            loading={statusExport === "process"}
            disabled={statusExport === "process"}
          >
            Export Data
          </CButton>
        </div> */}
      </div>
      <Table
        pagination={false}
        columns={coloumTable}
        dataSource={dataTable}
        style={{ marginTop: 24 }}
        // loading={statusListAttendace === "process"}
        // scroll={{ x: 2500 }}
        rowKey="id"
      />
    </div>
  );
};
export default UITable;
