"use client";
import React from "react";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi Data Booking Rahmat Hidayatullah"
      description="Informasi detail data booking"
    />
  );
};

export default Heading;
