export interface DataType {
  key: string;
  id: string;
  name: string;
  status_booking: string;
  project: string;
  tahap: string;
  blok: string;
  no_unit: string;
  deadline_berkas: string;
  action?: any;
}

export const dataTable: DataType[] = [
  {
    key: "1",
    id: "1",
    name: "Rahmat Hidayatullah",
    status_booking: "Booking - 24/02/2024",
    project: "Mongolia",
    tahap: "2",
    blok: "e-3",
    no_unit: "10",
    deadline_berkas: "14 Hari",
    action: null,
  },
];
