"use client";
import { Pagination } from "antd";
import React from "react";
// import { Pagination } from "antd";
// import { useDispatch, useSelector } from "react-redux";
// import {
//   CHANGE_LIMIT,
//   CHANGE_PAGE,
// } from "@/app/redux/features/attendace/constans";

const PaginateLimit: React.FC = () => {
  //   const dispatch: any = useDispatch();
  //   const attendaces = useSelector((state: any) => state.attendace);

  //   const { amountOfData, page, take } = attendaces;
  const page = 1;
  const take = 10;
  const amountOfData = 10;

  return (
    <Pagination
      current={page}
      defaultCurrent={page}
      pageSize={take}
      defaultPageSize={take}
      total={amountOfData}
      //   onChange={(page: number) => dispatch({ type: CHANGE_PAGE, value: page })}
      onChange={() => {}}
      //   onShowSizeChange={(_, size: number) =>
      //     dispatch({ type: CHANGE_LIMIT, value: size })
      //   }
      onShowSizeChange={() => {}}
    />
  );
};

export default PaginateLimit;
