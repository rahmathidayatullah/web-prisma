"use client";
import React from "react";
import CHeading from "@/app/components/molecules/c-heading";
import { useSelector } from "react-redux";
import CButton from "@/app/components/molecules/c-button";
import {
  ReloadOutlined,
  OrderedListOutlined,
  HeatMapOutlined,
} from "@ant-design/icons";
import Link from "next/link";
import { routerMenu } from "@/app/constants/routes";
import { Space } from "antd";

const Heading = ({ params }: { params: { id: string } }) => {
  //   const sitemap = useSelector((state: any) => state.sitemap);
  //   const { dialogCreateUnit, dataDetail } = sitemap;
  return (
    <div className="flex items-center justify-between">
      <div className="w-full">
        <CHeading
          //   title={`Informasi data detail project ${dataDetail?.project_name}`}
          title={`Informasi Detail Data Booking Rahmat Hidayatullah`}
          description="Data informasi detail booking"
        />
      </div>
      <Space direction="horizontal">
        <Link href={`${routerMenu.SITE_MAP_PROJECT}/${params.id}/phase`}>
          <CButton variant="primary" icon={<OrderedListOutlined />}>
            Kembali
          </CButton>
        </Link>
        <Link href={`${routerMenu.SITE_MAP_PROJECT}/${params.id}/block`}>
          <CButton variant="primary" icon={<HeatMapOutlined />}>
            Cancel Booking
          </CButton>
        </Link>
        <Link href={routerMenu.SITE_MAP_PROJECT}>
          <CButton variant="primary" icon={<ReloadOutlined />}>
            List Seluruh Berkas
          </CButton>
        </Link>
      </Space>
    </div>
  );
};

export default Heading;
