import React from "react";

const InformationKosumen = ({ params }: { params: { id: string } }) => {
  const dataDetail = {
    Nama: "Rahmat Hidayatullah",
    Alamat: "Jakarta",
    "No Hp": "089630912247",
    "No Darurat": "089630912246",
    "No KTP": "123123131212312",
    Project: "The Mognila",
    Tahap: "2",
    Blok: "e-3",
    "No Unit": "10",
  };
  return (
    <div>
      <h1 className="text-sm font-semibold">Informasi Konsumen</h1>
      <div className="mt-4">
        <div className="flex flex-wrap gap-y-5 gap-x-32 items-center">
          {Object.entries(dataDetail).map(([key, value]) => {
            return (
              <div className="flex item-center min-w-96" key={key}>
                <p className="min-w-48">{key}</p>
                <p className="min-w-10">:</p>
                <p>{value}</p>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default InformationKosumen;
