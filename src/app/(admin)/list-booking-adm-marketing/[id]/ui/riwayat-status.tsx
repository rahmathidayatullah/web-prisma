import React from "react";

const RiwayatStatus = ({ params }: { params: { id: string } }) => {
  const data = [
    {
      id: 1,
      name: "Review Admin - 24/02/2024",
    },
    {
      id: 2,
      name: "Proses KPR - 25/02/2024",
    },
    {
      id: 3,
      name: "SP3K - 28/02/2024",
    },
    {
      id: 4,
      name: "Akad - 30/02/2024",
    },
    {
      id: 5,
      name: "Akad - 01/03/2024",
    },
  ];
  return (
    <div>
      <h1 className="text-sm font-medium">Riwayat Status</h1>
      <ul className="flex flex-wrap gap-y-5 items-center mt-5">
        {data.map((item: any, index: any) => {
          if (index === 0) {
            return (
              <li key={item.id}>
                <div className="flex items-center gap-2">
                  <div className="h-4 w-4 rounded-full bg-white border border-green-400"></div>
                  <div className="h-0.5 w-10 bg-[#D7D7D7]"></div>
                  <p>{item.name}</p>
                </div>
              </li>
            );
          }
          return (
            <li key={item.id}>
              <div className="flex items-center gap-2">
                <div className="h-0.5 w-10 bg-[#D7D7D7]"></div>
                <div className="h-4 w-4 rounded-full bg-white border border-green-400"></div>
                <p>{item.name}</p>
              </div>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default RiwayatStatus;
