"use client";
import React, { useState } from "react";
import { Button, Form, Input } from "antd";
import CDialog from "@/app/components/molecules/c-dialog";

const BerkasFromMarketing = ({ params }: { params: { id: string } }) => {
  const [showDialogDelete, setShowDialogDelete] = useState(false);
  const [showDialogDeleteConfirm, setShowDialogDeleteConfirm] = useState(false);
  const [showDialogMessage, setShowDialogMessage] = useState(false);

  const [formConfirmPassowrd] = Form.useForm();
  const [formConfirmMessage] = Form.useForm();

  return (
    <div>
      <h1 className="text-sm font-semibold">Data Berkas Dari Marketing</h1>
      <div className="grid grid-cols-12 mt-5 gap-y-5">
        <div className="col-span-3">
          <div>
            <p>Slip Gaji Pemohon</p>
            <p className="text-[#00C2FF] mt-1">
              http://doc/berkas/slip-gajih.pdf
            </p>
          </div>
        </div>
        <div className="col-span-3">
          <div>
            <p>KTP</p>
            <p className="text-[#00C2FF] mt-1">
              http://doc/berkas/slip-gajih.pdf
            </p>
          </div>
        </div>
        <div className="col-span-3">
          <div>
            <p>BPJS Ketenagakerjaan</p>
            <p className="text-[#00C2FF] mt-1">
              http://doc/berkas/slip-gajih.pdf
            </p>
          </div>
        </div>
        <div className="col-span-3">
          <div>
            <p>SPT Tahunan</p>
            <p className="text-[#00C2FF] mt-1">
              http://doc/berkas/slip-gajih.pdf
            </p>
          </div>
        </div>
        <div className="col-span-3">
          <div>
            <p>Slip Gaji Pemohon Pasangan ( Optional )</p>
            <p className="text-[#00C2FF] mt-1">
              http://doc/berkas/slip-gajih.pdf
            </p>
          </div>
        </div>
        <div className="col-span-3">
          <div>
            <p>KTP Pasangan (Optional) </p>
            <p className="text-[#00C2FF] mt-1">
              http://doc/berkas/slip-gajih.pdf
            </p>
          </div>
        </div>
        <div className="col-span-3">
          <div>
            <p>Form Aplikasi</p>
            <p className="text-[#00C2FF] mt-1">
              http://doc/berkas/slip-gajih.pdf
            </p>
          </div>
        </div>
        <div className="col-span-3">
          <div>
            <p>Best TIme To Call</p>
            <p className="text-[#00C2FF] mt-1">
              http://doc/berkas/slip-gajih.pdf
            </p>
          </div>
        </div>
        <div className="col-span-3">
          <div>
            <p>SK Kerja</p>
            <p className="text-[#00C2FF] mt-1">
              http://doc/berkas/slip-gajih.pdf
            </p>
          </div>
        </div>
        <div className="col-span-3">
          <div>
            <p>NPWP</p>
            <p className="text-[#00C2FF] mt-1">
              http://doc/berkas/slip-gajih.pdf
            </p>
          </div>
        </div>
        <div className="col-span-3">
          <div>
            <p>Surat Keterangan Domisili</p>
            <p className="text-[#00C2FF] mt-1">
              http://doc/berkas/slip-gajih.pdf
            </p>
          </div>
        </div>
        <div className="col-span-3">
          <div>
            <p>Sikasep lolos checking</p>
            <p className="text-[#00C2FF] mt-1">
              http://doc/berkas/slip-gajih.pdf
            </p>
          </div>
        </div>
        <div className="col-span-3">
          <div>
            <p>Rekening Koran</p>
            <p className="text-[#00C2FF] mt-1">
              http://doc/berkas/slip-gajih.pdf
            </p>
          </div>
        </div>
        <div className="col-span-3">
          <div>
            <p>Buku Nikah ( Optional ) </p>
            <p className="text-[#00C2FF] mt-1">
              http://doc/berkas/slip-gajih.pdf
            </p>
          </div>
        </div>
        <div className="col-span-3">
          <div>
            <p>KK</p>
            <p className="text-[#00C2FF] mt-1">
              http://doc/berkas/slip-gajih.pdf
            </p>
          </div>
        </div>
        <div className="col-span-3">
          <div>
            <p>Berkas FLPP</p>
            <p className="text-[#00C2FF] mt-1">
              http://doc/berkas/slip-gajih.pdf
            </p>
          </div>
        </div>
        <div className="col-span-3">
          <div>
            <p>Form Wawancara</p>
            <p className="text-[#00C2FF] mt-1">
              http://doc/berkas/slip-gajih.pdf
            </p>
          </div>
        </div>
        <div className="col-span-3">
          <div>
            <p>BPJS Kesehatan</p>
            <p className="text-[#00C2FF] mt-1">
              http://doc/berkas/slip-gajih.pdf
            </p>
          </div>
        </div>
      </div>
      <div className="w-full flex items-center gap-5 mt-5">
        <Button
          type="primary"
          onClick={() => {
            setShowDialogDelete(true);
          }}
        >
          Berkas Belum Sesuai{" "}
        </Button>
        <Button>Approve Berkas </Button>
      </div>

      <CDialog
        title="Anda yakin ?"
        open={showDialogDelete}
        titleActionOk="Ya"
        titleActionCancel="Tidak"
        actionCancel={() => setShowDialogDelete(false)}
        actionOk={() => {
          setShowDialogDeleteConfirm(true);
          setShowDialogDelete(false);
        }}
      ></CDialog>

      <CDialog
        title="Konfirmasi password untuk menghapus data jabatan ?"
        open={showDialogDeleteConfirm}
        titleActionOk="Setuju"
        titleActionCancel="Tidak"
        actionCancel={() => {
          setShowDialogDeleteConfirm(false);
        }}
        // actionOk={onFinishDelete}
        actionOk={() => {
          setShowDialogDeleteConfirm(false);
          setShowDialogMessage(true);
        }}
        // loadingBtn={statusDeleteRoles === "process"}
      >
        <Form
          // onFinish={onFinishDelete}
          onFinish={() => {}}
          layout="vertical"
          style={{ width: "100%" }}
          form={formConfirmPassowrd}
          name="control-hooks"
        >
          <Form.Item
            label="Password"
            name="password"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <Input.Password />
          </Form.Item>
        </Form>
      </CDialog>

      <CDialog
        title="Masukkan alasan kenapa data belum sesuai ?"
        open={showDialogMessage}
        titleActionOk="Setuju"
        titleActionCancel="Tidak"
        actionCancel={() => {
          setShowDialogMessage(false);
        }}
        // actionOk={onFinishDelete}
        actionOk={() => {
          alert("Konfirmasi berkas berhasil dikirim");
          setShowDialogMessage(false);
        }}
        // loadingBtn={statusDeleteRoles === "process"}
      >
        <Form
          // onFinish={onFinishDelete}
          onFinish={() => {
            alert("Konfirmasi berkas berhasil dikirim");
            setShowDialogMessage(false);
          }}
          layout="vertical"
          style={{ width: "100%" }}
          form={formConfirmMessage}
          name="control-hooks"
        >
          <Form.Item
            label="Alasan belum berkas belum sesuai"
            name="reason"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <Input.TextArea rows={5} />
          </Form.Item>
        </Form>
      </CDialog>
    </div>
  );
};

export default BerkasFromMarketing;
