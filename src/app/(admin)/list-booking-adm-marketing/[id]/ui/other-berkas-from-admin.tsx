"use client";
import React from "react";
import { Button, Form, Input, Upload, message } from "antd";
import { formItemLayout } from "./interface";
import { UploadOutlined } from "@ant-design/icons";
import type { UploadProps } from "antd";

const OtherBerkasFromAdmin = ({ params }: { params: { id: string } }) => {
  const onFinish = (values: any) => {};
  const [form] = Form.useForm();

  const props: UploadProps = {
    accept: ".pdf,.docx",
    maxCount: 1,
    onChange(info) {
      if (info.file.status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === "done") {
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };

  return (
    <div>
      <div className="border-b pb-3">
        <h3 className="text-base font-semibold leading-7 text-gray-900 mb-5">
          Data Berkas Tambahan Admin Marketing
        </h3>
        <Form
          {...formItemLayout}
          onFinish={onFinish}
          layout="vertical"
          style={{ width: "100%" }}
          form={form}
          name="control-hooks"
        >
          <div className="border p-2 rounded-sm">
            <Form.Item name="sallary">
              <div className="flex items-center justify-between gap-3 bg-gray-100 p-2 rounded-md">
                <Upload {...props}>
                  <Button icon={<UploadOutlined />}>
                    Upload Surat Penjualan Rumah ( SPR )
                  </Button>
                </Upload>
                <Button onClick={() => {}}>Download Template</Button>
              </div>
            </Form.Item>
          </div>
          <Button className="mt-3" onClick={() => {}}>
            Upload Berkas
          </Button>
        </Form>
      </div>
      <div className="border-b pb-3 mt-5">
        <h3 className="text-base font-semibold leading-7 text-gray-900">
          Proses Selanjutnya
        </h3>
        <Button disabled className="mt-3" onClick={() => {}}>
          Proses Ke SP3K
        </Button>
        <p className="mt-1 text-xs text-red-600">
          SP3K akan bisa dilakukan setelah mengupload berkas SPR
        </p>
      </div>
    </div>
  );
};

export default OtherBerkasFromAdmin;
