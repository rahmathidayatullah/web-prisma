import React from "react";
import Heading from "./ui/heading";
import ContainerTab from "../../sitemap/projects/[id]/detail-project/ui/container-tab";
import InformationKosumen from "./ui/information-kosumen";
import RiwayatStatus from "./ui/riwayat-status";
import BerkasFromMarketing from "./ui/berkas-from-marketing";
import OtherBerkasFromAdmin from "./ui/other-berkas-from-admin";

const Page = ({ params }: { params: { id: string } }) => {
  return (
    <>
      <div className="h-full overflow-scroll">
        <div className="w-full">
          <Heading params={params} />
        </div>
        <div className="w-full mt-5 pb-5 border-b">
          <InformationKosumen params={params} />
        </div>
        <div className="w-full mt-5 pb-5 border-b">
          <RiwayatStatus params={params} />
        </div>
        <div className="w-full mt-5 pb-5 border-b">
          <BerkasFromMarketing params={params} />
        </div>
        <div className="w-full mt-5 pb-5 border-b">
          <OtherBerkasFromAdmin params={params} />
        </div>
      </div>
    </>
  );
};

export default Page;
