import React from "react";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data sitemap"
      description="Data informasi sitemap"
    />
  );
};

export default Heading;
