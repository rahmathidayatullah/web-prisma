export interface DataType {
  id: string;
  key: string;
  nama_project: string;
  nama_pemilik: string;
  alamat: string;
  harga_strategis_perunit: number;
  luas_lahan_perunit: number;
  luat_bangunan_perunit: number;
  tipe_unit: string;
  upload_site_plan: string;
  upload_gambar: string;
  action: string;
}

export const dataTable: DataType[] = [
  {
    id: "1",
    key: "1",
    nama_project: "The Primerose Emerald",
    nama_pemilik: "PT. Prisma Properties",
    alamat: "Jl Kusuma Bangsa, Jatikarta Bekasi",
    harga_strategis_perunit: 30000000,
    luas_lahan_perunit: 30,
    luat_bangunan_perunit: 60,
    tipe_unit: "30 x 60",
    upload_site_plan: "Image",
    upload_gambar: "Image",
    action: "Image",
  },
  {
    id: "1",
    key: "2",
    nama_project: "The Primerose Emerald",
    nama_pemilik: "PT. Prisma Properties",
    alamat: "Jl Kusuma Bangsa, Jatikarta Bekasi",
    harga_strategis_perunit: 30000000,
    luas_lahan_perunit: 30,
    luat_bangunan_perunit: 60,
    tipe_unit: "30 x 60",
    upload_site_plan: "Image",
    upload_gambar: "Image",
    action: "Image",
  },
];
