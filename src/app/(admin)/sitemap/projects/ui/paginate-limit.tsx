"use client";
import React from "react";
import { Pagination } from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  CHANGE_LIMIT,
  CHANGE_PAGE,
} from "@/app/redux/features/sitemaps/constants";

const PaginateLimit: React.FC = () => {
  const dispatch: any = useDispatch();
  const sitemap = useSelector((state: any) => state.sitemap);
  const { amountOfData, page, take } = sitemap;
  return (
    <Pagination
      current={page}
      defaultCurrent={page}
      pageSize={take}
      defaultPageSize={take}
      total={amountOfData}
      onChange={(page: number) => dispatch({ type: CHANGE_PAGE, value: page })}
      onShowSizeChange={(_, size: number) =>
        dispatch({ type: CHANGE_LIMIT, value: size })
      }
    />
  );
};

export default PaginateLimit;
