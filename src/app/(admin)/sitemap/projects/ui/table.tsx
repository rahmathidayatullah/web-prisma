"use client";

import CSearch from "@/app/components/molecules/c-search";
import { Button, Card, Empty, Space, Spin, Table } from "antd";
const { Meta } = Card;
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import type { DropdownProps, MenuProps, TableColumnsType } from "antd";
import { DataType } from "./interface";
import { routerMenu } from "@/app/constants/routes";
import Link from "next/link";
import CButton from "@/app/components/molecules/c-button";
import { PlusOutlined } from "@ant-design/icons";
import { fetchProjects } from "@/app/redux/features/sitemaps/actions";
import { Image as ImgAntd } from "antd";
import CCheckboxGroup from "@/app/components/molecules/c-checkbox-group";
import CDropdown from "@/app/components/molecules/c-dropdown";
import { useSessionStorage } from "@/app/hooks/useSessionStorage";
import CSelect from "@/app/components/molecules/c-select";
import { optionNewOldData } from "@/app/utils/constants";
import {
  CHANGE_KEYWORD,
  CHANGE_ORDER,
} from "@/app/redux/features/sitemaps/constants";

const UITable: React.FC = () => {
  const dispatch: any = useDispatch();
  const sitemap = useSelector((state: any) => state.sitemap);
  const { statusList, dataList, page, take, order, keyword } = sitemap;
  const [openSourceType, setOpenSourceType] = useState(false);
  const onSearch = () => {};

  const coloumTable: TableColumnsType<DataType> = [
    {
      title: "Nama Project",
      dataIndex: "project_name",
      key: "project_name",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Nama Pemilik",
      dataIndex: "company",
      key: "company",
      render: (value) => {
        if (value) {
          return value.name;
        }
        return "-";
      },
    },
    {
      title: "Alamat",
      dataIndex: "address",
      key: "address",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Harga Strategis PerUnit",
      dataIndex: "strategic_price",
      key: "strategic_price",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Luas Lahan Per Unit",
      dataIndex: "land_area",
      key: "land_area",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Luar Bangunan Per Unit",
      dataIndex: "building_area",
      key: "building_area",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    // {
    //   title: "Tipe Unit",
    //   dataIndex: "tipe_unit",
    //   key: "tipe_unit",
    //   render: (value) => {
    //     if (value) {
    //       return value;
    //     }
    //     return "-";
    //   },
    // },
    {
      title: "Site Plan",
      dataIndex: "siteplan_image",
      key: "siteplan_image",
      render: (value) => {
        if (value) {
          return (
            <ImgAntd width={100} height={100} src="error" fallback={value} />
          );
        }
        return "-";
      },
    },
    {
      title: "Foto Detail Rumah",
      dataIndex: "detail_image",
      key: "detail_image",
      render: (value) => {
        if (value.length) {
          return (
            <div className="flex items-center gap-5">
              {value.map((item: any) => {
                return (
                  <ImgAntd
                    key={item}
                    width={100}
                    height={100}
                    src="error"
                    fallback={item}
                  />
                );
              })}
            </div>
          );
        }
        return "-";
      },
    },

    {
      title: "Aksi",
      key: "action",
      render: (_, record: any) => {
        return (
          <div className="flex items-center gap-4">
            <Link
              href={`${routerMenu.SITE_MAP_PROJECT}/${record.id}/detail-project`}
            >
              <Button
                style={{ backgroundColor: "#219C90" }}
                size="small"
                type="primary"
              >
                Detail
              </Button>
            </Link>
            <Link
              href={`${routerMenu.SITE_MAP_PROJECT}/${record.id}/edit-project`}
            >
              <Button
                style={{ backgroundColor: "#FBB03B" }}
                size="small"
                type="primary"
              >
                Ubah Project
              </Button>
            </Link>
          </div>
        );
      },
    },
  ];

  const defaultCheckedList = coloumTable.map((item: any) => item.key as string);
  const [checkedList, setCheckedList] = useSessionStorage<string[]>(
    "data-projects",
    defaultCheckedList
  );

  const newColumns = coloumTable.map((item: any) => ({
    ...item,
    hidden: !checkedList.includes(item.key as string),
  }));

  const options = coloumTable.map(({ key, title }: any) => ({
    label: title,
    value: key,
  }));

  const items: MenuProps["items"] = [
    {
      label: (
        <CCheckboxGroup
          value={checkedList}
          options={options}
          onChange={(value: any) => {
            setCheckedList(value as string[]);
          }}
        />
      ),
      key: "0",
    },
  ];

  const handleOpenChangeSourceType: DropdownProps["onOpenChange"] = (
    nextOpen,
    info
  ) => {
    if (info.source === "trigger" || nextOpen) {
      setOpenSourceType(nextOpen);
    }
  };

  useEffect(() => {
    dispatch(fetchProjects());
  }, [dispatch, page, take, order, keyword]);

  return (
    <>
      <div className="flex flex-wrap gap-2 sm:gap-5 items-center justify-between my-5">
        <Space direction="horizontal">
          <CSearch
            onSearch={onSearch}
            value={keyword}
            onChange={(event: any) =>
              dispatch({ type: CHANGE_KEYWORD, value: event.target.value })
            }
          />

          <CSelect
            dataOption={optionNewOldData}
            onChange={(event: any) =>
              dispatch({ type: CHANGE_ORDER, value: event })
            }
            value={order}
            placeholder="Filter By Time"
            allowClear={false}
          />
        </Space>
        <div className="flex flex-wrap items-center gap-2 sm:gap-5">
          {/* <CDateRange
            onChange={(_: any, dateString: any) =>
              dispatch({ type: CHANGE_DATE_RANGE, value: dateString })
            }
          /> */}
          <CDropdown
            variant="sortir-coloum"
            menu={{ items }}
            onOpenChange={handleOpenChangeSourceType}
            open={openSourceType}
          />
          {/* <CButton
            icon={<DownloadOutlined />}
            variant="primary"
            onClick={() => dispatch(fetchAttendacesExports())}
            loading={statusExport === "process"}
            disabled={statusExport === "process"}
          >
            Export Data
          </CButton> */}
          <Link href={routerMenu.SITE_MAP_CREATE_PROJECT}>
            <CButton icon={<PlusOutlined />} variant="primary">
              Tambah Project
            </CButton>
          </Link>
        </div>
      </div>

      {/* <Table
        pagination={false}
        columns={newColumns}
        dataSource={dataList}
        style={{ marginTop: 24 }}
        loading={statusList === "process"}
        scroll={{ x: 2500 }}
        rowKey="id"
      /> */}
      <div className="flex flex-wrap gap-10 items-center">
        {statusList === "process" ? (
          <div className="flex w-full items-center justify-center py-10 border">
            <Spin />
          </div>
        ) : dataList.length ? (
          dataList.map((item: any) => {
            return (
              <Card
                key={item.id}
                hoverable
                style={{ width: 240 }}
                cover={
                  <div className="border">
                    {item.project_image ? (
                      <img
                        className="h-[238px] w-[238px] object-contain"
                        alt={`${item.id}-image`}
                        width={238}
                        height={238}
                        src={item.project_image}
                      />
                    ) : (
                      <img
                        className="h-[238px] w-[238px]"
                        alt={`${item.id}-image`}
                        width={238}
                        height={238}
                        src=""
                      />
                    )}
                  </div>
                }
              >
                <Meta
                  title={item.project_name}
                  // description={moment(item.createAt).format("DD MMM YYYY")}
                />
                <div className="flex gap-5 items-center mt-5">
                  <Link
                    href={`${routerMenu.SITE_MAP_PROJECT}/${item.id}/detail-project`}
                  >
                    <Button className="w-full">Detail</Button>
                  </Link>
                  <Link
                    href={`${routerMenu.SITE_MAP_PROJECT}/${item.id}/edit-project`}
                  >
                    <Button className="w-full">Edit</Button>
                  </Link>
                </div>
              </Card>
            );
          })
        ) : (
          <div className="flex w-full items-center justify-center py-10 border">
            <Empty />
          </div>
        )}
      </div>
    </>
  );
};

export default UITable;
