import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data project"
      description="Menambahkan data project baru"
    />
  );
};

export default Heading;
