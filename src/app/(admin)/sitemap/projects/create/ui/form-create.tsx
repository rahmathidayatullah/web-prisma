"use client";

import React, { useEffect, useState } from "react";
import { useGlobalContext } from "@/app/context/store";
import { useRouter } from "next/navigation";
import { useDispatch, useSelector } from "react-redux";
import { Form, Input, Select } from "antd";
import CSelect from "@/app/components/molecules/c-select";
import Link from "next/link";
import { routerMenu } from "@/app/constants/routes";
import CButtonAction from "@/app/components/molecules/c-button-action";
import { PlusOutlined, ReloadOutlined } from "@ant-design/icons";
import CUploadMultiple from "@/app/components/molecules/c-upload-multiple";
import { fetchCompanies } from "@/app/redux/features/master-data-attendaces/actions";
import {
  createProject,
  fetchProjectDetail,
  updateProject,
} from "@/app/redux/features/sitemaps/actions";
import {
  RESET_CREATE_PROJECT,
  RESET_STATUS_CREATE_PROJECT,
  RESET_STATUS_FETCH_PROJECT_DETAIL,
  RESET_STATUS_UPDATE_PROJECT,
  RESET_UPDATE_PROJECT,
} from "@/app/redux/features/sitemaps/constants";
import CInputNumber from "@/app/components/molecules/c-input-number";
import type { SelectProps } from "antd";

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    // sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    // sm: { span: 14 },
  },
};

const FormCreate = ({ params }: any) => {
  const router = useRouter();
  const dispatch: any = useDispatch();
  const { openMessage, openNotification } = useGlobalContext();
  const sitemap = useSelector((state: any) => state.sitemap);
  const {
    statusCreate,
    dataErrorCreate,
    statusUpdate,
    dataErrorUpdate,
    statusDetail,
    dataDetail,
  } = sitemap;
  const masterDataAttendaces = useSelector(
    (state: any) => state.masterDataAttendaces
  );
  const { dataCompanies } = masterDataAttendaces;
  const [form] = Form.useForm();

  const onFinish = (values: any) => {
    if (params?.id) {
      if (!imageSitePlan.length || !project_image.length) {
        alert("gambar sitepalan dan foto proyek harus diisi");
      } else {
        const newData: any = [];
        if (imageDetail.length) {
          imageDetail.forEach((item: any) => {
            if (item.originFileObj) {
              newData.push(item.originFileObj);
            } else {
              newData.push(item.url);
            }
          });
        }
        const body = {
          ...values,
          siteplan_image:
            imageSitePlan[0]?.originFileObj ?? imageSitePlan[0]?.url,
          project_image:
            project_image[0]?.originFileObj ?? project_image[0]?.url,
          detail_image: newData.length ? newData : null,
        };
        dispatch(updateProject(params.id, body));
      }
    } else {
      if (!imageSitePlan.length || !project_image.length) {
        alert("gambar harus diisi");
      } else {
        const body = {
          ...values,
          siteplan_image: imageSitePlan[0].originFileObj,
          project_image:
            project_image[0]?.originFileObj ?? project_image[0]?.url,
          detail_image: imageDetail?.length
            ? imageDetail.map((item: any) => item?.originFileObj)
            : null,
        };
        dispatch(createProject(body));
      }
    }
  };

  const onChangeOwner = (event: any) => {};

  const [imageSitePlan, setImageSitePlan] = useState<any>([]);
  const handleUploadSitePlan = (event: any) => {
    setImageSitePlan(event);
  };

  const [project_image, setProject_image] = useState<any>([]);
  const handleUploadProyek = (event: any) => {
    setProject_image(event);
  };

  const [imageDetail, setImageDetail] = useState<any>([]);
  const handleUploadDetail = (event: any) => {
    setImageDetail(event);
  };

  const onReset = () => {
    form.resetFields();
    setImageSitePlan([]);
    setImageDetail([]);
    dispatch({ type: RESET_CREATE_PROJECT });
    dispatch({ type: RESET_UPDATE_PROJECT });
  };

  const onSetValueDetail = () => {
    Object.keys(dataDetail).forEach((key) => {
      const value = dataDetail[key];
      if (key === "company") {
        form.setFieldValue(key, value.id);
      } else if (key === "siteplan_image") {
        if (value) {
          setImageSitePlan([
            {
              uid: "1",
              name: "siteplan_image.png",
              status: "done",
              url: value,
            },
          ]);
        }
        form.setFieldValue(key, value);
      } else if (key === "project_image") {
        if (value) {
          setProject_image([
            {
              uid: "1",
              name: "project_image.png",
              status: "done",
              url: value,
            },
          ]);
        }
        form.setFieldValue(key, value);
      } else if (key === "detail_image") {
        if (value) {
          const newData = value.map((item: any, index: any) => {
            return {
              uid: index + 1,
              name: `detail_image.png ${index + 1}.png`,
              status: "done",
              url: item,
            };
          });
          setImageDetail(newData);
        }
        form.setFieldValue(key, "fill data");
      } else {
        form.setFieldValue(key, value);
      }
    });
  };

  // const options: SelectProps["options"] = [];

  // const handleChange = (value: string) => {
  // };

  useEffect(() => {
    if (statusCreate === "success") {
      openMessage("success", "Berhasil menambahkan project baru .", "top");
      onReset();
      router.push(routerMenu.SITE_MAP_PROJECT);
    }
    if (statusCreate === "error") {
      openMessage(
        "error",
        dataErrorCreate?.response?.data?.message ??
          "Terjadi kesalahan, gagal menambahkan project baru .",
        "top"
      );
      dispatch({ type: RESET_STATUS_CREATE_PROJECT });
    }
    if (statusUpdate === "success") {
      openMessage("success", "Berhasil merubah project .", "top");
      onReset();
      router.push(routerMenu.SITE_MAP_PROJECT);
    }
    if (statusUpdate === "error") {
      openMessage(
        "error",
        dataErrorUpdate?.response?.data?.message ??
          "Terjadi kesalahan, gagal merubah project .",
        "top"
      );
      dispatch({ type: RESET_STATUS_UPDATE_PROJECT });
    }
    if (statusDetail === "success") {
      onSetValueDetail();
    }
  }, [statusCreate, statusUpdate, statusDetail]);

  useEffect(() => {
    dispatch(fetchCompanies());
    if (params?.id) {
      dispatch(fetchProjectDetail(params.id));
    } else {
      form.resetFields();
      setImageDetail([]);
      setImageSitePlan([]);
      setProject_image([]);
    }
  }, [dispatch, params]);

  return (
    <>
      <Form
        {...formItemLayout}
        onFinish={onFinish}
        layout="vertical"
        style={{ width: "100%" }}
        form={form}
        name="control-hooks"
      >
        <Form.Item
          label="Foto Proyek"
          name="project_image"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <div className="border rounded-md">
            {/* <CUploadSingle acceptImg=".svg" onChange={handleUploadSitePlan} /> */}
            <CUploadMultiple
              maxCount={1}
              onChange={handleUploadProyek}
              dataFileList={project_image}
            />
          </div>
        </Form.Item>

        <Form.Item
          label="Nama Project"
          name="project_name"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label="Nama Pemilik"
          name="company"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <CSelect
            dataOption={dataCompanies}
            onChange={onChangeOwner}
            value={form.getFieldValue("company")}
          />
        </Form.Item>
        <Form.Item
          label="Alamat"
          name="address"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <Input.TextArea rows={3} />
        </Form.Item>

        {/* <Form.Item
          label="Harga Strategis"
          name="strategic_price"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <CInputNumber rupiah style={{ width: "100%" }} />
        </Form.Item> */}

        {/* <Form.Item
          label="Harga Dasar"
          name="base_price"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <CInputNumber rupiah style={{ width: "100%" }} />
        </Form.Item> */}
        <Form.Item
          label="Luas Lahan ㎡"
          name="land_area"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <CInputNumber style={{ width: "100%" }} />
        </Form.Item>
        <Form.Item
          label="Luas Bangunan ㎡"
          name="building_area"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <CInputNumber style={{ width: "100%" }} />
        </Form.Item>
        {/* <Form.Item
          label="Tipe Unit"
          name="type_unit"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <Input value="30x60" />
        </Form.Item> */}

        {/* <Form.Item
          label="Tahap"
          name="phase"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <CInputNumber style={{ width: "100%" }} />
        </Form.Item>

        <Form.Item
          label="Blok"
          name="block"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <Select
            mode="tags"
            style={{ width: "100%" }}
            placeholder="Tags Mode"
            onChange={handleChange}
            options={options}
          />
        </Form.Item> */}

        <Form.Item
          label="Unggah site plan"
          name="siteplan_image"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <div className="border rounded-md">
            {/* <CUploadSingle acceptImg=".svg" onChange={handleUploadSitePlan} /> */}
            <CUploadMultiple
              maxCount={1}
              acceptImg=".svg"
              onChange={handleUploadSitePlan}
              dataFileList={imageSitePlan}
            />
          </div>
        </Form.Item>

        <Form.Item label="Unggah Detail Rumah" name="detail_image">
          <div className="border rounded-md">
            <CUploadMultiple
              maxCount={4}
              onChange={handleUploadDetail}
              dataFileList={imageDetail}
            />
          </div>
        </Form.Item>
        <div className="grid grid-cols-12 pt-10 mt-10 border-t">
          <div className="col-span-12">
            <Form.Item>
              <div className="flex gap-5 items-center justify-end">
                <Link href={routerMenu.SITE_MAP_PROJECT}>
                  <CButtonAction
                    icon={<ReloadOutlined />}
                    htmlType="button"
                    //   disabled={statusCreate === "process"}
                    //   loading={statusCreate === "process"}
                  >
                    Kembali
                  </CButtonAction>
                </Link>

                <CButtonAction
                  icon={<PlusOutlined />}
                  disabled={statusCreate === "process"}
                  loading={statusCreate === "process"}
                  htmlType="submit"
                  variant="primary"
                >
                  {params?.id ? "Ubah data" : "Tambah data"}
                </CButtonAction>
              </div>
            </Form.Item>
          </div>
        </div>
      </Form>
    </>
  );
};

export default FormCreate;
