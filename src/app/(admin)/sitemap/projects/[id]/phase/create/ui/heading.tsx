"use client";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data tahap"
      description="Menambahkan data tahap baru"
    />
  );
};

export default Heading;
