"use client";

import React, { useEffect } from "react";

import { useRouter } from "next/navigation";
import { formItemLayout } from "./interface";

import { useDispatch, useSelector } from "react-redux";

import { Input, Form } from "antd";
import Link from "next/link";
import CButtonAction from "@/app/components/molecules/c-button-action";
import { SaveOutlined, ReloadOutlined } from "@ant-design/icons";
import { routerMenu } from "@/app/constants/routes";
import { useGlobalContext } from "@/app/context/store";
import {
  RESET_STATUS_UPDATE_PHASES,
  RESET_UPDATE_PHASES,
} from "@/app/redux/features/phases/constans";
import {
  fetchPhasesDetail,
  updatePhases,
} from "@/app/redux/features/phases/actions";
import CInputNumber from "@/app/components/molecules/c-input-number";

const FormUpdate = ({
  params,
}: {
  params: { idPhase: string; id: string };
}) => {
  const { openMessage } = useGlobalContext();
  const router = useRouter();
  const dispatch: any = useDispatch();
  const [form] = Form.useForm();

  const phases = useSelector((state: any) => state.phases);
  const {
    dataPhasesDetail,
    statusDetailPhases,
    errorUpdate,
    statusUpdatePhases,
  } = phases;

  const onReset = () => {
    form.resetFields();
    dispatch({ type: RESET_UPDATE_PHASES });
  };

  const onFinish = (values: any) => {
    const body = {
      ...values,
    };
    dispatch(updatePhases(params.idPhase, body));
  };

  const onSetValueDetail = () => {
    Object.keys(dataPhasesDetail).forEach((key) => {
      const value = dataPhasesDetail[key];
      form.setFieldValue(key, value);
    });
  };

  useEffect(() => {
    if (statusDetailPhases === "success") {
      onSetValueDetail();
    }

    if (statusUpdatePhases === "success") {
      openMessage("success", "Berhasil merubah data tahap .", "top");
      onReset();
      router.push(`${routerMenu.SITE_MAP_PROJECT}/${params.id}/phase`);
    }
    if (statusUpdatePhases === "error") {
      openMessage(
        "error",
        errorUpdate?.response?.data?.message ?? "Gagal merubah data tahap .",
        "top"
      );
      dispatch({ type: RESET_STATUS_UPDATE_PHASES });
    }
  }, [dispatch, statusUpdatePhases, statusDetailPhases]);

  useEffect(() => {
    dispatch(fetchPhasesDetail(params.idPhase));
  }, [dispatch, params.idPhase]);

  return (
    <>
      <Form
        {...formItemLayout}
        onFinish={onFinish}
        layout="vertical"
        style={{ width: "100%" }}
        form={form}
        name="control-hooks"
      >
        <div className="grid grid-cols-12 gap-5">
          <div className="col-span-6">
            <Form.Item
              label="Nama Tahap"
              name="name"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="Harga Tanah Lebih"
              name="more_land_price"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CInputNumber rupiah style={{ width: "100%" }} />
            </Form.Item>
            <Form.Item
              label="Booking Fee"
              name="booking_fee"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CInputNumber rupiah style={{ width: "100%" }} />
            </Form.Item>
            <Form.Item
              label="Harga DP"
              name="down_payment"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CInputNumber rupiah style={{ width: "100%" }} />
            </Form.Item>
            <Form.Item
              label="Harga Dasar"
              name="base_price"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CInputNumber rupiah style={{ width: "100%" }} />
            </Form.Item>
            <Form.Item
              label="Cashback"
              name="cashback"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CInputNumber rupiah style={{ width: "100%" }} />
            </Form.Item>
            <Form.Item
              label="Dekat Jalan Utama"
              name="strategic_main_road"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CInputNumber rupiah style={{ width: "100%" }} />
            </Form.Item>
            <Form.Item
              label="Dekat Fasilitas Umum"
              name="strategic_facility"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <CInputNumber rupiah style={{ width: "100%" }} />
            </Form.Item>
          </div>
        </div>
        <div className="grid grid-cols-12 pt-10 mt-10 border-t">
          <div className="col-span-12">
            <Form.Item>
              <div className="flex gap-5 items-center justify-start">
                <Link
                  href={`${routerMenu.SITE_MAP_PROJECT}/${params.id}/phase`}
                >
                  <CButtonAction
                    icon={<ReloadOutlined />}
                    htmlType="button"
                    disabled={statusUpdatePhases === "process"}
                    loading={statusUpdatePhases === "process"}
                  >
                    Kembali
                  </CButtonAction>
                </Link>
                <CButtonAction
                  icon={<SaveOutlined />}
                  disabled={statusUpdatePhases === "process"}
                  loading={statusUpdatePhases === "process"}
                  htmlType="submit"
                  variant="primary"
                >
                  Ubah data
                </CButtonAction>
              </div>
            </Form.Item>
          </div>
        </div>
      </Form>
    </>
  );
};

export default FormUpdate;
