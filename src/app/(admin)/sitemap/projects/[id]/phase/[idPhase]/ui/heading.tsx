"use client";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading title="Informasi data tahap" description="Merubah data tahap" />
  );
};

export default Heading;
