"use client";
import React from "react";
import { Pagination } from "antd";
import { useDispatch, useSelector } from "react-redux";
import {
  CHANGE_LIMIT,
  CHANGE_PAGE,
} from "@/app/redux/features/phases/constans";

const PaginateLimit: React.FC = () => {
  const dispatch: any = useDispatch();
  const phases = useSelector((state: any) => state.phases);
  const { amountOfData, page, take } = phases;

  return (
    <Pagination
      current={page}
      defaultCurrent={page}
      pageSize={take}
      defaultPageSize={take}
      total={amountOfData}
      onChange={(page: number) => dispatch({ type: CHANGE_PAGE, value: page })}
      onShowSizeChange={(_, size: number) =>
        dispatch({ type: CHANGE_LIMIT, value: size })
      }
    />
  );
};

export default PaginateLimit;
