import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data project"
      description="Merubah data project"
    />
  );
};

export default Heading;
