"use client";
import React from "react";
import FormCreate from "../../../create/ui/form-create";

const FormUpdate = ({ params }: { params: { id: string } }) => {
  return <FormCreate params={params} />;
};

export default FormUpdate;
