import React from "react";
import CHeading from "@/app/components/molecules/c-heading";
import { Space } from "antd";
import Link from "next/link";
import { routerMenu } from "@/app/constants/routes";
import CButton from "@/app/components/molecules/c-button";
import { ReloadOutlined } from "@ant-design/icons";

const Heading = ({ params }: { params: { id: string } }) => {
  return (
    <div className="flex items-center justify-between">
      <div className="w-full">
        <CHeading
          title="Informasi data blok"
          description="Data informasi blok, seluruh data blok"
        />
      </div>
      <Space direction="horizontal">
        <Link
          href={`${routerMenu.SITE_MAP_PROJECT}/${params.id}/detail-project`}
        >
          <CButton variant="primary" icon={<ReloadOutlined />}>
            Kembali
          </CButton>
        </Link>
      </Space>
    </div>
  );
};

export default Heading;
