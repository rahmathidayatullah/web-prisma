import FormCreate from "./ui/form-create";
import Heading from "./ui/heading";
const Page = ({ params }: { params: { id: string } }) => {
  return (
    <>
      <div className="flex flex-col justify-center items-start h-full">
        <div className="w-full">
          <Heading />
        </div>
        <div className="h-full w-full overflow-scroll pt-10">
          <FormCreate params={params} />
        </div>
      </div>
    </>
  );
};

export default Page;
