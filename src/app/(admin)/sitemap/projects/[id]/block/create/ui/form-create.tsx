"use client";

import React, { useEffect } from "react";

import { useRouter } from "next/navigation";
import { formItemLayout } from "./interface";

import { useDispatch, useSelector } from "react-redux";

import { Input, Form } from "antd";
import Link from "next/link";
import CButtonAction from "@/app/components/molecules/c-button-action";
import { SaveOutlined, ReloadOutlined } from "@ant-design/icons";
import { routerMenu } from "@/app/constants/routes";
import { useGlobalContext } from "@/app/context/store";
import {
  RESET_CREATE_BLOCKS,
  RESET_STATUS_CREATE_BLOCKS,
} from "@/app/redux/features/blocks/constans";
import { createBlocks } from "@/app/redux/features/blocks/actions";

const FormCreate = ({ params }: { params: { id: string } }) => {
  const { openMessage } = useGlobalContext();
  const router = useRouter();
  const dispatch: any = useDispatch();
  const [form] = Form.useForm();

  const blocks = useSelector((state: any) => state.blocks);
  const { errorCreate, statusCreateBlocks } = blocks;

  const onReset = () => {
    form.resetFields();
    dispatch({ type: RESET_CREATE_BLOCKS });
  };

  const onFinish = (values: any) => {
    const body = {
      ...values,
    };
    dispatch(createBlocks(body, params.id));
  };

  useEffect(() => {
    if (statusCreateBlocks === "success") {
      openMessage("success", "Berhasil menambahkan blok baru .", "top");
      onReset();
      router.push(`${routerMenu.SITE_MAP_PROJECT}/${params.id}/block`);
    }
    if (statusCreateBlocks === "error") {
      openMessage(
        "error",
        errorCreate?.response?.data?.message ?? "Gagal menambahkan blok baru .",
        "top"
      );
      dispatch({ type: RESET_STATUS_CREATE_BLOCKS });
    }
  }, [dispatch, statusCreateBlocks]);

  return (
    <>
      <Form
        {...formItemLayout}
        onFinish={onFinish}
        layout="vertical"
        style={{ width: "100%" }}
        form={form}
        name="control-hooks"
      >
        <div className="grid grid-cols-12 gap-5">
          <div className="col-span-6">
            <Form.Item
              label="Nama Blok"
              name="name"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <Input />
            </Form.Item>
          </div>
        </div>
        <div className="grid grid-cols-12 pt-10 mt-10 border-t">
          <div className="col-span-12">
            <Form.Item>
              <div className="flex gap-5 items-center justify-start">
                <Link
                  href={`${routerMenu.SITE_MAP_PROJECT}/${params.id}/block`}
                >
                  <CButtonAction
                    icon={<ReloadOutlined />}
                    htmlType="button"
                    disabled={statusCreateBlocks === "process"}
                    loading={statusCreateBlocks === "process"}
                  >
                    Kembali
                  </CButtonAction>
                </Link>
                <CButtonAction
                  icon={<SaveOutlined />}
                  disabled={statusCreateBlocks === "process"}
                  loading={statusCreateBlocks === "process"}
                  htmlType="submit"
                  variant="primary"
                >
                  Tambah data
                </CButtonAction>
              </div>
            </Form.Item>
          </div>
        </div>
      </Form>
    </>
  );
};

export default FormCreate;
