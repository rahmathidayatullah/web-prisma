"use client";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading
      title="Informasi data blok"
      description="Menambahkan data blok baru"
    />
  );
};

export default Heading;
