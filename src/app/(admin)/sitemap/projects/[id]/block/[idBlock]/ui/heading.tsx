"use client";
import CHeading from "@/app/components/molecules/c-heading";

const Heading = () => {
  return (
    <CHeading title="Informasi data blok" description="Merubah data blok" />
  );
};

export default Heading;
