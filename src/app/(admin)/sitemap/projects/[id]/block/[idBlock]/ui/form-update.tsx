"use client";

import React, { useEffect } from "react";

import { useRouter } from "next/navigation";
import { formItemLayout } from "./interface";

import { useDispatch, useSelector } from "react-redux";

import { Input, Form } from "antd";
import Link from "next/link";
import CButtonAction from "@/app/components/molecules/c-button-action";
import { SaveOutlined, ReloadOutlined } from "@ant-design/icons";
import { routerMenu } from "@/app/constants/routes";
import { useGlobalContext } from "@/app/context/store";
import {
  RESET_STATUS_UPDATE_BLOCKS,
  RESET_UPDATE_BLOCKS,
} from "@/app/redux/features/blocks/constans";
import {
  fetchBlocksDetail,
  updateBlocks,
} from "@/app/redux/features/blocks/actions";

const FormUpdate = ({
  params,
}: {
  params: { idBlock: string; id: string };
}) => {
  const { openMessage } = useGlobalContext();
  const router = useRouter();
  const dispatch: any = useDispatch();
  const [form] = Form.useForm();

  const blocks = useSelector((state: any) => state.blocks);
  const {
    dataBlocksDetail,
    statusDetailBlocks,
    errorUpdate,
    statusUpdateBlocks,
  } = blocks;

  const onReset = () => {
    form.resetFields();
    dispatch({ type: RESET_UPDATE_BLOCKS });
  };

  const onFinish = (values: any) => {
    const body = {
      ...values,
    };
    dispatch(updateBlocks(params.idBlock, body));
  };

  const onSetValueDetail = () => {
    Object.keys(dataBlocksDetail).forEach((key) => {
      const value = dataBlocksDetail[key];
      form.setFieldValue(key, value);
    });
  };

  useEffect(() => {
    if (statusDetailBlocks === "success") {
      onSetValueDetail();
    }

    if (statusUpdateBlocks === "success") {
      openMessage("success", "Berhasil merubah data blok .", "top");
      onReset();
      router.push(`${routerMenu.SITE_MAP_PROJECT}/${params.id}/block`);
    }
    if (statusUpdateBlocks === "error") {
      openMessage(
        "error",
        errorUpdate?.response?.data?.message ?? "Gagal merubah data blok .",
        "top"
      );
      dispatch({ type: RESET_STATUS_UPDATE_BLOCKS });
    }
  }, [dispatch, statusUpdateBlocks, statusDetailBlocks]);

  useEffect(() => {
    dispatch(fetchBlocksDetail(params.idBlock));
  }, [dispatch, params.idBlock]);

  return (
    <>
      <Form
        {...formItemLayout}
        onFinish={onFinish}
        layout="vertical"
        style={{ width: "100%" }}
        form={form}
        name="control-hooks"
      >
        <div className="grid grid-cols-12 gap-5">
          <div className="col-span-6">
            <Form.Item
              label="Nama Blok"
              name="name"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <Input />
            </Form.Item>
          </div>
        </div>
        <div className="grid grid-cols-12 pt-10 mt-10 border-t">
          <div className="col-span-12">
            <Form.Item>
              <div className="flex gap-5 items-center justify-start">
                <Link
                  href={`${routerMenu.SITE_MAP_PROJECT}/${params.id}/block`}
                >
                  <CButtonAction
                    icon={<ReloadOutlined />}
                    htmlType="button"
                    disabled={statusUpdateBlocks === "process"}
                    loading={statusUpdateBlocks === "process"}
                  >
                    Kembali
                  </CButtonAction>
                </Link>
                <CButtonAction
                  icon={<SaveOutlined />}
                  disabled={statusUpdateBlocks === "process"}
                  loading={statusUpdateBlocks === "process"}
                  htmlType="submit"
                  variant="primary"
                >
                  Ubah data
                </CButtonAction>
              </div>
            </Form.Item>
          </div>
        </div>
      </Form>
    </>
  );
};

export default FormUpdate;
