import React from "react";
import Heading from "./ui/heading";
import PlotPlan from "./ui/tabs/plot-plan";
import DialogFormUnit from "./ui/dialog-form-unit";
import ContainerTab from "./ui/container-tab";
// import UITable from "./ui/table";
// import PaginateLimit from "./ui/paginate-limit";

const Page = ({ params }: { params: { id: string } }) => {
  return (
    <>
      <div className="flex flex-col justify-center items-start h-full">
        <div className="w-full">
          <Heading params={params} />
        </div>
        <div className="h-full w-full overflow-scroll">
          {/* <UITable /> */}
          <div className="mt-5">
            <ContainerTab params={params} />
            {/* <PlotPlan params={params} /> */}
          </div>
          <div className="mt-10 flex justify-end">
            {/* <PaginateLimit /> */}
          </div>
        </div>
      </div>
    </>
  );
};

export default Page;
