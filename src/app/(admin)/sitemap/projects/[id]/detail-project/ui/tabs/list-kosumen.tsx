"use client";
import { Button, Checkbox } from "antd";
import React, { useState } from "react";

import { Space, Table, Tag } from "antd";
import type { TableProps } from "antd";
import { TypeListKonsumen } from "./model";

const ListKonsumen = ({ params }: { params: { id: string } }) => {
  const [selectKonsumen, setSelectKonsumen] = useState<TypeListKonsumen[] | []>(
    []
  );
  const downloadDoc = () => {};
  const onChangeCheckbox = (e: any, record: any) => {
    let tempSelectKonsumen = [...selectKonsumen];
    if (!tempSelectKonsumen.length) {
      tempSelectKonsumen.push(record);
    } else {
      // Check if the item already exists in the data
      if (!tempSelectKonsumen.some((item: any) => item.id === record.id)) {
        tempSelectKonsumen = [...tempSelectKonsumen, record];
      } else {
        tempSelectKonsumen = tempSelectKonsumen.filter(
          (item: any) => item.id !== record.id
        );
      }
    }
    setSelectKonsumen(tempSelectKonsumen);

    console.log("record", record);
  };
  const columnsListKonsumen: TableProps<TypeListKonsumen>["columns"] = [
    {
      title: "Nama",
      dataIndex: "name_konsumen",
      key: "name_konsumen",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Status Booking",
      dataIndex: "status_booking",
      key: "status_booking",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Project",
      dataIndex: "project",
      key: "project",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Tahap",
      dataIndex: "tahap",
      key: "tahap",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Blok",
      dataIndex: "blok",
      key: "blok",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Nomor Unit",
      dataIndex: "no_unit",
      key: "no_unit",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Status Pembayaran",
      dataIndex: "status_pemabayaran",
      key: "status_pemabayaran",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Kirim Info Akad",
      dataIndex: "action",
      key: "action",
      render: (_, record) => (
        <div className="flex items-center gap-4">
          <Checkbox onChange={(e) => onChangeCheckbox(e, record)}></Checkbox>
        </div>
      ),
    },
  ];
  const dataListKonsumen: TypeListKonsumen[] = [
    {
      id: "1",
      key: "1",
      name_konsumen: "Rahmat Hidayatullah",
      status_booking: "Akad - 24/02/2024",
      project: "Mongolia",
      tahap: "1",
      blok: "a-1",
      no_unit: "10",
      status_pemabayaran: "Lunas",
      kirim_info_akad: null,
    },
    {
      id: "2",
      key: "2",
      name_konsumen: "Malik Ibrahim",
      status_booking: "Akad - 24/02/2024",
      project: "Mongolia",
      tahap: "1",
      blok: "a-5",
      no_unit: "15",
      status_pemabayaran: "Lunas",
      kirim_info_akad: null,
    },
    {
      id: "3",
      key: "3",
      name_konsumen: "Nur Chayo",
      status_booking: "Akad - 24/02/2024",
      project: "Mongolia",
      tahap: "1",
      blok: "c-2",
      no_unit: "20",
      status_pemabayaran: "Belum Lunas",
      kirim_info_akad: null,
    },
  ];

  const columnsListKonsumenSelect: TableProps<TypeListKonsumen>["columns"] = [
    {
      title: "Nama",
      dataIndex: "name_konsumen",
      key: "name_konsumen",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Status Booking",
      dataIndex: "status_booking",
      key: "status_booking",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Project",
      dataIndex: "project",
      key: "project",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Tahap",
      dataIndex: "tahap",
      key: "tahap",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Blok",
      dataIndex: "blok",
      key: "blok",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Nomor Unit",
      dataIndex: "no_unit",
      key: "no_unit",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Status Pembayaran",
      dataIndex: "status_pemabayaran",
      key: "status_pemabayaran",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
  ];
  const dataListKonsumenSelect: TypeListKonsumen[] = selectKonsumen;

  return (
    <div>
      <div className="border-b">
        <h1 className="text-sm font-semibold leading-7 text-gray-900 mb-3">
          List Konsumen
        </h1>

        <Table columns={columnsListKonsumen} dataSource={dataListKonsumen} />
      </div>
      <div className="border-b mt-5">
        <div className="flex items-center justify-between mb-5">
          <h1 className="text-sm font-semibold leading-7 text-gray-900 mb-3">
            Konsumen Dipilih
          </h1>
          {selectKonsumen.length ? <Button>Kirim Undangan</Button> : ""}
        </div>
        <Table
          columns={columnsListKonsumenSelect}
          dataSource={dataListKonsumenSelect}
        />
      </div>
    </div>
  );
};

export default ListKonsumen;
