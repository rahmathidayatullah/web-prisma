export const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    // sm: { span: 6 },
  },
  wrapperCol: {
    xs: { span: 24 },
    // sm: { span: 14 },
  },
};

export const information_code_data = [
  {
    id: 2,
    code: "blue",
    description: "OPEN",
  },
  {
    id: 3,
    code: "green",
    description: "Terbooking",
  },
  {
    id: 4,
    code: "yellow",
    description: "Proses KPR",
  },
  {
    id: 1,
    code: "red",
    description: "SP3K",
  },
  {
    id: 6,
    code: "lightblue",
    description: "Akad",
  },
  {
    id: 5,
    // code: "#7bbea3",
    code: "cyan",
    description: "Serah Terima",
  },
];

export interface DataType {
  id: string;
  key: string;
  tahap: string;
  blok: string;
  nomor: string;
  luas_lahan_perunit: number;
  luat_bangunan_perunit: number;
  tipe_unit: string;
  harga_dasar: number;
  strategis: string;
  harga_strategis_perunit: number;
  siteplan: string;
  detail_home: string;
  action: string;
}

export const dataTable: DataType[] = [
  {
    id: "1",
    key: "1",
    tahap: "1",
    blok: "e-1",
    nomor: "1",
    luas_lahan_perunit: 30,
    luat_bangunan_perunit: 60,
    tipe_unit: "e-1",
    harga_dasar: 30000000,
    strategis: "Ya",
    harga_strategis_perunit: 30000000,
    siteplan: "image",
    detail_home: "image multiple",
    action: "Image",
  },
];
