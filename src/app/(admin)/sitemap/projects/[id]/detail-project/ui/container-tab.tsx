"use client";
import React from "react";
import { Tabs } from "antd";
import type { TabsProps } from "antd";
import PlotPlan from "./tabs/plot-plan";
import ListLPA from "./tabs/list-lpa";
import ListSiAkad from "./tabs/list-siakad";
import ListKonsumen from "./tabs/list-kosumen";

const ContainerTab = ({ params }: { params: { id: string } }) => {
  const onChange = (key: string) => {
    console.log(key);
  };

  const items: TabsProps["items"] = [
    {
      key: "sitemenu",
      label: "Sitemenu",
      children: <PlotPlan params={params} />,
    },
    {
      key: "list-lpa",
      label: "List LPA",
      children: <ListLPA params={params} />,
    },
    {
      key: "list-siakad",
      label: "List Siakad",
      children: <ListSiAkad params={params} />,
    },
    {
      key: "kosumen",
      label: "Konsumen",
      children: <ListKonsumen params={params} />,
    },
  ];
  return (
    <div>
      <Tabs defaultActiveKey="1" items={items} onChange={onChange} />
    </div>
  );
};

export default ContainerTab;
