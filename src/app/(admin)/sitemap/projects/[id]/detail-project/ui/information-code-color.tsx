import { ColorPicker } from "antd";
import { information_code_data } from "./interface";

const InformationCodeColor = () => {
  return (
    <div>
      <h1 className="text-sm font-medium">Informasi Kode Warna</h1>
      <div className="flex flex-wrap items-center gap-10 mt-4">
        {information_code_data.map((item: any) => (
          <div key={item.id} className="flex gap-5 items-center mt-3">
            <ColorPicker disabled defaultValue={item.code} />
            {/* <p>{item.code}</p> */}
            <p>{item.description}</p>
          </div>
        ))}
      </div>
    </div>
  );
};

export default InformationCodeColor;
