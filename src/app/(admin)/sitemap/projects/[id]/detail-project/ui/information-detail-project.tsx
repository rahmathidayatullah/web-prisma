"use client";
import { useSelector } from "react-redux";
import { formatRupiah } from "@/app/constants/helper";

const InformationDetailProject = () => {
  const sitemap = useSelector((state: any) => state.sitemap);
  const { dataDetail: detail } = sitemap;
  const dataDetail = {
    "Nama Project": detail?.project_name ?? "-",
    "Nama Pemilik": detail?.company?.name ?? "-",
    Alamat: detail?.address ?? "-",
    // "Harga Strategis": detail?.strategic_price
    //   ? formatRupiah(detail?.strategic_price)
    //   : "-",
    // "Harga Dasar": detail?.base_price ? formatRupiah(detail?.base_price) : "-",
    "Luas Lahan": detail?.land_area ? `${detail?.land_area} ㎡` : "-",
    "Luas Bangunan": detail?.building_area
      ? `${detail?.building_area} ㎡`
      : "-",
  };
  return (
    <div>
      <h1 className="text-sm font-medium">Informasi Project</h1>
      <div className="mt-4">
        <div className="flex flex-wrap gap-y-5 gap-x-32 items-center">
          {Object.entries(dataDetail).map(([key, value]) => {
            return (
              <div className="flex item-center min-w-96" key={key}>
                <p className="min-w-48">{key}</p>
                <p className="min-w-10">:</p>
                <p>{value}</p>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default InformationDetailProject;
