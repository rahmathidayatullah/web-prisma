// import { getProjectById, getUnit } from "@/app/api/sitemapsss";
// import { axiosInstance } from "@/app/utils/axios";
// import axios from "axios";

// export async function isUnitLength(idProject: string) {
//   try {
//     const {
//       data: { data },
//     } = await getUnit(idProject);
//     return Boolean(data.length);
//   } catch (error) {
//     return false;
//   }
// }

// export async function fetchDetailProjectAndUnit(idProject: string) {
//   const projectById = getProjectById(idProject);
//   const unitByIdProject = getUnit(idProject);

//   Promise.all([projectById, unitByIdProject]).then((res) => {
//     return res;
//   });
// }

export const onAppendAttribute = (path: any, svg: any, item: any) => {
  const pathLength = path.getTotalLength();
  const points = [];
  // Get points along the path
  for (let i = 0; i < pathLength; i += 1) {
    const point = path.getPointAtLength(i);
    point.x -= 2;
    point.y += 2.8;
    points.push(point);
  }

  // Calculate centroid
  let totalX = 0;
  let totalY = 0;
  for (let i = 0; i < points.length; i += 1) {
    totalX += points[i].x;
    totalY += points[i].y;
  }
  const centerX = totalX / points.length;
  const centerY = totalY / points.length;

  // // // create element text
  const newText = document.createElementNS(
    "http://www.w3.org/2000/svg",
    "text"
  );
  newText.setAttribute("kerning", "auto");
  newText.setAttribute("font-family", "Myriad Pro");
  newText.setAttribute("fill", "black");
  newText.setAttribute("font-size", "8px");
  newText.setAttribute("x", `${centerX}px`);
  newText.setAttribute("y", `${centerY}px`);
  newText.textContent = item.number;

  // // append text in path
  svg.appendChild(newText);
};
