"use client";

import CDialog from "@/app/components/molecules/c-dialog";
import { Checkbox, Divider, Form } from "antd";
const CheckboxGroup = Checkbox.Group;
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { formItemLayout } from "./interface";
import { useGlobalContext } from "@/app/context/store";
import {
  RESET_CREATE_UNIT,
  RESET_STATUS_CREATE_UNIT,
  RESET_STATUS_UPDATE_UNIT,
  RESET_UPDATE_UNIT,
  SET_DIALOG_CREATE_UNIT,
  SET_DIALOG_EDIT_UNIT,
} from "@/app/redux/features/sitemaps/constants";
import { fetchBlocksAll } from "@/app/redux/features/blocks/actions";
import { fetchPhasesAll } from "@/app/redux/features/phases/actions";
import CSelect from "@/app/components/molecules/c-select";
import CRadioGroup from "@/app/components/molecules/c-radio-group";
import {
  createUnit,
  fetchProjectDetail,
  fetchUnitsByProject,
  updateUnit,
} from "@/app/redux/features/sitemaps/actions";
import { optionStatusUnit } from "@/app/utils/constants";
import CInputNumber from "@/app/components/molecules/c-input-number";
import type { CheckboxProps, GetProp } from "antd";
type CheckboxValueType = GetProp<typeof Checkbox.Group, "value">[number];

const DialogFormUnit = ({ params }: { params: { id: string } }) => {
  const dispatch: any = useDispatch();
  const { selectPath, svgContent2, onChangeSvgContant2, openMessage } =
    useGlobalContext();
  const idSvgSelect: any = selectPath?.getAttribute("id");
  const [formUnit] = Form.useForm();
  //   listen change field is_more_land
  const watchIsCategory = Form.useWatch("category", formUnit);
  const sitemap = useSelector((state: any) => state.sitemap);
  const phases = useSelector((state: any) => state.phases);
  const { dataListPhaseAll } = phases;
  const blocks = useSelector((state: any) => state.blocks);
  const { dataListBlockAll, selectBlock } = blocks;
  const {
    dataDetail,

    dialogCreateUnit,
    statusCreateUnit,
    dataErrorCreateUnit,

    dataDetailUnit,

    dialogEditUnit,
    statusUpdateUnit,
    dataErrorUpdate,
  } = sitemap;

  const plainOptions = ["Dekat Jalan Utama", "Dekat Fasilitas Umum"];
  const defaultCheckedList = [""];

  const [checkedList, setCheckedList] =
    useState<CheckboxValueType[]>(defaultCheckedList);

  const checkAll = plainOptions.length === checkedList.length;
  const indeterminate =
    checkedList.length > 0 && checkedList.length < plainOptions.length;

  const [mainRood, setMainRood] = useState(false);
  const [facility, setFacility] = useState(false);

  const onChange = (list: CheckboxValueType[]) => {
    if (list.length === 2) {
      formUnit.setFieldValue(
        "strategic_price",
        selectPhase.strategic_facility + selectPhase.strategic_main_road
      );
      setMainRood(true);
      setFacility(true);
    }

    if (list.length === 1) {
      if (list[0] === "Dekat Jalan Utama") {
        formUnit.setFieldValue(
          "strategic_price",
          selectPhase.strategic_main_road
        );
        setMainRood(true);
        setFacility(false);
      } else {
        formUnit.setFieldValue(
          "strategic_price",
          selectPhase.strategic_facility
        );
        setMainRood(false);
        setFacility(true);
      }
    }

    if (list.length === 0) {
      formUnit.setFieldValue("strategic_price", 0);
      setMainRood(false);
      setFacility(false);
    }

    setCheckedList(list);
  };

  const onCheckAllChange: CheckboxProps["onChange"] = (e) => {
    if (e.target.checked) {
      formUnit.setFieldValue(
        "strategic_price",
        selectPhase.strategic_facility + selectPhase.strategic_main_road
      );
      setMainRood(true);
      setFacility(true);
    } else {
      formUnit.setFieldValue("strategic_price", 0);
      setMainRood(false);
      setFacility(false);
    }
    setCheckedList(e.target.checked ? plainOptions : []);
  };

  const cancelCreateUnitFromDialog = () => {
    formUnit.resetFields();
    dispatch({
      type: SET_DIALOG_CREATE_UNIT,
      value: false,
    });
    dispatch({
      type: SET_DIALOG_EDIT_UNIT,
      value: false,
    });
  };

  const createOrUpdateUnit = () => {
    const dataBody: any = {
      project: Number(params.id),
      svgId: Number(idSvgSelect),
      phase: Number(formUnit.getFieldValue("phase")),
      block: Number(formUnit.getFieldValue("block")),
      number: Number(formUnit.getFieldValue("number")),
      status: formUnit.getFieldValue("status"),
      category: formUnit.getFieldValue("category"),
      base_price: Number(formUnit.getFieldValue("base_price")),
      // strategic_price: Number(formUnit.getFieldValue("strategic_price")),
      land_area: Number(formUnit.getFieldValue("land_area")),
      building_area: Number(formUnit.getFieldValue("building_area")),
      // is_more_land: watchIsMoreLand === "no" ? false : true,
      is_more_land: watchIsCategory === "non-standart" ? false : true,
      strategic_main_road: mainRood,
      strategic_facility: facility,
      more_land:
        watchIsCategory === "standart"
          ? 0
          : Number(formUnit.getFieldValue("more_land")),
      more_land_price:
        watchIsCategory === "standart"
          ? 0
          : Number(formUnit.getFieldValue("more_land_price")),
    };
    if (dialogCreateUnit) {
      dispatch(createUnit(dataBody));
    } else {
      dataBody.id = dataDetailUnit.id;
      dispatch(updateUnit(dataBody));
    }
  };

  const [selectStatusUnit, setSelectStatusUnit] = useState<any>(null);
  const changeStatusUnit = (event: any) => {};

  const onReset = () => {
    formUnit.resetFields();
    setMainRood(false);
    setFacility(false);
    dispatch({ type: RESET_CREATE_UNIT });
    dispatch({ type: RESET_UPDATE_UNIT });
  };

  const [selectPhase, setSelectPhase] = useState<any>(null);
  const onChangePhase = (event: any) => {
    const filterData = dataListPhaseAll.find((item: any) => item.id === event);
    setSelectPhase(filterData);
    formUnit.setFieldValue("more_land_price", filterData.more_land_price);
    formUnit.setFieldValue("base_price", filterData.base_price);
  };

  const onChangeMoreLandSize = (event: any) => {
    const moreLandPrice = formUnit.getFieldValue("more_land_price");
    const amount = moreLandPrice * event;

    formUnit.setFieldValue("amount_land_more", amount);
  };
  useEffect(() => {
    if (dialogCreateUnit) {
      // formUnit.setFieldValue("strategic_price", dataDetail?.strategic_price);
      formUnit.setFieldValue("base_price", dataDetail?.base_price);
      formUnit.setFieldValue("land_area", dataDetail?.land_area);
      formUnit.setFieldValue("building_area", `${dataDetail?.building_area}`);
      setCheckedList([]);
    } else if (dialogEditUnit) {
      if (dataDetailUnit) {
        Object.keys(dataDetailUnit).forEach((key) => {
          const value = dataDetailUnit[key];
          if (key === "block") {
            formUnit.setFieldValue(key, value?.id ?? null);
          } else if (key === "phase") {
            const strategic_facility = dataDetailUnit["strategic_facility"];
            const strategic_main_road = dataDetailUnit["strategic_main_road"];

            setSelectPhase(value);

            if (strategic_facility && strategic_main_road) {
              formUnit.setFieldValue(
                "strategic_price",
                value?.strategic_facility + value?.strategic_main_road
              );
              setCheckedList(["Dekat Jalan Utama", "Dekat Fasilitas Umum"]);
            } else if (!strategic_facility && strategic_main_road) {
              formUnit.setFieldValue(
                "strategic_price",
                value?.strategic_main_road
              );
              setCheckedList(["Dekat Fasilitas Umum"]);
            } else if (strategic_facility && !strategic_main_road) {
              formUnit.setFieldValue(
                "strategic_price",
                value?.strategic_facility
              );
              setCheckedList(["Dekat Jalan Utama"]);
            } else {
              formUnit.setFieldValue("strategic_price", 0);
            }

            formUnit.setFieldValue(key, value?.id ?? null);
          } else if (key === "category") {
            formUnit.setFieldValue("category", value);
            if (value === "non-standart") {
              const amountOfPriceMoreLand =
                dataDetailUnit["more_land"] !== 0
                  ? Number(dataDetailUnit["more_land_price"]) *
                    Number(dataDetailUnit["more_land"])
                  : "0";
              formUnit.setFieldValue("amount_land_more", amountOfPriceMoreLand);
            }
          } else if (key === "more_land") {
            formUnit.setFieldValue("is_more_land", value == 0 ? "no" : "yes");
            formUnit.setFieldValue("more_land", value == 0 ? 0 : value);
          } else if (key === "base_price") {
            const phase = dataDetailUnit["phase"];
            formUnit.setFieldValue("base_price", phase.base_price);
          } else {
            formUnit.setFieldValue(key, value);
          }
        });
      }
    }
  }, [dialogCreateUnit, dialogEditUnit]);

  useEffect(() => {
    if (statusCreateUnit === "success") {
      openMessage("success", "Berhasil menambahkan unit .", "top");
      onReset();
      dispatch({
        type: SET_DIALOG_CREATE_UNIT,
        value: false,
      });
      dispatch(fetchBlocksAll(params.id));
      dispatch(fetchPhasesAll(params.id));
      dispatch(fetchUnitsByProject(params.id));
      dispatch(fetchProjectDetail(params.id));
    }
    if (statusCreateUnit === "error") {
      openMessage(
        "error",
        dataErrorCreateUnit?.response?.data?.message ??
          "Gagal menambahkan unit .",
        "top"
      );
      dispatch({ type: RESET_STATUS_CREATE_UNIT });
    }
    if (statusUpdateUnit === "success") {
      openMessage("success", "Berhasil merubah unit .", "top");
      onReset();
      dispatch({
        type: SET_DIALOG_EDIT_UNIT,
        value: false,
      });
      dispatch(fetchBlocksAll(params.id));
      dispatch(fetchPhasesAll(params.id));
      dispatch(fetchUnitsByProject(params.id));
      dispatch(fetchProjectDetail(params.id));
    }
    if (statusUpdateUnit === "error") {
      openMessage(
        "error",
        dataErrorCreateUnit?.response?.data?.message ?? "Gagal merubah unit .",
        "top"
      );
      dispatch({ type: RESET_STATUS_UPDATE_UNIT });
    }
  }, [dispatch, statusCreateUnit, statusUpdateUnit]);

  return (
    <CDialog
      title={`${dialogCreateUnit ? "Tambah Unit Rumah" : "Ubah Unit Rumah"}`}
      open={dialogCreateUnit || dialogEditUnit}
      titleActionOk="Kirim"
      titleActionCancel="Batal"
      actionOk={createOrUpdateUnit}
      actionCancel={cancelCreateUnitFromDialog}
      loadingBtn={
        statusCreateUnit === "process" || statusUpdateUnit === "process"
      }
    >
      <Form
        {...formItemLayout}
        onFinish={createOrUpdateUnit}
        layout="vertical"
        style={{ width: "100%" }}
        form={formUnit}
        name="control-hooks"
      >
        <Form.Item
          label="Status Unit"
          name="status"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <CSelect
            dataOption={optionStatusUnit}
            onChange={changeStatusUnit}
            value={selectStatusUnit}
            placeholder="Pilih status unit"
          />
        </Form.Item>
        <Form.Item
          label="Nomor Unit"
          name="number"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <CInputNumber className="w-full" style={{ width: "100%" }} />
        </Form.Item>
        <Form.Item
          label="Tahap"
          name="phase"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <CSelect
            dataOption={dataListPhaseAll}
            // value={selectBlock}
            onChange={onChangePhase}
            placeholder="Pilih tahap"
          />
        </Form.Item>
        <Form.Item
          label="Blok"
          name="block"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <CSelect
            dataOption={dataListBlockAll}
            // value={selectBlock}
            // onChange={onChangeBlock}
            placeholder="Pilih blok"
          />
        </Form.Item>
        <Form.Item
          label="Kategori"
          name="category"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <CRadioGroup
            dataOption={[
              { value: "standart", label: "Standar" },
              { value: "non-standart", label: "Tidak Standar" },
            ]}
          />
        </Form.Item>

        {formUnit.getFieldValue("category") === "non-standart" ? (
          <Form.Item
            label="Harga Tanah Lebih"
            name="more_land_price"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <CInputNumber
              disabled
              rupiah
              className="w-full"
              style={{ width: "100%" }}
            />
          </Form.Item>
        ) : (
          ""
        )}

        {formUnit.getFieldValue("category") === "non-standart" ? (
          <Form.Item
            label="Ukuran Tanah Lebih ㎡"
            name="more_land"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <CInputNumber
              onChange={onChangeMoreLandSize}
              className="w-full"
              style={{ width: "100%" }}
            />
          </Form.Item>
        ) : (
          ""
        )}

        {formUnit.getFieldValue("category") === "non-standart" ? (
          <Form.Item
            label="Total Harga Tanah Lebih"
            name="amount_land_more"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <CInputNumber
              disabled
              rupiah
              className="w-full"
              style={{ width: "100%" }}
            />
          </Form.Item>
        ) : (
          ""
        )}

        <Form.Item
          label="Luas Bangunan ㎡"
          name="building_area"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <CInputNumber className="w-full" style={{ width: "100%" }} />
        </Form.Item>
        <Form.Item
          label="Luas Tanah ㎡"
          name="land_area"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <CInputNumber style={{ width: "100%" }} />
        </Form.Item>
        {/* <Form.Item
          label="Tipe Unit"
          name="unit_type"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <Input className="w-full" style={{ width: "100%" }} />
        </Form.Item> */}
        <Form.Item
          label="Harga Dasar"
          name="base_price"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <CInputNumber
            disabled
            rupiah
            className="w-full"
            style={{ width: "100%" }}
          />
        </Form.Item>

        {phases !== null ? (
          <div>
            <div className="mb-5">
              <Checkbox
                indeterminate={indeterminate}
                onChange={onCheckAllChange}
                checked={checkAll}
              >
                Dekat Keduanya
              </Checkbox>
              <CheckboxGroup
                options={plainOptions}
                value={checkedList}
                onChange={onChange}
              />
            </div>

            <Form.Item label="Harga Strategis" name="strategic_price">
              <CInputNumber
                disabled
                rupiah
                className="w-full"
                style={{ width: "100%" }}
              />
            </Form.Item>
          </div>
        ) : (
          ""
        )}
        {/* <Form.Item
          label="Ada Tanah Lebih"
          name="is_more_land"
          rules={[{ required: true, message: "Please input!" }]}
        >
          <CRadioGroup
            dataOption={[
              { value: "yes", label: "Ya" },
              { value: "no", label: "Tidak" },
            ]}
          />
        </Form.Item> */}
      </Form>
    </CDialog>
  );
};

export default DialogFormUnit;
