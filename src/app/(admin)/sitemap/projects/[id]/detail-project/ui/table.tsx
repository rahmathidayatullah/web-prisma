"use client";
import {
  Button,
  DropdownProps,
  Form,
  Input,
  MenuProps,
  Space,
  Table,
  TableColumnsType,
} from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { DataType } from "./interface";
import CCheckboxGroup from "@/app/components/molecules/c-checkbox-group";
import CDropdown from "@/app/components/molecules/c-dropdown";
import { useGlobalContext } from "@/app/context/store";
import CDialog from "@/app/components/molecules/c-dialog";
import {
  fetchProjectDetail,
  fetchUnitDetail,
  fetchUnitsByProject,
  removeUnit,
} from "@/app/redux/features/sitemaps/actions";
import { formatRupiah } from "@/app/constants/helper";
import { fetchPhasesAll } from "@/app/redux/features/phases/actions";
import { fetchBlocksAll } from "@/app/redux/features/blocks/actions";
import {
  RESET_DELETE_UNIT,
  RESET_STATUS_DELETE_UNIT,
} from "@/app/redux/features/sitemaps/constants";
import { useSessionStorage } from "@/app/hooks/useSessionStorage";

const TableUnit = ({ params }: { params: { id: string } }) => {
  const { openNotification } = useGlobalContext();
  const dispatch: any = useDispatch();
  const sitemap = useSelector((state: any) => state.sitemap);
  const { dataListUnit, statusDetailUnit, statusDeleteUnit, statusListUnit } =
    sitemap;

  const [form] = Form.useForm();
  const [openSourceType, setOpenSourceType] = useState(false);

  const [idUnit, setIdUnit] = useState<number | string>("");
  const [showDialogDelete, setShowDialogDelete] = useState(false);
  const [showDialogDeleteConfirm, setShowDialogDeleteConfirm] = useState(false);
  const onFinishDelete = () => {
    dispatch(removeUnit(idUnit));
  };

  const [keyword, setKeyword] = useState("");
  const onSearch = () => {};

  const coloumTable: TableColumnsType<DataType> = [
    {
      title: "Tahap",
      dataIndex: "phase",
      key: "phase",
      render: (value) => {
        if (value) {
          return value.name;
        }
        return "-";
      },
    },
    {
      title: "Blok",
      dataIndex: "block",
      key: "block",
      render: (value) => {
        if (value) {
          return value.name;
        }
        return "-";
      },
    },
    {
      title: "Nomor",
      dataIndex: "number",
      key: "number",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Luas Lahan",
      dataIndex: "land_area",
      key: "land_area",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Luar Bangunan",
      dataIndex: "building_area",
      key: "building_area",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Tipe Unit",
      dataIndex: "unit_type",
      key: "unit_type",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Harga Dasar",
      dataIndex: "base_price",
      key: "base_price",
      render: (value) => {
        if (value) {
          return formatRupiah(value);
        }
        return "-";
      },
    },
    {
      title: "Strategis",
      dataIndex: "strategis",
      key: "strategis",
      render: (value) => {
        if (value) {
          return value;
        }
        return "-";
      },
    },
    {
      title: "Harga Strategis",
      dataIndex: "strategic_price",
      key: "strategic_price",
      render: (value) => {
        if (value) {
          return formatRupiah(value);
        }
        return "-";
      },
    },
    // {
    //   title: "Siteplan",
    //   dataIndex: "project",
    //   key: "project",
    //   render: (value) => {
    //     if (value) {
    //       return (
    //         <ImgAntd
    //           width={100}
    //           height={100}
    //           src={value.siteplan_unit}
    //           fallback={value}
    //         />
    //       );
    //     }
    //     return "-";
    //   },
    // },
    // {
    //   title: "Detail Rumah",
    //   dataIndex: "project",
    //   key: "project",
    //   render: (value) => {
    //     if (value.length) {
    //       <div className="flex items-center gap-5">
    //         {value.detail_image.map((item: any) => {
    //           return (
    //             <ImgAntd width={100} height={100} src="error" fallback={item} />
    //           );
    //         })}
    //       </div>;
    //     }
    //     return "-";
    //   },
    // },
    {
      title: "Aksi",
      key: "aksi",
      render: (_, record: any) => {
        return (
          <div className="flex items-center gap-4">
            <Button
              // get detail and open dialog edit
              onClick={() => dispatch(fetchUnitDetail(record.id))}
              style={{ backgroundColor: "#FBB03B" }}
              size="small"
              type="primary"
              loading={statusDetailUnit === "process"}
            >
              Edit
            </Button>
            <Button
              style={{ backgroundColor: "#FF5151", color: "white" }}
              size="small"
              type="default"
              danger
              onClick={() => {
                setShowDialogDelete(true), setIdUnit(record.id);
                form.resetFields();
              }}
              //   loading={
              //     statusApprove === "process" || statusReject === "process"
              //   }
              //   disabled={
              //     statusApprove === "process" || statusReject === "process"
              //   }
              //   onClick={() => {
              //     setId(record.id),
              //       setAction("approve");
              //     approveSubmission(record.id, "approve");
              //   }}
            >
              Hapus
            </Button>
            {/* <Button
              style={{ backgroundColor: "#FF5151", color: "white" }}
              size="small"
              type="default"
              danger
              //   onClick={() => {
              //     setId(record.id),
              //       // setOpen(true);
              //       setAction("reject");
              //     approveSubmission(record.id, "reject");
              //   }}
            >
              Reject
            </Button> */}
          </div>
        );
      },
    },
  ];

  const defaultCheckedList = coloumTable.map((item: any) => item.key as string);

  const [checkedList, setCheckedList] = useSessionStorage<string[]>(
    "data-list-kavling",
    defaultCheckedList
  );

  const newColumns = coloumTable.map((item: any) => ({
    ...item,
    hidden: !checkedList.includes(item.key as string),
  }));

  const options = coloumTable.map(({ key, title }: any) => ({
    label: title,
    value: key,
  }));

  const items: MenuProps["items"] = [
    {
      label: (
        <CCheckboxGroup
          value={checkedList}
          options={options}
          onChange={(value: any) => {
            setCheckedList(value as string[]);
          }}
        />
      ),
      key: "0",
    },
  ];

  const handleOpenChangeSourceType: DropdownProps["onOpenChange"] = (
    nextOpen,
    info
  ) => {
    if (info.source === "trigger" || nextOpen) {
      setOpenSourceType(nextOpen);
    }
  };

  useEffect(() => {
    if (statusDeleteUnit === "success") {
      dispatch(fetchBlocksAll(params.id));
      dispatch(fetchPhasesAll(params.id));
      dispatch(fetchUnitsByProject(params.id));
      dispatch(fetchProjectDetail(params.id));
      setShowDialogDeleteConfirm(false);
      openNotification("topLeft", "Berhasil", "Data unit berhasil dihapus");
      dispatch({ type: RESET_DELETE_UNIT });
    }
    if (statusDeleteUnit === "error") {
      openNotification(
        "topLeft",
        "Terjadi kesalahan",
        statusDeleteUnit?.response?.data?.message ?? "Gagal menghapus data unit"
      );
      dispatch({ type: RESET_STATUS_DELETE_UNIT });
    }
  }, [dispatch, statusDeleteUnit]);

  return (
    <>
      <div className="mt-10">
        <h1 className="text-sm font-medium">List Kavling</h1>

        <div className="flex flex-wrap gap-2 sm:gap-5 items-center justify-between my-5">
          <Space direction="horizontal">
            {/* <CSearch
              onSearch={onSearch}
              value={keyword}
              // onChange={(event: any) =>
              //   dispatch({ type: CHANGE_KEYWORD, value: event.target.value })
              // }
            /> */}

            {/* filter here */}
          </Space>
          <div className="flex flex-wrap items-center gap-2 sm:gap-5">
            {/* <CDateRange
            onChange={(_: any, dateString: any) =>
              dispatch({ type: CHANGE_DATE_RANGE, value: dateString })
            }
          /> */}
            <CDropdown
              variant="sortir-coloum"
              menu={{ items }}
              onOpenChange={handleOpenChangeSourceType}
              open={openSourceType}
            />
            {/* <CButton
            icon={<DownloadOutlined />}
            variant="primary"
            onClick={() => dispatch(fetchAttendacesExports())}
            loading={statusExport === "process"}
            disabled={statusExport === "process"}
          >
            Export Data
          </CButton> */}
            {/* <Link href={routerMenu.SITE_MAP_CREATE_PROJECT}>
              <CButton icon={<PlusOutlined />} variant="primary">
                Tambah Project
              </CButton>
            </Link> */}
          </div>
        </div>

        <Table
          className="table-list-kavling"
          pagination={false}
          columns={newColumns}
          dataSource={dataListUnit}
          style={{ marginTop: 24, fontSize: 10 }}
          loading={statusListUnit === "process"}
          scroll={{ x: 1000 }}
          rowKey="id"
        />

        <CDialog
          title="Anda yakin menghapus data unit ?"
          open={showDialogDelete}
          titleActionOk="Ya"
          titleActionCancel="Tidak"
          actionCancel={() => setShowDialogDelete(false)}
          actionOk={() => {
            setShowDialogDeleteConfirm(true);
            setShowDialogDelete(false);
          }}
        ></CDialog>
        <CDialog
          title="Konfirmasi password untuk menghapus data unit ?"
          open={showDialogDeleteConfirm}
          titleActionOk="Setuju"
          titleActionCancel="Tidak"
          actionCancel={() => setShowDialogDeleteConfirm(false)}
          actionOk={onFinishDelete}
          loadingBtn={statusDeleteUnit === "process"}
        >
          <Form
            onFinish={onFinishDelete}
            layout="vertical"
            style={{ width: "100%" }}
            form={form}
            name="control-hooks"
          >
            <Form.Item
              label="Password"
              name="password"
              rules={[{ required: true, message: "Please input!" }]}
            >
              <Input.Password />
            </Form.Item>
          </Form>
        </CDialog>
      </div>
    </>
  );
};

export default TableUnit;

// const changeBlok = (event: any) => {

// const svgFromStorage = localStorage.getItem("svg");
// // console.log("svgFromStorage", svgFromStorage);

// let svgElement: any = "";
// if (svgFromStorage) {
//   const parser = new DOMParser();
//   const svgDoc = parser.parseFromString(svgFromStorage, "image/svg+xml");
//   svgElement = svgDoc.documentElement;
// }

// const dataUnitByBlock = dataListUnit.filter(
//   (item: any) => item.block.id === event
// );
// // console.log("dataUnitByBlock", dataUnitByBlock);
// let svg: SVGSVGElement | any = svgElement
//   ? svgElement
//   : document.getElementById("1");
// // const svg: SVGSVGElement | any = selectBlok
// //   ? defaultSVG
// //   : document.getElementById("1");
// // console.log("svg", svg);

// if (svgElement) {
//   console.log("svgElement 1", svgElement);
//   svg = document.getElementById("1");
// } else {
//   console.log("svgElement 2", svg);
// }
// if (!svg) {
//   alert("Terjadi kesalahan pada svg yang di upload");
//   return;
// }

// if (!selectBlok) {
//   // console.log("in");
//   // setDefaultSVG(svg);
//   localStorage.setItem("svg", new XMLSerializer().serializeToString(svg));
// }
// setSelectBlok(event);

// // const paths: HTMLCollection | any = selectBlok
// //   ? defaultSVG.getElementsByTagName("path")
// //   : svg.getElementsByTagName("path");

// const paths: HTMLCollection | any = svg.getElementsByTagName("path");

// if (!paths) {
//   alert("Terjadi kesalahan pada svg path yang di upload");
//   return;
// }

// for (let path of paths) {
//   const id = path.getAttribute("id");
//   if (!dataUnitByBlock.some((item: any) => item.svgId === id)) {
//     path.setAttribute("fill", "white"); // Change fill color to red for paths not in data
//   }
// }

// // for (let path of paths) {
// //   for (let itemBlock of dataUnitByBlock) {
// //     const idPath = path.getAttribute("id");
// //     if (itemBlock.svgId === idPath) {
// //       // console.log("idPath", idPath);
// //       // console.log("itemBlock", itemBlock.svgId);
// //       console.log("path", path);
// //       // path.setAttribute("fill", "white");
// //     }
// //     // else {
// //     //   path.setAttribute("fill", "white");
// //     // }
// //   }
// // }
// };
