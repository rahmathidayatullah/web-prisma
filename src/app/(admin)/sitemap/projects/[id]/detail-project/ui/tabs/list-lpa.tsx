"use client";
import { Button } from "antd";
import React from "react";

import { Space, Table, Tag } from "antd";
import type { TableProps } from "antd";

interface DataType {
  key: string;
  dok_lpa: string;
}

const ListLPA = ({ params }: { params: { id: string } }) => {
  const downloadDoc = () => {};
  const columns: TableProps<DataType>["columns"] = [
    {
      title: "Dokumen LPA",
      dataIndex: "dok_lpa",
      key: "dok_lpa",
      render: (text) => <a>Link dokument htttttppp:///sdfsdfsdfsdf.com</a>,
    },
    {
      title: "",
      dataIndex: "action",
      key: "action",
      render: (_, record) => (
        <div className="flex items-center gap-4">
          <Button size="small" type="default" onClick={downloadDoc}>
            Download
          </Button>
        </div>
      ),
    },
  ];
  const data: DataType[] = [
    {
      key: "1",
      dok_lpa: "John Brown",
    },
    {
      key: "2",
      dok_lpa: "Jim Green",
    },
  ];
  return (
    <div>
      <Table columns={columns} dataSource={data} />
    </div>
  );
};

export default ListLPA;
