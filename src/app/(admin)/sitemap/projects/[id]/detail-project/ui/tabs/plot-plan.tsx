"use client";

import React, { useEffect, useState } from "react";
import SVG from "react-inlinesvg";
import {
  TransformWrapper,
  TransformComponent,
  useControls,
} from "react-zoom-pan-pinch";

const TableUnit = dynamic(() => import("../table"), { ssr: false });

import { useDispatch, useSelector } from "react-redux";
import { useGlobalContext } from "@/app/context/store";
import {
  CHANGE_BLOCK,
  CHANGE_PHASE,
  CHANGE_UNIT,
  SET_DIALOG_CREATE_UNIT,
} from "@/app/redux/features/sitemaps/constants";
import InformationCodeColor from "../information-code-color";
import { fetchBlocksAll } from "@/app/redux/features/blocks/actions";
import { fetchPhasesAll } from "@/app/redux/features/phases/actions";
import DialogFormUnit from "../dialog-form-unit";
import {
  fetchProjectDetail,
  fetchUnitDetail,
  fetchUnitsByProject,
} from "@/app/redux/features/sitemaps/actions";
import { getUnit } from "@/app/api/sitemapsss";
import InformationDetailProject from "../information-detail-project";
import { onAppendAttribute } from "../features";
import dynamic from "next/dynamic";
import { Button, Space, Spin } from "antd";
import CSelect from "@/app/components/molecules/c-select";
import { optionStatusUnit } from "@/app/utils/constants";
import InfDetailImgProject from "../inf-detail-img-project";

const Controls = () => {
  const { zoomIn, zoomOut, resetTransform } = useControls();

  return (
    <div className="p-5 flex items-center gap-5">
      <Button onClick={() => zoomIn()}>+</Button>
      <Button onClick={() => zoomOut()}>-</Button>
      <Button onClick={() => resetTransform()}>x</Button>
    </div>
  );
};

export const addTextInUnit = (selectPath: any, form: any) => {
  const bbox = selectPath.getBBox();

  const centerX = bbox.x + bbox.width / 2 - 3;
  const centerY = bbox.y + bbox.height / 2 + 3;
  // // create element text
  const newText = document.createElementNS(
    "http://www.w3.org/2000/svg",
    "text"
  );
  newText.setAttribute("kerning", "auto");
  newText.setAttribute("font-family", "Myriad Pro");
  newText.setAttribute("fill", "white");
  newText.setAttribute("font-size", "8px");
  newText.setAttribute("x", `${centerX}px`);
  newText.setAttribute("y", `${centerY}px`);
  newText.textContent = form.getFieldValue("number");

  const svg: SVGSVGElement | any = document.getElementById("blabla");
  return svg.appendChild(newText);
};

const PlotPlan = ({ params }: { params: { id: string } }) => {
  const dispatch: any = useDispatch();
  const { onChangePathSelect, onChangeSvgContant2 } = useGlobalContext();
  const sitemap = useSelector((state: any) => state.sitemap);
  const {
    tahap,
    blok,
    unit,
    dialogCreateUnit,
    dataDetail,
    statusListUnit,
    dataListUnit,
  } = sitemap;

  const phases = useSelector((state: any) => state.phases);
  const { dataListPhaseAll } = phases;

  const blocks = useSelector((state: any) => state.blocks);
  const { dataListBlockAll } = blocks;

  // const [svgContent, setSvgContent] = useState(null);
  // function handleFileChange(event: any) {
  //   const file = event.target.files[0];
  //   if (!file) return;

  //   const reader = new FileReader();
  //   reader.onload = function (event: any) {
  //     let content = event.target.result;
  //     content = content.replace("<svg", '<svg id="blabla"');
  //     setSvgContent(content);
  //   };
  //   reader.readAsText(file);
  // }

  // when click unit siteplan

  // 1. clear params list unit, and load data unit all
  // 2. if !unit.length ? create : filter data and check status
  function eventClickShapes(event: MouseEvent, path: SVGPathElement): void {
    getUnitByIdProject(path);
    onChangePathSelect(path);
  }
  const [loadingClickUnit, setLoadingClickUnit] = useState<boolean>(false);
  const getUnitByIdProject = async (path: any) => {
    setLoadingClickUnit(true);
    const idSvgSelect: any = path?.getAttribute("id");
    const paramsReset = {};
    try {
      const {
        data: { data },
      } = await getUnit(paramsReset, params.id);
      if (!data.length) {
        dispatch({
          type: SET_DIALOG_CREATE_UNIT,
          value: true,
        });
      } else {
        const findDataById = data.find(
          (item: any) => item.svgId === path.getAttribute("id")
        );
        if (!Boolean(findDataById)) {
          // open dialog create form unit
          dispatch({
            type: SET_DIALOG_CREATE_UNIT,
            value: true,
          });
        } else {
          // fetch detail and open dialog edit form unit
          const unitSelect = data.find(
            (item: any) => idSvgSelect === item.svgId
          );
          dispatch(fetchUnitDetail(unitSelect.id));
        }
      }
    } catch (error) {
    } finally {
      setLoadingClickUnit(false);
    }
  };

  const loadDataImage = (urlSvg: any, dataListUnit: any) => {
    const svg: SVGSVGElement | any = document.getElementById("1");
    if (!svg) {
      alert("Terjadi kesalahan pada svg yang di upload");
      return;
    }
    const paths: HTMLCollection | any = svg.getElementsByTagName("path");
    if (!paths) {
      alert("Terjadi kesalahan pada svg path yang di upload");
      return;
    }

    const dataId = dataListUnit.map((item: any) => item.svgId);
    for (let path of paths) {
      if (path.getAttribute("fill")) {
        if (dataId.length) {
          dataListUnit.forEach((item: any) => {
            if (
              item.svgId === path.getAttribute("id") &&
              item.status === "open"
            ) {
              path.setAttribute("fill", "blue"); // Set the fill color to red, you can change it to any color you need
              onAppendAttribute(path, svg, item);
            } else if (
              item.svgId === path.getAttribute("id") &&
              item.status === "Terbooking"
            ) {
              path.setAttribute("fill", "green"); // Set the fill color to red, you can change it to any color you need
              onAppendAttribute(path, svg, item);
            } else if (
              item.svgId === path.getAttribute("id") &&
              item.status === "Proses KPR"
            ) {
              path.setAttribute("fill", "yellow"); // Set the fill color to red, you can change it to any color you need
              onAppendAttribute(path, svg, item);
            } else if (
              item.svgId === path.getAttribute("id") &&
              item.status === "SP3K"
            ) {
              path.setAttribute("fill", "red"); // Set the fill color to red, you can change it to any color you need
              onAppendAttribute(path, svg, item);
            } else if (
              item.svgId === path.getAttribute("id") &&
              item.status === "Akad"
            ) {
              path.setAttribute("fill", "lightblue"); // Set the fill color to red, you can change it to any color you need
              onAppendAttribute(path, svg, item);
            } else if (
              item.svgId === path.getAttribute("id") &&
              item.status === "Serah Terima"
            ) {
              path.setAttribute("fill", "cyan"); // Set the fill color to red, you can change it to any color you need
              onAppendAttribute(path, svg, item);
            }
          });
        }
        path.setAttribute("class", "styleTagPath");
        // path.remove();
        path.addEventListener("click", (event: MouseEvent) =>
          eventClickShapes(event, path)
        );
      }
    }
    // save svg to state context
    onChangeSvgContant2(svg);
  };

  const changeTahap = (event: any) => {
    dispatch({
      type: CHANGE_PHASE,
      value: event,
    });
  };
  const changeBlok = (event: any) => {
    dispatch({
      type: CHANGE_BLOCK,
      value: event,
    });
  };
  const changeStatusUnit = (event: any) => {
    dispatch({
      type: CHANGE_UNIT,
      value: event,
    });
  };

  useEffect(() => {
    dispatch(fetchBlocksAll(params.id));
    dispatch(fetchPhasesAll(params.id));
  }, [dispatch]);

  useEffect(() => {
    dispatch(fetchUnitsByProject(params.id));
  }, [dispatch, params.id, tahap, blok, unit]);

  useEffect(() => {
    dispatch(fetchProjectDetail(params.id));
  }, [dispatch, params.id]);

  return (
    <>
      <div className="grid grid-cols-12 gap-10">
        <div className="col-span-12">
          <div className="pb-10">
            <InformationDetailProject />
          </div>
          <hr />
        </div>
        <div className="col-span-12">
          <div className="pb-10">
            <InfDetailImgProject />
          </div>
          <hr />
        </div>
        <div className="col-span-12">
          <div className="pb-10">
            <InformationCodeColor />
          </div>
          <hr />
        </div>
        <div className="col-span-12">
          <div className="pb-10">
            <h1 className="text-sm font-medium">Denah Kavling</h1>
            <Space direction="horizontal" className="mt-5">
              <CSelect
                dataOption={dataListPhaseAll}
                onChange={changeTahap}
                value={tahap}
                placeholder="Filter By Tahap"
                allowClear={true}
              />
              <CSelect
                dataOption={dataListBlockAll}
                onChange={changeBlok}
                value={blok}
                placeholder="Filter By Blok"
                allowClear={true}
              />
              <CSelect
                dataOption={optionStatusUnit}
                onChange={changeStatusUnit}
                value={unit}
                placeholder="Filter By Status Unit"
                allowClear={true}
              />
            </Space>
            {/* <input type="file" accept=".svg" onChange={handleFileChange} /> */}
            <Spin
              tip="Load data siteplan ..."
              spinning={statusListUnit === "process" || loadingClickUnit}
            >
              {dataDetail &&
                dataDetail.siteplan_image &&
                statusListUnit === "success" && (
                  <div className="mt-10">
                    <div className="border border-1 border-gray-200">
                      <TransformWrapper
                        initialScale={1}
                        initialPositionX={0}
                        initialPositionY={0}
                      >
                        {({ zoomIn, zoomOut, resetTransform, ...rest }) => (
                          <>
                            <Controls />
                            <TransformComponent
                              wrapperStyle={{ border: "1px solid red;" }}
                            >
                              <div>
                                <SVG
                                  className="w-full h-[40rem]"
                                  src={dataDetail.siteplan_image}
                                  onLoad={(urlSvg: string) =>
                                    loadDataImage(urlSvg, dataListUnit)
                                  }
                                />
                              </div>
                            </TransformComponent>
                          </>
                        )}
                      </TransformWrapper>
                    </div>
                  </div>
                )}
            </Spin>
          </div>
          <hr />
        </div>
      </div>
      <div className="h-full w-full overflow-scroll">
        <TableUnit params={params} />
        <div className="mt-10 flex justify-end">{/* <PaginateLimit /> */}</div>
      </div>

      <DialogFormUnit params={params} />
    </>
  );
};

export default PlotPlan;
