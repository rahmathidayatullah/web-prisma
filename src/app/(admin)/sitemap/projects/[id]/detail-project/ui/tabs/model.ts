export interface TypeListKonsumen {
  key: string;
  id: string;
  name_konsumen: string;
  status_booking: string;
  project: string;
  tahap: string;
  blok: string;
  no_unit: string;
  status_pemabayaran: string;
  kirim_info_akad?: any;
}
