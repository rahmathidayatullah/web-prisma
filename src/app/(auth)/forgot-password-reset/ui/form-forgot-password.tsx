"use client";
import React, { useEffect } from "react";
import { Button, Form, Input } from "antd";
import type { FormInstance } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { forgotPassword } from "@/app/redux/features/auth/actions";
import { RESET_FORGOT_PASSWORD } from "@/app/redux/features/auth/constans";
import Link from "next/link";
interface SubmitButtonProps {
  form: FormInstance;
  disabled: boolean;
}

const SubmitButton: React.FC<React.PropsWithChildren<SubmitButtonProps>> = ({
  form,
  children,
  disabled,
}) => {
  const [submittable, setSubmittable] = React.useState<boolean>(false);

  // Watch all values
  const values = Form.useWatch([], form);

  React.useEffect(() => {
    form
      .validateFields({ validateOnly: true })
      .then(() => setSubmittable(true))
      .catch(() => setSubmittable(false));
  }, [form, values]);

  return (
    <Button
      type="primary"
      htmlType="submit"
      className="w-full"
      loading={disabled}
      disabled={!submittable || disabled}
      style={{ backgroundColor: "#219C90", color: "white" }}
    >
      {children}
    </Button>
  );
};

const FormForgotPassword = () => {
  const dispatch: any = useDispatch();
  const auth = useSelector((state: any) => state.auth);
  const { statusForgot, errorForgot } = auth;
  const [form] = Form.useForm();
  const onFinish = (values: any) => {
    dispatch(forgotPassword(values));
  };

  const onFinishFailed = (errorInfo: any) => {};

  type FieldType = {
    email?: string;
    password?: string;
  };

  useEffect(() => {
    if (statusForgot === "success") {
      alert("Cek email konfirmasi password, untuk mengubah password");
      dispatch({ type: RESET_FORGOT_PASSWORD });
    }
    if (statusForgot === "error") {
      alert(`Terjadi kesalahan ${statusForgot?.message?.message}`);
      dispatch({ type: RESET_FORGOT_PASSWORD });
    }
  }, [statusForgot]);

  useEffect(() => {
    return () => {
      dispatch({ type: RESET_FORGOT_PASSWORD });
    };
  }, []);

  return (
    <Form
      name="basic"
      form={form}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
      layout="vertical"
    >
      <Form.Item<FieldType>
        label="Email"
        name="email"
        rules={[
          {
            required: true,
            type: "email",
            message: "Email harus di isi dengan format yg sesuai !",
          },
        ]}
      >
        <Input placeholder="Masukkan email anda" />
      </Form.Item>

      <Form.Item className="flex w-full justify-end">
        <Link
          href="/login"
          className="login-form-forgot"
          style={{ color: "#219C90" }}
        >
          Kembali ke halaman login
        </Link>
      </Form.Item>

      <Form.Item>
        <SubmitButton disabled={statusForgot === "process"} form={form}>
          {statusForgot === "idle" && "Kirim"}
          {statusForgot === "process" && "Loading .."}
          {statusForgot === "success" && "Kirim"}
          {statusForgot === "error" && "Kirim"}
        </SubmitButton>
      </Form.Item>
      {statusForgot === "error" ? (
        <p className="text-center text-red-500 pb-2">
          {errorForgot?.message?.message === "User Not Found"
            ? "Tidak ada pengguna dengan email yg di masukkan"
            : "Terjadi kesalahan"}
        </p>
      ) : (
        ""
      )}
      {statusForgot === "success" ? (
        <p className="text-center text-red-500 pb-2">
          Cek email konfirmasi password, untuk mengubah password
        </p>
      ) : (
        ""
      )}
    </Form>
  );
};

export default FormForgotPassword;
