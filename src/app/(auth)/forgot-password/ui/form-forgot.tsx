"use client";
import React, { useEffect } from "react";
import { Button, Form, Input } from "antd";
import type { FormInstance } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { forgotPasswordReset } from "@/app/redux/features/auth/actions";
import { RESET_FORGOT_PASSWORD_RESET } from "@/app/redux/features/auth/constans";
interface SubmitButtonProps {
  form: FormInstance;
  disabled: boolean;
}

const SubmitButton: React.FC<React.PropsWithChildren<SubmitButtonProps>> = ({
  form,
  children,
  disabled,
}) => {
  const [submittable, setSubmittable] = React.useState<boolean>(false);

  // Watch all values
  const values = Form.useWatch([], form);

  React.useEffect(() => {
    form
      .validateFields({ validateOnly: true })
      .then(() => setSubmittable(true))
      .catch(() => setSubmittable(false));
  }, [form, values]);

  return (
    <Button
      type="primary"
      htmlType="submit"
      className="w-full"
      loading={disabled}
      disabled={!submittable || disabled}
      style={{ backgroundColor: "#219C90", color: "white" }}
    >
      {children}
    </Button>
  );
};

const FormLogin = () => {
  let paramsToken: any = "";
  if (typeof window !== "undefined") {
    // Client-side-only code
    const searchParams = new URLSearchParams(window.location.search);
    paramsToken = searchParams.get("token");
  }
  const dispatch: any = useDispatch();
  const auth = useSelector((state: any) => state.auth);
  const { statusForgotReset, errorForgotReset } = auth;
  const [form] = Form.useForm();
  const onFinish = (values: any) => {
    dispatch(forgotPasswordReset(values, paramsToken));
  };

  const onFinishFailed = (errorInfo: any) => {};

  useEffect(() => {
    if (statusForgotReset === "success") {
      alert("Silahkan login kembali ke aplikasi");
      dispatch({ type: RESET_FORGOT_PASSWORD_RESET });
    }
    if (statusForgotReset === "error") {
      alert(`Terjadi kesalahan ${errorForgotReset?.message?.message}`);
      dispatch({ type: RESET_FORGOT_PASSWORD_RESET });
    }
  }, [statusForgotReset]);

  useEffect(() => {
    return () => {
      dispatch({ type: RESET_FORGOT_PASSWORD_RESET });
    };
  }, []);

  return (
    <Form
      name="basic"
      form={form}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
      layout="vertical"
    >
      <Form.Item
        name="password"
        label="Password"
        rules={[
          {
            required: true,
            message: "Please input your password!",
          },
        ]}
        hasFeedback
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        name="confirm"
        label="Confirm Password"
        dependencies={["password"]}
        hasFeedback
        rules={[
          {
            required: true,
            message: "Please confirm your password!",
          },
          ({ getFieldValue }) => ({
            validator(_, value) {
              if (!value || getFieldValue("password") === value) {
                return Promise.resolve();
              }
              return Promise.reject(
                new Error("The new password that you entered do not match!")
              );
            },
          }),
        ]}
      >
        <Input.Password />
      </Form.Item>

      {/* <Form.Item className="flex w-full justify-end">
        <Link
          href="/login"
          className="login-form-forgot"
          style={{ color: "#219C90" }}
        >
          Kembali ke halaman login
        </Link>
      </Form.Item> */}

      <Form.Item>
        <SubmitButton disabled={statusForgotReset === "process"} form={form}>
          {statusForgotReset === "idle" && "Masuk"}
          {statusForgotReset === "process" && "Loading .."}
          {statusForgotReset === "success" && "Berhasil masuk"}
          {statusForgotReset === "error" && "Masuk"}
        </SubmitButton>
      </Form.Item>

      {statusForgotReset === "error" ? (
        <p className="text-center text-red-500 pb-2">
          {/* {errorForgotReset?.message?.message === "User Not Found"
            ? "Tidak ada pengguna dengan email yg di masukkan"
            : "Terjadi kesalahan"} */}
        </p>
      ) : (
        ""
      )}
      {statusForgotReset === "success" ? (
        <p className="text-center text-red-500 pb-2">
          {/* Cek email konfirmasi password, untuk mengubah password */}
        </p>
      ) : (
        ""
      )}
    </Form>
  );
};

export default FormLogin;
