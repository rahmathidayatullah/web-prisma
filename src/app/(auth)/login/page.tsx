"use client";
import React from "react";
import FormLogin from "./ui/form-login";
import Image from "next/image";
import { useSearchParams } from "next/navigation";

const Page = () => {
  const searchParams = useSearchParams();
  const isIt = searchParams.get("isIt");
  return (
    <div className="h-screen">
      <div className="block sm:grid grid-cols-12 gap-0 sm:gap-10 h-full p-4 sm:p-16">
        <div className="lg:col-span-6 col-span-12 mt-7 sm:mt-0">
          <div className="bg-white lg:bg-[#F2F2F2] w-full h-auto sm:h-full rounded-3xl">
            <div className="h-full flex items-center justify-center">
              <Image
                className="w-60 sm:w-96"
                src="/images/logoprisma-01.png"
                alt="img-brand"
                width={313}
                height={93}
              />
            </div>
          </div>
        </div>
        <div className="lg:col-span-6 col-span-12 mt-7 sm:mt-0">
          <div className="flex items-center justify-center h-auto sm:h-full">
            <div className="rounded-xl">
              <h1 className="text-base sm:text-4xl font-bold">
                Masuk {isIt || ""}
              </h1>
              <h3 className="text-sm sm:text-2xl font-semibold">
                Selamat datang! Masuk ke akun anda
              </h3>

              <div className="mt-4 sm:mt-10">
                <FormLogin />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Page;
