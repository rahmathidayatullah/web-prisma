"use client";
import React, { useEffect } from "react";
import { Button, Form, Input } from "antd";
import type { FormInstance } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { postLogin } from "@/app/redux/features/auth/actions";
import { useRouter, useSearchParams } from "next/navigation";
import { routerMenu } from "@/app/constants/routes";
import Link from "next/link";
interface SubmitButtonProps {
  form: FormInstance;
  disabled: boolean;
}

const SubmitButton: React.FC<React.PropsWithChildren<SubmitButtonProps>> = ({
  form,
  children,
  disabled,
}) => {
  const [submittable, setSubmittable] = React.useState<boolean>(false);

  // Watch all values
  const values = Form.useWatch([], form);

  React.useEffect(() => {
    form
      .validateFields({ validateOnly: true })
      .then(() => setSubmittable(true))
      .catch(() => setSubmittable(false));
  }, [form, values]);

  return (
    <Button
      type="primary"
      htmlType="submit"
      className="w-full"
      loading={disabled}
      disabled={!submittable || disabled}
      style={{ backgroundColor: "#219C90", color: "white" }}
    >
      {children}
    </Button>
  );
};

const FormLogin = () => {
  const searchParams = useSearchParams();
  const isIt = searchParams.get("isIt");
  const projectId = searchParams.get("projectId");

  const router = useRouter();
  const dispatch: any = useDispatch();
  const auth = useSelector((state: any) => state.auth);
  const { statusLogin, userData, dataError } = auth;
  const [form] = Form.useForm();
  const onFinish = (values: any) => {
    dispatch(postLogin(values));
  };

  const onFinishFailed = (errorInfo: any) => {};

  type FieldType = {
    email?: string;
    password?: string;
  };

  useEffect(() => {
    if (localStorage.getItem("userData")) {
      router.push(routerMenu.ATTENDACE);
    }
    if (statusLogin === "success") {
      if (isIt === "Marketing") {
        router.push(
          `${routerMenu.MARKETING_PROJECT}?prjectId=${projectId || ""}`
        );
      } else {
        localStorage.setItem("userData", JSON.stringify(userData));
        window.location.href = routerMenu.ATTENDACE;
      }
    }
  }, [dispatch, statusLogin]);

  return (
    <Form
      name="basic"
      form={form}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      autoComplete="off"
      layout="vertical"
    >
      <Form.Item<FieldType>
        label="Email"
        name="email"
        rules={[
          {
            required: true,
            type: "email",
            message: "Email harus di isi dengan format yg sesuai !",
          },
        ]}
      >
        <Input placeholder="Masukkan email anda" />
      </Form.Item>

      <Form.Item<FieldType>
        label="Password"
        name="password"
        rules={[{ required: true, message: "Password harus di isi !" }]}
      >
        <Input.Password placeholder="Masukkan password anda" />
      </Form.Item>
      {statusLogin === "error" ? (
        <p className="text-center text-red-500 pb-2">
          {dataError && dataError?.message?.response?.message
            ? "Tidak ada pengguna dengan email dan password yg di masukkan"
            : dataError?.message
            ? "Password atau email tidak sesuai"
            : "Terjadi kesalahan"}
        </p>
      ) : (
        ""
      )}

      <Form.Item className="flex w-full justify-end">
        <Link
          href="/forgot-password-reset"
          className="login-form-forgot"
          style={{ color: "#219C90" }}
        >
          Lupa Passowrd?
        </Link>
      </Form.Item>

      <Form.Item>
        <SubmitButton disabled={statusLogin === "process"} form={form}>
          {statusLogin === "idle" && "Masuk"}
          {statusLogin === "process" && "Loading .."}
          {statusLogin === "success" && "Berhasil masuk"}
          {statusLogin === "error" && "Masuk"}
        </SubmitButton>
      </Form.Item>
    </Form>
  );
};

export default FormLogin;
