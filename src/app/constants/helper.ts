export function formatRupiah(number: number) {
  return "Rp. " + number.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
}

export const getBase64 = (
  file: File,
  callback: (base64String: string) => void
) => {
  const reader = new FileReader();
  reader.addEventListener("load", () => callback(reader.result as string));
  reader.readAsDataURL(file);
};
