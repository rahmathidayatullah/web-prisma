export const routerMenu = {
  EMPLOYEE: "/employee/management-employee",
  CREATE_EMPLOYEE: "/employee/management-employee/create",

  ATTENDACE: "/attendaces/attendace",

  OVERTIME: "/attendaces/overtime",
  CREATE_OVERTIME: "/attendaces/overtime/create",

  SUBMISSION: "/attendaces/submission",
  CREATE_SUBMISSION: "/attendaces/submission/create",

  ANNOUCEMENT: "/attendaces/master-data/annoucement",
  CREATE_ANNOUCEMENT: "/attendaces/master-data/annoucement/create",

  CATEGORY_SUBMISSION: "/attendaces/master-data/category-submission",
  CREATE_CATEGORY_SUBMISSION:
    "/attendaces/master-data/category-submission/create",

  MASTER_DATA_ROLES: "/employee/master-data/roles",
  CREATE_MASTER_DATA_ROLES: "/employee/master-data/roles/create",

  MASTER_DATA_BLOCKS: "/sitemap/master-data/blocks",
  CREATE_MASTER_DATA_BLOCKS: "/sitemap/master-data/blocks/create",

  MASTER_DATA_PHASES: "/sitemap/master-data/phases",
  CREATE_MASTER_DATA_PHASES: "/sitemap/master-data/phases/create",

  MASTER_DATA_SHIFTS: "/employee/master-data/shifts",

  MASTER_DATA_DIVISION: "/employee/master-data/divisions",
  CREATE_MASTER_DATA_DIVISION: "/employee/master-data/divisions/create",

  INVENTORY: "/inventory",

  SITE_MAP_PROJECT: "/sitemap/projects",
  SITE_MAP_CREATE_PROJECT: "/sitemap/projects/create",

  // marketing booking
  GENERATE_FORM_FLPP: "/generate-form-flpp", //:unitId
  MARKETING_DETAIL_BOOKING: "/detail-booking-marketing", //:unitId
  MARKETING_BOOKING: "/booking-marketing", //:unitId
  MARKETING_LIST_BOOKING: "/list-booking-marketing",
  MARKETING_PROJECT: "/projects-marketing-detail", //:projectId

  // admin marketing
  ADM_MARKETING_LIST_BOOKING: "/list-booking-adm-marketing",
};
