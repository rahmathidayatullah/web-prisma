import { axiosInstance } from "../utils/axios";

export async function getProject(params: any) {
  return await axiosInstance.get("/projects", {
    params,
  });
}
export async function postProject(body: any) {
  return await axiosInstance.post(`/projects`, body, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}
export async function getProjectById(id: string) {
  return await axiosInstance.get(`/projects/${id}`);
}
export async function patchProjectById(id: string, body: any) {
  return await axiosInstance.patch(`/projects/${id}`, body, {
    headers: {
      "Content-Type": "multipart/form-data",
    },
  });
}
export async function deleteProjectById(id: string | number) {
  return await axiosInstance.delete(`/project/${id}`);
}

export async function getUnit(
  params: {
    block?: string;
    phase?: string;
    status?: string;
  },
  id: string
) {
  // return await axiosInstance.get(`/units/projects/${id}`);
  return await axiosInstance.get(`/units/projects/${id}`, {
    params,
  });
}
export async function getUnitById(id: string) {
  return await axiosInstance.get(`/units/${id}`);
}

// without id
export async function postUnit(body: any) {
  return await axiosInstance.post(`/units`, body);
}

// with id
export async function patchUnitById(idProject: string, body: any) {
  return await axiosInstance.post(`/units/projects/${idProject}`, body);
}

export async function deleteUnitById(
  id: string | number
  // body: {
  //   password: string;
  // }
) {
  // const config: AxiosRequestConfig = {
  //   data: body,
  // };
  // return await axiosInstance.delete(`/units/${id}`, config);
  return await axiosInstance.delete(`/units/${id}`);
}
