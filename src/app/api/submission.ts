import { axiosInstance } from "../utils/axios";

export async function getSubmissions(params: any) {
  return await axiosInstance.get("/submissions", {
    params,
  });
}

export async function getSubmissionsExport(params: any) {
  return await axiosInstance.get("/submissions/export", {
    params,
  });
}

export async function getSubmissionsById(id: any) {
  return await axiosInstance.get(`/submissions/${id}`);
}

export async function postSubmissions(body: any) {
  return await axiosInstance.post(`/submissions`, body);
}

export async function patchSubmissions(id: string, body: any) {
  return await axiosInstance.patch(`/submissions/${id}`, body);
}

export async function approveSubmissions(id: string, body: any) {
  return await axiosInstance.patch(`/submissions/status/${id}`, body);
}

export async function rejectSubmissions(id: string, body: any) {
  return await axiosInstance.patch(`/submissions/status/${id}`, body);
}
