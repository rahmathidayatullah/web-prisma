import { axiosInstance } from "../utils/axios";

export async function postDivision(body: any) {
  return await axiosInstance.post(`/division`, body);
}
export async function getDivision() {
  return await axiosInstance.get("/division");
}
export async function getDivisionAll() {
  return await axiosInstance.get("/division/all");
}
export async function getDivisionById(id: string) {
  return await axiosInstance.get(`/division/${id}`);
}
export async function patchDivisionById(id: string, body: any) {
  return await axiosInstance.patch(`/division/${id}`, body);
}
export async function deleteDivisionById(id: string | number) {
  return await axiosInstance.delete(`/division/${id}`);
}
