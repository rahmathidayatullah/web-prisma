import { axiosInstance } from "../utils/axios";

export async function postPhases(body: any, id: string) {
  return await axiosInstance.post(`/phases/project/${id}`, body);
}
export async function getPhases(params: any, id: string) {
  return await axiosInstance.get(`/phases/project/${id}`, {
    params,
  });
}
export async function getPhasesAll(id: string) {
  return await axiosInstance.get(`/phases/project/${id}/all`);
}
export async function getPhasesById(id: string) {
  return await axiosInstance.get(`/phases/${id}`);
}
export async function patchPhasesById(id: string, body: any) {
  return await axiosInstance.patch(`/phases/${id}`, body);
}
export async function deletePhasesById(id: string | number) {
  return await axiosInstance.delete(`/phases/${id}`);
}
