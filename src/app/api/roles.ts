import { axiosInstance } from "../utils/axios";

export async function postRoles(body: any) {
  return await axiosInstance.post(`/roles`, body);
}
export async function getRoles(params: any) {
  return await axiosInstance.get("/roles", {
    params,
  });
}
export async function getRolesAll() {
  return await axiosInstance.get("/roles/all");
}
export async function getRolesById(id: string) {
  return await axiosInstance.get(`/roles/${id}`);
}
export async function patchRolesById(id: string, body: any) {
  return await axiosInstance.patch(`/roles/${id}`, body);
}
export async function deleteRolesById(id: string | number) {
  return await axiosInstance.delete(`/roles/${id}`);
}
