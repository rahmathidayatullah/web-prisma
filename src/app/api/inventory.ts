import { axiosInstance } from "../utils/axios";

export async function login(payload: any) {
  return await axiosInstance.post(`/auth/login`, payload);
}

export function logout() {
  localStorage.clear();
  window.location.href = "/login";
}
