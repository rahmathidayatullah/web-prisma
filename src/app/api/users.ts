import { axiosInstance } from "../utils/axios";

export async function postUsers(body: any) {
  return await axiosInstance.post(`/users`, body);
}
export async function getUsers(params: any) {
  return await axiosInstance.get("/users", {
    params,
  });
}
export async function getUsersById(id: string) {
  return await axiosInstance.get(`/users/${id}`);
}
export async function patchUsersById(id: string, body: any) {
  return await axiosInstance.patch(`/users/${id}`, body);
}
export async function deleteUsersById(id: number | string) {
  return await axiosInstance.delete(`/users/${id}`);
}
