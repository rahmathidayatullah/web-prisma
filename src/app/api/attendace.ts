import { axiosInstance } from "../utils/axios";

export async function getAttendances(params: any) {
  return await axiosInstance.get("/attendances", {
    params,
  });
}

export async function getAttendancesById(id: any) {
  return await axiosInstance.get(`/attendances/${id}`);
}

export async function getAttendancesExport(params: any) {
  return await axiosInstance.get(`/attendances/export`, {
    params,
  });
}

export async function patchAttendances(id: any, body: any) {
  return await axiosInstance.patch(`/attendances/${id}`, body);
}

export async function approveAttendaces(id: string, body: any) {
  return await axiosInstance.patch(`/attendances/status/${id}`, body);
}

export async function rejectAttendaces(id: string, body: any) {
  return await axiosInstance.patch(`/attendances/status/${id}`, body);
}
