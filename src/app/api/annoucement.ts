import axios from "axios";
import { axiosInstance } from "../utils/axios";

export async function getAnnoucement(params: any) {
  return await axiosInstance.get("/announcements", {
    params,
  });
}

export async function getAnnoucementById(id: any) {
  return await axiosInstance.get(`/announcements/${id}`);
}

export async function postAnnoucement(body: any) {
  return await axiosInstance.post(`/announcements`, body);
}

export async function patchAnnoucementById(id: string, body: any) {
  return await axiosInstance.patch(`/announcements/${id}`, body);
}

export async function deleteAnnoucementById(id: number | string) {
  return await axiosInstance.delete(`/announcements/${id}`);
}
