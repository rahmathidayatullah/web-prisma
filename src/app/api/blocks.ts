import { axiosInstance } from "../utils/axios";

export async function postBlocks(body: any, id: string) {
  return await axiosInstance.post(`/blocks/project/${id}`, body);
}
export async function getBlocks(params: any, id: string) {
  return await axiosInstance.get(`/blocks/project/${id}`, {
    params,
  });
}
export async function getBlocksAll(id: string) {
  return await axiosInstance.get(`/blocks/project/${id}/all`);
}
export async function getBlocksById(id: string) {
  return await axiosInstance.get(`/blocks/${id}`);
}
export async function patchBlocksById(id: string, body: any) {
  return await axiosInstance.patch(`/blocks/${id}`, body);
}
export async function deleteBlocksById(id: string | number) {
  return await axiosInstance.delete(`/blocks/${id}`);
}
