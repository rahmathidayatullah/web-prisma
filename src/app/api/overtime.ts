import { axiosInstance } from "../utils/axios";

export async function getOvertimes(params: any) {
  return await axiosInstance.get("/overtimes", {
    params,
  });
}

export async function getOvertimesExport(params: any) {
  return await axiosInstance.get("/overtimes/export", {
    params,
  });
}

export async function postOvertimes(body: any) {
  return await axiosInstance.post("/overtimes", body);
}

export async function patchOvertimes(id: string, body: any) {
  return await axiosInstance.patch(`/overtimes/${id}`, body);
}

export async function getOvertimesById(id: string) {
  return await axiosInstance.get(`/overtimes/${id}`);
}

export async function approveOvertime(id: string, body: any) {
  return await axiosInstance.patch(`/overtimes/status/${id}`, body);
}

export async function rejectOvertime(id: string, body: any) {
  return await axiosInstance.patch(`/overtimes/status/${id}`, body);
}
