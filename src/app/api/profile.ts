import { axiosInstance } from "../utils/axios";

export async function getUsersProfile() {
  return await axiosInstance.get(`/users/profile`);
}
export async function patchUsersProfile(body: any) {
  return await axiosInstance.patch(`/users/profile`, body);
}
