import { axiosInstance } from "../utils/axios";

// export async function getCompanies(params: any) {
export async function getCompanies() {
  return await axiosInstance.get("/companies");
}
export async function getShifts() {
  return await axiosInstance.get("/shifts");
}

//
export async function postSubmissionCategories(body: any) {
  return await axiosInstance.post(`/submission-categories`, body);
}
export async function getSubmissionCategories(params: any) {
  return await axiosInstance.get("/submission-categories", {
    params,
  });
}
export async function getSubmissionCategoriesAll() {
  return await axiosInstance.get("/submission-categories/all");
}
export async function getSubmissionCategoriesById(id: string) {
  return await axiosInstance.get(`/submission-categories/${id}`);
}

export async function patchSubmissionCategoriesById(id: string, body: any) {
  return await axiosInstance.patch(`/submission-categories/${id}`, body);
}
export async function deleteSubmissionCategoriesById(id: number | string) {
  return await axiosInstance.delete(`/submission-categories/${id}`);
}

//
export async function postDivision(body: any) {
  return await axiosInstance.post(`/division`, body);
}
export async function getDivision(params: any) {
  return await axiosInstance.get("/division", {
    params,
  });
}
export async function getDivisionAll() {
  return await axiosInstance.get("/division/all");
}
export async function getDivisionById(id: string) {
  return await axiosInstance.get(`/division/${id}`);
}

export async function patchDivisionById(id: string, body: any) {
  return await axiosInstance.patch(`/division/${id}`, body);
}
export async function deleteDivisionById(id: number | string) {
  return await axiosInstance.delete(`/division/${id}`);
}
