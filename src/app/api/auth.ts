import { axiosInstance } from "../utils/axios";

export async function login(payload: any) {
  return await axiosInstance.post(`/auth/login`, payload);
}

export async function postForgotPassword(payload: any) {
  return await axiosInstance.post(`/auth/forgot-password`, payload);
}

export async function postUpdateForgotPassword(payload: any) {
  return await axiosInstance.post(`/auth/update-forgot-password`, payload);
}

export function logout(data: any) {
  localStorage.clear();
  if (data?.isIt && data?.idProject) {
    window.location.href = `/login?isIt=${data.isIt}&projectId=${data.idProject}`;
  } else {
    window.location.href = "/login";
  }
}
