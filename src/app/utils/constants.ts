import moment from "moment";

// // Get the current date with format
// export const currentDateWithFormat = moment().format("YYYY-MM-DD");

// // Get the current date default
export const currentDateDefault = moment();

// // Add 1 month to the current date default
export const futureDateOneMonth = currentDateDefault
  .add(1, "month")
  .format("YYYY-MM-DD");

// // Add 1 year to the current date default
// export const futureDateOneYear = currentDateDefault
//   .add(1, "year")
//   .format("YYYY-MM-DD");

// Get the current year
const currentYear = moment().year();

// Generate start date for January 1st of the current year
export const currentDateWithFormat = moment(`${currentYear}-01-01`).format(
  "YYYY-MM-DD"
);

// Generate end date for December 31st of the current year
export const futureDateOneYear = moment(`${currentYear}-12-31`).format(
  "YYYY-MM-DD"
);

export const openNewTab = (url: any) => {
  // Create a new <a> element
  const newTab = document.createElement("a");

  // Set the href attribute to the URL
  newTab.href = url;

  // Set the target attribute to '_blank' to open in a new tab
  newTab.target = "_blank";

  // Programmatically click the new <a> element to open the new tab
  newTab.click();
};

// Get the current year and month
// const currentYear = moment().year();
const currentMonth = moment().month();

// Create the start date as the 1st of the current month

export const dateStartNow = moment()
  .set({ year: currentYear, month: currentMonth, date: 1 })
  .format("YYYY-MM-DD");
// Create the end date as the last day of the current month
export const dateEndNow = moment(dateStartNow)
  .endOf("month")
  .format("YYYY-MM-DD");

export function removeEmptyAttributes(obj: any) {
  for (const key in obj) {
    if (obj[key] === null || obj[key] === undefined || obj[key] === "") {
      delete obj[key];
    }
  }
  return obj;
}

export const optionStatusUnit = [
  { id: 1, label: "Open", value: "open" },
  { id: 2, label: "Terbooking", value: "Terbooking" },
  { id: 3, label: "Proses KPR", value: "Proses KPR" },
  { id: 4, label: "SP3K", value: "SP3K" },
  { id: 5, label: "Akad", value: "Akad" },
  { id: 6, label: "Serah Terima", value: "Serah Terima" },
];

export const optionNewOldData = [
  {
    id: 1,
    label: "Terbaru",
    value: "DESC",
  },
  {
    id: 2,
    label: "Lampau",
    value: "ASC",
  },
];
