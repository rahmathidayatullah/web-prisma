import axios from "axios";
import { configDefault } from "./config";

// const configDefault = {
//   api_host: "https://api.dotlike.site"
// }
let storage = null;
let userData = {
  access_token: "",
};
if (typeof localStorage !== "undefined") {
  storage = localStorage.getItem("userData");
  if (storage !== null) {
    userData = JSON.parse(storage);
    // userData = {
    //   access_token:
    //     "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6ImhyZDRAbWFpbC5jb20iLCJzdWIiOjEsImlhdCI6MTcxMTM2NDAxNCwiZXhwIjoxNzEzOTU2MDE0fQ.0X_JInyTl7DWII6ltu1IXgx3XaCSntoI_I9h0nltuhY",
    // };
  }
} else {
  console.error("Your browser not local storage");
}

export const axiosInstance = axios.create({
  baseURL: configDefault.api_host,
  //   timeout: 1000,
  //   headers: { "X-Custom-Header": "foobar" },
});

// Add a request interceptor
axiosInstance.interceptors.request.use(
  function (config) {
    if (userData.access_token) {
      if (config.data instanceof FormData) {
        config.headers.Authorization = `Bearer ${userData.access_token}`;
        config.headers["Content-Type"] = "multipart/form-data";
      } else if (config.data && typeof config.data === "object") {
        config.headers.Authorization = `Bearer ${userData.access_token}`;
        config.headers["Content-Type"] = "application/json";
      } else {
        config.headers.Authorization = `Bearer ${userData.access_token}`;
      }
    }

    // Do something before request is sent
    return config;
  },
  function (error) {
    // Do something with request error
    return Promise.reject(error);
  }
);

// Add a response interceptor
axiosInstance.interceptors.response.use(
  function (response) {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    return response;
  },
  function (error) {
    // Any status codes that falls outside the range of 2xx cause this function to trigger
    // Do something with response error
    return Promise.reject(error);
  }
);
