"use client";
import React from "react";
import Heading from "./ui/heading";
import { useSearchParams } from "next/navigation";
import FormFlpp from "./ui/form-flpp";

const Page = () => {
  const searchParams = useSearchParams();
  const id = searchParams.get("projectId");
  const queryParams = { id: id || "" };
  return (
    <div className="p-3">
      <Heading queryParams={queryParams} />
      <FormFlpp queryParams={queryParams} />
    </div>
  );
};

export default Page;
