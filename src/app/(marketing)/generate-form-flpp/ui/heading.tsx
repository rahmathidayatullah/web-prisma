"use client";
import React from "react";
import BtnSmall from "@/app/components/atoms/button/mobile/btn-small";
import { routerMenu } from "@/app/constants/routes";
import { useRouter } from "next/navigation";
import { LeftOutlined } from "@ant-design/icons";

const Heading = ({ queryParams }: any) => {
  const router = useRouter();
  const onLinkDetailBooking = () => {
    router.push(`${routerMenu.MARKETING_DETAIL_BOOKING}?unitId=1`);
  };
  return (
    <div>
      <div className="flex items-center justify-between">
        <h1 className="font-bold">Generate Form FLPP</h1>
        <BtnSmall
          onClick={onLinkDetailBooking}
          icon={<LeftOutlined />}
        ></BtnSmall>
      </div>
    </div>
  );
};

export default Heading;
