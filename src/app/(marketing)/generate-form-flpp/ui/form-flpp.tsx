"use client";
import React from "react";
import { Form, Input, Radio } from "antd";
import { useRouter } from "next/navigation";
import CInputNumber from "@/app/components/molecules/c-input-number";
import CDateSingle from "@/app/components/molecules/c-date-single";
import BtnSmall from "@/app/components/atoms/button/mobile/btn-small";

const FormFlpp = ({ queryParams }: any) => {
  const router = useRouter();
  const [form] = Form.useForm();
  //   listen change field is_more_land
  const watchStatusPerkawinan = Form.useWatch("status_perkawinan", form);
  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      // sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      // sm: { span: 14 },
    },
  };
  const changeStartWork = (date: any, dateString: any) => {
    // setStart_work_date(dateString);
  };
  const onFinish = (values: any) => {
    console.log("onDownload");
  };
  //   const onDownload = () => {

  //   };
  return (
    <Form
      {...formItemLayout}
      onFinish={onFinish}
      layout="vertical"
      style={{ width: "100%" }}
      form={form}
      name="control-hooks"
    >
      <Form.Item label="Status Perkawinan" name="status_perkawinan">
        <Radio.Group>
          <Radio value="single">Single</Radio>
          <Radio value="merrid">Menikah</Radio>
        </Radio.Group>
      </Form.Item>
      {watchStatusPerkawinan === "merrid" ? (
        <Form.Item label="Status Pemohon" name="status_pemohon">
          <Radio.Group>
            <Radio value="single">Suami</Radio>
            <Radio value="merrid">Istri</Radio>
          </Radio.Group>
        </Form.Item>
      ) : (
        ""
      )}
      <Form.Item label="Gaji Perbulan" name="sallary">
        <CInputNumber rupiah style={{ width: "100%" }} />
      </Form.Item>
      <Form.Item label="Peghasilan Perbulan" name="sallary">
        <CInputNumber rupiah style={{ width: "100%" }} />
      </Form.Item>
      <Form.Item
        label="Pekerjaan"
        name="work"
        rules={[{ required: true, message: "Please input!" }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Tempat Lahir"
        name="place"
        rules={[{ required: true, message: "Please input!" }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Tempat Lahir"
        name="place"
        rules={[{ required: true, message: "Please input!" }]}
      >
        <Input />
      </Form.Item>
      <Form.Item
        label="Mulai Kerja"
        name="start_work_date"
        rules={[{ required: true, message: "Please input!" }]}
      >
        <CDateSingle onChange={changeStartWork} />
      </Form.Item>
      <BtnSmall className="w-full">Download Form FLPP</BtnSmall>
    </Form>
  );
};

export default FormFlpp;
