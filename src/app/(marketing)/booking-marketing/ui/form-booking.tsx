"use client";
import React, { useEffect } from "react";
import { useRouter, useSearchParams } from "next/navigation";
import { Form, Input } from "antd";
import CInputNumber from "@/app/components/molecules/c-input-number";
import CSelect from "@/app/components/molecules/c-select";
import BtnSmall from "@/app/components/atoms/button/mobile/btn-small";
import { routerMenu } from "@/app/constants/routes";

const FormBooking = ({ queryParams }: any) => {
  // console.log("Heading queryParams unit", queryParams);
  const [form] = Form.useForm();
  const router = useRouter();

  const formItemLayout = {
    labelCol: {
      xs: { span: 24 },
      // sm: { span: 6 },
    },
    wrapperCol: {
      xs: { span: 24 },
      // sm: { span: 14 },
    },
  };

  const onFinish = (values: any) => {
    router.push(`${routerMenu.MARKETING_LIST_BOOKING}`);
  };
  const onChangeProject = (item: any) => {};
  const onChangePhase = (item: any) => {};
  const onChangeBlock = (item: any) => {};
  const onChangeUnit = (item: any) => {};
  // const onLinkListBooking = () => {
  //   router.push(`${routerMenu.MARKETING_LIST_BOOKING}`);
  // };

  return (
    <div>
      <div className="mt-2">
        <Form
          {...formItemLayout}
          onFinish={onFinish}
          layout="vertical"
          style={{ width: "100%" }}
          form={form}
          name="control-hooks"
        >
          <Form.Item
            label="Nama Konsumen"
            name="konsumen"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Alamat"
            name="address"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <Input.TextArea rows={3} />
          </Form.Item>
          <Form.Item
            label="No Hp"
            name="phoneNumber"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <CInputNumber style={{ width: "100%" }} />
          </Form.Item>
          <Form.Item
            label="No Darurat"
            name="emergencyContact"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <CInputNumber style={{ width: "100%" }} />
          </Form.Item>
          <Form.Item
            label="No KTP"
            name="noKtp"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <CInputNumber style={{ width: "100%" }} />
          </Form.Item>
          <Form.Item
            label="Project"
            name="projectId"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <CSelect
              dataOption={[
                { label: "The Mognolia", value: "1" },
                { label: "Cherry Ville", value: "2" },
              ]}
              onChange={onChangeProject}
              value={form.getFieldValue("projectId")}
            />
          </Form.Item>
          <Form.Item
            label="Tahap"
            name="phase"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <CSelect
              dataOption={[
                { label: "1", value: "1" },
                { label: "2", value: "2" },
                { label: "3", value: "3" },
              ]}
              // value={selectBlock}
              onChange={onChangePhase}
              placeholder="Pilih tahap"
            />
          </Form.Item>
          <Form.Item
            label="Blok"
            name="block"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <CSelect
              dataOption={[
                { label: "1", value: "1" },
                { label: "2", value: "2" },
                { label: "3", value: "3" },
              ]}
              // value={selectBlock}
              onChange={onChangeBlock}
              placeholder="Pilih blok"
            />
          </Form.Item>
          <Form.Item
            label="Nomor Unit"
            name="noUnit"
            rules={[{ required: true, message: "Please input!" }]}
          >
            <CSelect
              dataOption={[
                { label: "1", value: "1" },
                { label: "2", value: "2" },
                { label: "3", value: "3" },
              ]}
              // value={selectBlock}
              onChange={onChangeUnit}
              placeholder="Pilih unit"
            />
          </Form.Item>
          <BtnSmall className="w-full">Kirim</BtnSmall>
        </Form>
      </div>
    </div>
  );
};

export default FormBooking;
