"use client";
import React from "react";
import { LeftOutlined } from "@ant-design/icons";
import BtnSmall from "@/app/components/atoms/button/mobile/btn-small";
import { useRouter } from "next/navigation";
import { routerMenu } from "@/app/constants/routes";

const Heading = ({ queryParams }: any) => {
  // console.log("Heading queryParams unit", queryParams);
  const router = useRouter();
  const onLinkProject = () => {
    router.push(`${routerMenu.MARKETING_PROJECT}?projectId=1`);
  };
  return (
    <div>
      <div className="flex items-center justify-between">
        <h1 className="font-bold">Booking</h1>
        <BtnSmall onClick={onLinkProject} icon={<LeftOutlined />}></BtnSmall>
      </div>
    </div>
  );
};

export default Heading;
