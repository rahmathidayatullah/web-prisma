"use client";
import React from "react";
import FormBooking from "./ui/form-booking";
import Heading from "./ui/heading";
import { useSearchParams } from "next/navigation";
const Page = () => {
  const searchParams = useSearchParams();
  const id = searchParams.get("unitId");
  const queryParams = { id: id || "" };

  console.log("id unitId", queryParams.id);
  return (
    <div className="p-3">
      <Heading queryParams={queryParams} />
      <FormBooking queryParams={queryParams} />
    </div>
  );
};

export default Page;
