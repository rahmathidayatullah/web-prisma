"use client";
import React, { useEffect } from "react";
import InformationCodeColor from "./ui/information-code-color";
import InformationFoto from "./ui/information-foto";
import InformationProject from "./ui/information-project";
import InformationSitemap from "./ui/information-sitemap";
import Heading from "./ui/heading";

import { useSearchParams } from "next/navigation";
import { useDispatch } from "react-redux";
import {
  fetchUserProfile,
  fetchUserProfileFromMobile,
} from "@/app/redux/features/profile/actions";
import { logout } from "@/app/api/auth";

const Page = () => {
  const dispatch: any = useDispatch();
  const searchParams = useSearchParams();
  const projectId = searchParams.get("projectId");
  const token = searchParams.get("token");
  const isIt = searchParams.get("isIt");
  const queryParams = { id: projectId || "" };

  useEffect(() => {
    if (token) {
      const userData = {
        access_token: token,
        user: null,
      };
      alert(`token, ${token}`);
      alert(`id project, ${projectId}`);
      alert(`isIt, ${isIt}`);
      localStorage.setItem("userData", JSON.stringify(userData));
      dispatch(fetchUserProfileFromMobile(token));
    } else {
      alert("tidak bisa access halaman gagal mendapatkan token");
      const data = {
        isIt,
        projectId,
      };
      dispatch(logout(data));
    }
  }, [token]);

  console.log("id project", queryParams.id);
  return (
    <div className="p-3">
      <Heading queryParams={queryParams} />
      <div className="mt-4 pb-3 border-b border-gray-200">
        <InformationProject queryParams={queryParams} />
      </div>
      <div className="mt-4 pb-3 border-b border-gray-200">
        <InformationFoto queryParams={queryParams} />
      </div>
      <div className="mt-4 pb-3 border-b border-gray-200">
        <InformationCodeColor queryParams={queryParams} />
      </div>
      <div className="mt-4">
        <InformationSitemap queryParams={queryParams} />
      </div>
    </div>
  );
};

export default Page;
