import React from "react";

const InfomationProject = ({ queryParams }: any) => {
  return (
    <div>
      <div>
        <h2 className="font-medium">Information Project</h2>
        <table className="text-sm mt-2">
          <tbody>
            <tr>
              <td className="min-w-[8rem]">Nama Project</td>
              <td className="min-w-[1rem]">:</td>
              <td>Cherry Ville</td>
            </tr>
            <tr>
              <td>Nama Pemilik</td>
              <td>:</td>
              <td>PT Prime Inti Development</td>
            </tr>
            <tr>
              <td>Alamat</td>
              <td>:</td>
              <td>Perumnas Mustika Raya 1, Seloretno, Jakarta</td>
            </tr>
            <tr>
              <td>Luas Lahan</td>
              <td>:</td>
              <td>Cherry Ville</td>
            </tr>
            <tr>
              <td>Luas Bangunan</td>
              <td>:</td>
              <td>Cherry Ville</td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default InfomationProject;
