"use client";
import React from "react";
import { Image } from "antd";

const InformationFoto = ({ queryParams }: any) => {
  return (
    <div>
      <h2 className="font-medium">Information Foto Project</h2>
      <div className="mt-2">
        <h3 className="text-sm">Logo</h3>
        <div className="mt-1">
          <div className="border w-max rounded-sm">
            <Image
              width={80}
              height={80}
              src="/images/logo-proyek.JPG"
              alt="img-1"
            />
          </div>
        </div>
      </div>
      <div className="mt-2">
        <h3 className="text-sm">Detail</h3>
        <div className="mt-1 w-full overflow-scroll flex items-center gap-2">
          <div className="border w-max rounded-sm">
            <Image.PreviewGroup
              preview={{
                onChange: (current, prev) => {},
              }}
            >
              <Image
                width={80}
                height={80}
                src="/images/1_2 - Photo.jpg"
                alt="img-1"
              />
            </Image.PreviewGroup>
          </div>
          <div className="border w-max rounded-sm">
            <Image.PreviewGroup
              preview={{
                onChange: (current, prev) => {},
              }}
            >
              <Image
                width={80}
                height={80}
                src="/images/1_2 - Photo.jpg"
                alt="img-1"
              />
            </Image.PreviewGroup>
          </div>
          <div className="border w-max rounded-sm">
            <Image.PreviewGroup
              preview={{
                onChange: (current, prev) => {},
              }}
            >
              <Image
                width={80}
                height={80}
                src="/images/1_2 - Photo.jpg"
                alt="img-1"
              />
            </Image.PreviewGroup>
          </div>
          <div className="border w-max rounded-sm">
            <Image.PreviewGroup
              preview={{
                onChange: (current, prev) => {},
              }}
            >
              <Image
                width={80}
                height={80}
                src="/images/1_2 - Photo.jpg"
                alt="img-1"
              />
            </Image.PreviewGroup>
          </div>
        </div>
      </div>
    </div>
  );
};

export default InformationFoto;
