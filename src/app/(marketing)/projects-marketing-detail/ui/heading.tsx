"use client";
import React from "react";
import BtnSmall from "@/app/components/atoms/button/mobile/btn-small";
import { routerMenu } from "@/app/constants/routes";
import { useRouter } from "next/navigation";

const Heading = ({ queryParams }: any) => {
  const router = useRouter();
  const onLinkListBooking = () => {
    router.push(`${routerMenu.MARKETING_LIST_BOOKING}`);
  };
  return (
    <div>
      <div className="flex items-center justify-between">
        <h1 className="font-bold">Informasi Project Cherry Ville</h1>
        <BtnSmall onClick={onLinkListBooking}>List Booking</BtnSmall>
      </div>
    </div>
  );
};

export default Heading;
