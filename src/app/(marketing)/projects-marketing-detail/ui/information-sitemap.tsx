"use client";
import React, { useState } from "react";
import CSelect from "@/app/components/molecules/c-select";
import { Button, Modal, Space } from "antd";
import SVG from "react-inlinesvg";
import {
  TransformWrapper,
  TransformComponent,
  useControls,
} from "react-zoom-pan-pinch";
import { routerMenu } from "@/app/constants/routes";
import Link from "next/link";
import { useRouter } from "next/navigation";

const Controls = () => {
  const { zoomIn, zoomOut, resetTransform } = useControls();

  return (
    <div className="flex items-center gap-5 mb-2">
      <Button onClick={() => zoomIn()}>+</Button>
      <Button onClick={() => zoomOut()}>-</Button>
      <Button onClick={() => resetTransform()}>x</Button>
    </div>
  );
};

const InfomationSitemap = ({ queryParams }: any) => {
  const router = useRouter();
  const options = [
    { value: "jack", label: "Jack" },
    { value: "lucy", label: "Lucy" },
    { value: "Yiminghe", label: "yiminghe" },
    { value: "disabled", label: "Disabled", disabled: true },
  ];

  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  // const loadDataImage = (urlSvg: any, dataListUnit: any) => {
  const loadDataImage = (urlSvg: any) => {
    const svg: SVGSVGElement | any = document.getElementById("1");
    if (!svg) {
      alert("Terjadi kesalahan pada svg yang di upload");
      return;
    }
    const paths: HTMLCollection | any = svg.getElementsByTagName("path");
    if (!paths) {
      alert("Terjadi kesalahan pada svg path yang di upload");
      return;
    }

    for (let path of paths) {
      if (path.getAttribute("fill")) {
        console.log("test");
        path.setAttribute("class", "styleTagPath");
        path.addEventListener("click", (event: MouseEvent) => {
          // eventClickShapes(event, path);
          // showModal();
          router.push(`${routerMenu.MARKETING_BOOKING}?unitId=1`);
        });
      }
    }
  };

  return (
    <div>
      <div>
        <h2 className="font-medium">Denah Lokasi</h2>
        <div className="mt-2">
          <Space direction="vertical">
            <CSelect
              dataOption={options}
              onChange={() => {}}
              // value={tahap}
              placeholder="Filter By Tahap"
              allowClear={true}
            />
            <CSelect
              dataOption={options}
              onChange={() => {}}
              // value={blok}
              placeholder="Filter By Blok"
              allowClear={true}
            />
            <CSelect
              dataOption={options}
              onChange={() => {}}
              // value={unit}
              placeholder="Filter By Status Unit"
              allowClear={true}
            />
          </Space>
        </div>
        <div>
          <Modal
            title="Basic Modal"
            open={isModalOpen}
            onOk={handleOk}
            onCancel={handleCancel}
          >
            <p>Some contents...</p>
            <p>Some contents...</p>
            <p>Some contents...</p>
          </Modal>
        </div>
        <div className="mt-2">
          <TransformWrapper
            initialScale={1}
            initialPositionX={0}
            initialPositionY={0}
          >
            {({ zoomIn, zoomOut, resetTransform, ...rest }) => (
              <>
                <Controls />
                <TransformComponent
                  wrapperStyle={{
                    border: "1px solid #efefef; border-radius: 8px;",
                  }}
                >
                  <div>
                    <SVG
                      // className="w-full h-[40rem]"
                      className="w-full h-[18rem]"
                      src="/images/test2-01 1 1.svg"
                      // src={dataDetail.siteplan_image}
                      // onLoad={(urlSvg: string) => loadDataImage(urlSvg, dataListUnit)}
                      onLoad={(urlSvg: string) => loadDataImage(urlSvg)}
                    />
                  </div>
                </TransformComponent>
              </>
            )}
          </TransformWrapper>
        </div>
      </div>
    </div>
  );
};

export default InfomationSitemap;
