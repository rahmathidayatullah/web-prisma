import React from "react";

const InformationCodeColor = ({ queryParams }: any) => {
  return (
    <div>
      <h2 className="font-medium">Information Code Warna</h2>
      <table className="text-sm mt-2 border-spacing-4">
        <tbody>
          <tr>
            <td className="min-w-[3rem]">
              <div className="w-5 h-5 rounded-sm bg-[#0066FF]"></div>
            </td>
            <td className="min-w-[1rem]">:</td>
            <td className="py-1">Open</td>
          </tr>
          <tr>
            <td>
              <div className="w-5 h-5 rounded-sm bg-[#42FF24]"></div>
            </td>
            <td>:</td>
            <td className="py-1">Terbooking</td>
          </tr>
          <tr>
            <td>
              <div className="w-5 h-5 rounded-sm bg-[#FFF500]"></div>
            </td>
            <td>:</td>
            <td className="py-1">Proses KPR</td>
          </tr>
          <tr>
            <td>
              <div className="w-5 h-5 rounded-sm bg-[#FF0000]"></div>
            </td>
            <td>:</td>
            <td className="py-1">SP3K</td>
          </tr>
          <tr>
            <td>
              <div className="w-5 h-5 rounded-sm bg-[#76ABFC]"></div>
            </td>
            <td>:</td>
            <td className="py-1">Akad</td>
          </tr>
          <tr>
            <td>
              <div className="w-5 h-5 rounded-sm bg-[#0AF0FF]"></div>
            </td>
            <td>:</td>
            <td className="py-1">Serah Terima</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
};

export default InformationCodeColor;
