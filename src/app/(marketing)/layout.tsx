import type { Metadata } from "next";
import { Inter } from "next/font/google";
import Providers from "../redux/Provider";
import { ConfigProvider } from "antd";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Pt Prisma Properties - Dashboard",
  description: "Generated by create next app",
};

export default function AdminLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <Providers>
          <ConfigProvider
            theme={{
              token: {
                colorPrimary: "#219C90",
              },
            }}
          >
            {children}
          </ConfigProvider>
        </Providers>
      </body>
    </html>
  );
}
