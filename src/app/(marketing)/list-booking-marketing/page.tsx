"use client";
import React from "react";
import Heading from "./ui/heading";
import ListBooking from "./ui/list-booking";

const Page = () => {
  // const searchParams = useSearchParams();
  // const id = searchParams.get("unit");
  // const queryParams = { id: id || "" };
  return (
    <div className="p-3">
      <Heading />
      <ListBooking />
    </div>
  );
};

export default Page;
