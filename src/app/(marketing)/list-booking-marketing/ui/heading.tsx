"use client";
import React from "react";
import { LeftOutlined } from "@ant-design/icons";
import BtnSmall from "@/app/components/atoms/button/mobile/btn-small";
import { useRouter } from "next/navigation";
import { routerMenu } from "@/app/constants/routes";

const Heading = () => {
  const router = useRouter();
  const onLinkProject = () => {
    router.push(`${routerMenu.MARKETING_PROJECT}?projectId=1`);
  };
  const onLinkAddBooking = () => {
    router.push(`${routerMenu.MARKETING_BOOKING}?unitId=1`);
  };
  return (
    <div>
      <div className="flex items-center justify-between">
        <h1 className="font-bold">Booking</h1>
        <div className="flex item-center gap-1">
          <BtnSmall onClick={onLinkProject} icon={<LeftOutlined />}></BtnSmall>
          <BtnSmall onClick={onLinkAddBooking}>Tambah Booking</BtnSmall>
        </div>
      </div>
    </div>
  );
};

export default Heading;
