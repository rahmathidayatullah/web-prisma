"use client";
import BtnSmall from "@/app/components/atoms/button/mobile/btn-small";
import { routerMenu } from "@/app/constants/routes";
import { Badge, Tag } from "antd";
import { useRouter } from "next/navigation";
import React from "react";

const ListBooking = () => {
  const router = useRouter();
  const data = [
    {
      id: "1",
      name: "Rahmat Hidayatullah",
      status: "Booking - 24/02/2024",
      proyek: "The Cherry Vile",
      phase: "1",
      block: "a-1",
      no: "1",
      deadline_berkas: "14 Hari",
      status_berkas: "0%",
    },
    {
      id: "2",
      name: "Rahmat Hidayatullah",
      status: "Booking - 24/02/2024",
      proyek: "The Cherry Vile",
      phase: "1",
      block: "a-1",
      no: "1",
      deadline_berkas: "14 Hari",
      status_berkas: "Reject",
    },
    {
      id: "3",
      name: "Rahmat Hidayatullah",
      status: "Booking - 24/02/2024",
      proyek: "The Cherry Vile",
      phase: "1",
      block: "a-1",
      no: "1",
      deadline_berkas: "14 Hari",
      status_berkas: "Request SiKasep",
    },
  ];

  const onEditBooking = () => {
    router.push(`${routerMenu.MARKETING_BOOKING}?bookingId=10`);
  };
  const onDetailBooking = () => {
    router.push(`${routerMenu.MARKETING_DETAIL_BOOKING}?unitId=10`);
  };

  return (
    <div>
      <div>
        <ul>
          {data.map((item: any) => {
            return (
              <li key={item.id} className="rounded-sm mt-4 border">
                <div className="flex items-center justify-between pt-1 px-3">
                  <span className="text-xs">{item.name}</span>
                  <Tag color="green">{item.status}</Tag>
                </div>
                <div className="flex items-center justify-between mt-2 px-3">
                  <span className="text-xs">Proyek</span>
                  <Tag color="default">{item.proyek}</Tag>
                </div>
                <div className="flex items-center justify-between mt-2 px-3">
                  <span className="text-xs">Tahap</span>
                  <Tag color="default">{item.phase}</Tag>
                </div>
                <div className="flex items-center justify-between mt-2 px-3">
                  <span className="text-xs">Blok</span>
                  <Tag color="default">{item.block}</Tag>
                </div>
                <div className="flex items-center justify-between mt-2 px-3">
                  <span className="text-xs">Nomor</span>
                  <Tag color="default">{item.no}</Tag>
                </div>
                <div className="flex items-center justify-between mt-2 px-3">
                  <span className="text-xs">Deadline Berkas</span>
                  <Tag color="default">{item.deadline_berkas}</Tag>
                </div>
                <div className="flex items-center justify-between mt-2 px-3 pb-2">
                  <span className="text-xs">Status Berkas</span>
                  <Tag color="error">{item.status_berkas}</Tag>
                </div>
                <div className="border-t flex items-center justify-between py-2 px-3">
                  <BtnSmall onClick={onEditBooking}>Edit</BtnSmall>
                  <BtnSmall onClick={onDetailBooking}>Detail</BtnSmall>
                </div>
              </li>
            );
          })}
        </ul>
      </div>
    </div>
  );
};

export default ListBooking;
