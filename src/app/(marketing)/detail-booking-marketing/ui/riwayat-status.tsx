"use client";
import BtnSmall from "@/app/components/atoms/button/mobile/btn-small";
import { Modal, Tag, Timeline } from "antd";
import React, { useState } from "react";

const RiwayatStatus = ({ queryParams }: any) => {
  const [isModalOpen, setIsModalOpen] = useState(false);

  const showModal = () => {
    setIsModalOpen(true);
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <>
      <div>
        <h1 className="font-bold mb-3">Riwayat Status</h1>
        <Timeline
          items={[
            {
              children: (
                <div className="flex items-center gap-2">
                  <span>Booking 24/02/2024</span>
                </div>
              ),
            },
            {
              children: (
                <div className="flex items-center gap-2">
                  <span>Cancel</span>
                  <BtnSmall onClick={() => showModal()}>Lihat Alasan</BtnSmall>
                </div>
              ),
            },
            {
              children: (
                <div className="flex items-center gap-2">
                  <span>Review Admin 25/02/2024</span>
                </div>
              ),
            },
            {
              children: (
                <div className="flex items-center gap-2">
                  <span>Proses KPR 26/02/2024</span>
                </div>
              ),
            },
            {
              children: (
                <div className="flex items-center gap-2">
                  <span>SP3K 27/02/2024</span>
                </div>
              ),
            },
            {
              children: (
                <div className="flex items-center gap-2">
                  <span>Reject 27/02/2024</span>
                  <BtnSmall onClick={() => showModal()}>Lihat Alasan</BtnSmall>
                </div>
              ),
            },
            {
              children: (
                <div className="flex items-center gap-2">
                  <span>Akad 28/02/2024</span>
                </div>
              ),
            },
            {
              children: (
                <div className="flex items-center gap-2">
                  <span>Serah Terima 30/02/2024</span>
                </div>
              ),
            },
          ]}
        />
      </div>

      {/* <div className="flex flex-col gap-2 items-start">
        <span className="text-sm">Booking Fee </span>
        <Tag color="red">Menunggu Pembayaran (30 Menut) </Tag>
      </div> */}
      <div className="flex flex-col gap-2 items-start">
        <span className="text-sm">Booking Fee </span>
        <Tag color="green">Terbayar</Tag>
      </div>
      <Modal
        title="Alasan Ditolak"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
      </Modal>
    </>
  );
};

export default RiwayatStatus;
