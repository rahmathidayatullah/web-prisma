"use client";
import BtnSmall from "@/app/components/atoms/button/mobile/btn-small";
import { routerMenu } from "@/app/constants/routes";
import { useRouter } from "next/navigation";
import React from "react";

import { UploadOutlined } from "@ant-design/icons";
import type { UploadProps } from "antd";
import { Button, message, Upload } from "antd";

const UploadBerkas = () => {
  const router = useRouter();

  const onLinkGenerateFormFlpp = () => {
    router.push(`${routerMenu.GENERATE_FORM_FLPP}`);
  };

  const onSubmitForm = () => {
    router.push(`${routerMenu.MARKETING_LIST_BOOKING}`);
  };

  const props: UploadProps = {
    accept: ".pdf,.docx",
    maxCount: 1,
    onChange(info) {
      if (info.file.status !== "uploading") {
        console.log(info.file, info.fileList);
      }
      if (info.file.status === "done") {
        message.success(`${info.file.name} file uploaded successfully`);
      } else if (info.file.status === "error") {
        message.error(`${info.file.name} file upload failed.`);
      }
    },
  };

  return (
    <div>
      <h1 className="font-bold mb-3">Generate Form FLPP</h1>
      <BtnSmall onClick={onLinkGenerateFormFlpp}>Generate Dokumen</BtnSmall>
      <div className="flex items-center justify-between mb-3 mt-5">
        <h1 className="font-bold">Upload Berkas</h1>
        <h1 className="text-sm">Status Berkas 80%</h1>
      </div>
      <ul>
        <li>
          <div className="border p-2 rounded-sm">
            <Upload {...props}>
              <Button icon={<UploadOutlined />}>KTP</Button>
            </Upload>
          </div>
        </li>
        <li className="mt-2">
          <div className="border p-2 rounded-sm">
            <Upload {...props}>
              <Button icon={<UploadOutlined />}>KTP Pasangan (Optional)</Button>
            </Upload>
          </div>
        </li>
        <li className="mt-2">
          <div className="border p-2 rounded-sm">
            <Upload {...props}>
              <Button icon={<UploadOutlined />}>KK</Button>
            </Upload>
          </div>
        </li>
        <li className="mt-2">
          <div className="border p-2 rounded-sm">
            <Upload {...props}>
              <Button icon={<UploadOutlined />}>NPWP</Button>
            </Upload>
          </div>
        </li>
        <li className="mt-2">
          <div className="border p-2 rounded-sm">
            <Upload {...props}>
              <Button icon={<UploadOutlined />}>BPJS Ketenagakerjaan</Button>
            </Upload>
          </div>
        </li>
        <li className="mt-2">
          <div className="border p-2 rounded-sm">
            <Upload {...props}>
              <Button icon={<UploadOutlined />}>BPJS Kesehatan</Button>
            </Upload>
          </div>
        </li>
        <li className="mt-2">
          <div className="border p-2 rounded-sm">
            <Upload {...props}>
              <Button icon={<UploadOutlined />}>
                Buku Nikah ( Optional ){" "}
              </Button>
            </Upload>
          </div>
        </li>
        <li className="mt-2">
          <div className="border p-2 rounded-sm">
            <Upload {...props}>
              <Button icon={<UploadOutlined />}>Best Time To Call</Button>
            </Upload>
          </div>
        </li>
        <li className="mt-2">
          <div className="border p-2 rounded-sm">
            <Upload {...props}>
              <Button icon={<UploadOutlined />}>SPT Tahunan</Button>
            </Upload>
          </div>
        </li>
        <li className="mt-2">
          <div className="border p-2 rounded-sm">
            <Upload {...props}>
              <Button icon={<UploadOutlined />}>
                Surat Keterangan Domisili
              </Button>
            </Upload>
          </div>
        </li>
        <li className="mt-2">
          <div className="border p-2 rounded-sm">
            <Upload {...props}>
              <Button icon={<UploadOutlined />}>Form Aplikasi</Button>
            </Upload>
          </div>
        </li>
        <li className="mt-2">
          <div className="border p-2 rounded-sm">
            <Upload {...props}>
              <Button icon={<UploadOutlined />}>Form Wawancara</Button>
            </Upload>
          </div>
        </li>
        <li className="mt-2">
          <div className="border p-2 rounded-sm">
            <Upload {...props}>
              <Button icon={<UploadOutlined />}>Rekening Koran</Button>
            </Upload>
          </div>
        </li>
        <li className="mt-2">
          <div className="border p-2 rounded-sm">
            <Upload {...props}>
              <Button icon={<UploadOutlined />}>SK Kerja</Button>
            </Upload>
          </div>
        </li>
        <li className="mt-2">
          <div className="border p-2 rounded-sm">
            <Upload {...props}>
              <Button icon={<UploadOutlined />}>Slip Gaji Pemohon</Button>
            </Upload>
          </div>
        </li>
        <li className="mt-2">
          <div className="border p-2 rounded-sm">
            <Upload {...props}>
              <Button icon={<UploadOutlined />}>
                Slip Gaji Pemohon Pasangan ( Optional )
              </Button>
            </Upload>
          </div>
        </li>
      </ul>
      <BtnSmall className="w-full mt-4" onClick={onSubmitForm}>
        Kirim
      </BtnSmall>
    </div>
  );
};

export default UploadBerkas;
