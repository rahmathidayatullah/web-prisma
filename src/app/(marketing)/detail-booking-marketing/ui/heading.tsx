"use client";
import React from "react";
import { LeftOutlined } from "@ant-design/icons";
import BtnSmall from "@/app/components/atoms/button/mobile/btn-small";
import { useRouter } from "next/navigation";
import { routerMenu } from "@/app/constants/routes";

const Heading = ({ queryParams }: any) => {
  const router = useRouter();
  const onLinkListBooking = () => {
    router.push(`${routerMenu.MARKETING_LIST_BOOKING}`);
  };
  return (
    <div>
      <div className="flex items-center justify-between">
        <h1 className="font-bold">Detail Booking Rahmat H</h1>
        <BtnSmall
          onClick={onLinkListBooking}
          icon={<LeftOutlined />}
        ></BtnSmall>
      </div>
    </div>
  );
};

export default Heading;
