"use client";
import React from "react";
import Heading from "./ui/heading";
import { useSearchParams } from "next/navigation";
import RiwayatStatus from "./ui/riwayat-status";
import UploadBerkas from "./ui/upload-berkas";

const Page = () => {
  const searchParams = useSearchParams();
  const id = searchParams.get("bookingId");
  const queryParams = { id: id || "" };

  return (
    <div className="p-3">
      <Heading queryParams={queryParams} />
      <div className="mt-4">
        <RiwayatStatus />
      </div>
      <div className="mt-4">
        <UploadBerkas />
      </div>
    </div>
  );
};

export default Page;
