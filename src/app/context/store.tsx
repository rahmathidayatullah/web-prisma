// "use client";

// import { notification } from "antd";
// import { MessageType } from "antd/es/message/interface";
// import { NotificationPlacement } from "antd/es/notification/interface";
// import { create } from "domain";
// import {
//   createContext,
//   useContext,
//   Dispatch,
//   SetStateAction,
//   useState,
//   useCallback,
// } from "react";

// type DataType = {
//   firstName: string;
// };

// interface ContextProps {
//   userId: string;
//   setUserId: Dispatch<SetStateAction<string>>;
//   data: DataType[];
//   setData: Dispatch<SetStateAction<DataType[]>>;
// }

// const GlobalContext = createContext<ContextProps>({
//   userId: "",
//   setUserId: (): string => "",
//   data: [],
//   setData: (): DataType[] => [],
// });

// export const GlobalContextProvider = ({ children }: any) => {
//   const [messageApi, contextHolder] = notification.useNotification();
//   //   const [userId, setUserId] = useState("");
//   //   const [data, setData] = useState<[] | DataType[]>([]);

//   return (
//     <GlobalContext.Provider value={{ userId, setUserId, data, setData }}>
//       {contextHolder}
//       {children}
//     </GlobalContext.Provider>
//   );
// };

// export const useGlobalContext: any = () => useContext(useGlobalContext);

"use client";

import { notification } from "antd";
import { NotificationPlacement } from "antd/es/notification/interface";
import { createContext, useContext, useCallback, useState } from "react";

const GlobalContext = createContext<any>({});

export const GlobalContextProvider = ({ children }: any) => {
  const [selectPath, setPathSelect] = useState<any>(null);
  const [svgContent2, setSvgContent2] = useState<any>(null);

  const onChangeSvgContant2 = (value: any) => {
    setSvgContent2(value);
  };

  const onChangePathSelect = (value: any) => {
    setPathSelect(value);
  };
  const [messageApi, contextHolder] = notification.useNotification();

  const openMessage = useCallback(
    (
      type: "error" | "info" | "warning" | "success" | undefined,
      message: string,
      placement?: NotificationPlacement
    ) => {
      messageApi.open({
        type: type,
        message,
        placement,
      });
    },
    [messageApi]
  );

  const openNotification = useCallback(
    (placement: NotificationPlacement, title: string, description: string) => {
      messageApi.info({
        message: title,
        description,
        placement,
      });
    },
    [messageApi]
  );

  return (
    <GlobalContext.Provider
      value={{
        openNotification,
        openMessage,
        selectPath,
        onChangePathSelect,
        svgContent2,
        onChangeSvgContant2,
      }}
    >
      {contextHolder}
      {children}
    </GlobalContext.Provider>
  );
};

export const useGlobalContext: any = () => useContext(GlobalContext);

// openMessage("warning", "Password yang di inputkan tidak sesuai", "top");
// openNotification("topLeft", "Berhasil", `Data berhasil di ${action}`);
