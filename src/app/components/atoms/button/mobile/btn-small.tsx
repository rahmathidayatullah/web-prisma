import { Button } from "antd";
import React from "react";

interface BtnSmall {
  loading?: boolean;
  disabled?: boolean;
  children?: any;
  onClick?: any;
  icon?: any;
  className?: any;
}

const BtnSmall = ({
  loading,
  disabled,
  children,
  onClick,
  icon,
  className = "bg-[#219C90] text-white",
}: BtnSmall) => {
  const classNameProps = className;
  return (
    <Button
      icon={icon}
      type="primary"
      htmlType="submit"
      loading={loading}
      disabled={disabled}
      className={classNameProps}
      onClick={onClick}
    >
      {children}
    </Button>
  );
};

export default BtnSmall;
