import React from "react";
import { Select } from "antd";

interface TypeCSelect {
  label: string;
  value: string;
}

interface Props {
  dataOption: TypeCSelect[];
  onChange?: any;
  value?: string;
  disabled?: boolean;
  placeholder?: string;
  allowClear?: boolean;
}

const CSelect: React.FC<Props> = ({
  dataOption,
  onChange,
  value,
  disabled,
  placeholder = "",
  allowClear = false,
}) => {
  if (dataOption?.length === 0) {
    return (
      <div className="border rounded-md py-1 px-2">Tidak ada data option</div>
    );
  }

  return (
    <Select
      placeholder={placeholder}
      value={value}
      onChange={onChange}
      disabled={disabled}
      allowClear={allowClear}
    >
      {dataOption?.map((item: TypeCSelect, index: number) => {
        return (
          <Select.Option key={index} value={item.value}>
            {item.label}
          </Select.Option>
        );
      })}
    </Select>
  );
};

export default CSelect;
