import React from "react";
import { Table } from "antd";

interface typeCTable {
  pagination: any;
  columns: any;
  dataSource: any;
  loading: boolean;
  scroll: any;
  rowKey: string;
}

const CTable = ({
  pagination,
  columns,
  dataSource,
  loading,
  scroll,
  rowKey,
}: typeCTable) => {
  return (
    <Table
      pagination={pagination}
      columns={columns}
      dataSource={dataSource}
      loading={loading}
      scroll={scroll}
      rowKey={rowKey}
    />
  );
};

export default CTable;
