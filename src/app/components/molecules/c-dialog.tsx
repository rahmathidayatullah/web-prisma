import { Modal } from "antd";
import React from "react";
import CButton from "./c-button";

interface typeCDialog {
  title: string;
  open: boolean;
  titleActionOk: string;
  titleActionCancel: string;
  actionCancel: any;
  actionOk: any;
  loadingBtn?: boolean;
  children?: any;
}

const CDialog = ({
  title,
  open,
  titleActionOk,
  titleActionCancel,
  actionCancel,
  actionOk,
  loadingBtn,
  children,
}: typeCDialog) => {
  return (
    <Modal
      title={title}
      open={open}
      onOk={actionOk}
      onCancel={actionCancel}
      footer={[
        <CButton
          key="1"
          size="small"
          variant="primary"
          onClick={actionOk}
          disabled={loadingBtn}
          loading={loadingBtn}
        >
          {titleActionOk}
        </CButton>,
        <CButton
          key="2"
          size="small"
          variant="danger"
          onClick={actionCancel}
          disabled={loadingBtn}
          loading={loadingBtn}
        >
          {titleActionCancel}
        </CButton>,
      ]}
    >
      {children}
    </Modal>
  );
};

export default CDialog;
