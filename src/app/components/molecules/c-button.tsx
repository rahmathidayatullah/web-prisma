import React from "react";
import { Button } from "antd";
import { SortAscendingOutlined } from "@ant-design/icons";

interface typeCButton {
  loading?: boolean;
  children: any;
  variant: "primary" | "secondary" | "danger" | "transparent";
  onClick?: any;
  disabled?: boolean;
  size?: "large" | "default" | "small" | any;
  icon?: any;
}

const CButton = ({
  loading,
  children,
  variant,
  onClick,
  disabled,
  size = "default",
  icon,
}: typeCButton) => {
  if (variant === "danger") {
    return (
      <Button
        onClick={onClick}
        disabled={disabled}
        loading={loading}
        style={{ backgroundColor: "#FF5151", color: "white" }}
        danger
        type="primary"
        size={size}
        icon={icon}
      >
        {children}
      </Button>
    );
  } else if (variant === "secondary") {
    return (
      <Button
        onClick={onClick}
        disabled={disabled}
        loading={loading}
        style={{ backgroundColor: "#219C90" }}
        type="primary"
        size={size}
        icon={icon}
      >
        {children}
      </Button>
    );
  } else if (variant === "primary") {
    return (
      <Button
        onClick={onClick}
        disabled={disabled}
        loading={loading}
        style={{ backgroundColor: "#219C90", color: "white" }}
        type="primary"
        size={size}
        icon={icon}
      >
        {children}
      </Button>
    );
  }

  //   transparent
  return (
    <Button
      onClick={onClick}
      disabled={disabled}
      loading={loading}
      style={{ backgroundColor: "#219C90" }}
      type="primary"
      icon={icon}
    >
      {children}
    </Button>
  );
};

export default CButton;
