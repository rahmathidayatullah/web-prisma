import React from "react";
import { Select } from "antd";

interface typeCSelectMultiple {
  onChange: any;
  options: any;
  value: any;
}

const CSelectMultiple = ({ onChange, options, value }: typeCSelectMultiple) => {
  return (
    <Select
      mode="multiple"
      allowClear
      style={{ width: "100%" }}
      placeholder="Please select"
      // defaultValue={['a10', 'c12']}
      defaultValue={value}
      value={value}
      onChange={onChange}
      options={options}
    />
  );
};

export default CSelectMultiple;
