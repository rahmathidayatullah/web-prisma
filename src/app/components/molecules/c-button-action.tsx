import React from "react";
import { Button } from "antd";

interface typeCButtonAction {
  loading?: boolean;
  children: any;
  variant?: "primary" | "secondary" | "danger" | "transparent";
  onClick?: any;
  disabled?: boolean;
  size?: "large" | "default" | "small" | any;
  htmlType?: any;
  icon?: any;
}

const CButtonAction = ({
  loading,
  children,
  variant,
  onClick,
  disabled,
  size = "default",
  htmlType,
  icon,
}: typeCButtonAction) => {
  if (variant === "danger") {
    return (
      <Button
        onClick={onClick}
        disabled={disabled}
        loading={loading}
        style={{ backgroundColor: "#FF5151", color: "white" }}
        danger
        type="primary"
        size={size}
        htmlType={htmlType}
        icon={icon}
      >
        {children}
      </Button>
    );
  } else if (variant === "secondary") {
    return (
      <Button
        onClick={onClick}
        disabled={disabled}
        loading={loading}
        style={{ backgroundColor: "#219C90" }}
        type="primary"
        size={size}
        htmlType={htmlType}
        icon={icon}
      >
        {children}
      </Button>
    );
  } else if (variant === "primary") {
    return (
      <Button
        onClick={onClick}
        disabled={disabled}
        loading={loading}
        style={{ backgroundColor: "#219C90", color: "white" }}
        type="primary"
        size={size}
        htmlType={htmlType}
        icon={icon}
      >
        {children}
      </Button>
    );
  }

  //   transparent
  return (
    <Button
      onClick={onClick}
      disabled={disabled}
      loading={loading}
      type="default"
      htmlType={htmlType}
      icon={icon}
    >
      {children}
    </Button>
  );
};

export default CButtonAction;
