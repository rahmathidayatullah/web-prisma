import { Dropdown, Space } from "antd";
import React from "react";
import CButton from "./c-button";
import { FilterOutlined } from "@ant-design/icons";

interface typeCDropdown {
  menu?: any;
  onClick?: any;
  variant: "sortir-coloum" | "default";
  onOpenChange?: any;
  open?: any;
}

const CDropdown = ({
  menu,
  onClick,
  variant,
  onOpenChange,
  open,
}: typeCDropdown) => {
  if (variant === "sortir-coloum") {
    return (
      <Dropdown
        menu={menu}
        trigger={["click"]}
        onOpenChange={onOpenChange}
        open={open}
      >
        <a onClick={onClick}>
          <Space>
            <CButton variant="primary" icon={<FilterOutlined />}>
              Sortir Kolom
            </CButton>
          </Space>
        </a>
      </Dropdown>
    );
  }
  return (
    <Dropdown
      menu={menu}
      trigger={["click"]}
      onOpenChange={onOpenChange}
      open={open}
    >
      <a onClick={onClick}>
        <Space>
          <CButton variant="primary">Sortir Kolom</CButton>
        </Space>
      </a>
    </Dropdown>
  );
};

export default CDropdown;
