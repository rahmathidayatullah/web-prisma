import React, { useEffect, useState } from "react";
import { PlusOutlined } from "@ant-design/icons";
import { Modal, Upload, message } from "antd";
import type { GetProp, UploadFile, UploadProps } from "antd";
import { getBase64 } from "@/app/constants/helper";

type FileType = Parameters<GetProp<UploadProps, "beforeUpload">>[0];

const getBase64This = (file: FileType): Promise<string> =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result as string);
    reader.onerror = (error) => reject(error);
  });

interface CUploadMultipleProps {
  onChange?: any;
  dataFileList?: any;
  disabled?: boolean;
  maxCount?: number;
  acceptImg?: string;
}

const CUploadMultiple: React.FC<CUploadMultipleProps> = ({
  onChange,
  dataFileList = [],
  disabled,
  maxCount = 1,
  acceptImg = "image/png, image/jpeg",
}) => {
  const [previewOpen, setPreviewOpen] = useState(false);
  const [previewImage, setPreviewImage] = useState("");
  const [previewTitle, setPreviewTitle] = useState("");
  const [fileList, setFileList] = useState<UploadFile[]>(
    // dataFileList
    []
    // [
    // {
    //   uid: "-1",
    //   name: "image.png",
    //   status: "done",
    //   url: "https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png",
    // },
    //   ]
  );
  useEffect(() => {
    if (dataFileList.length) {
      setFileList(dataFileList);
    }
  }, [dataFileList]);
  const [base64Photo, setBase64Photo] = useState<string>("");

  const handleCancel = () => setPreviewOpen(false);

  const handlePreview = async (file: UploadFile) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64This(file.originFileObj as FileType);
    }

    setPreviewImage(file.url || (file.preview as string));
    setPreviewOpen(true);
    setPreviewTitle(
      file.name || file.url!.substring(file.url!.lastIndexOf("/") + 1)
    );
  };

  const [isFalse, setIsFalse] = useState(false);
  const handleChange: UploadProps["onChange"] = ({
    fileList: newFileList,
  }: any) => {
    if (isFalse) {
      setFileList([]);
      setBase64Photo("");
      setIsFalse(false);
    } else {
      setFileList(newFileList);
      // if (newFileList.length) {
      onChange(newFileList);
      // getBase64(newFileList[0].originFileObj, (base64String: any) => {
      //   setBase64Photo(base64String);
      //   onChange(base64String); // Callback to parent component
      // });
      // }
    }
  };

  const beforeUpload = (file: FileType) => {
    const isJpgOrPng =
      file.type === "image/jpeg" ||
      file.type === "image/png" ||
      file.type === "image/svg+xml";
    if (!isJpgOrPng) {
      message.error("You can only upload JPG/PNG file!");
    }
    const isLt2M = file.size / 1024 / 1024 < 10;
    if (!isLt2M) {
      message.error("Image must be smaller than 1MB!");
      setIsFalse(true);
    }
    return isJpgOrPng && isLt2M;
  };

  const uploadButton = (
    <button style={{ border: 0, background: "none" }} type="button">
      <PlusOutlined />
      <div style={{ marginTop: 8 }}>Upload</div>
    </button>
  );

  return (
    <>
      <Upload
        disabled={disabled}
        // action="https://run.mocky.io/v3/435e224c-44fb-4773-9faf-380c5e6a2188"
        listType="picture-card"
        fileList={fileList}
        onPreview={handlePreview}
        onChange={handleChange}
        maxCount={maxCount}
        beforeUpload={beforeUpload}
        accept={acceptImg}
      >
        {fileList.length >= 4 ? null : uploadButton}
      </Upload>
      <Modal
        open={previewOpen}
        title={previewTitle}
        footer={null}
        onCancel={handleCancel}
      >
        <img alt="example" style={{ width: "100%" }} src={previewImage} />
      </Modal>
    </>
  );
};

export default CUploadMultiple;
