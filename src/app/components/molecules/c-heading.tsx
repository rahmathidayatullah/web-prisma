import React from "react";

interface typeHeading {
  title: string;
  description: string;
}
const CHeading = ({ title, description }: typeHeading) => {
  return (
    <div className="border-b pb-5">
      <div className="px-0">
        <h3 className="text-base font-semibold leading-7 text-gray-900">
          {title}
        </h3>
        <p className="mt-1 max-w-2xl text-sm leading-6 text-gray-500">
          {description}
        </p>
      </div>
    </div>
  );
};

export default CHeading;
