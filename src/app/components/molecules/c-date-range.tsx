import React from "react";
import { DatePicker } from "antd";

const { RangePicker } = DatePicker;
const dateFormat = "YYYY-MM-DD";

interface typeCDateRange {
  onChange: any;
}

const CDateRange = ({ onChange }: typeCDateRange) => {
  return (
    <RangePicker format={dateFormat} showToday={false} onChange={onChange} />
  );
};

export default CDateRange;
