"use client";

import { Radio } from "antd";

interface typeCRadioGroup {
  onChange?: any;
  value?: any;
  dataOption: any;
}

const CRadioGroup = ({ dataOption, onChange, value }: typeCRadioGroup) => {
  return (
    <Radio.Group onChange={onChange} value={value}>
      {dataOption.map((item: any) => {
        return (
          <Radio key={item.value} value={item.value}>
            {item.label}
          </Radio>
        );
      })}
      {/* <Radio value={2}>B</Radio>
      <Radio value={3}>C</Radio>
      <Radio value={4}>D</Radio> */}
    </Radio.Group>
  );
};

export default CRadioGroup;
