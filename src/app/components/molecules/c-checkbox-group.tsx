import { Checkbox, CheckboxOptionType } from "antd";
import React from "react";

interface typeCCheckboxGroup {
  value: any;
  options: any;
  onChange: any;
}

const CCheckboxGroup = ({ value, options, onChange }: typeCCheckboxGroup) => {
  return (
    <Checkbox.Group
      className="flex flex-col gap-3"
      value={value}
      options={options as CheckboxOptionType[]}
      onChange={onChange}
    />
  );
};

export default CCheckboxGroup;
