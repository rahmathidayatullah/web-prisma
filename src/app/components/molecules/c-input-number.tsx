"use client";
import { InputNumber } from "antd";
import React from "react";
import type { InputNumberProps } from "antd";

const CInputNumber = ({
  onChange,
  rupiah,
  disabled,
  className,
  style,
  defaultValue,
  value,
}: {
  disabled?: boolean;
  rupiah?: boolean;
  onChange?: InputNumberProps["onChange"];
  className?: string;
  style?: any;
  defaultValue?: number;
  min?: number;
  max?: number;
  value?: number;
}) => {
  if (rupiah) {
    return (
      <InputNumber<number>
        disabled={disabled}
        defaultValue={defaultValue}
        formatter={(value) =>
          `Rp. ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
        }
        parser={(value) =>
          value?.replace(/\Rp.\s?|(,*)/g, "") as unknown as number
        }
        onChange={onChange}
        className={className}
        style={style}
        value={value}
      />
    );
  }
  return (
    <InputNumber
      defaultValue={defaultValue}
      onChange={onChange}
      value={value}
      disabled={disabled}
      className={className}
      style={style}
    />
  );
};

export default CInputNumber;
