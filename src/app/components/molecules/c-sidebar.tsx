"use client";
import React, { useEffect, useState } from "react";
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
  GroupOutlined,
  FormOutlined,
  CalendarOutlined,
  HomeOutlined,
  TeamOutlined,
} from "@ant-design/icons";
import { Layout, Menu, Button, theme, Breadcrumb, Avatar } from "antd";
import Link from "next/link";
import { fetchUserProfile } from "@/app/redux/features/profile/actions";
import { useDispatch, useSelector } from "react-redux";
import Image from "next/image";
import type { MenuProps } from "antd";
import { Dropdown, Space } from "antd";
import { logout } from "@/app/api/auth";
import { routerMenu } from "@/app/constants/routes";
import useMessage from "@/app/hooks/useMessage";
// import { useViewport } from "@/app/hooks/useViewport";

const { Header, Sider, Content } = Layout;

interface typeSidebar {
  children: any;
}

const CSidebar = ({ children }: typeSidebar) => {
  // const { width } = useViewport();
  const items: MenuProps["items"] = [
    {
      label: <a href="https://www.antgroup.com">Personal Info</a>,
      key: "0",
    },
    {
      label: (
        <Button className="w-full" onClick={() => logout(null)}>
          Logout
        </Button>
      ),
      key: "1",
    },
    // {
    //   type: "divider",
    // },
    // {
    //   label: "3rd menu item",
    //   key: "3",
    // },
  ];

  const menuSidebar = [
    {
      icon: <FormOutlined />,
      key: "9",
      label: (
        <div className="relative">
          <Link href={routerMenu.ANNOUCEMENT}>Pengumuman</Link>
        </div>
      ),
    },
    {
      key: "1",
      icon: <TeamOutlined />,
      label: "Karyawan",
      children: [
        {
          key: "2",
          // icon: <VideoCameraOutlined />,
          label: (
            <div className="relative">
              <Link href={routerMenu.EMPLOYEE}>Data Karyawan</Link>
              {/* <Avatar
                size="small"
                className="w-4 h-4 bg-red-500 absolute right-5 top-1/2 transform -translate-y-1/2"
              >
                <span className="text-xs">1</span>
              </Avatar> */}
            </div>
          ),
        },
        {
          key: "11",
          label: (
            <div className="relative">
              <Link href={routerMenu.MASTER_DATA_ROLES}>Jabatan</Link>
            </div>
          ),
        },
        {
          key: "13",
          label: (
            <div className="relative">
              <Link href={routerMenu.MASTER_DATA_DIVISION}>Divisi</Link>
            </div>
          ),
        },
        // {
        //   key: "12",
        //   label: (
        //     <div className="relative">
        //       <Link href={routerMenu.MASTER_DATA_SHIFTS}>Shifts</Link>
        //     </div>
        //   ),
        // },
      ],
    },

    {
      key: "3",
      icon: <CalendarOutlined />,
      label: "Absensi",
      children: [
        {
          key: "4",
          // icon: <VideoCameraOutlined />,
          label: (
            <div className="relative">
              <Link href={routerMenu.ATTENDACE}>Data Absensi</Link>
              {/* <div className="absolute right-0 top-1/2 transform -translate-y-1/2">
                <Avatar style={{ backgroundColor: "red" }} size="small">
                  <span className="text-xs">1</span>
                </Avatar>
              </div> */}
            </div>
          ),
        },
        {
          key: "5",
          // icon: <UploadOutlined />,
          // label: <Link href="/employee/overtime">Lembur</Link>,
          label: (
            <div className="relative">
              <Link href={routerMenu.OVERTIME}>Data Lembur</Link>
              {/* <div className="absolute right-0 top-1/2 transform -translate-y-1/2">
                <Avatar style={{ backgroundColor: "red" }} size="small">
                  <span className="text-xs">4</span>
                </Avatar>
              </div> */}
            </div>
          ),
        },
        {
          key: "6",
          label: (
            <div className="relative">
              <Link href={routerMenu.SUBMISSION}>Data Pengajuan</Link>
              {/* <div className="absolute right-0 top-1/2 transform -translate-y-1/2">
                <Avatar style={{ backgroundColor: "red" }} size="small">
                  <span className="text-xs">2</span>
                </Avatar>
              </div> */}
            </div>
          ),
        },
        {
          key: "10",
          label: (
            <div className="relative">
              <Link href={routerMenu.CATEGORY_SUBMISSION}>
                Kategori Submission
              </Link>
            </div>
          ),
        },
      ],
    },
    {
      key: "7",
      icon: <GroupOutlined />,
      label: <Link href={routerMenu.INVENTORY}>Inventory</Link>,
    },
    {
      key: "site-map",
      icon: <HomeOutlined />,
      label: "Site Map",
      children: [
        {
          key: "data-project",
          // icon: <VideoCameraOutlined />,
          label: (
            <div className="relative">
              <Link href={routerMenu.SITE_MAP_PROJECT}>Data Project</Link>
              {/* <Avatar
                size="small"
                className="w-4 h-4 bg-red-500 absolute right-5 top-1/2 transform -translate-y-1/2"
              >
                <span className="text-xs">1</span>
              </Avatar> */}
            </div>
          ),
        },
        {
          key: "data-booking",
          // icon: <VideoCameraOutlined />,
          label: (
            <div className="relative">
              <Link href={routerMenu.ADM_MARKETING_LIST_BOOKING}>
                Data Booking
              </Link>
              {/* <Avatar
                size="small"
                className="w-4 h-4 bg-red-500 absolute right-5 top-1/2 transform -translate-y-1/2"
              >
                <span className="text-xs">1</span>
              </Avatar> */}
            </div>
          ),
        },
        // {
        //   key: "tahap",
        //   label: (
        //     <div className="relative">
        //       <Link href={routerMenu.MASTER_DATA_PHASES}>Tahap</Link>
        //     </div>
        //   ),
        // },
        // {
        //   key: "blok",
        //   label: (
        //     <div className="relative">
        //       <Link href={routerMenu.MASTER_DATA_BLOCKS}>Blok</Link>
        //     </div>
        //   ),
        // },
      ],
    },
  ];
  const dispatch: any = useDispatch();
  const [collapsed, setCollapsed] = useState(false);
  const {
    token: { colorBgContainer, borderRadiusLG },
  } = theme.useToken();

  const profile = useSelector((state: any) => state.profile);

  useEffect(() => {
    // if (width < 1028) {
    //   setCollapsed(true);
    // }
    dispatch(fetchUserProfile());
  }, [dispatch]);

  return (
    <Layout className="h-screen">
      <Sider
        trigger={null}
        collapsible
        collapsed={collapsed}
        className="cs main border-r"
        theme="light"
      >
        <div className="demo-logo-vertical p-5">
          <Image
            className="img-brand-1"
            src="/images/logoprisma-01.png"
            alt="img-brand"
            width={153}
            height={45}
          />
          <Image
            className="img-brand-2"
            src="/icons/logoprismaproperties-03.svg"
            alt="img-brand"
            width={60}
            height={25}
          />
        </div>
        <Menu
          theme="light"
          mode="inline"
          defaultSelectedKeys={["1"]}
          items={menuSidebar}
        />
      </Sider>
      <Layout>
        <Header
          style={{ padding: 0, background: colorBgContainer, lineHeight: 1.3 }}
          className="flex items-center justify-between"
        >
          <div className="flex items-center">
            <Button
              type="text"
              icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
              onClick={() => setCollapsed(!collapsed)}
              style={{
                fontSize: "16px",
                width: 64,
                height: 64,
              }}
            />
            <Breadcrumb
              items={[
                // {
                //   title: "Home",
                // },
                {
                  title: <a href="">Beranda</a>,
                },
                // {
                //   title: <a href="">Absensi</a>,
                // },
                // {
                //   title: "An Application",
                // },
              ]}
            />
          </div>
          <div className="pr-7">
            <div className="flex items-center gap-4">
              <div>
                <h1 className="font-bold">
                  {profile?.dataProfile?.user?.name ?? "-"}
                </h1>
                <p className="text-xs">
                  {profile?.dataProfile?.user?.role?.name ?? "-"}
                </p>
              </div>
              <Dropdown menu={{ items }} trigger={["click"]}>
                <a onClick={(e) => e.preventDefault()}>
                  <Space>
                    {profile?.dataProfile?.user?.role ? (
                      <Avatar
                        size="large"
                        icon={<UserOutlined />}
                        src={`${profile?.dataProfile?.user?.photo}`}
                      />
                    ) : (
                      <Avatar size="large" icon={<UserOutlined />} />
                    )}
                  </Space>
                </a>
              </Dropdown>
            </div>
          </div>
        </Header>
        <Content
          style={{
            margin: "24px 16px",
            padding: 24,
            minHeight: 280,
            background: colorBgContainer,
            borderRadius: borderRadiusLG,
          }}
        >
          {children}
        </Content>
      </Layout>
    </Layout>
  );
};

export default CSidebar;
