import { DatePicker } from "antd";
import React from "react";

interface typeCDateSingle {
  onChange?: any;
  value?: any;
}
const CDateSingle = ({ onChange, value }: typeCDateSingle) => {
  const dateFormat = "YYYY-MM-DD";
  return (
    <DatePicker
      className="w-full"
      format={dateFormat}
      showToday={false}
      onChange={onChange}
      value={value}
    />
  );
};

export default CDateSingle;
