import React from "react";
import { Input } from "antd";
import { SearchOutlined } from "@ant-design/icons";

const { Search } = Input;

interface typeCSearch {
  onSearch: any;
  value?: any;
  onChange?: any;
}
const CSearch = ({ onSearch, value, onChange }: typeCSearch) => {
  return (
    <Search
      placeholder="Cari data"
      allowClear
      enterButton={
        <div className="flex items-center justify-center">
          <SearchOutlined />
        </div>
      }
      size="middle"
      onSearch={onSearch}
      value={value}
      onChange={onChange}
    />
  );
};

export default CSearch;
