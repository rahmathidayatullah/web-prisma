/** @type {import('next').NextConfig} */
const nextConfig = {
  experimental: {
    missingSuspenseWithCSRBailout: false,
  },
  images: {
    domains: ["prisma-bucket-production.sgp1.digitaloceanspaces.com"],
  },
};

export default nextConfig;
